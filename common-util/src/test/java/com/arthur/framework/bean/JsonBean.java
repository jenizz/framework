package com.arthur.framework.bean;

import java.io.Serializable;

public class JsonBean implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 5614707825699276032L;
	
	private String name;
	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
