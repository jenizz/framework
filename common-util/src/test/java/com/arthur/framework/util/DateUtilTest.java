/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:52:01 </p>
 * <p><b>ClassName</b>: DateUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.util;

import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Test;

import com.arthur.framework.util.DateUtil.DatePattern;

/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:52:01 </p>
 * <p><b>ClassName</b>: DateUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class DateUtilTest {

	@Test
	public void test(){
		Date date = DateUtil.parseDate("2017-08-08", DatePattern.DATE);
		assertNotNull(date);
	}
}
