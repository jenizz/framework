/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:19:34 </p>
 * <p><b>ClassName</b>: JsonUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.util;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.arthur.framework.bean.JsonBean;

/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:19:34 </p>
 * <p><b>ClassName</b>: JsonUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 */
public class JsonUtilTest {
	
	@Test
	public void test(){
		JsonBean test = new JsonBean();
		test.setName("testName");
		test.setValue("testValue");
		String json = JsonUtil.serialize(test);
		assertNotNull(json);
		
		try{
			JsonBean object = JsonUtil.getBean(json, JsonBean.class);
			assertNotNull(object);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
