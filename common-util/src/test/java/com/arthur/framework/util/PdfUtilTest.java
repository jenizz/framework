package com.arthur.framework.util;

import java.io.IOException;

import org.junit.Test;

import com.arthur.framework.util.file.HtmlUtil;

public class PdfUtilTest {

	@Test
	public void test() throws IOException {
		// String html = FileUtils.readFileToString(new File("/Volumes/DAT/data.html"));
		String html = "test";
		
		// html转pdf，并保存到指定文件
		HtmlUtil.template("classpath:/templates/NewFile.html")	// 含css样式的HTML模版	（必选）
				.appendToBody(html)								// html字符串片段		（必选）
				.toPdf()										// 转pdf				（必选）
				.enablePrintOnly()								// PDF仅可用于浏览和打印（可忽略）
				.addWatermark("上海三零卫士")					// 添加文字水印		（可忽略）
				.write()										// 输出PDF文件到内存	（必需）
				.saveAs("/Volumes/DAT/template.pdf");			// 保存到指定文件		（可选 getFile | getBytes）
		
	}
}
