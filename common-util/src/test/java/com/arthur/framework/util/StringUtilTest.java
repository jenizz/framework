/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:42:58 </p>
 * <p><b>ClassName</b>: StringUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.util;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:42:58 </p>
 * <p><b>ClassName</b>: StringUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class StringUtilTest {

	@Test
	public void test(){
		boolean isNotEmpty = StringUtil.isNotEmpty(new Object());
		assertTrue(isNotEmpty);
		
		String[] strs = StringUtil.split("a,b,c,d",",");
		assertTrue(strs.length == 4);
	}
}
