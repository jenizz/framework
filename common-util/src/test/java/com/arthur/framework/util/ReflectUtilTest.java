/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:05:44 </p>
 * <p><b>ClassName</b>: ReflectUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.util;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.arthur.framework.util.reflect.ReflectUtil;

/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:05:44 </p>
 * <p><b>ClassName</b>: ReflectUtilTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class ReflectUtilTest {

	@Test
	public void test(){
		Class<?> clazz = ReflectUtil.getClass("com.arthur.framework.util.string.StringUtil");
		assertNotNull(clazz);
	}
}
