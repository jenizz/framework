package com.arthur.framework.support;

import com.arthur.framework.exception.ArthurRuntimeException;

public class RuntimeProcessor {

	public interface RuntimeCallback<T> {
		public T execute() throws Throwable;
	}
	
	public static <T> T run(RuntimeCallback<T> call) {
		try{
			return call.execute();
		}catch (Throwable e) {
			throw new ArthurRuntimeException(e);
		}
	}
}
