package com.arthur.framework.support;

public abstract class Watermark {

	// 水印文字
	private String text;
	
	// 水印旋转角度
	private int rotate;
	
	// 水印间的横向间距
	private int xSpacing;
	
	// 水印间的纵向间距
	private int ySpacing;
	
	// 横向重复个数
	private int xRepeats;
	
	// 纵向重复个数
	private int yRepeats;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getRotate() {
		return rotate;
	}

	public void setRotate(int rotate) {
		this.rotate = rotate;
	}

	public int getXSpacing() {
		return xSpacing;
	}

	public void setXSpacing(int xSpacing) {
		this.xSpacing = xSpacing;
	}

	public int getYSpacing() {
		return ySpacing;
	}

	public void setYSpacing(int ySpacing) {
		this.ySpacing = ySpacing;
	}

	public int getXRepeats() {
		return xRepeats;
	}

	public void setXRepeats(int xRepeats) {
		this.xRepeats = xRepeats;
	}

	public int getYRepeats() {
		return yRepeats;
	}

	public void setYRepeats(int yRepeats) {
		this.yRepeats = yRepeats;
	}

}
