package com.arthur.framework.support;

import java.io.IOException;

import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.PDFCreationListener;

import com.arthur.framework.util.file.PdfUtil;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * <p>描述: PDF水印 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月15日 上午9:05:51
 * @version 1.0.2017
 */
public class PdfWatermark extends Watermark implements PDFCreationListener {
	
	private static final int DEFAULT_FONT_SIZE = 30;

	private BaseFont base;
	
	private Font font;
	
	public PdfWatermark() {
		this("www.30wish.net");
	}
	
	public PdfWatermark(String watermark) {
		try {
			// 设置水印基本属性
			this.setText(watermark);
			this.setRotate(45);
			this.setXSpacing(100);
			this.setYSpacing(200);
			this.setXRepeats(5);
			this.setYRepeats(5);
			
			// 设置字体
			this.base = BaseFont.createFont(PdfUtil.FONT_PATH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			this.font = new Font(base, DEFAULT_FONT_SIZE, Font.BOLD, new GrayColor(0.96f));
		} catch (DocumentException | IOException e) {
			throw new RuntimeException("创建字体失败: {}", e);
		}
	}

	/**
	 * 预写入事件
	 * @see org.xhtmlrenderer.pdf.PDFCreationListener#preWrite(org.xhtmlrenderer.pdf.ITextRenderer, int)
	 * @author chanlong(陈龙)
	 * @date 2017年6月16日 上午9:18:46
	 */
	@Override
	public void preWrite(final ITextRenderer renderer, final int pages) {
		PdfWriter writer = renderer.getWriter();
		// 设置pdf页事件
		writer.setPageEvent(new PdfPageEventHelper(){
			@Override
			public void onEndPage(PdfWriter writer, Document document) {
				PdfContentByte under = writer.getDirectContentUnder();
				Phrase phrase = new Phrase(getText(), font);
				// 输出水印文字
				for (int i = 0; i < getXRepeats(); i++) {
					for (int j = 0; j < getYRepeats(); j++) {
						float height = writer.getPageSize().getHeight();
						ColumnText.showTextAligned(under, Element.ALIGN_CENTER, phrase, 
												   (100 + getXSpacing() * i), 
												   ((height-100)- getYSpacing() * j), 
												   j % 2 == 1 ? getRotate() : 0 - getRotate());
					}
				}
			}
		});
	}
	
	@Override
	public void onClose(final ITextRenderer renderer) {
		
	}

	@Override
	public void preOpen(final ITextRenderer renderer) {
		
	}
}
