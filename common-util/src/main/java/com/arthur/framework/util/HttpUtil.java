package com.arthur.framework.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

public abstract class HttpUtil {

	public static Cookie[] getCookies(final HttpServletRequest request) {
		return request.getCookies();
	}
	
	public static Cookie getCookie(final HttpServletRequest request, final String name) {
		Cookie[] cookies = getCookies(request);
		for (Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase(name)) return cookie;
		}
		return null;
	}

	public static void write(final HttpServletResponse response, final String data, final int status) {
		response.setStatus(status);
		response.setCharacterEncoding("UTF-8");

		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(data);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(out);
		}
	}

}
