/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:41:32 </p>
 * <p><b>ClassName</b>: StringUtil.java </p> 
 * <p><b>Description</b>: 字符串工具类 </p> 
 */
package com.arthur.framework.util.string;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.GenericTypeValidator;

import com.arthur.framework.util.DateUtil;
import com.google.common.base.Splitter;

/**
 * <p><b>描述</b>: 字符串工具类 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午9:56:39
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public abstract class StringUtil {

	private static Log log = LogFactory.getLog(DateUtil.class);

	/** Comment for <code>EMPTY_STRING</code> **/
	public static final String EMPTY_STRING = "";
	
	/** Comment for <code>COMMA</code> **/
	public static final String COMMA = ",";
	
	public static String uuid(){
		String[] uuids = UUID.randomUUID().toString().split("-");
		return new StringBuilder(uuids[1]).append(uuids[2]).append(uuids[3]).append(uuids[4]).append(uuids[0]).toString();
	}
	
	public static byte[] getBytes(final String string) {
		return string != null ? string.getBytes() : null;
	}
	
	public static boolean isUUID(final String uuid){
		try {
			UUID.fromString(uuid);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	public static String encoding(final String str, final String charset, final String encoding) {
		try {
			return new String(str.getBytes(charset), encoding);
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}
	
	public static String[] add(String[] x, String sep, String[] y) {
		String[] result = new String[x.length];
		for (int i = 0; i < x.length; i++) {
			result[i] = x[i] + sep + y[i];
		}
		return result;
	}
	
	/**
	 * 是否为空.
	 * @param orig 源对象
	 * @return boolean
	 * @since 2017年3月4日 上午9:57:01
	 * @author chanlong(陈龙)
	 */
	public static boolean isEmpty(final Object orig) {
		try{
			return StringUtils.isEmpty(orig.toString()) || "null".equals(orig) || "[]".equals(orig);
		}catch(NullPointerException e){
			return true;
		}
	}
	
	/**
	 * 是否不为空.
	 * @param orig 源对象
	 * @return boolean
	 * @since 2017年3月4日 上午9:57:40
	 * @author chanlong(陈龙)
	 */
	public static boolean isNotEmpty(final Object orig) {
		try{
			if (orig instanceof Object[] && ArrayUtils.isNotEmpty((Object[]) orig)) {
				return StringUtils.isNotEmpty(((Object[]) orig)[0].toString());
			} else {
				return StringUtils.isNotEmpty(orig.toString()) && !"null".equals(orig.toString()) && !"[]".equals(orig.toString());
			}
		}catch(NullPointerException e){
			return false;
		}
	}
	
	/**
	 * @param str
	 * @param i
	 * @return String
	 */
	public static String left(final String str, int i) {
		if (i > str.length()) i = str.length();
		return str.substring(0, i);
	}
	
	/**
	 * @param str
	 * @param i
	 * @return String
	 */
	public static String right(final String str, int i) {
		if (i > str.length()) i = str.length();
		return str.substring(str.length() - i, str.length());
	}
	
	/**
	 * @param str
	 * @param i
	 * @return String
	 */
	public static String mid(final String str, int i) {
		return mid(str, i, str.length());
	}
	
	/**
	 * @param str
	 * @param i
	 * @param len
	 */
	public static String mid(final String str, int i, int len) {
		if (i + len > str.length()) return str.substring(i, str.length());
		return str.substring(i, i + len);
	}
	
	/**
	 * @param str
	 * @return int
	 */
	public static int length(final String str) {
		if (str == null) {
			return 0;
		} else {
			return str.length();
		}
	}
	
	/**
	 * @param args
	 * @return int
	 */
	public static int length(final Object[] args) {
		if (args == null) {
			return 0;
		} else {
			return args.length;
		}
	}
	
	/**
	 * @param strs
	 * @param suffix
	 * @return String[]
	 */
	public static String[] suffix(final String[] strs, final String suffix) {
		if (suffix == null) return strs;
		
		String[] result = new String[strs.length];
		for (int i = 0; i < strs.length; i++) {
			result[i] = suffix(strs[i], suffix);
		}
		return result;
	}

	/**
	 * @param str
	 * @param suffix
	 * @return String
	 */
	public static String suffix(final String str, final String suffix) {
		return (suffix == null) ? str : str + suffix;
	}
	
	/**
	 * @param strs
	 * @param prefix
	 * @return String[]
	 */
	public static String[] prefix(final String[] strs, final String prefix) {
		if (prefix == null) return strs;
		
		String[] result = new String[strs.length];
		for (int i = 0; i < strs.length; i++) {
			result[i] = prefix + strs[i];
		}
		return result;
	}
	
	/**
	 * @param str
	 * @param seperators
	 * @return String[]
	 */
	public static String[] splitHasNVL(final String str, final String seperator, boolean flag) {
		ArrayList<String> result = new ArrayList<String>();
		String temp = str;
		
		while (temp != null && temp.indexOf(seperator) != -1) {
			String value = temp.substring(0, temp.indexOf(seperator));
			result.add(value);
			temp = temp.substring(temp.indexOf(seperator) + seperator.length(), temp.length());
		}
		
		if (flag) {
			result.add(temp);
		} else if (!isBlankOrNull(temp)) {
			result.add(temp);
		}
		
		String[] datas = new String[result.size()];
		for (int i = 0; i < datas.length; i++) {
			datas[i] = (String) result.get(i);
		}
		return datas;
	}
	
	/**
	 * @param str
	 * @param seperators
	 * @return String[]
	 */
	public static String[] splitHasNVL(final String str, final String seperator) {
		return splitHasNVL(str, seperator, false);
	}
	
	/**
	 * @param str
	 * @param div
	 * @return String[]
	 */
	public static String[] splitUniq(final String str, final String seperator) {
		StringTokenizer stk = new StringTokenizer(str, seperator);
		ArrayList<String> valueList = new ArrayList<String>();
		while (stk.hasMoreElements()) {
			valueList.add(stk.nextToken());
		}
		return valueList.toArray(new String[0]);
	}
	
	/**
	 * 分解为字符串数组.
	 * @param orig 源对象
	 * @param separator 分隔字串
	 * @return 字串数组
	 * @since 2017年3月4日 上午9:58:00
	 * @author chanlong(陈龙)
	 */
	public static String[] split(final Object orig, final String separator) {
		try{
			List<String> result = Splitter.on(separator).trimResults().omitEmptyStrings().splitToList(orig.toString());
			return result.toArray(new String[]{});
		}catch(NullPointerException e){
			return null;
		}
	}

	/**
	 * 首字母小写.
	 * @param orig 源对象
	 * @return String
	 * @since 2017年3月4日 上午9:58:30
	 * @author chanlong(陈龙)
	 */
	public static String toLowerCaseOnFirst(final String orig) {
		char[] cs = orig.toCharArray();
		if (cs[0] >= 65 && cs[0] <= 90) cs[0] += 32;
		return String.valueOf(cs);
	}
	
	/**
	 * 首字母大写.
	 * @param orig 源对象
	 * @return String
	 * @since 2017年3月4日 下午3:56:28
	 * @author chanlong(陈龙)
	 */
	public static String toUpperCaseOnFirst(final String orig) {
		char[] cs = orig.toCharArray();
		if (cs[0] >= 97 && cs[0] <= 122) cs[0] -= 32;
		return String.valueOf(cs);
	}

	/**
	 * 获取参数.
	 * @param value 值
	 * @return Object
	 * @since 2017年3月4日 下午3:57:12
	 * @author chanlong(陈龙)
	 */
	public static Object getPramameter(final Object value) {
		return isNotEmpty(value) ? value : null;
	}
	
	/**
	 * @param string
	 * @param times
	 * @return String
	 */
	public static String repeat(String string, int times) {
		StringBufferUtil buf = new StringBufferUtil(string.length() * times);
		for (int i = 0; i < times; i++) buf.append(string);
		return buf.toString();
	}
	
	/**
	 * @param seperator
	 * @param strings
	 * @return
	 */
	public static String join(final String[] values, final String seperator) {
		int length = values.length;
		if (length == 0) return "";
		
		StringBuilder buf = new StringBuilder(length * values[0].length()).append(values[0]);
		for (int i = 1; i < length; i++) {
			buf.append(seperator).append(values[i]);
		}
		
		return buf.toString();
	}
	
	/**
	 * @param seperator
	 * @param objects
	 * @return
	 */
	public static String join(final Iterator<?> values, final String seperator) {
		StringBuilder buf = new StringBuilder();
		buf.append(values.next());
		while (values.hasNext()) {
			buf.append(seperator).append(values.next());
		}
		return buf.toString();
	}
	
	/**
	 * @param array
	 * @param delimiter
	 * @param label
	 * @return
	 */
	public static String join(final Object[] array, final String delimiter) {
		return toArrString(array, delimiter, "", false);
	}
	
	/**
	 * @param array
	 * @param delimiter
	 * @param label
	 * @return
	 */
	public static String join(final Object[] array, final String delimiter, final String label) {
		return toArrString(array, delimiter, label, false);
	}
	
	/**
	 * 转SQL字符串.
	 * @param value 源对象
	 * @param label 标记
	 * @return String
	 * @since 2017年3月4日 下午4:19:18
	 * @author chanlong(陈龙)
	 */
	public static String joinSQL(final Object value, final String label) {
		try {
			if (value instanceof Object[]) {
				Object[] values = (Object[]) value;
				return toArrString(values, label, true);
			} else {
				return EMPTY_STRING;
			}
		} catch (NullPointerException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * @param qualifiedName
	 * @return String
	 */
	public static String unqualify(final String qualifiedName) {
		return unqualify(qualifiedName, ".");
	}
	
	/**
	 * @param qualifiedName
	 * @param seperator
	 * @return String
	 */
	public static String unqualify(final String qualifiedName, final String seperator) {
		return qualifiedName.substring(qualifiedName.lastIndexOf(seperator) + 1);
	}
	
	/**
	 * @param qualifiedName
	 * @return String
	 */
	public static String qualifier(final String qualifiedName) {
		int loc = qualifiedName.lastIndexOf(".");
		if (loc < 0) {
			return EMPTY_STRING;
		} else {
			return qualifiedName.substring(0, loc);
		}
	}

	/**
	 * @param qualifiedName
	 * @return String
	 */
	public static String root(String qualifiedName) {
		int loc = qualifiedName.indexOf(".");
		return (loc < 0) ? qualifiedName : qualifiedName.substring(0, loc);
	}
	
	/**
	 * SQL防注入
	 * @param sql SQL
	 * @return String
	 * @since 2017年3月4日 下午4:25:24
	 * @author chanlong(陈龙)
	 */
	public final static String unSqlInjection(final String sql) {
		if (isEmpty(sql)) return EMPTY_STRING;
		return sql.replaceAll(".*([';]+|(--)+).*", " ");
	}
	
	/**
	 * @param str
	 * @return
	 */
	public static int countChineseChar(final String str) {
		Pattern p = Pattern.compile("[\u4e00-\u9fa5]");
		Matcher m = p.matcher(str);
		return m.groupCount();
	}
	
	/**
	 * @param tfString
	 * @return boolean
	 */
	public static boolean booleanValue(String tfString) {
		String trimmed = tfString.trim().toLowerCase();
		return trimmed.equals("true") || trimmed.equals("t");
	}
	
	private static String toArrString(Object[] array, String delimiter, String label, boolean unSQLInj) {
		int len = array.length;
		if (len == 0) return StringUtil.EMPTY_STRING;
		if (delimiter == null) delimiter = COMMA;
		
		Object s;
		StringBufferUtil buf = new StringBufferUtil(len * 12);
		for (int i = 0; i < len - 1; i++) {
			s = array[i];
			if (s == null) s = "";
			if (unSQLInj) s = unSqlInjection(s.toString());
			buf.append(label + array[i] + label).append(delimiter);
		}
		{
			s = array[len - 1];
			if (s == null) s = "";
			if (unSQLInj) s = unSqlInjection(s.toString());
			buf.append(label + s + label);
		}
		return buf.toString();
	}
	
	/**
	 * @param string
	 * @param placeholders
	 * @param replacements
	 * @return String[]
	 */
	public static String[] multiply(String string, Iterator<?> placeholders, Iterator<?> replacements) {
		String[] result = new String[] { string };
		while (placeholders.hasNext()) {
			result = multiply(result, (String) placeholders.next(), (String[]) replacements.next());
		}
		return result;
	}
	
	/**
	 * @param strings
	 * @param placeholder
	 * @param replacements
	 * @return String[]
	 */
	static String[] multiply(String[] strings, String placeholder, String[] replacements) {
		String[] results = new String[replacements.length * strings.length];
		int n = 0;
		for (int i = 0; i < replacements.length; i++) {
			for (int j = 0; j < strings.length; j++) {
				results[n++] = replace(strings[j], placeholder, replacements[i]);
			}
		}
		return results;
	}
	
	/**
	 * key:value;
	 * 
	 * @return HashMap Returns the stylepairs.
	 */
	public static HashMap<String, String> getPairs(String str) {
		HashMap<String, String> pairs = new HashMap<String, String>();
		if (!isBlankOrNull(str)) {
			String[] arr = split(str, ";");
			for (String pair : arr) {
				String[] arr2 = split(pair, ":");
				if (arr2.length == 2) pairs.put(arr2[0], arr2[1]);
			}
		}
		return pairs;
	}
	
	/**
	 * @param fileName
	 * @return String
	 */
	public static String getExtName(String fileName) {
		String[] names = fileName.split("\\.");
		String extName = "";
		if (names != null && names.length > 0) extName = names[names.length - 1];
		return extName;
	}
	
	/** 数组转字符窜 **/
	private static String toArrString(Object[] array, String label, boolean unSQLInj) {
		Object str;
		int length = array.length;
		if (length == 0) return EMPTY_STRING;
		
		StringBuilder bud = new StringBuilder(length * 12);
		for (int i = 0; i < length; i++) {
			str = array[i];
			if (str == null) str = EMPTY_STRING;
			if (unSQLInj) str = unSqlInjection(str.toString());
			
			bud.append(label + str + label);
			if (i < length - 1) bud.append(COMMA);
		}
		
		return bud.toString();
	}

	/**
	 * 去除字符串中的换行、回车.
	 * @param value
	 * @return
	 * @author Administrator(陈龙)
	 * @date 2014-1-17  上午10:36:15
	 */
	public static String nowrap(final String value){
		try {
			return replaceAll(value,"\t|\r|\n","");
		} catch (Exception e) {
			return "";
		}
	}	

	/**
	 * 基于正则表达式截取字符串.
	 * @param value
	 * @param regex
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2014年8月3日  下午6:13:06
	 */
	public static String substr(final String value, final String regex){
		String returnstr = null;
		
		if(!isEmpty(value)){
			Pattern pattern = Pattern.compile(regex);
			Matcher matcher = pattern.matcher(value);
			while(matcher.find()) returnstr = matcher.group(1);
		}
		
		return returnstr;
	}
	
	/**
	 * @param template
	 * @param placeholder
	 * @param replacement
	 * @return String
	 */
	public static String replace(final String template, final String placeholder, final String replacement) {
		int loc = template.indexOf(placeholder);
		if (loc < 0) {
			return template;
		} else {
			return new StringBufferUtil(template.substring(0, loc)).append(replacement).append(template.substring(loc + placeholder.length())).toString();
		}
	}
	
	public static String replaceAll(final String template, final String placeholder, final String replacement) {
		int loc = template.indexOf(placeholder);
		if (loc < 0) {
			return template;
		} else {
			String rep = replaceAll(template.substring(loc + placeholder.length()), placeholder, replacement);
			return new StringBuilder(template.substring(0, loc)).append(replacement).append(rep).toString();
		}
	}
	
	/**
	 * 基于正则表达式替换字符串.
	 * @param value
	 * @param regex
	 * @param replacements
	 * @return
	 * @author chanlong(陈龙)
	 * @throws CommonException 
	 * @date 2014年6月16日  下午3:57:07
	 */
	public static String replaceAll(final String value, final String regex, final String... replacements) throws Exception{
		String returnstr = value;
		try {
			Pattern pattern = Pattern.compile(regex);
			if(replacements.length == 1){
				Matcher matcher = pattern.matcher(returnstr);
				returnstr = matcher.replaceAll(replacements[0]);
			}else{
				for (String replacement : replacements) {
					Matcher matcher = pattern.matcher(returnstr);
					returnstr = matcher.replaceFirst(replacement);
				}
			}
		} catch (IndexOutOfBoundsException e) {
			throw new Exception("要替换的操作数与正则表示不一致", e);
		}
		return returnstr;
	}
	
	public static boolean isWebEmpty(Object obj) {
		return isEmpty(obj) || "undefined".equals(obj);
	}
	
	public static boolean isJSONEmpty(Object obj) {
		return isEmpty(obj) || "{}".equals(obj);
	}
	
	public static boolean isJSONArrayEmpty(Object obj) {
		return isEmpty(obj) || "[]".equals(obj);
	}
	
	public static boolean isValueEmpty(Object obj) {
		if (obj == null) return true;
		return isStrBlankOrNull(obj.toString());
	}
	
	public static boolean isBlankOrNull(Object obj) {
		if (obj == null) return true;
		return isBlankOrNull(obj.toString());
	}
	
	public static boolean isBlankOrNull(String str) {
		if (str == null || str.length() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isTrimBlankOrNull(String str) {
		if (isBlankOrNull(str) || str.trim().length() == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isStrBlankOrNull(String str) {
		if (isTrimBlankOrNull(str) || "null".equalsIgnoreCase(str)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isDate(Object obj) {
		if (obj == null) return false;
		return isDate(obj.toString());
	}
	
	public static boolean isDate(String str) {
		Date result = GenericTypeValidator.formatDate(str, null);
		if (result != null) return true;
		return false;
	}

	public static boolean isNumeric(Object obj) {
		if (obj == null) return false;
		return isNumeric(obj.toString());
	}
	
	public static boolean isNumeric(String str) {
		Double result = GenericTypeValidator.formatDouble(str);
		if (result != null)
			return true;
		return false;
	}
	
	public static boolean isBoolean(Object obj) {
		if (obj == null) return false;
		return isBoolean(obj.toString());
	}
	
	public static boolean isBoolean(String str) {
		return (str != null && (str.equalsIgnoreCase("TRUE") || str.equalsIgnoreCase("FALSE") || isNumeric(str)));
	}
}
