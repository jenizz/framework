/*
 * Created on 2004-10-11
 */
package com.arthur.framework.util.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * RTTI执行工具。
 * 
 * @author zhouxw
 */
@SuppressWarnings("rawtypes")
public abstract class RttiUtil {

	/**
	 * <p><b>Title</b>: invoke </p> 
	 * <p><b>Author</b>: long.chan 2017年3月7日 上午10:26:33 </p>
	 * <p><b>Description</b>: 通过RIIT调用实例的方法,返回结果.(非静态方法) </p> 
	 *
	 * @param obj
	 * @param methodName
	 * @param methodParamTypes
	 * @param paramValues
	 * @return
	 * @throws Throwable
	 */
	public static Object invoke(final Object obj, final String methodName, final Class[] methodParamTypes, final Object[] paramValues) throws Throwable {
		Object result = null; 
		Class<? extends Object> cls = obj.getClass();
		
		Method method = cls.getMethod(methodName.trim(), methodParamTypes);
		result = method.invoke(obj, paramValues);
		return result;
	}
	
	/**
	 * <p><b>Title</b>: invoke </p> 
	 * <p><b>Author</b>: long.chan 2017年3月7日 上午10:32:36 </p>
	 * <p><b>Description</b>: 通过RIIT调用类的方法,返回结果.(静态方法) </p> 
	 *
	 * @param className
	 * @param staticMethodName
	 * @param methodParamTypes
	 * @param paramValues
	 * @return
	 * @throws Throwable
	 */
	public static Object invoke(final String className, final String staticMethodName, final Class[] methodParamTypes, final Object[] paramValues) throws Throwable {
		return invoke(className, staticMethodName, methodParamTypes, paramValues, null);
	}
	
	/**
	 * <p><b>Title</b>: invoke </p> 
	 * <p><b>Author</b>: long.chan 2017年3月7日 上午10:31:37 </p>
	 * <p><b>Description</b>: 通过RIIT调用类的方法,返回结果.(静态方法) </p> 
	 *
	 * @param className
	 * @param staticMethodName
	 * @param methodParamTypes
	 * @param paramValues
	 * @param loader
	 * @return
	 * @throws Throwable
	 */
	public static Object invoke(final String className, final String staticMethodName, final Class[] methodParamTypes, final Object[] paramValues, final ClassLoader loader) throws Throwable { 
		String clsname = className.trim();
		Object result = null; 
		Class<?> cls = null;
		
		if (loader != null) {
			cls = loader.loadClass(clsname);
		} else {
			cls = Class.forName(clsname);
		}
		
		Method m = cls.getMethod(staticMethodName.trim(), methodParamTypes); 
		result = m.invoke(null, paramValues);
		return result;
	}

	/**
	 * @param className
	 * @return Object
	 * @throws Throwable
	 */
	public static Object instance(final String className) throws Throwable {
		return instance(className, new Class[] {}, new Object[] {}, Thread.currentThread().getContextClassLoader());
	}
	
	/**
	 * @param className
	 * @param cl
	 * @return Object
	 * @throws Throwable
	 */
	public static Object instance(final String className, final ClassLoader loader) throws Throwable {
		return instance(className, new Class[] {}, new Object[] {}, loader);
	}
	
	/**
	 * <p><b>Title</b>: instance </p> 
	 * <p><b>Author</b>: long.chan 2017年3月7日 上午10:34:41 </p>
	 * <p><b>Description</b>:  对名为className的类，实例化一个对象。 </p> 
	 *
	 * @param className
	 * @param paramTypes
	 * @param paramValues
	 * @param loader
	 * @return
	 * @throws Throwable
	 */
	public static Object instance(final String className, final Class[] paramTypes, final Object[] paramValues, final ClassLoader loader) throws Throwable {
		Object result = null;
		Class<?> cls = Class.forName(className.trim(), true, loader);
		
		if (cls != null) {
			if (paramTypes == null || paramValues == null)
				result = cls.newInstance();
			else {
				Constructor<?> con = cls.getConstructor(paramTypes);
				result = con.newInstance(paramValues);
			}
		}
		return result;
	}

	/**
	 * @param className
	 * @return Object
	 * @throws Throwable
	 */
	public static Object instanceOnce(final String className) throws Throwable {
		String clsname = className.trim();
		Object result = null; 
		if (objMap.get(clsname) == null) {
			synchronized (initLock) {
				result = Class.forName(clsname).newInstance();
				objMap.put(clsname, result);
			}
		} else {
			result = objMap.get(clsname);
		} 
		return result; // 可以返回空 说明此类不存在 然后继续找
	}

	/**  */
	private final static Object initLock = new Object();

	/**  */
	private final static HashMap<String, Object> objMap = new HashMap<String, Object>();

}
