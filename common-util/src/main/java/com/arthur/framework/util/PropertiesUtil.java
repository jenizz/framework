/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:19:12 </p>
 * <p><b>ClassName</b>: PropertiesUtil.java </p> 
 * <p><b>Description</b>: 配置文件工具类 </p> 
 */
package com.arthur.framework.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

/**
 * <p><b>描述</b>: 配置文件读取工具 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午3:58:44
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class PropertiesUtil {

	private static Properties properties = new Properties();
	private static PropertiesUtil instance = null;
	
	/**
	 * 加载配置文件.
	 * @param filepath 文件路径
	 * @return PropertiesUtil
	 * @since 2017年3月4日 下午4:01:59
	 * @author chanlong(陈龙)
	 */
	public static PropertiesUtil loader(final String filepath) {
		loader(filepath, properties);
		return newInstance();
	}
	
	/**
	 * 加载配置文件.
	 * @param filepath
	 * @param properties
	 * @author chanlong(陈龙)
	 * @date 2017年3月31日 上午11:38:18
	 */
	public static void loader(final String filepath, final Properties properties) {
		InputStreamReader reader = null;
		InputStream input = null;
		try {
			input = PropertiesUtil.class.getResourceAsStream(filepath);
			if (input == null) return;
			
			reader = new InputStreamReader(input, "UTF-8");
			if(filepath.endsWith(".xml")){
				properties.loadFromXML(input);
			}else{
				properties.load(reader);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			IOUtils.closeQuietly(input);
			IOUtils.closeQuietly(reader);
		}
	}
	
	/**
	 * 获取配置内容.
	 * @param key 键
	 * @return 值
	 * @since 2017年3月4日 下午4:02:24
	 * @author chanlong(陈龙)
	 */
	public String getProperty(final String key){
		return properties.getProperty(key);
	}
	
	/*
	 * 创建实例
	 */
	public static PropertiesUtil newInstance(){
		if(instance == null){
			instance = new PropertiesUtil();
		}
		return instance;
	}
}
