package com.arthur.framework.util.file;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.arthur.framework.util.string.StringUtil;

/**
 * <p>描述: 文件工具类 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月15日 下午2:01:50
 * @version 1.0.2017
 */
public class FileUtil {

	/**
	 * 获取指定类型的临时文件.
	 * @param type 文件类型
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午1:53:37
	 */
	public static File getTempFile(final FileType type){
		return new File(new StringBuilder(FileUtils.getTempDirectoryPath()).append(StringUtil.uuid()).append(type).toString());
	}
	
	/**
	 * 上传文件.
	 * @author chanlong(陈龙)
	 * @param filename 
	 * @param filepath 
	 * @param bytes 
	 * @date 2018年3月13日 下午2:35:46
	 */
	public static FileUtil upload (final byte[] data, final String filepath, final String filename) {
		File file = new File(filepath);
		if (!file.exists()) file.mkdirs();
		
		try {
			file = new File(FilenameUtils.concat(filepath, filename));
			FileUtils.writeByteArrayToFile(file, data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new FileUtil(filename);
	}
	
	public String getUrl(final String path) {
		return FilenameUtils.concat(path, filename);
	}
	
	public enum FileType {
		pdf(".pdf"),
		png(".png"),
		ppt(".ppt"),
		doc(".doc"),
		xls(".xls"),
		pptx(".pptx"),
		docx(".docx"),
		xlsx(".xlsx"),
		html(".html");
		
		private String extension;
		
		private FileType(String extension){
			this.extension = extension;
		}

		@Override
		public String toString() {
			return extension;
		}
	}
	
	private String filename;
	
	private FileUtil (String filename) {
		this.filename = filename;
	}
}
