package com.arthur.framework.util.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;

import com.arthur.framework.util.CloseUtil;
import com.arthur.framework.util.PropUtil;
import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>Author</b>: long.chan 2017年3月7日 上午9:25:00 </p>
 * <p><b>ClassName</b>: PathUtil.java </p> 
 * <p><b>Description</b>: </p> 
 */
public abstract class PathUtil {

    private static List<String> classRepository = null;
    
    /**
     * @return List<String>
     */
    public final static List<String> getClassPaths() {
        if (classRepository == null) {
            classRepository = new ArrayList<String>();
            
            /** 取环境变量 */
            String classPath = System.getProperty("java.class.path");
            
            /** 取得该路径下的所有文件夹 */
            if (classPath != null && !"".equals(classPath)) {
                StringTokenizer tokenizer = new StringTokenizer(classPath, File.pathSeparator);
                while (tokenizer.hasMoreTokens()) {
                    classRepository.add(tokenizer.nextToken());
                }
            }
        }
        return classRepository;
    }
    
    /**
     * @return String
     * @throws Throwable
     */
    public final static String getSkinPath() throws Throwable {
        String value = PropUtil.getProperty("SKIN");
        if (StringUtil.isEmpty(value)) {
            return "defualt";
        }
        return value;
    }
    
    /**
     * @param cls
     * @return String
     */
    public static String getPathFromClass(final Class<?> cls) throws IOException {
		String path = null;
		if (cls == null) {
			throw new NullPointerException();
		}

		URL url = getClassLocationURL(cls);
		if (url != null) {
			path = url.getPath();
			if ("jar".equalsIgnoreCase(url.getProtocol())) {
				path = new URL(path).getPath();
				int location = path.indexOf("!/");
				if (location != -1) {
					path = path.substring(0, location);
				}
			}
			File file = new File(path);
			path = file.getCanonicalPath();
		}
		return path;
    }
    
    /**
     * 
     * @param relatedPath
     * @param cls
     * @return String
     * @throws IOException
     */
    public static String getFullPathRelateClass(final String relatedPath, final Class<?> cls) throws IOException {
        String path = null;
        if (relatedPath == null) {
            throw new NullPointerException();
        }
        
        String clsPath = getPathFromClass(cls);
        File clsFile = new File(clsPath);
        String tempPath = clsFile.getParent()+File.separator+relatedPath;
        File file = new File(tempPath);
        path = file.getCanonicalPath();
        return path;
    }
    
    /**
     * @param cls
     * @return URL
     */
    private static URL getClassLocationURL(final Class<?> cls) {
        if (cls == null) throw new IllegalArgumentException("cls lost");
        
        URL result = null;
        final String clsAsResource = cls.getName().replace('.', '/').concat(".class");
        final ProtectionDomain pd = cls.getProtectionDomain();
        
        if (pd != null) {
            final CodeSource cs = pd.getCodeSource();
            if (cs != null) result = cs.getLocation();
            if (result != null) {
                if ("file".equals(result.getProtocol())) {
                    try {
                        if (result.toExternalForm().endsWith(".jar") || result.toExternalForm().endsWith(".zip")) {
                        	result = new URL("jar:".concat(result.toExternalForm()).concat("!/").concat(clsAsResource));
                        } else if (new File(result.getFile()).isDirectory()) {
                        	result = new URL(result, clsAsResource);
                        }
                    } catch (MalformedURLException ignore) {
                    	ignore.printStackTrace();
                    }
                }
            }
        }
        
        if (result == null) {
            final ClassLoader clsLoader = cls.getClassLoader();
            result = clsLoader != null ? clsLoader.getResource(clsAsResource) : ClassLoader.getSystemResource(clsAsResource);
        }
        return result;
    }
    
	public static String getTmpPath() {
		return System.getProperty("java.io.tmpdir");
	}
}

class ReadEnv {
    final static Properties envVars = new Properties();
    final static Properties getEnvVars() throws Throwable {
        Process p = null;
        Runtime r = Runtime.getRuntime();
        String OS = System.getProperty("os.name").toLowerCase();
        
        if (OS.indexOf("windows 9") > -1) {
            p = r.exec("command.com /c set");
        } else if ((OS.indexOf("nt") > -1) 
        		|| (OS.indexOf("windows 20") 	> -1)
                || (OS.indexOf("windows xp") 	> -1)
                || (OS.indexOf("windows 7") 	> -1)
                || (OS.indexOf("windows vista") > -1)
                || (OS.indexOf("windows 8.1") 	> -1)) {
            p = r.exec("cmd.exe /c set");
        } else {
        	try {
        		p = r.exec("env");
        	}catch(Throwable e){
                return envVars;
        	}
        }
        
        InputStream in = null;
        try {
            in = p.getInputStream();
            InputStreamReader is = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(is);
            String line;
            while ((line = br.readLine()) != null) {
                int idx = line.indexOf('=');
                String key = line.substring(0, idx);
                String value = line.substring(idx + 1);
                envVars.setProperty(key, value);
            }
            return envVars;
        } finally {
            CloseUtil.close(in);
        }
    }
}