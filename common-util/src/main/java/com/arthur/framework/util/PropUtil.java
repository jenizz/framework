package com.arthur.framework.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.Set;

import com.arthur.framework.util.string.StringUtil;

public abstract class PropUtil {
	
	static class _PropertyResourceBundle extends PropertyResourceBundle {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		public _PropertyResourceBundle(ResourceBundle rs) throws IOException {
			super(new InputStream() {
				@Override
				public int read() throws IOException {
					return -1;
				}
			});
			lookup = new HashMap(System.getProperties());
			lookup.putAll(System.getenv());
			this.read(rs);
		}

		public void read(ResourceBundle rs) {
			Iterator<String> keys = rs.keySet().iterator();
			while (keys.hasNext()) {
				String k = keys.next();
				lookup.put(k, rs.getObject(k));
			}
		}

		@Override
		public Object handleGetObject(String key) {
			if (key == null) {
				throw new NullPointerException();
			}
			return lookup.get(key);
		}

		@Override
		@SuppressWarnings("restriction")
		public Enumeration<String> getKeys() {
			ResourceBundle parent = this.parent;
			return new sun.util.ResourceBundleEnumeration(lookup.keySet(), (parent != null) ? parent.getKeys() : null);
		}

		@Override
		protected Set<String> handleKeySet() {
			return lookup.keySet();
		}

		private Map<String, Object> lookup;
	}

	private static String APP_BUNDLE_NAME = "AppResources";
	private static Map<String, ResourceBundle> BUNDLE = new HashMap<String, ResourceBundle>();

	public final static ResourceBundle getPropertyBundle(String bundleName) {
		String key = System.getProperty("APP_BUNDLE_NAME");
		if (StringUtil.isNotEmpty(bundleName)) key = bundleName;
		if (StringUtil.isEmpty(key)) key = APP_BUNDLE_NAME;
		
		if (!BUNDLE.containsKey(key))
			synchronized (PropUtil.class) {
				if (!BUNDLE.containsKey(key))
					try {
						if (!BUNDLE.containsKey(key)) {
							ResourceBundle rso = ResourceBundle.getBundle(key, Locale.getDefault(), Thread.currentThread().getContextClassLoader());
							_PropertyResourceBundle rsb = new _PropertyResourceBundle(rso);
							BUNDLE.put(key, rsb);
						}
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
			}
		return BUNDLE.get(key);
	}

	public final static ResourceBundle getPropertyBundle() {
		return getPropertyBundle(null);
	}

	public final static ResourceBundle getLocalPropertyBundle(String bundleName) {
		String name = bundleName;
		if (StringUtil.isEmpty(name)) name = APP_BUNDLE_NAME;
		ResourceBundle rsb = ResourceBundle.getBundle(name, Locale.getDefault(), Thread.currentThread().getContextClassLoader());
		return rsb;
	}

	public final static ResourceBundle getLocalPropertyBundle() {
		return getLocalPropertyBundle(null);
	}

	/**
	 * @param bundName
	 * @param key
	 * @return
	 */
	public static String getProperty(String bundName, String key, String def) {
		try {
			ResourceBundle rsb;
			if (!StringUtil.isEmpty(bundName)) {
				rsb = getPropertyBundle(bundName);
			} else {
				rsb = getPropertyBundle();
			}
			if (!rsb.containsKey(key)) {
				return def;
			}
			return rsb.getString(key);
		} catch (Throwable ignore) {
			ignore.printStackTrace();
			return def;
		}
	}

	public static String getProperty(String key, String def) {
		return getProperty(APP_BUNDLE_NAME, key, def);
	}

	public static String getProperty(String key) {
		return getProperty(APP_BUNDLE_NAME, key, "");
	}

	public static String INVOKE_BUNDLE_NAME = "AppInvokes";

	public final static ResourceBundle getInvokePropertyBundle() throws Throwable {
		String s = System.getProperty("INVOKE_BUNDLE_NAME");
		if (StringUtil.isEmpty(s)) s = INVOKE_BUNDLE_NAME;
		return ResourceBundle.getBundle(s, Locale.getDefault(), Thread.currentThread().getContextClassLoader());
	}

	public static String getInvokeProperty(String key) {
		try {
			ResourceBundle b = getInvokePropertyBundle();
			if (!b.containsKey(key)) return "";
			return b.getString(key);
		} catch (Throwable ignore) {
			ignore.printStackTrace();
			return "";
		}
	}
}