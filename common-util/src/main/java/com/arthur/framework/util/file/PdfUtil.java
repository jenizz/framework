package com.arthur.framework.util.file;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.PDFEncryption;

import com.arthur.framework.support.PdfWatermark;
import com.arthur.framework.util.file.FileUtil.FileType;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.BaseFont;

/**
 * <p>描述: PDF工具类 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月14日 上午10:28:50
 * @version 1.0.2017
 */
public class PdfUtil {
	
	public static String FONT_PATH = "/static/fonts/SIMKAI.TTF";
	
	private static ITextRenderer renderer;
	
	private static PdfUtil instance;
	
	private OutputStream out;
	
	private File pdf;
	
	/**
	 * 创建PDF文件渲染器.
	 * @param htmlfile html文件
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午2:24:00
	 */
	public static PdfUtil createRenderer(final File htmlfile) {
		instance = new PdfUtil();
		try {
			instance.pdf = FileUtil.getTempFile(FileType.pdf);
			instance.out = FileUtils.openOutputStream(instance.pdf);
			instance.setRenderer(htmlfile);
		} catch (IOException e) {
			throw new RuntimeException("The createRenderer's function is error : {}", e);
		} 
		return instance;
	}
	
	/**
	 * 获取PDF文件对象.
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午3:16:38
	 */
	public File getFile() {
		return pdf;
	}
	
	/**
	 * 设置PDF文件渲染器.
	 * @param htmlfile html文件
	 * @author chanlong(陈龙)
	 * @param out 
	 * @date 2017年6月15日 下午2:40:26
	 */
	public void setRenderer(final File htmlfile) {
		try {
			renderer = new ITextRenderer();
			renderer.getFontResolver().addFont(FONT_PATH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
			renderer.setDocument(htmlfile);
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 添加可控权限.
	 * @param encryption
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午2:39:38
	 */
	public PdfUtil addEncryption(final PDFEncryption encryption) {
		renderer.setPDFEncryption(encryption); return this;
	}

	/**
	 * 添加文字水印.
	 * @param watermark
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午2:38:39
	 */
	public PdfUtil addWatermark(final String watermark) {
		renderer.setListener(new PdfWatermark(watermark)); return this;
	}
	
	/**
	 * 输出pdf.
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午3:11:19
	 */
	public boolean write(){
		try {
			renderer.layout();
			renderer.createPDF(out, true); 
			return true;
		} catch (DocumentException | IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			IOUtils.closeQuietly(out);
		}
	}
	
}
