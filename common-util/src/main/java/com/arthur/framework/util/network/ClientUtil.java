package com.arthur.framework.util.network;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public final class ClientUtil {
	
	private HttpClient client;
	
	private List<NameValuePair> params;
	
	private static ClientUtil instance;
	
	public static ClientUtil http() {
		if(instance == null) instance = new ClientUtil();
		return instance;
	}
	
	public ClientUtil() {
		client = HttpClientBuilder.create().build();
	}
	
	public ClientUtil addParam(final String key, final String value) {
		if (params == null) params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(key, value));
		return this;
	}
	
	public String post(final String url) {
		HttpPost httpPost = new HttpPost(url);    
	    httpPost.setHeader("Content-Type","application/x-www-form-urlencoded"); 
	    String resp = null;

	    try{
	        httpPost.setEntity(new UrlEncodedFormEntity(params));
	        HttpResponse response = client.execute(httpPost);
	        HttpEntity entity = response.getEntity();
	        resp=EntityUtils.toString(entity,"UTF-8");
	    }catch (Exception e) {
	    	e.printStackTrace();
	    } 
	    
	    return resp;
	}
	
	public String delete(final String url) {
		HttpDelete httpDelete = new HttpDelete(url);
		String resp = null;
		try {
			HttpResponse response = client.execute(httpDelete);
			HttpEntity entity = response.getEntity();
			resp = EntityUtils.toString(entity, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resp;
	}
}
