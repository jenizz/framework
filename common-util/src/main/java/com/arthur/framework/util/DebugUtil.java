package com.arthur.framework.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class DebugUtil {

	public static long start = 0;
	
	public static void ready() {
		start = System.currentTimeMillis();
	}

	public static long finish() {
		return (System.currentTimeMillis() - start) / 1000;
	}
	
	public static void debug(final Class<?> cls, String message){
		Log log = LogFactory.getLog(cls);
		message += ":["+(System.currentTimeMillis()-start)/1000+"]秒";
		System.out.println(message);
		log.debug(message);
	}
}
