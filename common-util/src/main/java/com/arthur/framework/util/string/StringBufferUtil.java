/*
 * Created on 2004-9-25
 */
package com.arthur.framework.util.string;

/**
 * @author zhouxw
 */
public class StringBufferUtil {

	private StringBuilder buff;

	/**
	 * @param buff
	 */
	public StringBufferUtil(StringBuilder buff) {
		this.buff = buff;
	}

	/**
	 * @param buff
	 */
	public StringBufferUtil(String str) {
		this.buff = new StringBuilder(str);
	}
	
	/**
	 * @param i
	 */
	public StringBufferUtil(int i) {
		this.buff = new StringBuilder(i);
	}

	/**
	 * 
	 */
	public StringBufferUtil() {
		this.buff = new StringBuilder();
	}

	/**
	 * @param s
	 * @return StringBuffer
	 */
	public StringBufferUtil appendln(String s) {
		buff.append(s).append("\n");
		return this;
	}

	/**
	 * @param s
	 * @return StringBuffer
	 */
	public StringBufferUtil append(String s) {
		buff.append(s);
		return this;
	}

	/**
	 * @param s
	 * @return StringBuffer
	 */
	public StringBufferUtil appendln(Object s) {
		buff.append(s).append("\n");
		return this;
	}

	/**
	 * @param s
	 * @return StringBuffer
	 */
	public StringBufferUtil append(Object s) {
		buff.append(s);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return buff.toString();
	}
}