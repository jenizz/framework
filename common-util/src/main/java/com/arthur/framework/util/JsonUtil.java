/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:19:12 </p>
 * <p><b>ClassName</b>: JsonUtil.java </p> 
 * <p><b>Description</b>: JSON工具类 </p> 
 */
package com.arthur.framework.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.parser.Feature;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * <p><b>描述</b>: JSON工具类 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午3:30:35
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public abstract class JsonUtil {

	/**
	 * 序列化.
	 * @param object 要序列化的对象
	 * @return 序列化后的字符串
	 * @since 2017年3月4日 下午3:30:52
	 * @author chanlong(陈龙)
	 */
	public static String serialize(final Object object) {
		return JSON.toJSONString(object, SerializerFeature.WriteDateUseDateFormat, SerializerFeature.PrettyFormat);
	}

	/**
	 * 反序列化.
	 * @param <T> 泛型
	 * @param json JSON字符串
	 * @param clazz 对象类型
	 * @return 反序列化后的对象
	 * @since 2017年3月4日 下午3:31:10
	 * @author chanlong(陈龙)
	 */
	public static <T> T getBean(final String json, final Class<T> clazz) {
		return JSON.parseObject(json, clazz, Feature.AutoCloseSource);
	}
	
	public static <T> TypeReference<T> getTypeReference(Class<T> cls) {
		return new TypeReference<T>() {};
	}

}
