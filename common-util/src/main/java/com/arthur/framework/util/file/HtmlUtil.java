package com.arthur.framework.util.file;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.arthur.framework.helper.FileHelper.FileTypeHelper;
import com.arthur.framework.helper.pdf.PDFileHelper;
import com.arthur.framework.util.file.FileUtil.FileType;


/**
 * <p>描述: html工具 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月14日 上午10:31:01
 * @version 1.0.2017
 */
public class HtmlUtil {
	
	private static final String BODY_START_MARKUP = "<body>";
	private static final String BODY_OVER_MARKUP = "</body>";
	private static final String CLASSPATH = "classpath:";

	private static StringBuilder template;
	
	private static HtmlUtil instance;
	
	/**
	 * 获取模版.
	 * @param filepath 模版文件
	 * @author chanlong(陈龙)
	 * @date 2017年6月14日 下午4:59:56
	 */
	public static HtmlUtil template(final String filepath) {
		if(instance == null) instance = new HtmlUtil();
		
		File file = null;
		InputStream source = null;
		try {
			if(filepath.startsWith(CLASSPATH)){
				file = FileUtil.getTempFile(FileType.html);
				source = HtmlUtil.class.getResourceAsStream(filepath.substring(CLASSPATH.length()));
				FileUtils.copyInputStreamToFile(source, file);
			}else{
				file = new File(filepath);
			}
			
			if(file != null && file.exists()){
				template = new StringBuilder(FileUtils.readFileToString(file));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(source);
		}
		
		return instance;
	}
	
	/**
	 * 添加html到body.
	 * @param html html字符串
	 * @author chanlong(陈龙)
	 * @date 2017年6月14日 下午5:20:16
	 */
	public HtmlUtil appendToBody(final String content) {
		int start = template.lastIndexOf(BODY_START_MARKUP) + BODY_START_MARKUP.length();
		int over = template.lastIndexOf(BODY_OVER_MARKUP);
		template = template.replace(start, over, content);
		return this;
	}
	
	/**
	 * 转pdf.
	 * @author chanlong(陈龙)
	 * @date 2017年6月14日 下午5:45:01
	 */
	public PDFileHelper toPdf() {
		return new PDFileHelper(FileTypeHelper.html(template.toString())).toPdf();
	}
	
	public StringBuilder getTemplate() {
		return template;
	}
	
	private HtmlUtil() { }
}
