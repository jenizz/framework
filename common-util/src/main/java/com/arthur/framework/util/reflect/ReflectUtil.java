/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:18:10 </p>
 * <p><b>ClassName</b>: ReflectUtil.java </p> 
 * <p><b>Description</b>: 反射工具类 </p> 
 */
package com.arthur.framework.util.reflect;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.arthur.framework.util.DebugUtil;
import com.arthur.framework.util.string.StringUtil;

import aj.org.objectweb.asm.ClassReader;
import aj.org.objectweb.asm.ClassVisitor;
import aj.org.objectweb.asm.Label;
import aj.org.objectweb.asm.MethodVisitor;
import aj.org.objectweb.asm.Opcodes;
import aj.org.objectweb.asm.Type;

/**
 * <p><b>描述</b>: 反射工具类 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午9:53:49
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public abstract class ReflectUtil {
	
	private static Log log = LogFactory.getLog(ReflectUtil.class);
	
	/**
	 * <p>描述: 忽略字段的反射能力 </p>
	 * <p>Company: Professional</p>
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 上午9:09:52
	 * @version 1.0.2017
	 */
	@Target({ ElementType.FIELD, ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface PropertyIgnore {}
	
	/**
	 * 获取类对象.
	 * @param <T> 泛型
	 * @param clsname 类名
	 * @since 2017年3月4日 上午9:53:59
	 * @author chanlong(陈龙)
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> getClass(final String clsname) {
		try {
			return (Class<T>) Class.forName(clsname);
		} catch (ClassNotFoundException e) {
			log.error("ReflectUtil::getClass is error:{}", e);
			throw new RuntimeException("ReflectUtil::getClass is error:{}", e);
		}
	}
	
	/**
	 * 获取类对象.
	 * @param <T> 泛型
	 * @param instance 对象实例
	 * @since 2017年3月4日 上午9:55:42
	 * @author chanlong(陈龙)
	 */
	@SuppressWarnings("unchecked")
	public static <T> Class<T> getClass(final T instance) {
		return (Class<T>) instance.getClass();
	}
	
	public static Object getInstance(final String _class) {
		try {
			return ClassUtils.getClass(_class).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getInstance(final String _class, final Class<T> _type) {
		return (T) getInstance(_class);
	}

	/**
	 * 以Map形式，返回对象中的属性.
	 * @param orig 源对象
	 * @author chanlong(陈龙)
	 * @date 2014年7月10日  下午5:22:23
	 */
	@SuppressWarnings("unchecked")
	public static <T> Map<String, T> getProperties(final Object orig) {
		PropertyDescriptor[] properties = PropertyUtils.getPropertyDescriptors(orig);
		Map<String, T> map = new LinkedHashMap<>();
		try {
			// 获取@PropertyIgnore标注的字段名称
			List<String> names = getFieldNamesWithAnnotation(orig.getClass(), PropertyIgnore.class);
			
			for (PropertyDescriptor property : properties) {
				String name = property.getName();
				
				// 排除内部类、持久化信息、非域属性及忽略的属性
				if ("PK".equalsIgnoreCase(name) 
				 || "class".equalsIgnoreCase(name)
				 || "handler".equalsIgnoreCase(name)
				 || "hibernateLazyInitializer".equalsIgnoreCase(name)
				 || !PropertyUtils.isReadable(orig, name) 
				 || !PropertyUtils.isWriteable(orig, name)
				 || names.contains(name)) continue;
				
				Object value = PropertyUtils.getProperty(orig, name);
				if (value != null) map.put(name, (T) value);
			}
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			log.error("ReflectUtil::getProperties is error:{}", e);
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getProperty(final Object bean, final String name) {
		try {
			return (T) PropertyUtils.getProperty(bean, name);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			log.error("ReflectUtil::getProperty is error:{}", e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 设置实体对象的值.
	 * @param bean
	 * @param name
	 * @param value
	 * @author chanlong(陈龙)
	 * @date 2014年6月20日  下午3:00:08
	 */
	public static <E> void setProperty(final E bean, final String name, final Object value){
		try {
			PropertyUtils.setProperty(bean, name, value);
		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			log.error("ReflectUtil::setProperty is error:{}", e);
		}
	}
	
	/**
	 * 复制对象属性.
	 * @param orig 源对象
	 * @param dest 目标对象
	 * @since 2017年3月4日 上午9:54:04
	 * @author chanlong(陈龙)
	 */
	public static <O, D> D copyProperties(final O orig, final D dest) {
		PropertyDescriptor[] properties = PropertyUtils.getPropertyDescriptors(orig);
		try {
			// 获取@PropertyIgnore标注的字段名称
			List<String> names = getFieldNamesWithAnnotation(orig.getClass(), PropertyIgnore.class);
			
			for (PropertyDescriptor property : properties) {
				String name = property.getName();
				
				// 排除内部类、持久化信息、非域属性及忽略的属性
				if ("PK".equalsIgnoreCase(name) 
				 || "class".equalsIgnoreCase(name)
				 || "handler".equalsIgnoreCase(name)
				 || "hibernateLazyInitializer".equalsIgnoreCase(name)
				 || !PropertyUtils.isReadable(orig, name) 
				 || !PropertyUtils.isWriteable(orig, name)
				 || names.contains(name)) continue;
				
				try{
					Object ovalue = PropertyUtils.getProperty(orig, name);
					if (!StringUtil.isEmpty(ovalue)) PropertyUtils.setProperty(dest, name, ovalue);
				}catch(NoSuchMethodException e){ continue; }
			}
			return dest;
		} catch (IllegalAccessException | InvocationTargetException e) {
			log.error("ReflectUtil::copyProperties is error:{}", e);
			throw new RuntimeException("ReflectUtil::copyProperties is error:{}", e);
		}
	}
	
	/**
	 * 复制对象属性.
	 * @param dest 目标对象
	 * @param option 属性映射
	 * @author chanlong(陈龙)
	 * @date 2017年11月16日 上午9:04:11
	 */
	public static <D> D copyProperties(final D dest, final Map<String, Object> option) {
		if (option != null) {
			// 获取@PropertyIgnore标注的字段名称
			List<String> names = getFieldNamesWithAnnotation(dest.getClass(), PropertyIgnore.class);
			option.forEach((key, val) -> {
				try {
					if(val != null && !names.contains(key)) BeanUtils.setProperty(dest, key, val);
				} catch (IllegalAccessException | InvocationTargetException e) {
					log.error("ReflectUtil::copyProperties is error:{}", e);
				}
			});
		}
		return dest;
	}
	
	/**
	 * 将Map键值对集合复制到指定的对象中并返回该对象的集合
	 * 注：Map中的键名和指定对象中的属性名相同时才会发生复制操作
	 * @param <T>
	 * @param _class 类对象
	 * @param _options 
	 */
	public static <E, T extends Annotation> List<E> copyProperties(final Class<E> _class, final List<Map<String,Object>> _options){
		DebugUtil.ready();
		
		List<E> list = new ArrayList<E>();
		try {
			Field[] fields = _class.getDeclaredFields();
			for (Map<String, Object> map : _options) {
				E bean = _class.newInstance();
				for (Field field : fields) {
					String name = field.getName();
					Object value = map.get(name.toUpperCase());
					if(!StringUtil.isEmpty(value) && !isAnnotationDeclaredLocally(PropertyIgnore.class, field.getClass())){
						PropertyUtils.setProperty(bean, name, value);
					}
				}
				list.add(bean);
			}
		} catch (IllegalAccessException | InvocationTargetException | InstantiationException | NoSuchMethodException e) {
			log.error("ReflectUtil::copyProperties is error:{}", e);
		} 
		
		DebugUtil.debug(ReflectUtil.class, "方法copyProperties()的执行时间为");
		return list;
	}
	
	/**
	 * 获取指定注解的字段名称集合.
	 * @param _class 对象类
	 * @param _annatation 注解类
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 下午12:15:58
	 */
	public static List<String> getFieldNamesWithAnnotation(final Class<?> _class, final Class<? extends Annotation> _annatation) {
		List<Field> fields = FieldUtils.getFieldsListWithAnnotation(_class, _annatation);
		return fields.parallelStream().reduce(new ArrayList<String>(), (list, field) -> {
			list.add(field.getName()); return list;
		}, (left, right) -> left);
	}
	
	/**
	 * 目标是否标注指定注解.
	 * @param _annatation 注解
	 * @param _fieldClass 字段类对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 上午9:21:04
	 */
	public static boolean isAnnotationDeclaredLocally(final Class<? extends Annotation> _annatation, final Class<?> _fieldClass) {
		try {
			for (Annotation ann : _fieldClass.getDeclaredAnnotations()) {
				if (ann.annotationType() == _annatation) return true;
			}
		} catch (Throwable ex) {
			throw new RuntimeException(ex);
		}
		return false;
	}
	
	/**
	 * 查找指定对象的方法.
	 * @param _class 目标类
	 * @param _method 方法名称
	 * @param _parameterTypes 方法参数类型表
	 * @author chanlong(陈龙)
	 * @date 2014年4月26日  上午9:19:33
	 */
	public static <T> Method getMethod(final Class<T> _class, final String _method, final Class<?>... _parameterTypes) {
		try {
			return _class.getDeclaredMethod(_method, _parameterTypes);
		} catch (SecurityException | NoSuchMethodException e) {
			log.error("ReflectUtil::getMethod is error:{}", e);
			throw new RuntimeException("ReflectUtil::getMethod is error:{}", e);
		}
	}
	
	/**
	 * 根据方法名访问指定对象的方法.
	 * @param orig 对象源
	 * @param method 方法名称
	 * @param parameterTypes 参数类型表
	 * @author chanlong(陈龙)
	 * @date 2013年12月28日  下午3:56:36
	 */
	public static <T> void invoke(final T orig, final String method, final Class<?>... parameterTypes) {
		try {
			orig.getClass().getDeclaredMethod(method, parameterTypes).invoke(orig);
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			log.error("ReflectUtil::invoke is error:{}", e);
		} 
	}
	
	/**
	 * 根据方法名访问指定对象的方法.
	 * @param orig 对象源
	 * @param method 方法名
	 * @param parameterMap 参数类型及参数值映射
	 * @author chanlong(陈龙)
	 * @date 2013年12月28日  下午2:58:39
	 */
	public static <T> void invoke(final T orig, final String method, final Map<Class<?>, Object> parameterMap) {
		try {
			Class<?>[] keys = parameterMap.keySet().toArray(new Class<?>[]{});
			Object[] orgs = parameterMap.values().toArray(new Object[]{});
			orig.getClass().getDeclaredMethod(method, keys).invoke(orig, orgs);
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			log.error("ReflectUtil::invoke is error:{}", e);
		} 
	}
	
	/**
	 * 获取方法指定参数的索引位置.
	 * @param _class 类对象
	 * @param _method 方法对象
	 * @param _parameterName 参数名
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 上午9:30:18
	 */
	public static int getMethodParameterIndex(final Class<?> _class, final Method _method, final String _parameterName) {
		String[] names = getMethodParameterNames(_class, _method);
		return ArrayUtils.indexOf(names, _parameterName);
	}
	
	/**
	 * 获取方法的参数名称列表.
	 * @param _class 类对象
	 * @param _method 方法对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 上午9:32:15
	 */
	public static String[] getMethodParameterNames(final Class<?> _class, final Method _method){
		final Class<?>[] parameterTypes = _method.getParameterTypes();
		if (parameterTypes == null || parameterTypes.length == 0) {
			return null;
		}
		
		final Type[] types = new Type[parameterTypes.length];
		for (int i = 0; i < parameterTypes.length; i++) {
			types[i] = Type.getType(parameterTypes[i]);
		}
		
		final String[] parameterNames = new String[parameterTypes.length];
		String className = _class.getName();  
        int lastDotIndex = className.lastIndexOf(".");  
        className = className.substring(lastDotIndex + 1) + ".class";  
        InputStream is = _class.getResourceAsStream(className);  
        
		try {
			ClassReader classReader = new ClassReader(is);
			classReader.accept(new ClassVisitorImpl(Opcodes.ASM4).addArguments(_method, types, parameterNames), 0);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			IOUtils.closeQuietly(is);
		}
		
		return parameterNames;
	}
	
	/**
	 * <p><b>Author</b>: long.chan 2017年2月7日 下午1:30:55 </p>
	 * <p><b>ClassName</b>: ReflectUtil.java </p> 
	 * <p><b>Description</b>: </p> 
	 */
	private static class ClassVisitorImpl extends ClassVisitor{
		
		private String[] parameterNames;
		
		private Method method;
		
		private Type[] types;

		/**
		 * <p><b>Title</b>: ClassVisitorImpl </p> 
		 * <p><b>Author</b>: long.chan 2017年2月7日 下午12:52:07 </p>
		 * <p><b>Description</b>: 构造函数 </p> 
		 *
		 * @param asm4
		 *
		 */
		public ClassVisitorImpl(int asm4) {
			super(asm4);
		}
		
		/**
		 * <p><b>Title</b>: addMethod </p> 
		 * <p><b>Author</b>: long.chan 2017年2月7日 下午1:02:55 </p>
		 * <p><b>Description</b>:  </p> 
		 *
		 * @param method
		 * @return
		 *
		 */
		public ClassVisitor addArguments(final Method method, final Type[] types, final String[] parameterNames) {
			this.parameterNames = parameterNames;
			this.method = method;
			this.types = types;
			return this;
		}

		/**
		 * 
		 * <p><b>Title</b>: visitMethod </p> 
		 * <p><b>Author</b>: long.chan 2017年2月7日 下午12:59:39 </p>
		 * <p><b>Description</b>:  </p> 
		 * @param access
		 * @param name
		 * @param desc
		 * @param signature
		 * @param exceptions
		 * @return
		 * @see aj.org.objectweb.asm.ClassVisitor#visitMethod(int, java.lang.String, java.lang.String, java.lang.String, java.lang.String[])
		 *
		 */
		@Override  
        public MethodVisitor visitMethod(final int access, final String name, final String desc, final String signature, final String[] exceptions) {  
            Type[] argumentTypes = Type.getArgumentTypes(desc);  
            if (!method.getName().equals(name) || !Arrays.equals(argumentTypes, types)) {  
                return null;  
            }  
            return new MethodVisitorImpl(Opcodes.ASM4).addArguments(method, parameterNames);  
        }  
	}
	
	/**
	 * <p><b>Author</b>: long.chan 2017年2月7日 下午1:31:04 </p>
	 * <p><b>ClassName</b>: ReflectUtil.java </p> 
	 * <p><b>Description</b>: </p> 
	 */
	private static class MethodVisitorImpl extends MethodVisitor{
		
		private String[] parameterNames;
		
		private Method method;
		
		/**
		 * <p><b>Title</b>: MethodVisitorImpl </p> 
		 * <p><b>Author</b>: long.chan 2017年2月7日 下午12:56:18 </p>
		 * <p><b>Description</b>: 构造函数 </p> 
		 *
		 * @param opcodes
		 *
		 */
		public MethodVisitorImpl(int opcodes) {
			super(opcodes);
		}
		
		/**
		 * <p><b>Title</b>: addArguments </p> 
		 * <p><b>Author</b>: long.chan 2017年2月7日 下午1:29:13 </p>
		 * <p><b>Description</b>:  </p> 
		 *
		 * @param method
		 * @param parameterNames
		 * @return
		 *
		 */
		public MethodVisitor addArguments(Method method, String[] parameterNames) {
			this.parameterNames = parameterNames;
			this.method = method;
			return this;
		}

		@Override  
        public void visitLocalVariable(final String name, final String desc, final String signature, final Label start, final Label end, final int index) {  
            if (Modifier.isStatic(method.getModifiers())) {  
                parameterNames[index] = name;  
            } else if (index > 0 && index <= parameterNames.length) {  
                parameterNames[index - 1] = name;  
            }  
        }  
	}
}
