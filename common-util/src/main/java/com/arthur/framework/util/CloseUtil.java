package com.arthur.framework.util;

import java.io.Closeable;
import java.net.Socket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.Context;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class CloseUtil {

	private static Log log = LogFactory.getLog(DateUtil.class);

	public static final void close(final Closeable orig) {
		if (orig != null) {
			try {
				orig.close();
			} catch (Throwable ignore) {
				log.error(ignore);
			}
		}
	}

	public static final void close(final Socket socket) {
		if (socket != null) {
			try {
				socket.close();
			} catch (Throwable ignore) {
				log.error(ignore);
			}
		}
	}

	public static final void close(final Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (Throwable ignore) {
				log.error(ignore);
			}
		}
	}

	public static final void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (Throwable ignore) {
				log.error(ignore);
			}
		}
	}

	public static void close(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (Throwable ignore) {
				log.error(ignore);
			}
		}
	}

	public static void close(Context context) {
		if (context != null) {
			try {
				context.close();
			} catch (Throwable ignore) {
				log.error(ignore);
			}
		}
	}
}
