/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午3:51:49 </p>
 * <p><b>ClassName</b>: DateUtil.java </p> 
 * <p><b>Description</b>: 日期工具类 </p> 
 */
package com.arthur.framework.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>描述</b>: 日期工具类 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午3:29:31
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class DateUtil {
	
	private static Log log = LogFactory.getLog(DateUtil.class);
	
	private int m_year;
	private int m_month;
	private int m_day;
	private int m_hour;
	private int m_minute;
	private int m_second;
	
	private final static String DATE_DIVISION = "-";
	private final static String TIME_DIVISION = ":";
	private final static String DATE_TIME_DIVISION = " ";
	
	private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * <p><b>Title</b>: DateUtil </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午3:51:11 </p>
	 * <p><b>Description</b>: 构造日历(当天) </p> 
	 */
	public DateUtil() {
		GregorianCalendar now = new GregorianCalendar();
		m_year 	 = now.get(GregorianCalendar.YEAR);
		m_month  = now.get(GregorianCalendar.MONTH) + 1;
		m_day 	 = now.get(GregorianCalendar.DATE);
		m_hour 	 = now.get(GregorianCalendar.HOUR_OF_DAY);
		m_minute = now.get(GregorianCalendar.MINUTE);
		m_second = now.get(GregorianCalendar.SECOND);
	}
	
	public DateUtil(Date date) throws Throwable {
		this(FORMAT.format(date));
	}
	
	/**
	 * <p><b>Title</b>: DateUtil </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:12:31 </p>
	 * <p><b>Description</b>: 构造日历(指定日期)  </p> 
	 *
	 * @param date
	 * @throws Throwable
	 */
	public DateUtil(final String date) throws Throwable {
		int dfs[] = getDateFields(date);
		m_year 	 = dfs[0];
		m_month  = dfs[1];
		m_day 	 = dfs[2];
		m_hour 	 = dfs[3];
		m_minute = dfs[4];
		m_second = dfs[5];
		
		if (!isValid(m_year, m_month, m_day, m_hour, m_minute, m_second)) {
			throw new RuntimeException("DateUtil(" + date + ")");
		} else {
			return;
		}
	}
	
	/**
	 * <p><b>Title</b>: DateUtil </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:16:07 </p>
	 * <p><b>Description</b>: 构造日历 </p> 
	 *
	 * @param dateUtil
	 * @throws Throwable
	 */
	public DateUtil(final DateUtil dateUtil) throws Throwable {
		if (dateUtil == null) {
			throw new RuntimeException("DateUtil(Null)");
		}
		if (!dateUtil.isValid()) {
			throw new RuntimeException("DateUtil(" + dateUtil + ")");
		} else {
			m_year 	 = dateUtil.m_year;
			m_month  = dateUtil.m_month;
			m_day 	 = dateUtil.m_day;
			m_hour 	 = dateUtil.m_hour;
			m_minute = dateUtil.m_minute;
			m_second = dateUtil.m_second;
			return;
		}
	}
	
	/**
	 * @return DateUtil
	 * @throws Throwable
	 */
	public DateUtil getRealMonthStart() throws Throwable {
		return new DateUtil(m_year, m_month, 1, 0, 0, 0);
	}
	
	/**
	 * @return DateUtil
	 * @throws Throwable
	 */
	public DateUtil getRealMonthEnd() throws Throwable {
		DateUtil r = getMonthStart();
		if (r.getMonth() == 1 
		 || r.getMonth() == 3 
		 || r.getMonth() == 5
		 || r.getMonth() == 7 
		 || r.getMonth() == 8 
		 || r.getMonth() == 10
		 || r.getMonth() == 12) {
			r.advanceDays(30);
		}
		
		if (r.getMonth() == 4 
		 || r.getMonth() == 6 
		 || r.getMonth() == 9 
		 || r.getMonth() == 11) {
			r.advanceDays(29);
		}
		
		if (r.getMonth() == 2) {
			int year = r.getYear();
			if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
				r.advanceDays(28);
			} else {
				r.advanceDays(27);
			}
		}
		return r;
	}
	
	/**
	 * @return DateUtil
	 * @throws Throwable
	 */
	public DateUtil getMonthStart() throws Throwable {
		int m_year_last  = m_year;
		int m_month_last = m_month - 1;
		int m_day_last   = m_day;
		
		if (m_day_last >= 1) {
			m_month_last = m_month;
		}
		
		if (m_month_last == 0) {
			m_month_last = 12;
			m_year_last = m_year - 1;
		}
		return new DateUtil(m_year_last, m_month_last, 1, 0, 0, 0);
	}

	/**
	 * <p><b>Title</b>: DateUtil </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:17:14 </p>
	 * <p><b>Description</b>: 构造日历(指定年月日时分秒) </p> 
	 *
	 * @param year		年
	 * @param month 	月
	 * @param day		日
	 * @param hour		时
	 * @param minute	分
	 * @param second	秒
	 * @throws Throwable
	 *
	 */
	public DateUtil(int year, int month, int day, int hour, int minute, int second) throws Throwable {
		if (!isValid(year, month, day, hour, minute, second)) {
			throw new RuntimeException("DateUtil(" + year + "," + month + "," + day + "," + hour + "," + minute + "," + second + ")");
		} else {
			m_year 	 = year;
			m_month  = month;
			m_day 	 = day;
			m_hour 	 = hour;
			m_minute = minute;
			m_second = second;
			return;
		}
	}
	
	/**
	 * @param days
	 * @return DateUtil
	 * @throws Throwable
	 */
	public DateUtil getAdvanceDayUtil(int days) throws Throwable {
		DateUtil adr = new DateUtil(this);
		adr.advanceDays(days);
		return adr;
	}

	/**
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午3:52:07 </p>
	 * <p><b>ClassName</b>: DateUtil.java </p> 
	 * <p><b>Description</b>: 日期范围 </p> 
	 */
	public enum DateRange{
		YEAR, MONTH, WEEK, DATE
	}
	
	/**
	 * <p><b>Author</b>: long.chan 2017年3月6日 上午11:15:23 </p>
	 * <p><b>ClassName</b>: DateUtil.java </p> 
	 * <p><b>Description</b>: 日期模式 </p> 
	 */
	public enum DatePattern{
		TIME("HH:mm"), DATE("yyyy-MM-dd"), DATETIME("yyyy-MM-dd HH:mm:ss");

		private String value;

		private DatePattern(String value) {
			this.value = value;
		}

		public String value() {
			return value;
		}
	}

	/**
	 * <p><b>Title</b>: current </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午3:52:56 </p>
	 * <p><b>Description</b>: 当天日期 </p> 
	 *
	 * @return String
	 */
	public static String current(){
		return DateFormatUtils.format(new Date(), DatePattern.DATE.value);
	}
	
	/**
	 * <p><b>Title</b>: currentime </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午3:53:03 </p>
	 * <p><b>Description</b>: 当天日期 </p> 
	 *
	 * @return String
	 */
	public static String currentime(){
		return DateFormatUtils.format(new Date(), DatePattern.DATETIME.value);
	}
	
	/**
	 * <p><b>Title</b>: isValid </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:10:24 </p>
	 * <p><b>Description</b>: 验证日期(当前日期) </p> 
	 *
	 * @return boolean
	 */
	public boolean isValid() {
		return isValid(m_year, m_month, m_day, m_hour, m_minute, m_second);
	}
	
	/**
	 * <p><b>Title</b>: isValid </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:11:01 </p>
	 * <p><b>Description</b>: 验证日期 </p> 
	 *
	 * @param sDate
	 * @return
	 * @throws Throwable
	 */
	public final static boolean isValid(final String datetime) throws Throwable {
		int dfs[];
		try {
			dfs = getDateFields(datetime);
		} catch (RuntimeException e) {
			return false;
		}
		return isValid(dfs[0], dfs[1], dfs[2], dfs[3], dfs[4], dfs[5]);
	}
	
	/**
	 * <p><b>Title</b>: isValid </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:07:29 </p>
	 * <p><b>Description</b>: 验证日期 </p> 
	 *
	 * @param year 年
	 * @param month 月
	 * @param day 日
	 * @param hour 时
	 * @param minute 分
	 * @param second 秒
	 * @return boolean
	 */
	public final static boolean isValid(int year, int month, int day, int hour, int minute, int second) {
		if (hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59) {
			return false;
		}
		if (year < 0 || month < 1 || month > 12 || day < 1 || day > 31) {
			return false;
		}
		switch (month) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12: return true;
			case 4:
			case 6:
			case 9:
			case 11: return day <= 30;
			case 2:{
				if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
					return day <= 29;
				}else{
					return day <= 28;
				}
			}
		}
		return false;
	}
	
	/**
	 * <p><b>Title</b>: getDateFields </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:02:53 </p>
	 * <p><b>Description</b>: 获取年月日时分秒 </p> 
	 *
	 * @param datetime 完整日期
	 * @return int[]
	 * @throws Throwable
	 */
	public final static int[] getDateFields(final String datetime) throws Throwable {
		String dayAndtime[] = StringUtil.split(datetime, DATE_TIME_DIVISION);
		if (dayAndtime.length != 2 && dayAndtime.length != 1) {
			throw new RuntimeException("DateUtil.getDateFields(" + datetime + ")");
		}
		
		int dates[] = getIntsFromStr(dayAndtime[0], DATE_DIVISION);
		if (dates.length != 3) {
			throw new RuntimeException("DateUtil.getDateFields(" + datetime + ")");
		}
		
		int times[] = new int[3];
		if (dayAndtime.length == 2) {
			int ts[] = getIntsFromStr(dayAndtime[1], TIME_DIVISION);
			if (ts.length > 3) {
				throw new RuntimeException("DateUtil.getDateFields(" + datetime + ")");
			}
			if (ts.length == 1) {
				times[0] = ts[0];
				times[1] = 0;
				times[2] = 0;
			}
			if (ts.length == 2) {
				times[0] = ts[0];
				times[1] = ts[1];
				times[2] = 0;
			}
			if (ts.length == 3) {
				times[0] = ts[0];
				times[1] = ts[1];
				times[2] = ts[2];
			}
		} else {
			times[0] = 0;
			times[1] = 0;
			times[2] = 0;
		}
		
		
		int r[] = new int[6];
			r[0] = dates[0];
			r[1] = dates[1];
			r[2] = dates[2];
			r[3] = times[0];
			r[4] = times[1];
			r[5] = times[2];
		return r;
	}
	
	/**
	 * 转日期.
	 * @param date 日期字符串
	 * @param pattern 日期模式
	 * @return 日期
	 * @since 2017年3月4日 下午3:30:04
	 * @author chanlong(陈龙)
	 */
	public static Date parseDate(final Object date, final DatePattern... pattern) {
		try {
			return DateUtils.parseDate(String.valueOf(date), DatePattern.DATE.value(), DatePattern.DATETIME.value());
		} catch (ParseException e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 返回指定范围类型的时间组 <br>
	 * ps: 默认为返回当前月的时间组 <br> 
	 * eg: range(RangeType.MONTH, null).
	 * 
	 * @param type 范围类型(year|month|week)
	 * @param basis 依据时间(null of new Date())
	 * @return Date[] 
	 * @author chanlong(陈龙)
	 * @date 2016年2月13日 上午11:51:34
	 */
	public static Date[] range(final DateRange type, final Date basis){
		switch (type) {
			case MONTH: return rangeOfMonth(basis);
			case DATE: {
				String date = DateFormatUtils.format(basis, DatePattern.DATETIME.value);
				System.out.println(date);
			}
			default: return rangeOfMonth(basis);
		}
	}
	
	/**
	 * 根据字符串值返回时间范围.
	 * @param amount
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2016年8月5日 下午3:00:30
	 */
	public static Date[] range(final Object value){
		String[] dates = StringUtil.split(value, StringUtil.COMMA);
		return new Date[]{
				DateUtil.parseDate(dates[0], DatePattern.DATE, DatePattern.DATETIME), 
				DateUtil.parseDate(dates[1], DatePattern.DATE, DatePattern.DATETIME)};
	}
	
	/**
	 * 根据天数返回时间范围.
	 * @param amount
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2016年8月5日 下午3:00:30
	 */
	public static Date[] range(final int amount){
		Date _over = new Date();
		Date _from = DateUtils.addDays(_over, -amount);
		return new Date[]{_from, _over};
	}
	
	/**
	 * @return String
	 */
	public String getTimeString() {
		String affHour   = m_hour   >= 10 ? String.valueOf(m_hour)   : "0" + String.valueOf(m_hour);
		String affMinute = m_minute >= 10 ? String.valueOf(m_minute) : "0" + String.valueOf(m_minute);
		String affSecond = m_second >= 10 ? String.valueOf(m_second) : "0" + String.valueOf(m_second);
		return affHour + TIME_DIVISION + affMinute + TIME_DIVISION + affSecond;
	}
	
	/**
	 * <p><b>Title</b>: getDateString </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:33:45 </p>
	 * <p><b>Description</b>: 返回日期(yyyy-MM-dd) </p> 
	 *
	 * @param division
	 * @return
	 */
	public String getDateString(final String division) {
		String affMonth = m_month >= 10 ? String.valueOf(m_month) : "0" + String.valueOf(m_month);
		String affDay = m_day >= 10 ? String.valueOf(m_day) : "0" + String.valueOf(m_day);
		return String.valueOf(m_year) + (StringUtil.isEmpty(division) ? DATE_DIVISION : division) + affMonth 
									  + (StringUtil.isEmpty(division) ? DATE_DIVISION : division) + affDay;
	}
	
	/**
	 * <p><b>Title</b>: getDatetimeString </p> 
	 * <p><b>Author</b>: long.chan 2017年3月6日 下午4:31:49 </p>
	 * <p><b>Description</b>: 返回日期(yyyy-MM-dd HH:mm:ss) </p> 
	 *
	 * @return
	 */
	public String getDatetimeString() {
		String affMonth  = m_month 	>= 10 ? String.valueOf(m_month)  : "0" + String.valueOf(m_month);
		String affDay 	 = m_day 	>= 10 ? String.valueOf(m_day) 	 : "0" + String.valueOf(m_day);
		String affHour 	 = m_hour 	>= 10 ? String.valueOf(m_hour) 	 : "0" + String.valueOf(m_hour);
		String affMinute = m_minute >= 10 ? String.valueOf(m_minute) : "0" + String.valueOf(m_minute);
		String affSecond = m_second >= 10 ? String.valueOf(m_second) : "0" + String.valueOf(m_second);
		
		return String.valueOf(m_year) + DATE_DIVISION 		+ affMonth 
									  + DATE_DIVISION 		+ affDay 
									  + DATE_TIME_DIVISION 	+ affHour 
									  + TIME_DIVISION 		+ affMinute 
									  + TIME_DIVISION 		+ affSecond;
	}
	
	/**
	 * @return java.sql.Date
	 */
	public java.sql.Date getSqlDate() {
		return java.sql.Date.valueOf(getDateString(""));
	}
	
	/**
	 * @return java.util.Date
	 */
	public java.util.Date getUtilDate() {
		java.util.Date date = new java.util.Date();
		date.setTime(getTime());
		return date;
	}
	
	/**
	 * @return long
	 */
	public long getTime() {
		return this.getSqlDate().getTime();
	}
	
	/**
	 * @return int
	 * @throws Throwable
	 */
	public int getWeekDay() throws Throwable {
		DateUtil std = new DateUtil("2000-01-03");
		int days = daysBetween(std);
		int m = days % 7;
		if (m >= 0) {
			return m + 1;
		} else {
			return m + 8;
		}
	}	
	
	/**
	 * @return DateUtil
	 * @throws Throwable
	 */
	public DateUtil getWeekEnd() throws Throwable {
		DateUtil r = getWeekStart();
		r.advanceDays(7);
		return r;
	}
	
	/**
	 * @return DateUtil
	 * @throws Throwable
	 */
	public DateUtil getWeekStart() throws Throwable {
		DateUtil std = new DateUtil("2000-01-03");
		int days = daysBetween(std);
		int m = days % 7;
		DateUtil r;
		if (m >= 0) {
			r = getAdvanceDayUtil(-m);
		} else {
			r = getAdvanceDayUtil(-m - 7);
		}
		return r;
	}
	
	/**
	 * @param date
	 * @return int
	 */
	public int daysBetween(DateUtil date) {
		return toJulian() - date.toJulian();
	}
	
	/**
	 * @param days
	 */
	public Date advanceDays(int days) {
		return fromJulian(toJulian() + days);
	}
	
	/**
	 * @param j
	 */
	private Date fromJulian(int j) {
		int ja = j;
		int JGREG = 0x231519;
		if (j >= JGREG) {
			int jalpha = (int) (((double) (float) (j - 0x1c7dd0) - 0.25D) / 36524.25D);
			ja += (1 + jalpha) - (int) (0.25D * (double) jalpha);
		}
		
		int jb = ja + 1524;
		int jc = (int) (6680D + ((double) (float) (jb - 0x253abe) - 122.09999999999999D) / 365.25D);
		int jd = (int) ((double) (365 * jc) + 0.25D * (double) jc);
		int je = (int) ((double) (jb - jd) / 30.600100000000001D);
		
		m_day = jb - jd - (int) (30.600100000000001D * (double) je);
		m_month = je - 1;
		if (m_month > 12) {
			m_month -= 12;
		}
		
		m_year = jc - 4715;
		if (m_month > 2) {
			m_year--;
		}
		
		if (m_year <= 0) {
			m_year--;
		}
		
		return this.getUtilDate();
	}
	
	/**
	 * @return Date
	 */
	public final static java.sql.Date getCurrDate() {
		return new java.sql.Date(new java.sql.Timestamp(System.currentTimeMillis()).getTime());
	}
	
	/**
	 * @param sd
	 * @param ed
	 * @return int
	 */
	public final static int daysBetween(DateUtil sd, DateUtil ed) {
		return ed.toJulian() - sd.toJulian();
	}
	
	/**
	 * @return String
	 * @throws ParseException
	 */
	public final static String getLastTime() {
		try {
			String lastTime = new java.sql.Timestamp(System.currentTimeMillis()).toString();
			return lastTime;
		} catch (Throwable ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @return String
	 * @throws ParseException
	 */
	public final static String getLastTime(String style) {
		try {
			if (style == null) {
				style = "yyyy-MM-dd HH:mm:ss";
			} else if ("yyyy-MM-dd hh:mm:ss".equals(style)) {
				style = "yyyy-MM-dd HH:mm:ss";
			}
			return format(new Date(), style);
		} catch (Throwable ex) {
			throw new RuntimeException(ex);
		}
	}
	
	/**
	 * @param day
	 * @return java.util.Date
	 */
	public final static java.util.Date getFirstDayOfMonth(java.util.Date day) {
		if (day == null) day = new java.util.Date();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(day);
		
		gc.set(GregorianCalendar.DAY_OF_MONTH, 1);
		gc.set(GregorianCalendar.HOUR, 0);
		gc.set(GregorianCalendar.MINUTE, 0);
		gc.set(GregorianCalendar.SECOND, 0);
		return gc.getTime();
	}
	
	/**
	 * @param day
	 * @return java.util.Date
	 */
	public final static java.util.Date getLastDayOfMonth(java.util.Date day) {
		if (day == null) day = new java.util.Date();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(day);
		
		int lastMonthDay = gc.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
		gc.set(GregorianCalendar.DAY_OF_MONTH, lastMonthDay);
		gc.set(GregorianCalendar.HOUR, 23);
		gc.set(GregorianCalendar.MINUTE, 59);
		gc.set(GregorianCalendar.SECOND, 59);
		return gc.getTime();
	}
	
	/**
	 * @param day
	 * @return java.util.Date
	 */
	public final static java.util.Date getFirstDayOfQuarter(java.util.Date day) {
		if (day == null) day = new java.util.Date();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(day);
		
		int currentMonth = gc.get(GregorianCalendar.MONTH);
		if (currentMonth <= GregorianCalendar.MARCH) {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.JANUARY, 1);
		} else if (currentMonth <= GregorianCalendar.JUNE) {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.APRIL, 1);
		} else if (currentMonth <= GregorianCalendar.SEPTEMBER) {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.JULY, 1);
		} else {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.OCTOBER, 1);
		}
		return getFirstDayOfMonth(gc.getTime());
	}
	
	/**
	 * @param day
	 * @return java.util.Date
	 */
	public final static java.util.Date getLastDayOfQuarter(java.util.Date day) {
		if (day == null) day = new java.util.Date();
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(day);
		
		int currentMonth = gc.get(GregorianCalendar.MONTH);
		if (currentMonth <= GregorianCalendar.MARCH) {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.MARCH, 1);
		} else if (currentMonth <= GregorianCalendar.JUNE) {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.JUNE, 1);
		} else if (currentMonth <= GregorianCalendar.SEPTEMBER) {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.SEPTEMBER, 1);
		} else {
			gc.set(gc.get(GregorianCalendar.YEAR), GregorianCalendar.DECEMBER, 1);
		}
		return getLastDayOfMonth(gc.getTime());
	}
	
	/**
	 * @param value
	 * @return
	 * @throws ParseException
	 */
	public final static java.util.Date getDate(String value) throws ParseException {
		java.util.Date d = null;
		if (value == null) {
			return d;
		} else if (value.length() > 10) {
			if (value.endsWith("Z"))
				value = value.substring(0, value.length() - 1);
			if (value.indexOf('T') >= 0)
				value = StringUtil.replaceAll(value, "T", " ");
			if (value.length() < 18) {
				value = value.substring(0, 10);
				d = new SimpleDateFormat("yyyy-MM-dd").parse(value);
			} else {
				d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value);
			}
		} else if (StringUtil.isNotEmpty(value))
			d = new SimpleDateFormat("yyyy-MM-dd").parse(value);
		return d;
	}
	
	/**
	 * @param timeStr
	 * @param dateStyle
	 * @return java.util.Date
	 */
	public final static java.util.Date getDate(String timeStr, String dateStyle) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(dateStyle);
			Date date = sdf.parse(timeStr);
			return date;
		} catch (Throwable ex) {
			throw new RuntimeException(ex);
		}
	}
	
	/**
	 * @param timeStr
	 * @param dateStyle
	 * @return String
	 */
	public final static String format(String timeStr, String dateStyle) {
		String style = dateStyle == null ? "yyyy-MM-dd" : dateStyle;
		if (style.length() > timeStr.length() && timeStr.indexOf(" ") < 0) {
			timeStr += " 00:00:00"; // 冗余修正
		}
		java.util.Date date = getDate(timeStr, style);
		return format(date, style);
	}
	
	/**
	 * 将日期类型转化为字符串类型
	 * 
	 * @param timeStr
	 * @param sFormat
	 * @return String
	 */
	public final static String format(java.util.Date date, String sFormat) {
		sFormat = sFormat == null ? "yyyy-MM-dd" : sFormat;
		SimpleDateFormat sy1 = null;
		if ("YY-MM-DD".equalsIgnoreCase(sFormat))
			sFormat = "yy-MM-dd";
		else if ("YYMMDD".equalsIgnoreCase(sFormat))
			sFormat = "yyMMdd";
		else if ("MM-DD".equalsIgnoreCase(sFormat))
			sFormat = "MM-dd";
		else if ("MMDD".equalsIgnoreCase(sFormat))
			sFormat = "MMdd";
		else if ("YYYY-MM-DD".equalsIgnoreCase(sFormat))
			sFormat = "yyyy-MM-dd";
		else if ("YYYY-MM".equalsIgnoreCase(sFormat))
			sFormat = "yyyy-MM";
		else if ("YYYYMM".equalsIgnoreCase(sFormat))
			sFormat = "yyyyMM";
		else if ("YYYY".equalsIgnoreCase(sFormat))
			sFormat = "yyyy";
		else if ("YYYYMMDD".equalsIgnoreCase(sFormat))
			sFormat = "yyyyMMdd";
		else if ("YYYYMMDDHHMMSS".equalsIgnoreCase(sFormat))
			sFormat = "yyyyMMddHHmmss";
		else if ("YYYYMMDDHH".equalsIgnoreCase(sFormat))
			sFormat = "yyyyMMddHH";
		else if ("YYYYMMDDHHMM".equalsIgnoreCase(sFormat))
			sFormat = "yyyyMMddHHmm";
		else if ("YYYYMMDDMMSS".equalsIgnoreCase(sFormat))
			sFormat = "yyyyMMddmmss";
		else if ("YYYYMMDDHHSS".equalsIgnoreCase(sFormat))
			sFormat = "yyyyMMddHHss";
		sy1 = new SimpleDateFormat(sFormat);
		return sy1.format(date);
	}
	
	public static String format(Object date, String dateStyle) {
		if (date == null) {
			return null;
		}
		if ((date instanceof java.util.Date)) {
			return format((java.util.Date) date, dateStyle);
		}
		String s = date.toString();
		if (s.length() == 0) {
			return null;
		}
		return format(s, dateStyle);
	}
	
	/**
	 * @return 返回当前中国日期
	 * 
	 */
	public final static String getLastTimeChina() {
		return ChineseDate.today();
	}
	
	/**
	 * @return 返回当前中国节日
	 */
	public final static String getFtvChina() {
		String ftv = ChineseDate.todayFtv();
		if (ftv != null)
			return ftv.substring(5);
		return ftv;
	}
	
	/**
	 * @param date
	 * @return 返回当时中国节日
	 */
	public final static String getFtvChina(Date date) {
		String ftv = ChineseDate.dateFtv(date);
		if (ftv != null)
			return ftv.substring(5);
		return ftv;
	}
	
	/**
	 * @param date
	 * @param adDays
	 * @return 返回当时中国节日
	 * @throws Throwable
	 */
	public final static String getFtvChina(Date date, int adDays) throws Throwable {
		DateUtil dateUtil = new DateUtil(date);
		dateUtil.advanceDays(adDays);
		String ftv = ChineseDate.dateFtv(dateUtil.getUtilDate());
		if (ftv != null) return ftv.substring(5);
		return ftv;
	}
	
	/**
	 * @return int
	 */
	private int toJulian() {
		int jy = m_year;
		if (m_year < 0) {
			jy++;
		}
		
		int jm = m_month;
		if (m_month > 2) {
			jm++;
		} else {
			jy--;
			jm += 13;
		}
		
		int jul = (int) (Math.floor(365.25D * (double) jy) + Math.floor(30.600100000000001D * (double) jm) + (double) m_day + 1720995D);
		int IGREG = 0x8fc1d;
		if (m_day + 31 * (m_month + 12 * m_year) >= IGREG) {
			int ja = (int) (0.01D * (double) jy);
			jul += (2 - ja) + (int) (0.25D * (double) ja);
		}
		return jul;
	}
	
	// 返回依据日期所在月的第一天和最后一天
	private static Date[] rangeOfMonth(final Date basis){
		Date now = basis == null ? new Date() : basis;
		Calendar calendar = DateUtils.toCalendar(now);
		
		Date fromDate = DateUtils.setDays(now, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date overDate = DateUtils.setDays(now, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		
		return new Date[]{fromDate, overDate};
	}
	
	private static int[] getIntsFromStr(String str, String token) {
		StringTokenizer st = new StringTokenizer(str, token);
		Vector<String> v = new Vector<String>();
		
		for (; st.hasMoreTokens(); v.add(st.nextToken())){
			// do nothing;
		}
		
		int ints[] = new int[v.size()];
		for (int i = 0; i < v.size(); i++) {
			ints[i] = Integer.parseInt((String) v.get(i));
		}
		return ints;
	}

	@Override
	public boolean equals(Object v) {
		if (!(v instanceof DateUtil)) {
			return false;
		}
		DateUtil d = (DateUtil) v;
		return m_year == d.m_year && m_month == d.m_month && m_day == d.m_day
				&& m_hour == d.m_hour && m_minute == d.m_minute
				&& m_second == d.m_second;
	}
	
	@Override
	public String toString() {
		return getDatetimeString();
	}
	
	/**
	 * @return the m_year
	 */
	public int getYear() {
		return m_year;
	}
	
	/**
	 * @return int
	 */
	public int getQuarter() {
		return (m_month + 2) / 3;
	}

	/**
	 * @return the m_month
	 */
	public int getMonth() {
		return m_month;
	}

	/**
	 * @return the m_day
	 */
	public int getDay() {
		return m_day;
	}

	/**
	 * @return the m_hour
	 */
	public int getHour() {
		return m_hour;
	}

	/**
	 * @return the m_minute
	 */
	public int getMinute() {
		return m_minute;
	}

	/**
	 * @return the m_second
	 */
	public int getSecond() {
		return m_second;
	}
}
