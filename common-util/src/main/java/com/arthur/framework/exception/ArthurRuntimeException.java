package com.arthur.framework.exception;

public class ArthurRuntimeException extends RuntimeException {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 5050475331315544165L;

	public ArthurRuntimeException(Throwable e) {
		super(e.getMessage(), e);
	}
}
