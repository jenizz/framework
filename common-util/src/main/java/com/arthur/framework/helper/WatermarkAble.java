package com.arthur.framework.helper;

public interface WatermarkAble<H> {

	H addWatermark(final String text);

	H write();

}
