package com.arthur.framework.helper;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;

import com.arthur.framework.util.file.FileUtil;
import com.arthur.framework.util.file.FileUtil.FileType;

/**
 * <p>描述: File帮助类 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月15日 上午10:30:38
 * @version 1.0.2017
 */
public abstract class FileHelper {
	
	private File file;
	
	/**
	 * 保存文件到指定路径.
	 * @param filepath
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 上午10:36:43
	 */
	public boolean saveAs(final String filepath) {
		return saveAs(new File(filepath));
	}

	/**
	 * 保存文件到指定路径.
	 * @param destFile
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 上午10:36:50
	 */
	public boolean saveAs(final File destFile) {
		try {
			FileUtils.copyFile(this.file, destFile);
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	/**
	 * 获取文件输出流.
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 上午11:12:39
	 */
	public OutputStream getOutput() {
		try {
			return FileUtils.openOutputStream(file);
		} catch (IOException e) {
			throw new RuntimeException("get the File OutputStream is error : {}", e);
		}
	}
	
	/**
	 * 获取文件字节码.
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 上午11:15:08
	 */
	public byte[] getBytes() {
		try {
			return FileUtils.readFileToByteArray(file);
		} catch (IOException e) {
			throw new RuntimeException("get the File Bytes is error : {}", e);
		}
	}
	
	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	public interface FileTypeHelper {
		static FileTypeHelper html(final String content){
			return new FileTypeHelper(){
				@Override
				public File getFile() {
					File file = FileUtil.getTempFile(FileType.html);
					try {
						FileUtils.writeStringToFile(file, content);
					} catch (IOException e) {
						e.printStackTrace();
					}
					return file;
				}
			};
		}
		
		static FileTypeHelper file(final String filePath){
			return new FileTypeHelper(){
				@Override
				public File getFile() {
					return new File(filePath);
				}
			};
		}

		File getFile();
	}
}
