package com.arthur.framework.helper.pdf;

import org.xhtmlrenderer.pdf.PDFEncryption;

import com.arthur.framework.helper.FileHelper;
import com.arthur.framework.helper.WatermarkAble;
import com.arthur.framework.util.file.PdfUtil;
import com.arthur.framework.util.string.StringUtil;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * <p>描述: pdf文件帮助类 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月15日 上午11:00:04
 * @version 1.0.2017
 */
public class PDFileHelper extends FileHelper implements WatermarkAble<PDFileHelper> {

	private PdfUtil util;
	
	public PDFileHelper(FileTypeHelper type) {
		this.setFile(type.getFile());
	}
	
	/**
	 * 转PDF.
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午3:25:47
	 */
	public PDFileHelper toPdf() {
		util = PdfUtil.createRenderer(super.getFile()); 
		this.setFile(util.getFile());
		return this;
	}
	
	/**
	 * 仅启用打印权限.
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午3:25:59
	 */
	public PDFileHelper enablePrintOnly() {
		return enableEncryption(PDFEncryptionType.PRINT_ONLY);
	}
	
	/**
	 * 启用文件保护.
	 * @param type PDFEncryptionType
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午2:45:46
	 */
	public PDFileHelper enableEncryption(final PDFEncryptionType type) {
		return enableEncryption(type, "antilong", "antilong");
	}
	
	/**
	 * 启用文件保护.
	 * @param type PDFEncryptionType
	 * @param userPassword 用户密码
	 * @param ownerPassword 管理者密码
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午2:48:43
	 */
	public PDFileHelper enableEncryption(final PDFEncryptionType type, final String userPassword, final String ownerPassword) {
		util = util.addEncryption(type.get(userPassword, ownerPassword)); return this;
	}
	
	/**
	 * 添加文字水印
	 * @see com.arthur.framework.helper.WatermarkAble#addWatermark(java.lang.String)
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午2:45:33
	 */
	@Override
	public PDFileHelper addWatermark(final String text) {
		util = util.addWatermark(text); return this;
	}
	
	/**
	 * 输出文件
	 * @see com.arthur.framework.helper.WatermarkAble#write()
	 * @author chanlong(陈龙)
	 * @date 2017年6月15日 下午3:13:34
	 */
	@Override
	public PDFileHelper write() {
		util.write(); return this;
	}
	
	public enum PDFEncryptionType{
		PRINT_ONLY{
			@Override
			public PDFEncryption get(final String userPwd, final String ownerPwd) {
				return new PDFEncryption(StringUtil.getBytes(userPwd), StringUtil.getBytes(ownerPwd), PdfWriter.ALLOW_PRINTING);
			}
		};
		public abstract PDFEncryption get(final String userPwd, final String ownerPwd);
	}
}
