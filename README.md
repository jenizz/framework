#framework

	基于SpringBoot的开发脚手架

-  **common-core：**脚手架核心组件，默认打开Spring Cache、Spring Session、Transcation及Async、Scheduling、Swagger等，默认加载common-util、common-orm、common-crypt三个组件;

- common-orm：基于SpringDataJPA、Hibernate、Mybatis的ORM组件;
- common-auth：基于Spring Security的安全组件，包含角色权限的实体域;
- common-user：用户管理组件，包含组织用户信息的实体域;
- common-util：工具类组件;
- common-crypt：加密解密组件;
- common-search：基于Elasticsearch的检索引擎;
- common-echart：对echart3的封装，来自第三方开源项目;
- common-workflow：基于Activit5的工作流引擎;
- common-datasource：基于Druid的数据库连接组件，支持连接、用户、密码等配置加密，支持多数据源。
