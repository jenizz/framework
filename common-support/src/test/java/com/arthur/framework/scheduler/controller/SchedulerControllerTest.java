package com.arthur.framework.scheduler.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest
public class SchedulerControllerTest extends AbstractEnvironment {

	@Autowired    
    private WebApplicationContext context;
	
	private MockMvc mock;
	
	@Before  
    public void setup() {    
        this.mock = MockMvcBuilders.webAppContextSetup(this.context).build();    
    }  
	
	@Test
	public void test() throws Exception {
		String url = "http://localhost:9090/scheduler/task/add";
		
		JSONObject map = new JSONObject();
		map.put("key", "com.arthur.framework.scheduler.service.SchedulerServiceTest");
		map.put("name", "测试计划任务");
		map.put("cron", "0/10 * * * * ?");
		
		MvcResult result = mock.perform(post(url).contentType(MediaType.APPLICATION_JSON).content(JSONObject.toJSONString(map)))
							   .andExpect(status().isOk())
							   .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
							   .andReturn();   
		
		System.out.println(result.getResponse().getContentAsString()); 
	}
}
