package com.arthur.framework.scheduler.support.type;

public enum SchedulerTaskType {

	REST_URL, LOCAL_API, DYNAMIC_TASK;
	
	public boolean match(final String type) {
		return name().equals(type);
	}
}
