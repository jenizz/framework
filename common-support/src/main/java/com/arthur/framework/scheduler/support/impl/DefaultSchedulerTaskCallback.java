package com.arthur.framework.scheduler.support.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arthur.framework.scheduler.support.SchedulerTaskCallbackAdapter;

public class DefaultSchedulerTaskCallback extends SchedulerTaskCallbackAdapter {

	private static final Logger log = LoggerFactory.getLogger(DefaultSchedulerTaskCallback.class);

	@Override
	public void run() {
		log.debug("默认计划任务已执行");
	}

}
