package com.arthur.framework.scheduler.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.restclient.model.RestfulModel;
import com.arthur.framework.scheduler.model.SchedulerModel;
import com.arthur.framework.scheduler.support.type.SchedulerTaskType;
import com.arthur.framework.util.string.StringUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p><b>描述</b>: 计划任务-任务详情 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年9月18日 上午9:52:18
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_sys_scheduler_task")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class SchedulerTask extends CommonEntityUUID<SchedulerTask> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -3816550776789922412L;

	// 任务key
	@Column(name = "key_", nullable = false, unique = true)
	private String key;
	
	// 任务type
	@Column(name = "type_", nullable = false)
	private String type;
	
	// 任务名称
	@Column(name = "name_", nullable = false)
	private String name;
	
	// 触发条件
	@Column(name = "cron_", nullable = false)
	private String cron;
	
	// 任务状态
	@Column(name = "status_")
	private Integer status;
	
	// 任务ID
	@Column(name = "task_id")
	private String taskId;
	
	// 任务实现类
	@Column(name = "task_class")
	private String taskClass;
		
	// 外部任务media
	@Column(name = "media_")
	private String media;
		
	// 外部任务method
	@Column(name = "method_")
	private String method;
		
	// 外部任务params
	@Lob @Basic(fetch = FetchType.LAZY)
	@Column(name = "params_")
	private String params;

	public SchedulerTask() {

	}
	
	public SchedulerTask(final SchedulerModel model) {
		this.key = model.getSchedulerFutureKey();
		this.type = model.getType().name();
		if(SchedulerTaskType.REST_URL.match(type)) {
			RestfulModel restful = model.getRestful();
			this.setMedia(restful.getMedia().name());
			this.setMethod(restful.getMethod().name());
			if(StringUtil.isNotEmpty(restful.getParams())) this.setParams(restful.getParams().toJSONString());
		}
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskClass() {
		return taskClass;
	}

	public void setTaskClass(String taskClass) {
		this.taskClass = taskClass;
	}

	public String getMedia() {
		return media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

}
