package com.arthur.framework.scheduler.model;

import org.springframework.http.HttpMethod;
import org.springframework.scheduling.support.CronTrigger;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.orm.model.AbstractCommonModel;
import com.arthur.framework.restclient.model.RestfulModel;
import com.arthur.framework.restclient.support.type.ContentType;
import com.arthur.framework.scheduler.entity.SchedulerTask;
import com.arthur.framework.scheduler.support.SchedulerTaskAble;
import com.arthur.framework.scheduler.support.SchedulerTaskCallback;
import com.arthur.framework.scheduler.support.impl.UrlSchedulerTaskCallback;
import com.arthur.framework.scheduler.support.type.SchedulerTaskType;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.arthur.framework.util.string.StringUtil;

public class SchedulerModel extends AbstractCommonModel<SchedulerModel, SchedulerTask> implements SchedulerTaskAble {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -1720494558676648131L;

	// 任务key
	private String key;
	
	// 任务type
	private SchedulerTaskType type;
	
	// 任务名称
	private String name;
	
	// 触发条件
	private String cron;
	
	// 任务状态
	private Integer status;
	
	// 任务ID（业务ID）
	private String taskId;
	
	// 任务实现类
	private String taskClass;
	
	@PropertyIgnore
	private RestfulModel restful;
	
	public SchedulerModel() {
		this.status = StatusType.ACTIVITY.value();
	}
	
	public SchedulerModel(final SchedulerTask task) {
		this.key = task.getKey();
		this.type = SchedulerTaskType.valueOf(task.getType());
		
		if (StringUtil.isNotEmpty(key) && type.equals(SchedulerTaskType.REST_URL)) {
			if (restful ==  null) restful = new RestfulModel();
			restful.setUrl(key);
			restful.setMedia(ContentType.valueOf(task.getMedia()));
			restful.setMethod(HttpMethod.valueOf(task.getMethod()));
			restful.setParams(JSONObject.parseObject(task.getParams()));
		}
	}

	@Override
	public SchedulerTaskCallback getSchedulerCallback() {
		switch (type) {
			case REST_URL: return new UrlSchedulerTaskCallback(restful);
			case DYNAMIC_TASK: {
				SchedulerTaskCallback callback = ReflectUtil.getInstance(taskClass, SchedulerTaskCallback.class);
				if (callback != null) callback.putProperty("taskId", taskId);
				return callback;
			}
			default: return StringUtil.isEmpty(key) ? DEFAULT_CALL : ReflectUtil.getInstance(key, SchedulerTaskCallback.class);
		}
	}
	
	@Override
	public CronTrigger getSchedulerTrigger() {
		return StringUtil.isEmpty(cron) ? new CronTrigger(DEFAULT_CRON) : new CronTrigger(cron);
	}
	
	@Override
	public String getSchedulerFutureKey() {
		return StringUtil.isEmpty(key) ? DEFAULT_KEY : key;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public SchedulerTaskType getType() {
		return type;
	}

	public void setType(String type) {
		this.type = SchedulerTaskType.valueOf(type);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getTaskClass() {
		return taskClass;
	}

	public void setTaskClass(String taskClass) {
		this.taskClass = taskClass;
	}

	public RestfulModel getRestful() {
		return restful;
	}

	public void setRestful(RestfulModel restful) {
		this.restful = restful;
	}
}
