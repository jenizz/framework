package com.arthur.framework.scheduler.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.scheduler.entity.SchedulerTask;
import com.arthur.framework.scheduler.model.SchedulerModel;
import com.arthur.framework.scheduler.repository.SchedulerRepository;
import com.arthur.framework.scheduler.support.SchedulerTaskCallback;

@Service
public class SchedulerService implements ISchedulerService {
	
	private static final Logger log = LoggerFactory.getLogger(SchedulerService.class);

	@Autowired
	private SchedulerRepository dao;
	
	@Autowired
	private ThreadPoolTaskScheduler scheduler;
	
	/**
	 * @see com.arthur.framework.scheduler.service.ISchedulerService#synchro()
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午10:36:27
	 */
	@Override
	public ISchedulerService synchro() throws Exception {
		// 从数据库加载任务列表
		List<SchedulerTask> result = dao.findAll((example) -> {
			example.setStatus(StatusType.ACTIVITY.value());
			return Example.of(example);
		});
		// 添加任务到内存
		result.parallelStream().forEach((task) -> addTask(task));
		return this;
	}
	
	@Override
	public void start() {
		tasks.forEach((key, task) -> addFuture(task));
	}
	
	@Override
	public void pause(final String id) throws Exception {
		// 暂停已激活的任务
		if (ALL.equals(id)) {
			tasks.forEach((key, task) -> pauseFuture(task));
		} else {
			pauseFuture(getSchedulerTask(id));
		}
	}
	
	@Override
	public void stoped(final String id) throws Exception {
		List<String> keys = new ArrayList<>();
		// 停止已激活的任务
		if (ALL.equals(id)) {
			tasks.forEach((key, task) -> {
				SchedulerTask temp = getStopedSchedulerTask(task);
				if (temp != null) keys.add(temp.getId());
			});
		} else {
			SchedulerTask temp = getStopedSchedulerTask(getSchedulerTask(id));
			if (temp != null) keys.add(temp.getId());
		}
		// 从‘tasks’中，删除任务映射
		keys.parallelStream().forEach((key) -> tasks.remove(key));
	}
	
	@Override
	public void resume(final String id) throws Exception {
		// 恢复已暂停的任务
		if (ALL.equals(id)) {
			tasks.forEach((key, task) -> addFuture(task));
		} else {
			addFuture(getSchedulerTask(id));
		}
	}
	
	@Override
	public void startup(final String id) throws Exception {
		// 启动已停止的任务
		if (ALL.equals(id)) {
			startupFuture(null);
		} else {
			startupFuture(getSchedulerTask(id));
		}
	}
	
	@Override
	public void delete(final String id) throws Exception {
		// 删除计划任务
		if (ALL.equals(id)) {
			deleteFuture(null);
		} else {
			deleteFuture(getSchedulerTask(id));
		}
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public SchedulerTask addSchedulerTask(final SchedulerModel model) throws Exception {
		SchedulerTask task = model.toEntity(new SchedulerTask(model)).save();
		this.addTask(task).addFuture(task);
		return task;
	}
	
	@Override
	public SchedulerTask getSchedulerTask(final String id) throws Exception {
		return dao.findOne((example) -> {
			example.setId(id);
			example.setStatus(null);
			return Example.of(example);
		});
	}
	
	
	/* 添加任务-详细 */
	private SchedulerService addTask(final SchedulerTask task) {
		if (tasks == null) tasks = new HashMap<>();
		if (!tasks.containsKey(task.getId())) {
			tasks.put(task.getId(), task);
		}
		return this;
	}
	
	/* 添加并启动计划任务 */
	private SchedulerService addFuture(final SchedulerTask task) {
		if (futures == null) futures = new HashMap<>();
		SchedulerModel model = new SchedulerModel(task).toModel(task);
		
		SchedulerTaskCallback callback = model.getSchedulerCallback();
		if (callback != null) {
			CronTrigger trigger = model.getSchedulerTrigger();
			String id = task.getId();

			if (!futures.containsKey(id)) {
				ScheduledFuture<?> future = scheduler.schedule(callback, trigger);
				futures.put(id, future);
			}
		}
		return this;
	}
	
	/* 暂停并从内存中移除计划任务 */
	private void pauseFuture(final SchedulerTask task) {
		ScheduledFuture<?> future = futures.get(task.getId());
		future.cancel(true);
		futures.remove(task.getId());
	}
	
	/* 删除计划任务 */
	private void deleteFuture(final SchedulerTask task) {
		try{
			if (task == null) {
				dao.deleteAll();
				tasks.clear();
				pause(ALL);
			} else {
				dao.delete(task);
				pauseFuture(task);
				tasks.remove(task.getId());
			}
		} catch (Exception e) {
			log.error("删除计划任务失败: {}", e);
		}
	}
	
	/* 启动计划任务 */
	private void startupFuture(final SchedulerTask task) {
		try {
			if (task == null) {
				dao.updateStatus(StatusType.ACTIVITY.value());
			} else {
				dao.updateStatusById(StatusType.ACTIVITY.value(), task.getId());
			}
			this.synchro().start();
		} catch (Exception e) {
			log.error("启动计划任务失败: {}", e);
		}
	}
	
	/* 停止并从内存中移除计划任务 */
	private SchedulerTask getStopedSchedulerTask(final SchedulerTask task) {
		SchedulerTask temp = task;
		try {
			task.setStatus(StatusType.DELETE.value());
			dao.saveAndFlush(task);
		} catch (Exception e) {
			return null;
		}
		return temp;
	}

	private Map<String, ScheduledFuture<?>> futures;
	
	private Map<String, SchedulerTask> tasks;
	
	@Override
	public ThreadPoolTaskScheduler getScheduler() {
		return scheduler;
	}
	
	@Override
	public Map<String, SchedulerTask> getSchedulerTasks() {
		return tasks;
	}

	@Override
	public Map<String, ScheduledFuture<?>> getSchedulerFutures() {
		return futures;
	}

	@Override
	public ScheduledFuture<?> getSchedulerFuture(final String id) {
		return futures.get(id);
	}
}
