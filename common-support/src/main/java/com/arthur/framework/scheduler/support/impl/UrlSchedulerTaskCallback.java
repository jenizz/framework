package com.arthur.framework.scheduler.support.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arthur.framework.restclient.model.RestfulModel;
import com.arthur.framework.scheduler.support.SchedulerTaskCallbackAdapter;
import com.arthur.framework.util.DateUtil;

public class UrlSchedulerTaskCallback extends SchedulerTaskCallbackAdapter {

	private static final Logger log = LoggerFactory.getLogger(UrlSchedulerTaskCallback.class);

	@Override
	public void run() {
		log.info("UrlSchedulerTaskCallback is invoked. {}", DateUtil.currentime());
	}

	public UrlSchedulerTaskCallback() {

	}
	
	public UrlSchedulerTaskCallback(RestfulModel restful) {
		this.restful = restful;
	}
	
	private RestfulModel restful;

	public RestfulModel getRestful() {
		return restful;
	}

	public void setRestful(RestfulModel restful) {
		this.restful = restful;
	}

}
