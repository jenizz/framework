package com.arthur.framework.scheduler.support;

import org.springframework.scheduling.support.CronTrigger;

import com.arthur.framework.scheduler.support.impl.DefaultSchedulerTaskCallback;

public interface SchedulerTaskAble {
	
	public static final String DEFAULT_CRON = "0 0 0 * * ? "; // 每日凌晨0点执行
	
	public static final String DEFAULT_KEY = DefaultSchedulerTaskCallback.class.getName();
	
	public static final SchedulerTaskCallback DEFAULT_CALL = new DefaultSchedulerTaskCallback();

	/**
	 * 获取计划任务回调函数.
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午10:49:42
	 */
	SchedulerTaskCallback getSchedulerCallback();

	/**
	 * 获取计划任务触发条件.
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午10:50:08
	 */
	CronTrigger getSchedulerTrigger();

	/**
	 * 获取计划任务key.
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午11:26:12
	 */
	String getSchedulerFutureKey();

}
