package com.arthur.framework.scheduler.controller;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.arthur.framework.controller.AbstractController;
import com.arthur.framework.scheduler.entity.SchedulerTask;
import com.arthur.framework.scheduler.model.SchedulerModel;
import com.arthur.framework.scheduler.service.ISchedulerService;
import com.arthur.framework.support.Response;

import io.swagger.annotations.ApiOperation;

@RestController @RequestMapping({ "/scheduler" })
public class SchedulerController extends AbstractController<SchedulerTask> {

	private static final Logger log = LoggerFactory.getLogger(SchedulerController.class);

	@Autowired
	private ISchedulerService service;
	
	@Override
	@PostConstruct
	public void init() {
		try {
			// 调用父类的初始化
			super.init();
			// 同步计划任务
			service.synchro().start();
		} catch (Exception e) {
			log.error("同步并启动计划任务失败:{}", e.getMessage());
		}
	}
	
	@ApiOperation("添加计划任务")
	@RequestMapping(path = "/task/add", method = RequestMethod.POST)
	public Response addTask(@RequestBody final SchedulerModel model) {
		try {
			return new Response(HttpStatus.OK, "计划任务已添加", service.addSchedulerTask(model));
		} catch (Exception e) {
			log.error("添加计划任务失败:{}", e.getMessage());
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "添加计划任务失败", e.getMessage());
		}
	}
	
	@ApiOperation("暂停指定计划任务 id为‘all’时暂停所有任务")
	@RequestMapping(path = "/task/pause/{id}", method = RequestMethod.PUT)
	public Response pause(@PathVariable final String id) {
		try {
			service.pause(id);
			return new Response(HttpStatus.OK, "计划任务已暂停");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("暂停计划任务失败:{}", e.getMessage());
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "暂停计划任务失败", e.getMessage());
		}
	}
	
	@ApiOperation("停止指定计划任务 id为‘all’时停止所有任务")
	@RequestMapping(path = "/task/stoped/{id}", method = RequestMethod.PUT)
	public Response stoped(@PathVariable final String id) {
		try {
			service.stoped(id);
			return new Response(HttpStatus.OK, "计划任务已停止");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("停止计划任务失败:{}", e.getMessage());
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "停止计划任务失败", e.getMessage());
		}
	}
	
	@ApiOperation("恢复指定计划任务 id为‘all’时恢复所有任务")
	@RequestMapping(path = "/task/resume/{id}", method = RequestMethod.PUT)
	public Response resume(@PathVariable final String id) {
		try {
			service.resume(id);
			return new Response(HttpStatus.OK, "计划任务已恢复");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("恢复计划任务失败:{}", e.getMessage());
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "恢复计划任务失败", e.getMessage());
		}
	}
	
	@ApiOperation("启动指定计划任务 id为‘all’时启动所有任务")
	@RequestMapping(path = "/task/startup/{id}", method = RequestMethod.PUT)
	public Response startup(@PathVariable final String id) {
		try {
			service.startup(id);
			return new Response(HttpStatus.OK, "计划任务已启动");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("启动计划任务失败:{}", e.getMessage());
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "启动计划任务失败", e.getMessage());
		}
	}
	
	@ApiOperation("删除指定计划任务 id为‘all’时删除所有任务")
	@RequestMapping(path = "/task/delete/{id}", method = RequestMethod.DELETE)
	public Response delete(@PathVariable final String id) {
		try {
			service.delete(id);
			return new Response(HttpStatus.OK, "计划任务已删除");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("删除计划任务失败:{}", e.getMessage());
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "删除计划任务失败", e.getMessage());
		}
	}
}
