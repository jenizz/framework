package com.arthur.framework.scheduler.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.orm.service.repository.CommonRepositoryAware;
import com.arthur.framework.scheduler.entity.SchedulerTask;

public interface SchedulerRepository extends CommonRepositoryAware<SchedulerTask, String> {

	@Modifying
	@Transactional
	@Query("update SchedulerTask set status = :status where status = 0")
	void updateStatus(@Param("status") final int status) throws Exception;

	@Modifying
	@Transactional
	@Query("update SchedulerTask set status = :status where id = :id ")
	void updateStatusById(@Param("status") final int status, @Param("id") final String id);

}
