package com.arthur.framework.scheduler.support;

import java.util.Map;

public interface SchedulerTaskCallback extends Runnable {

	void setProperties(final Map<String, Object> property);
	
	void putProperty(final String key, final Object value);
	
	Map<String, Object> getProperties();

	Object getProperty(final String key);
}
