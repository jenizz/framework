package com.arthur.framework.scheduler.service;

import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import com.arthur.framework.scheduler.entity.SchedulerTask;
import com.arthur.framework.scheduler.model.SchedulerModel;

public interface ISchedulerService {
	
	static final String ALL = "all";
	
	/**
	 * 同步数据库中的计划任务列表.
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午10:36:41
	 */
	ISchedulerService synchro() throws Exception;

	/**
	 * 启动计划任务.
	 * @param scheduler
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午10:41:10
	 */
	void start() throws Exception;
	
	/**
	 * 暂停计划任务.
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午5:20:58
	 */
	void pause(final String id) throws Exception;
	
	/**
	 * 停止计划任务.
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午5:20:58
	 */
	void stoped(final String id) throws Exception;
	
	/**
	 * 恢复计划任务.
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午5:20:58
	 */
	void resume(final String id) throws Exception;
	
	/**
	 * 启动计划任务.
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午5:20:58
	 */
	void startup(final String id) throws Exception;
	
	/**
	 * 删除计划任务.
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午5:20:58
	 */
	void delete(final String id) throws Exception;
	
	/**
	 * 添加计划任务.
	 * @param model
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午12:00:40
	 */
	SchedulerTask addSchedulerTask(final SchedulerModel model) throws Exception;
	
	/**
	 * 获取计划任务.
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午2:26:49
	 */
	SchedulerTask getSchedulerTask(final String id) throws Exception;
	
	/**
	 * 获取计划任务列表.
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午10:37:01
	 */
	Map<String, SchedulerTask> getSchedulerTasks() throws Exception;

	/**
	 * 获取计划任务管理池.
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午10:43:36
	 */
	ThreadPoolTaskScheduler getScheduler();

	/**
	 * 获取计划任务回调.
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午11:02:53
	 */
	Map<String, ScheduledFuture<?>> getSchedulerFutures();

	/**
	 * 获取计划任务回调.
	 * @param key
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午11:04:30
	 */
	ScheduledFuture<?> getSchedulerFuture(final String key);
}
