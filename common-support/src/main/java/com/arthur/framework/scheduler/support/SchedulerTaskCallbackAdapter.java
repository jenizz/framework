package com.arthur.framework.scheduler.support;

import java.util.HashMap;
import java.util.Map;

public abstract class SchedulerTaskCallbackAdapter implements SchedulerTaskCallback {

	private Map<String, Object> properties;
	
	@Override
	public void run() {

	}

	@Override
	public void setProperties(Map<String, Object> properties) {
		this.properties = properties;
	}

	@Override
	public void putProperty(final String key, final Object value) {
		if (properties == null) properties = new HashMap<>();
		properties.put(key, value);
	}
	
	@Override
	public Object getProperty(final String key) {
		return properties == null ? null : properties.get(key);
	}

	@Override
	public Map<String, Object> getProperties() {
		return properties;
	}

}
