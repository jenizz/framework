package com.arthur.framework.restclient.support.type;

import org.springframework.http.MediaType;

public enum ContentType {

	APPLICATION_JSON, APPLICATION_FORM_URLENCODED;
	
	public MediaType mediaType() {
		return MediaType.valueOf(name());
	}
}
