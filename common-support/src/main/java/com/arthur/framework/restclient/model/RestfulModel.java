package com.arthur.framework.restclient.model;

import java.io.Serializable;

import org.springframework.http.HttpMethod;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.restclient.support.type.ContentType;

public class RestfulModel implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -5781457550015472076L;

	// RestUrl
	private String url;
	
	// Parameters
	private JSONObject params;
	
	// ContentType
	private ContentType media;
	
	// RequestMethod
	private HttpMethod method;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public JSONObject getParams() {
		return params;
	}

	public void setParams(JSONObject params) {
		this.params = params;
	}

	public ContentType getMedia() {
		return media;
	}

	public void setMedia(ContentType media) {
		this.media = media;
	}

	public HttpMethod getMethod() {
		return method;
	}

	public void setMethod(HttpMethod method) {
		this.method = method;
	}
	
}
