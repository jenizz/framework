package com.arthur.framework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
@EnableAsync					// 开启异步调用
@EnableScheduling				// 开启线程池
@PropertySource("classpath:configurations.properties")
public class SupportConfigration {

	@Autowired
	private Environment env;
	
	/**
	 * 开启线程池.
	 * @since 2017年3月11日 上午12:13:52
	 * @author chanlong(陈龙)
	 */
	@Bean
	public ThreadPoolTaskExecutor createThreadPoolTaskExecutor() {
		RelaxedPropertyResolver resolver = new RelaxedPropertyResolver(env, "config.thread.");
		
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setMaxPoolSize(resolver.getProperty("max-pool-size", Integer.class));
        taskExecutor.setCorePoolSize(resolver.getProperty("core-pool-size", Integer.class));
        taskExecutor.setQueueCapacity(resolver.getProperty("queue-capacity", Integer.class));
        
        return taskExecutor;
	}
	
	/**
	 * 开启定时任务.
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 下午1:27:01
	 */
	@Bean  
    public ThreadPoolTaskScheduler createThreadPoolTaskScheduler() { 
		RelaxedPropertyResolver resolver = new RelaxedPropertyResolver(env, "config.scheduler.");
        ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();  
        scheduler.setThreadNamePrefix(resolver.getProperty("thread-prefix", String.class));  
        scheduler.setPoolSize(resolver.getProperty("max-pool-size", Integer.class));  
        return scheduler; 
    }
}
