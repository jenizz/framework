package com.arthur.framework.logger.service;

import java.util.List;
import java.util.Map;

import com.arthur.framework.logger.entity.OperateLogger;
import com.arthur.framework.orm.service.bo.CommonBoAware;

/**
 * <p>描述: 操作日志服务 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月25日 上午10:53:18
 * @version 1.0.2017
 */
public interface IOperateLoggerService extends CommonBoAware<OperateLogger> {

	/**
	 * 获取操作日志列表.
	 * @param example 实体对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月25日 上午10:54:17
	 */
	List<OperateLogger> findAll(final OperateLogger example) throws Exception;

	/**
	 * 操作日志统计.
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月25日 下午2:26:04
	 */
	Map<String, Object> count() throws Exception;
	
}
