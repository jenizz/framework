package com.arthur.framework.logger.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

/**
 * <p>描述: 记录操作日志 </p>
 * <p>Company: Professional</p>
 * <p>日志模版内置变量：</p>
 * <ul>
 * 	<li>${currentUser}：当前登录用户</li>
 * 	<li>${currentDate}：当前操作时间</li>
 * 	<li>${logname}：日志名称</li>
 * 	<li>${userip}：用户的IP</li>
 * </ul>
 * @author chanlong(陈龙)
 * @date 2017年5月23日 下午4:22:52
 * @version 1.0.2017
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Loggerable {
	
	/**
	 * 日志名称.
	 * @author chanlong(陈龙)
	 * @date 2017年5月25日 下午4:14:24
	 */
	String name();


	/**
	 * 日志处理器.
	 * @author chanlong(陈龙)
	 * @date 2017年7月26日 下午12:42:28
	 */
	String target() default "com.arthur.framework.logger.entity.OperateLogger";
	
	/**
	 * 日志模版.
	 * 用户【${currentUser}】, 于【${currentDate}】, 执行【${logname}】操作, 请求ip【${userip}】, 请求参数【[{"参数":"参数值"}]】
	 * @author chanlong(陈龙)
	 * @date 2017年7月18日 下午5:02:59
	 */
	String template() default "用户【${currentUser}】, 于【${currentDate}】,执行【${logname}】操作, 请求IP【${userip}】，请求参数【...】";
	
	/**
	 * 返回映射.
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 上午11:10:33
	 */
	LoggerReturn returnmap() default @LoggerReturn(target = "");
	
	@Target({ ElementType.ANNOTATION_TYPE })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface LoggerReturn {
		String target();
		String[] mapping() default {};
	}
	
	/**
	 * 日志参数配置
	 * <p>描述: 简单类型 </p>
	 * <p>Company: Professional</p>
	 * @author chanlong(陈龙)
	 * @date 2017年7月18日 下午5:30:06
	 * @version 1.0.2017
	 */
	@Target({ ElementType.FIELD, ElementType.PARAMETER })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface LoggerAttribute {
		/**
		 * Alias for {@link #name}.
		 * @author chanlong(陈龙)
		 * @date 2017年7月28日 上午8:48:12
		 */
		@AliasFor("name")
		String value();
		
		/**
		 * 参数值名.
		 * @author chanlong(陈龙)
		 * @date 2017年7月28日 上午8:48:43
		 */
		@AliasFor("value")
		String name() default "";
	}
	
	/**
	 * <p>描述: 日志名称（动态）</p>
	 * <p>Company: Professional</p>
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 上午11:17:03
	 * @version 1.0.2017
	 */
	@Target({ ElementType.PARAMETER })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface LoggerName { }
	
	/**
	 * 日志参数配置
	 * <p>描述: 对象类型 </p>
	 * <p>Company: Professional</p>
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 上午11:17:50
	 * @version 1.0.2017
	 */
	@Target({ ElementType.TYPE, ElementType.PARAMETER })
	@Retention(RetentionPolicy.RUNTIME)
	public @interface LoggerModel {
		/**
		 * 是否递归下级对象类型.
		 * @author chanlong(陈龙)
		 * @date 2017年7月28日 上午8:46:03
		 */
		boolean recursion() default false;
	}
	
}
