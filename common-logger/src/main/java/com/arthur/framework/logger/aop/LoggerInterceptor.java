package com.arthur.framework.logger.aop;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.arthur.framework.aop.AbstractInterceptor;
import com.arthur.framework.aop.CommonInterceptor;
import com.arthur.framework.logger.annotation.Loggerable;
import com.arthur.framework.logger.annotation.Loggerable.LoggerAttribute;
import com.arthur.framework.logger.annotation.Loggerable.LoggerModel;
import com.arthur.framework.logger.annotation.Loggerable.LoggerName;
import com.arthur.framework.util.DebugUtil;
import com.arthur.framework.util.reflect.ReflectUtil;

/**
 * 记录操作日志，统计接口性能，调用频率
 * <p>描述: 日期拦截器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月25日 下午4:29:26
 * @version 1.0.2017
 */
@Aspect @Component
public class LoggerInterceptor extends AbstractInterceptor<LoggerAttribute> {
	
	private static final Logger log = LoggerFactory.getLogger(LoggerInterceptor.class);
	
	@Autowired
	private ThreadPoolTaskExecutor executor;
	
	/**
	 * 拦截@Loggerable标注的Controller类.
	 * @param point
	 * @author chanlong(陈龙)
	 * @date 2017年7月18日 下午2:12:16
	 */
	@Around("@annotation(com.arthur.framework.logger.annotation.Loggerable)")
	public void performed(final ProceedingJoinPoint point) {
		DebugUtil.ready();
		
		try {
			// 被拦截方法执行前，AOP预处理
			preinit(point, LoggerAttribute.class, LoggerName.class.getName(), LoggerModel.class.getName());
			
			// 执行被拦截的方法
			Object returnmap = point.proceed();
			
			// 被拦截的方法执行后，继续处理
			chain(point, returnmap);
		} catch (Throwable e) {
			exception(point, e);
		}
	}
	
	/**
	 * @see com.arthur.framework.aop.AbstractInterceptor#chain(org.aspectj.lang.ProceedingJoinPoint)
	 * @author chanlong(陈龙)
	 * @param returnmap 
	 * @throws Exception 
	 * @date 2017年7月24日 下午2:25:49
	 */
	@Override
	protected void chain(final JoinPoint point, final Object returnmap) throws Exception {
		// 获取方法对象
		Method method = getMethodBySignature(point);
		
		// 执行日志处理
		process(method, returnmap);
	}
	
	/**
	 * @see com.arthur.framework.aop.AbstractInterceptor#process(com.arthur.framework.controller.AbstractController, java.lang.reflect.Method, java.lang.Object)
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 下午6:00:58
	 */
	@Override
	protected void process(final Method _method, final Object _return) throws Exception {
		// 获取方法上的日志注解
		Loggerable logger = _method.getAnnotation(Loggerable.class);
		// 获取日志处理类
		Object target = ReflectUtil.getInstance(logger.target());
		
		// 构造参数映射
		Map<Class<?>, Object> params = new LinkedHashMap<>();
		params.put(CommonInterceptor.class, this);
		params.put(ThreadPoolTaskExecutor.class, executor);
		params.put(Method.class, _method);
		params.put(Object.class, _return);
		
		// 调用处理方法
		ReflectUtil.invoke(target, PERFORM, params);
	}
	
	/**
	 * @see com.arthur.framework.aop.AbstractInterceptor#exception(org.aspectj.lang.ProceedingJoinPoint, java.lang.Throwable)
	 * @author chanlong(陈龙)
	 * @date 2017年7月24日 下午2:26:06
	 */
	@Override
	protected void exception(final JoinPoint point, final Throwable e) {
		log.error("处理AOP拦截事务失败：{}", e);
	}
}
