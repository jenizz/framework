/**
 * <p>Copyright:Copyright(c) 2017</p>
 * <p>Company:Professional</p>
 * <p>Package:com.arthur.framework.service</p>
 * <p>File:OperatoeLoggerService.java</p>
 * <p>类更新历史信息</p>
 * @todo chanlong(陈龙) 创建于 2017年5月25日 上午9:07:12
 */
package com.arthur.framework.logger.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import com.arthur.framework.logger.dao.IOperateLoggerRepository;
import com.arthur.framework.logger.entity.OperateLogger;
import com.arthur.framework.orm.service.bo.CommonBoAdapter;

/**
 * <p>描述: 操作日志服务 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月25日 上午9:07:12
 * @version 1.0.2017
 */
@Service
public class OperateLoggerService extends CommonBoAdapter<OperateLogger> implements IOperateLoggerService {
	
	@Autowired
	private IOperateLoggerRepository dao;
	
	@Override
	public List<OperateLogger> findAll(final OperateLogger example){
		return dao.findAll(Example.of(example));
	}

	@Override
	public Map<String, Object> count() throws Exception {
		return dao.countByResource();
	}
}
