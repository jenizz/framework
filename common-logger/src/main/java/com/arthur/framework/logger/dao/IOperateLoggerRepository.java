package com.arthur.framework.logger.dao;

import java.util.Map;

import org.springframework.data.jpa.repository.Query;

import com.arthur.framework.logger.entity.OperateLogger;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

/**
 * <p>描述: 操作日志数据访问类 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月25日 上午10:51:27
 * @version 1.0.2017
 */
public interface IOperateLoggerRepository extends CommonRepositoryAware<OperateLogger, String> {

	@Query("select o.resource as resource, count(o.resource) as count from OperateLogger o group by o.resource")
	Map<String, Object> countByResource();

}
