package com.arthur.framework.logger.entity;

import java.lang.reflect.Method;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.aop.CommonInterceptor;
import com.arthur.framework.aop.ProcessorIntercepter;
import com.arthur.framework.aop.model.AnnotationModel;
import com.arthur.framework.aop.model.ValueModel;
import com.arthur.framework.auth.model.Session;
import com.arthur.framework.auth.model.UserInfo;
import com.arthur.framework.controller.AbstractController;
import com.arthur.framework.crypt.MD5Crypt;
import com.arthur.framework.logger.annotation.Loggerable;
import com.arthur.framework.logger.annotation.Loggerable.LoggerAttribute;
import com.arthur.framework.logger.annotation.Loggerable.LoggerModel;
import com.arthur.framework.logger.annotation.Loggerable.LoggerName;
import com.arthur.framework.logger.annotation.Loggerable.LoggerReturn;
import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.util.DateUtil;
import com.arthur.framework.util.DateUtil.DatePattern;
import com.arthur.framework.util.DebugUtil;
import com.arthur.framework.util.JsonUtil;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.string.StringUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>描述: 操作日志 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月23日 下午5:29:58
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_sys_logger")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class OperateLogger extends CommonEntityUUID<OperateLogger> implements ProcessorIntercepter<LoggerAttribute> {

	private static final Logger log = LoggerFactory.getLogger(OperateLogger.class);

	@Override
	public void perform(final CommonInterceptor<LoggerAttribute> interceptor, final ThreadPoolTaskExecutor executor, final Method _method, final Object _return) {
		// 获取AOP切入点
		JoinPoint point = interceptor.getJoinPoint();
		
		// 获取AOP切入目标，此处为Controller
		AbstractController<?> controller = (AbstractController<?>) point.getTarget();
		
		// 获取Session对象，用于获取当前登录用户
		HttpSession session = controller.getSession();
		
		// 获取Request对象，用于获取用户请求信息
		HttpServletRequest request = controller.getRequest();
		
		// 获取当前登录用户
		Session current = Session.getCurrentUser(session);
		
		// 获取用户的组织信息
		UserInfo ou = current == null ? new UserInfo() : current.getUserinfo();
		
		// 获取参数值映射模型
		ValueModel valueModel = interceptor.getValueModel();
		
		// 获取参数注解模型
		AnnotationModel<LoggerAttribute> annotationModel = interceptor.getAnnotationModel();
		
		// 设置日志对象
		this.setLogger(request, _method, ou);
		this.setDescription(_method, annotationModel, valueModel, _return);
		
		// 异步执行保存操作
		executor.execute(() -> {
			try {
				this.save();
			} catch (Exception e) {
				log.error("保存操作日志失败：{}", e);
			}
		});
	}
	
	/**
	 * 设置日志描述.
	 * @param method
	 * @param models
	 * @param values
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 下午3:08:03
	 */
	public void setDescription(final Method _method, final AnnotationModel<LoggerAttribute> annotationModel, final ValueModel valueModel, final Object _return) {
		Loggerable logger = _method.getAnnotation(Loggerable.class);
		
		// 获取日志模版
		String logtemp = getLoggerTemplate(logger);
		
		// 设置日志参数
		StringBuilder params = getParameterValues(annotationModel, valueModel);
		
		// 设置日志属性
		String content = String.valueOf(annotationModel.getValue(0));
		setReturn(logger, _return);
		setResource(logger, valueModel);
		setContent(content);
		setContentpwd(MD5Crypt.encrypt(content));
		setDescription(logtemp.replace("${logname}", getResource()).replace("...", params));
	}
	
	/* 获取日志模版 */
	private String getLoggerTemplate(final Loggerable logger){
		String currentDate = DateUtil.format(getTime(), DatePattern.DATETIME.value());
		return logger.template().replace("${currentUser}", getUserName())
				   	  			.replace("${currentDate}", currentDate)
				   	  			.replace("${userip}", getIp());
	}
	
	/* 获取被拦截方法的参数列表 */
	private StringBuilder getParameterValues(final AnnotationModel<LoggerAttribute> annotationModel, final ValueModel valueModel) {
		StringBuilder params = new StringBuilder();
		for (int i = 0, size = annotationModel.size(); i < size; i++) {
			LoggerAttribute attr = annotationModel.getAnnotation(i);
			Object value = annotationModel.getValue(i);
			params.append(attr.value()).append("=").append(value);
			if(i < size - 1) params.append(",");
		}
		
		// 从LoggerModel中获取参数
		if(valueModel.containsKey(LoggerModel.class)){
			if (params.length() > 0) params.append(",");
			params.append(JsonUtil.serialize(valueModel.get(LoggerModel.class)));
		}
		
		return params;
	}
	
	/**
	 * 设置日志操作资源.
	 * @param log
	 * @param values
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 下午3:08:24
	 */
	public void setResource(final Loggerable logger, final ValueModel valueModel) {
		String name = logger.name();
		if (name.startsWith("$")) {
			JSONObject json = JSONObject.parseObject(name.replace("$", ""));
			Object value = json.get(valueModel.get(LoggerName.class));
			setResource(String.valueOf(value));
		} else {
			setResource(logger.name());
		}
	}
	
	/* 设置日志对象 */
	private void setLogger(final HttpServletRequest request, final Method _method, final UserInfo ou){
		this.setIp(request.getRemoteAddr());
		this.setUrl(getUrlOrPath(_method));
		this.setTime(DateUtil.currentime());
		this.setTake(DebugUtil.finish());
		this.setUserId(ou.getUserId());
		this.setUserName(ou.getUserName());
		this.setOrganId(ou.getOrganId());
		this.setOrganName(ou.getOrganName());
	}
	
	/* 设置返回值 */
	private void setReturn(final Loggerable logger, final Object _return){
		LoggerReturn _returnmap = logger.returnmap();
		String _target = _returnmap.target();
		String[] _mapping = _returnmap.mapping();
		if(StringUtil.isNotEmpty(_target)) {
			String str = JSONObject.toJSONString(_return);
			JSONObject json = JSONObject.parseObject(str);
			
			String tmp = json.getString(_target);
			if (tmp.startsWith("{")) {
				JSONObject data = json.getJSONObject(_target);
				for(String name : _mapping){
					ReflectUtil.setProperty(this, name, data.getString(name));
				}
			}
		}
	}
	
	/* 获取接口访问路径 */
	private String getUrlOrPath(final Method method) {
		RequestMapping mapping = method.getAnnotation(RequestMapping.class);
		String _url = "";
		if(mapping != null){
			String[] path = mapping.path();
			String[] url = mapping.value();
			_url = path.length < 1 ? url[0] : path[0];
		}
		return _url;
	}
	
	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -2831645969551505100L;

	// 操作人IP
	@Column(name = "ip_")
	private String ip;
	
	// 接口地址
	@Column(name = "url_")
	private String url;
	
	// 操作时间
	@Column(name = "time_")
	@OrderBy("time desc")
	private String time;
	
	// 耗时
	@Column(name = "take_")
	private Long take;
	
	// 操作用途
	@Column(name = "using_")
	private String using;
	
	// 操作资源
	@Column(name = "resource_")
	private String resource;
	
	// 日志描述
	@Column(name = "description_")
	private String description;
		
	// 操作人ID
	@Column(name = "user_id")
	private String userId;
	
	// 操作人姓名
	@Column(name = "user_name")
	private String userName;
	
	// 操作部门ID
	@Column(name = "organ_id")
	private String organId;
	
	// 操作部门名称
	@Column(name = "organ_name")
	private String organName;
	
	// 文件路径
	@Column(name = "file_path")
	private String filepath;
	
	// 文件密码
	@Column(name = "file_pwd")
	private String filepswd;
	
	// 操作内容
	@Column(name = "content_")
	private String content;
	
	// 操作内容密码
	@Column(name = "content_pwd")
	private String contentpwd;
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getTake() {
		return take;
	}

	public void setTake(Long take) {
		this.take = take;
	}

	public String getUsing() {
		return using;
	}

	public void setUsing(String using) {
		this.using = using;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrganId() {
		return organId;
	}

	public void setOrganId(String organId) {
		this.organId = organId;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getFilepswd() {
		return filepswd;
	}

	public void setFilepswd(String filepswd) {
		this.filepswd = filepswd;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentpwd() {
		return contentpwd;
	}

	public void setContentpwd(String contentpwd) {
		this.contentpwd = contentpwd;
	}
}
