package com.arthur.framework.datasource.support;

import com.arthur.framework.crypt.DESCrypt;
import com.arthur.framework.support.RuntimeProcessor;
import com.arthur.framework.support.RuntimeProcessor.RuntimeCallback;

public final class DataSourceCrypt {

	private static String KEY = "V0FRM1E1dTgxcGc9";
	
	/**
	 * 解密.
	 * @param crypt
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年4月5日 上午11:22:03
	 */
	public static String decrypt(final String crypt) {
		return RuntimeProcessor.run(new RuntimeCallback<String>() {
			@Override
			public String execute() throws Throwable {
				return new String(DESCrypt.decrypt(DESCrypt.decryptBASE64(crypt), KEY));
			}
		});
	}
	
	/**
	 * 加密.
	 * @param text
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年4月5日 上午11:22:14
	 */
	public static String encrypt(final String text) {
		return RuntimeProcessor.run(new RuntimeCallback<String>() {
			@Override
			public String execute() throws Throwable {
				return DESCrypt.encryptBASE64(DESCrypt.encrypt(text.getBytes(), KEY));
			}
		});
	}
	
	/**
	 * 随机生成key.
	 * @param salt
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年4月5日 上午11:22:30
	 */
	public static String key(final String salt) {
		return RuntimeProcessor.run(new RuntimeCallback<String>() {
			@Override
			public String execute() throws Throwable {
				return DESCrypt.encryptBASE64(DESCrypt.initKey(salt).getBytes());
			}
		});
	}
	
	public static void main(String[] args) {
		try {
			System.out.println("key : " + DataSourceCrypt.key("chanlong"));
			System.out.println("url : " + DataSourceCrypt.encrypt("jdbc:mysql://112.124.104.215:3306/miauth?prepStmtCacheSize=512&cachePrepStmts=true&autoReconnect=true&characterEncoding=utf-8&allowMultiQueries=true"));
			System.out.println("user : " + DataSourceCrypt.encrypt("miwawa"));
			System.err.println("pswd : " + DataSourceCrypt.encrypt("TJZ8GNX8Xs52Ejzc"));
			System.err.println("=== "+DataSourceCrypt.decrypt("tME/D0e3T35X7W8yh5sGPn+dutbRRlAhr+VrHyoA430IzdpwRFvW1btu/6K0RnYPPSU/Vm1nS+1LrT9ev/ZD8nIy2dhquyyBfEKjPjxWiWVkq1QU0DaT3ebwm/VsaAgFPyXJ0VY1BO2sxatmZAkNiqCTiSPZEUMABBSHP4uFTppdZjd8v6RoQOfgwuGmKP0leNn+XFPS1io="));
			System.out.println("url : " + DataSourceCrypt.encrypt("jdbc:mysql://192.168.33.190:3306/credit?prepStmtCacheSize=512&cachePrepStmts=true&autoReconnect=true&characterEncoding=utf-8&allowMultiQueries=true"));
			System.out.println("user : " + DataSourceCrypt.encrypt("root"));
			System.err.println("pswd : " + DataSourceCrypt.encrypt("123456"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
