/**
 * 
 * <p><b>Author</b>: long.chan 2017年2月6日 下午12:29:47 </p>
 * <p><b>ClassName</b>: DynamicDataSourceRegister.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.bind.RelaxedDataBinder;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotationMetadata;

import com.arthur.framework.util.string.StringUtil;
import com.mchange.v2.c3p0.ComboPooledDataSource;

/**
 * <p><b>Author</b>: long.chan 2017年2月6日 下午12:29:47 </p>
 * <p><b>ClassName</b>: DynamicDataSourceRegister.java </p> 
 * <p><b>Description</b>: </p> 
 */
public class MultiDataSourceRegister implements ImportBeanDefinitionRegistrar, EnvironmentAware {

	private DataSource defaultDataSource;
	
    private PropertyValues dataSourcePropertyValues;
	
	/**
	 * <p><b>Title</b>: setEnvironment </p> 
	 * <p><b>Author</b>: long.chan 2017年2月6日 下午12:29:47 </p>
	 * <p><b>Description</b>:  </p> 
	 * @param env
	 * @see org.springframework.context.EnvironmentAware#setEnvironment(org.springframework.core.env.Environment)
	 */
	@Override
	public void setEnvironment(final Environment env) {
		initDefaultDataSource(env);
	}
	
	/**
	 * <p><b>Title</b>: registerBeanDefinitions </p> 
	 * <p><b>Author</b>: long.chan 2017年2月6日 下午12:29:47 </p>
	 * <p><b>Description</b>:  </p> 
	 * @param arg0
	 * @param arg1
	 * @see org.springframework.context.annotation.ImportBeanDefinitionRegistrar#registerBeanDefinitions(org.springframework.core.type.AnnotationMetadata, org.springframework.beans.factory.support.BeanDefinitionRegistry)
	 * 
	 */
	@Override
	public void registerBeanDefinitions(final AnnotationMetadata metadata, final BeanDefinitionRegistry registry) {
		Map<Object, Object> targetDataSources = new HashMap<Object, Object>();
		targetDataSources.put("default", defaultDataSource);
		
		// 创建DynamicDataSource
        GenericBeanDefinition definition = new GenericBeanDefinition();
        definition.setBeanClass(MultiDataSource.class);
        definition.setSynthetic(true);
        
        MutablePropertyValues mpv = definition.getPropertyValues();
        mpv.addPropertyValue("defaultTargetDataSource", defaultDataSource);
        mpv.addPropertyValue("targetDataSources", targetDataSources);
        
        registry.registerBeanDefinition("dataSource", definition);
	}
	
	/** 初始化数据源 **/
	private void initDefaultDataSource(final Environment env) {
		RelaxedPropertyResolver resolver = new RelaxedPropertyResolver(env, "multi.datasource.");
		
		defaultDataSource = buildDataSource(resolver);
		
		propertyBinder(defaultDataSource, env);
	}
	
	/** 创建数据源 **/
	@SuppressWarnings("unchecked")
	private DataSource buildDataSource(final RelaxedPropertyResolver resolver) {
		String type = resolver.getProperty("type");
		
		try {
			Class<? extends DataSource> dataSourceType = StringUtil.isEmpty(type) ? ComboPooledDataSource.class : (Class<? extends DataSource>) Class.forName(type);
			
			return DataSourceBuilder.create()
									.type(dataSourceType)
									.url(resolver.getProperty("url"))
									.username(resolver.getProperty("username"))
									.password(resolver.getProperty("password"))
									.driverClassName(resolver.getProperty("driver-class-name"))
									.build();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	/** 绑定扩展属性 **/
	private void propertyBinder(final DataSource dataSource, final Environment env) {
		RelaxedDataBinder binder = new RelaxedDataBinder(dataSource);
		binder.setConversionService(new DefaultConversionService());
		binder.setIgnoreNestedProperties(false);
		binder.setIgnoreInvalidFields(false);
		binder.setIgnoreUnknownFields(true);
		
		if (dataSourcePropertyValues == null) {
			Map<String, Object> property = new RelaxedPropertyResolver(env, "c3p0").getSubProperties(".");
			dataSourcePropertyValues = new MutablePropertyValues(property);
		}
		
		binder.bind(dataSourcePropertyValues);
	}
}
