package com.arthur.framework.datasource;

import com.arthur.framework.util.string.StringUtil;

public class MultiDataSourceHolder {
	private static final ThreadLocal<String> LOCAL = new ThreadLocal<String>();
	
	public static final String DEFAULT = "default"; 

	public static void setDataSource(final String dataSource) {
		LOCAL.set(StringUtil.isEmpty(dataSource) ? DEFAULT : dataSource);
	}

	public static String getDataSource() {
		return LOCAL.get();
	}

	public static void clear() {
		LOCAL.remove();
	}
}
