/**
 * <p>Copyright:Copyright(c) 2017</p>
 * <p>Company:Professional</p>
 * <p>Package:DataSourceCrypt</p>
 * <p>File:Dynamic.java</p>
 * <p>类更新历史信息</p>
 * @todo chanlong(陈龙) 创建于 2017年4月5日 下午1:28:10
 */
package com.arthur.framework.datasource.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>描述: 启用多数据源时，标注service方法的数据源 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年4月5日 下午1:28:10
 * @version 1.0.2017
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSource {
	
	String value() default "default";
}
