package com.arthur.framework.datasource.service;

import java.util.Map;

public interface IDataSourceConfigService {
	
	public Map<String, Object> getConfig();
}
