package com.arthur.framework.datasource.support;

public final class DruidDataSourceConstant {

    public final static String PROP_URL = "url";
	public final static String PROP_NAME = "name";
	public final static String PROP_USERNAME = "username";
	public final static String PROP_PASSWORD = "password";
	public final static String PROP_DRIVERCLASSNAME = "driverClassName";
    
	public final static String PROP_MINIDLE = "minIdle";
	public final static String PROP_MAXIDLE = "maxIdle";
	public final static String PROP_MAXWAIT = "maxWait";
    public final static String PROP_MAXACTIVE = "maxActive";
    public final static String PROP_INITIALSIZE = "initialSize";
    public final static String PROP_TESTONBORROW = "testOnBorrow";
    public final static String PROP_TESTONRETURN = "testOnReturn";
    public final static String PROP_TESTWHILEIDLE = "testWhileIdle";
    public final static String PROP_VALIDATIONQUERY = "validationQuery";
    public final static String PROP_PHY_TIMEOUT_MILLIS = "phyTimeoutMillis";
    public final static String PROP_INITCONNECTIONSQLS = "initConnectionSqls";
    public final static String PROP_NUMTESTSPEREVICTIONRUN = "numTestsPerEvictionRun";
    public final static String PROP_VALIDATIONQUERY_TIMEOUT = "validationQueryTimeout";
    public final static String PROP_MINEVICTABLEIDLETIMEMILLIS = "minEvictableIdleTimeMillis";
    public final static String PROP_TIMEBETWEENEVICTIONRUNSMILLIS = "timeBetweenEvictionRunsMillis";
    public final static String PROP_ACCESSTOUNDERLYINGCONNECTIONALLOWED = "accessToUnderlyingConnectionAllowed";

    public final static String PROP_FILTERS = "filters";
    public final static String PROP_LOGABANDONED = "logAbandoned";
    public final static String PROP_REMOVEABANDONED = "removeAbandoned";
    public final static String PROP_EXCEPTION_SORTER = "exceptionSorter";
    public final static String PROP_CONNECTIONPROPERTIES = "connectionProperties";
    public final static String PROP_REMOVEABANDONEDTIMEOUT = "removeAbandonedTimeout";
    public final static String PROP_POOLPREPAREDSTATEMENTS = "poolPreparedStatements";
    public final static String PROP_MAXOPENPREPAREDSTATEMENTS = "maxOpenPreparedStatements";
    public final static String PROP_EXCEPTION_SORTER_CLASS_NAME = "exception-sorter-class-name";
}
