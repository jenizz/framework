package com.arthur.framework.config;

import java.util.Map;

import com.arthur.framework.datasource.service.IDataSourceConfigService;

public class DataSourceMultiProperties {

	private String[] names;
	
	private IDataSourceConfigService service;
	
	private Map<String, DataSourceBasicProperties> pools;

	public String[] getNames() {
		return names;
	}

	public void setNames(String[] names) {
		this.names = names;
	}

	public IDataSourceConfigService getService() {
		return service;
	}

	public void setService(IDataSourceConfigService service) {
		this.service = service;
	}

	public Map<String, DataSourceBasicProperties> getPools() {
		return pools;
	}

	public void setPools(Map<String, DataSourceBasicProperties> pools) {
		this.pools = pools;
	}
	
}
