package com.arthur.framework.config;

import java.util.Map;

import org.springframework.boot.web.servlet.ServletRegistrationBean;

import com.arthur.framework.datasource.support.DataSourceCrypt;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;

public class DataSourceStateProperties {

	private Boolean crypt;
	
	private String deny;
	
	private String allow;
	
	private String resetEnable;
	
	private String loginUsername;
	
	private String loginPassword;
	
	public void registerInitParameter(final ServletRegistrationBean bean){
		Map<String, String> map = ReflectUtil.getProperties(this);
		bean.setInitParameters(map);
	}
	
	@PropertyIgnore
	public Boolean getCrypt() {
		return crypt;
	}

	public void setCrypt(Boolean crypt) {
		this.crypt = crypt;
	}

	public String getDeny() {
		return deny;
	}

	public void setDeny(String deny) {
		this.deny = deny;
	}

	public String getAllow() {
		return allow;
	}

	public void setAllow(String allow) {
		this.allow = allow;
	}

	public String getResetEnable() {
		return resetEnable;
	}

	public void setResetEnable(String resetEnable) {
		this.resetEnable = resetEnable;
	}

	public String getLoginUsername() {
		return crypt != null && crypt ? DataSourceCrypt.decrypt(loginUsername) : loginUsername;
	}

	public void setLoginUsername(String loginUsername) {
		this.loginUsername = loginUsername;
	}

	public String getLoginPassword() {
		return crypt != null && crypt ? DataSourceCrypt.decrypt(loginPassword) : loginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		this.loginPassword = loginPassword;
	}
}
