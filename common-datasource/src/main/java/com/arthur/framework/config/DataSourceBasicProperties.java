package com.arthur.framework.config;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.arthur.framework.datasource.support.DataSourceCrypt;

@ConfigurationProperties(prefix = "spring.datasource")
public class DataSourceBasicProperties {
	
	private Class<? extends DataSource> type;
	
	private String driverClassName;
	
	private Boolean crypt;
	
	private String url;
	
	private String username;
	
	private String password;

	private DataSourceDruidProperties druid;
	
	private DataSourceMultiProperties multi;
	
	private DataSourceStateProperties state;
	
	public DataSource buildDataSource() {
		DataSourceBuilder builder = DataSourceBuilder.create()
				 									 .driverClassName(getDriverClassName())
				 									 .url(getUrl())
				 									 .username(getUsername())
				 									 .password(getPassword());
		if(getType() != null){
			return builder.type(getType()).build();
		}else{
			return builder.build();
		}
	}
	
	public Class<? extends DataSource> getType() {
		return type;
	}

	public void setType(Class<? extends DataSource> type) {
		this.type = type;
	}

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public Boolean getCrypt() {
		return crypt;
	}

	public void setCrypt(Boolean crypt) {
		this.crypt = crypt;
	}

	public String getUrl() {
		return crypt != null && crypt ? DataSourceCrypt.decrypt(url) : url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return crypt != null && crypt ? DataSourceCrypt.decrypt(username) : username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return crypt != null && crypt ? DataSourceCrypt.decrypt(password) : password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public DataSourceDruidProperties getDruid() {
		return druid;
	}

	public void setDruid(DataSourceDruidProperties druid) {
		this.druid = druid;
	}

	public DataSourceMultiProperties getMulti() {
		return multi;
	}

	public void setMulti(DataSourceMultiProperties multi) {
		this.multi = multi;
	}

	public DataSourceStateProperties getState() {
		return state;
	}

	public void setState(DataSourceStateProperties state) {
		this.state = state;
	}

}
