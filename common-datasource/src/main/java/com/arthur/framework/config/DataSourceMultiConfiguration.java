package com.arthur.framework.config;

import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import com.arthur.framework.datasource.MultiDataSource;

@Configuration
@EnableTransactionManagement
@EnableConfigurationProperties({ DataSourceBasicProperties.class })
public class DataSourceMultiConfiguration {
	
	@Autowired
	private DataSourceBasicProperties basic;
	
	private DataSourceDruidProperties druid;
	
	/**
	 * 注入默认数据源.
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年4月5日 下午1:22:23
	 */
	public DataSource dataSource() {
		try {
			druid = basic.getDruid();
			
			DruidDataSource dataSource = (DruidDataSource) basic.buildDataSource();
			
			if (druid != null) {
				DruidDataSourceFactory.config(dataSource, druid.getPropertyValues());
			}
			
			return dataSource;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	@Primary
	@Bean
	public MultiDataSource multiDataSource(){
		try {
			MultiDataSource dataSource = new MultiDataSource();
			dataSource.addDataSource("default", dataSource());
			registerMultiDataSource(dataSource);
			return dataSource;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 	注册Druid监控Servlet.
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年4月5日 上午9:20:48
	 */
	@Bean
	public ServletRegistrationBean druidStatViewServle() {
		ServletRegistrationBean regist = new ServletRegistrationBean(new StatViewServlet(), "/druid/*");
		
		if (basic.getState() != null) basic.getState().registerInitParameter(regist);
		
		return regist;
	}
	
	/**
	 * 注册Druid监控过滤器.
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年4月5日 上午9:21:31
	 */
	@Bean
	public FilterRegistrationBean druidStatFilter() {
		FilterRegistrationBean regist = new FilterRegistrationBean(new WebStatFilter());

		// 添加过滤规则.
		regist.addUrlPatterns("/*");

		// 添加不需要忽略的格式信息.
		regist.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		
		return regist;
	}
	
	/*
	 * 注册多数据源
	 */
	private void registerMultiDataSource(final MultiDataSource dataSource) throws SQLException {

		// 注册多数据源
		DataSourceMultiProperties multi = basic.getMulti();
		if(multi != null){
			String[] names = multi.getNames();
			for (String key : names) {
				Map<String, DataSourceBasicProperties> pools = multi.getPools();
				if(pools != null){
					DataSourceBasicProperties pool = pools.get(key);
					DruidDataSource ds = (DruidDataSource) pool.buildDataSource();
					if (druid != null) {
						DruidDataSourceFactory.config(ds, druid.getPropertyValues());
					}
					dataSource.addDataSource(key, ds);
				}
			}
		}
	}

}
