package com.arthur.framework.config;

import java.util.List;
import java.util.Map;

import com.alibaba.druid.filter.Filter;
import com.arthur.framework.util.reflect.ReflectUtil;

public class DataSourceDruidProperties {
	
	/**
	 * 是否初始化线程
	 */
	private String init;
	
    /**
     * 配置这个属性的意义在于，如果存在多个数据源，监控的时候可以通过名字来区分开来。
     * 如果没有配置，将会生成一个名字，格式是："DataSource-" + System.identityHashCode(this)。
     */
    private String name;
    
	/**
	 * 初始化时建立物理连接的个数。 
	 * 初始化发生在显示调用init方法，或者第一次getConnection时
	 */
	private String initialSize;
	
	/**
	 * 最大连接池数量
	 */
	private String maxActive;
	
	/**
	 * 已经不再使用，配置了也没效果
	 */
	private String maxIdle;
	
	/**
	 * 最小连接池数量
	 */
	private String minIdle;
	
	/**
	 * 获取连接时最大等待时间，单位毫秒。 
	 * 配置了maxWait之后，缺省启用公平锁，并发效率会有所下降， 
	 * 如果需要可以通过配置useUnfairLock属性为true使用非公平锁。
	 */
	private String maxWait;
	
	/**
	 * 是否启用非公平锁。
	 */
	private String useUnfairLock;
	
	/**
	 * 是否缓存preparedStatement，也就是PSCache。 
	 * PSCache对支持游标的数据库性能提升巨大，比如说oracle。在mysql下建议关闭。
	 * 默认为：false
	 */
	private String poolPreparedStatements;
	
	/** 
	 * 要启用PSCache，必须配置大于0，当大于0时，poolPreparedStatements自动触发修改为true。 
	 * 在Druid中，不会存在Oracle下PSCache占用内存过多的问题，可以把这个数值配置大一些，比如说100
	 */
	private String maxOpenPreparedStatements;
	
	/**
	 * 连接保持空闲而不被驱逐的最长时间，单位毫秒
	 */
	private String minEvictableIdleTimeMillis;
	
	/**
	 * 单位：秒，检测连接是否有效的超时时间。
	 * 底层调用jdbc Statement对象的void setQueryTimeout(int seconds)方法
	 */
	private String validationQueryTimeout;
	
	/**
	 * 用来检测连接是否有效的sql，要求是一个查询语句。 
	 * 如果validationQuery为null，testOnBorrow、testOnReturn、testWhileIdle都不会其作用。
	 */
	private String validationQuery;
	
	/**
	 * 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。 
	 * 默认为：true，建议设置为false
	 */
	private String testOnBorrow;
	
	/**
	 * 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。 
	 * 默认为：false
	 */
	private String testOnReturn;
	
	/**
	 * 建议配置为true，不影响性能，并且保证安全性。申请连接的时候检测， 
	 * 如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效。
	 * 默认为：false
	 */
	private String testWhileIdle;
	
	/**
	 * 有两个含义：
	 * 1) Destroy线程会检测连接的间隔时间 
	 * 2) testWhileIdle的判断依据，详细看testWhileIdle属性的说明
	 */
	private String timeBetweenEvictionRunsMillis;
	
	/**
	 * 不再使用，一个DruidDataSource只支持一个EvictionRun
	 */
	private String numTestsPerEvictionRun;
	
	/**
	 * 物理连接初始化的时候执行的sql
	 */
	private String initConnectionSqls;
	
	/**
	 * 当数据库抛出一些不可恢复的异常时，抛弃连接
	 */
	private String exceptionSorter;
	
	/**
	 * 标记当Statement或连接被泄露时是否打印程序的stack traces日志。
	 */
	private String logAbandoned;
	
	/**
	 * 标记是否删除泄露的连接，如果超过了removeAbandonedTimout的限制。
	 */
	private String removeAbandoned;
	
	/**
	 * 超时时间；单位为秒。180秒=3分钟
	 */
	private String removeAbandonedTimeout;
	
	private String phyTimeoutMillis;
	
	/**
	 * 控制PoolGuard是否容许获取底层连接，默认为：false
	 */
	private String accessToUnderlyingConnectionAllowed;
	
	/**
	 * 属性类型是字符串，通过别名的方式配置扩展插件，常用的插件有：
	 * 日志用的filter: log4j 
	 * 监控统计用的filter: stat 
	 * 防御sql注入的filter: wall 
	 * 数据库密码加密的filter: config 
	 * 多个插件之间用‘,’分隔
	 */
	private String filters;
	
	/**
	 * 类型是List<com.alibaba.druid.filter.Filter>， 
	 * 如果同时配置了filters和proxyFilters，是组合关系，并非替换关系
	 */
	private List<Filter> proxyFilters;
	
	/**
	 * 以properties文件中的配置格式设置链接属性
	 */
	private String connectionProperties;

	public Map<String, Object> getPropertyValues() {
		return ReflectUtil.getProperties(this);
	}

	public String getInit() {
		return init;
	}

	public void setInit(String init) {
		this.init = init;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitialSize() {
		return initialSize;
	}

	public void setInitialSize(String initialSize) {
		this.initialSize = initialSize;
	}

	public String getMaxActive() {
		return maxActive;
	}

	public void setMaxActive(String maxActive) {
		this.maxActive = maxActive;
	}

	public String getMaxIdle() {
		return maxIdle;
	}

	public void setMaxIdle(String maxIdle) {
		this.maxIdle = maxIdle;
	}

	public String getMinIdle() {
		return minIdle;
	}

	public void setMinIdle(String minIdle) {
		this.minIdle = minIdle;
	}

	public String getMaxWait() {
		return maxWait;
	}

	public void setMaxWait(String maxWait) {
		this.maxWait = maxWait;
	}

	public String getUseUnfairLock() {
		return useUnfairLock;
	}

	public void setUseUnfairLock(String useUnfairLock) {
		this.useUnfairLock = useUnfairLock;
	}

	public String getPoolPreparedStatements() {
		return poolPreparedStatements;
	}

	public void setPoolPreparedStatements(String poolPreparedStatements) {
		this.poolPreparedStatements = poolPreparedStatements;
	}

	public String getMaxOpenPreparedStatements() {
		return maxOpenPreparedStatements;
	}

	public void setMaxOpenPreparedStatements(String maxOpenPreparedStatements) {
		this.maxOpenPreparedStatements = maxOpenPreparedStatements;
	}

	public String getMinEvictableIdleTimeMillis() {
		return minEvictableIdleTimeMillis;
	}

	public void setMinEvictableIdleTimeMillis(String minEvictableIdleTimeMillis) {
		this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
	}

	public String getValidationQueryTimeout() {
		return validationQueryTimeout;
	}

	public void setValidationQueryTimeout(String validationQueryTimeout) {
		this.validationQueryTimeout = validationQueryTimeout;
	}

	public String getValidationQuery() {
		return validationQuery;
	}

	public void setValidationQuery(String validationQuery) {
		this.validationQuery = validationQuery;
	}

	public String getTestOnBorrow() {
		return testOnBorrow;
	}

	public void setTestOnBorrow(String testOnBorrow) {
		this.testOnBorrow = testOnBorrow;
	}

	public String getTestOnReturn() {
		return testOnReturn;
	}

	public void setTestOnReturn(String testOnReturn) {
		this.testOnReturn = testOnReturn;
	}

	public String getTestWhileIdle() {
		return testWhileIdle;
	}

	public void setTestWhileIdle(String testWhileIdle) {
		this.testWhileIdle = testWhileIdle;
	}

	public String getTimeBetweenEvictionRunsMillis() {
		return timeBetweenEvictionRunsMillis;
	}

	public void setTimeBetweenEvictionRunsMillis(String timeBetweenEvictionRunsMillis) {
		this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
	}

	public String getNumTestsPerEvictionRun() {
		return numTestsPerEvictionRun;
	}

	public void setNumTestsPerEvictionRun(String numTestsPerEvictionRun) {
		this.numTestsPerEvictionRun = numTestsPerEvictionRun;
	}

	public String getInitConnectionSqls() {
		return initConnectionSqls;
	}

	public void setInitConnectionSqls(String initConnectionSqls) {
		this.initConnectionSqls = initConnectionSqls;
	}

	public String getExceptionSorter() {
		return exceptionSorter;
	}

	public void setExceptionSorter(String exceptionSorter) {
		this.exceptionSorter = exceptionSorter;
	}

	public String getLogAbandoned() {
		return logAbandoned;
	}

	public void setLogAbandoned(String logAbandoned) {
		this.logAbandoned = logAbandoned;
	}

	public String getRemoveAbandoned() {
		return removeAbandoned;
	}

	public void setRemoveAbandoned(String removeAbandoned) {
		this.removeAbandoned = removeAbandoned;
	}

	public String getRemoveAbandonedTimeout() {
		return removeAbandonedTimeout;
	}

	public void setRemoveAbandonedTimeout(String removeAbandonedTimeout) {
		this.removeAbandonedTimeout = removeAbandonedTimeout;
	}

	public String getPhyTimeoutMillis() {
		return phyTimeoutMillis;
	}

	public void setPhyTimeoutMillis(String phyTimeoutMillis) {
		this.phyTimeoutMillis = phyTimeoutMillis;
	}

	public String getAccessToUnderlyingConnectionAllowed() {
		return accessToUnderlyingConnectionAllowed;
	}

	public void setAccessToUnderlyingConnectionAllowed(String accessToUnderlyingConnectionAllowed) {
		this.accessToUnderlyingConnectionAllowed = accessToUnderlyingConnectionAllowed;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public List<Filter> getProxyFilters() {
		return proxyFilters;
	}

	public void setProxyFilters(List<Filter> proxyFilters) {
		this.proxyFilters = proxyFilters;
	}

	public String getConnectionProperties() {
		return connectionProperties;
	}

	public void setConnectionProperties(String connectionProperties) {
		this.connectionProperties = connectionProperties;
	}
}
