package com.arthur.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.arthur.framework.orm.service.repository.CommonRepositoryFactoryBean;

@EnableJpaRepositories(basePackages = { 
	"com.arthur.framework.**.dao"
}, repositoryFactoryBeanClass = CommonRepositoryFactoryBean.class)
@SpringBootApplication
public class AuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}
}
