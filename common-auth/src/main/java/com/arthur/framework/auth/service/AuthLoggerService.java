package com.arthur.framework.auth.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.auth.entity.AuthLogger;
import com.arthur.framework.orm.service.bo.CommonBoAdapter;

@Service @Transactional(isolation = Isolation.READ_COMMITTED)
public class AuthLoggerService extends CommonBoAdapter<AuthLogger> implements IAuthLoggerService {

}
