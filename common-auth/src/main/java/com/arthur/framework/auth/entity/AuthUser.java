package com.arthur.framework.auth.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.auth.enums.UserStatusType;
import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p><b>描述</b>: 用户 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月15日 下午5:03:43
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_auth_user")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class AuthUser extends CommonEntityUUID<AuthUser> implements UserDetails {

	private static final long serialVersionUID = -7076601262807387222L;

	// 用户名
	@Column(name = "username_", nullable = false, unique = true)
	private String username;
	
	// 密码
	@Column(name = "password_", nullable = false)
	private String password;
	
	// 公钥
	@Column(name = "public_key")
	private String publicKey;
	
	// 业务主键
	@Column(name = "business_key")
	private String businessKey;
	
	// 盐
	@Column(name = "salt_")
	private String salt;
	
	// 排序号
	@Column(name = "serial_")
	private Integer serial;
	
	// 状态
	@Column(name = "status_")
	private Integer status;
	
	// 权限
	@Transient @PropertyIgnore
	private Set<GrantedAuthority> authorities;
	
	/**
	 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
	 * @author chanlong(陈龙)
	 * @date 2017年3月28日 上午10:38:04
	 */
	@Override @JsonIgnore @JSONField(serialize = false)
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities == null ? new HashSet<>() : authorities;
	}
	
	/**
	 * 设置用户授权.
	 * @param roles
	 * @author chanlong(陈龙)
	 * @date 2018年1月24日 下午1:14:27
	 */
	public void setAuthorities(final List<AuthRole> roles) {
		roles.stream().forEach(role -> addAuthority(new SimpleGrantedAuthority(role.getKey())));
	}
	
	/**
	 * 添加用户授权.
	 * @param authority
	 * @author chanlong(陈龙)
	 * @date 2017年5月16日 下午3:41:48
	 */
	public AuthUser addAuthority(final GrantedAuthority authority) {
		if(authorities == null) authorities = new HashSet<>();
		authorities.add(authority);
		return this;
	}
	
	public AuthUser() { }
	
	public AuthUser(String id) {
		this.setId(id);
	}
	
	public AuthUser(StatusType status) {
		this.setStatus(status.value());
	}
	
	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public void setAuthorities(Set<GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
	
	@Override @JsonIgnore @JSONField(serialize = false)
	public boolean isAccountNonExpired() {
		return !UserStatusType.EXPIRED.compareTo(status);
	}

	@Override @JsonIgnore @JSONField(serialize = false)
	public boolean isAccountNonLocked() {
		return !UserStatusType.LOCKED.compareTo(status);
	}

	@Override @JsonIgnore @JSONField(serialize = false)
	public boolean isCredentialsNonExpired() {
		return !UserStatusType.INCERTIFICATE.compareTo(status);
	}

	@Override @JsonIgnore @JSONField(serialize = false)
	public boolean isEnabled() {
		return UserStatusType.ENABLED.compareTo(status);
	}
}
