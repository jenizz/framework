package com.arthur.framework.auth.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.arthur.framework.auth.dao.IAuthOperationRepository;
import com.arthur.framework.auth.dao.IAuthPermissionRepository;
import com.arthur.framework.auth.dao.IAuthResourceRepository;
import com.arthur.framework.auth.dao.IAuthRolePermissionRepository;
import com.arthur.framework.auth.dao.IAuthRoleRepository;
import com.arthur.framework.auth.dao.IAuthRoleUserRepository;
import com.arthur.framework.auth.dao.IAuthUserRepository;
import com.arthur.framework.auth.entity.AuthOperation;
import com.arthur.framework.auth.entity.AuthPermission;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.entity.AuthRolePermission;
import com.arthur.framework.auth.entity.AuthRoleUser;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.auth.enums.OperationCode;
import com.arthur.framework.auth.enums.ResourceType;
import com.arthur.framework.auth.support.AuthSecurityEndecrypt;
import com.arthur.framework.auth.support.AuthSecurityMetadataSource;
import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.orm.service.bo.CommonBoAdapter;
import com.arthur.framework.util.string.StringUtil;

/**
 * <p>描述: 权限相关服务 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月5日 上午10:25:57
 * @version 1.0.2017
 */
@Service @Transactional(isolation = Isolation.READ_COMMITTED)
public class AuthService extends CommonBoAdapter<CommonEntityAware> implements IAuthService {
	
	private static final Logger log = LoggerFactory.getLogger(AuthService.class);

	@Autowired
	private IAuthRoleRepository roleDao;
	
	@Autowired
	private IAuthUserRepository userDao;
	
	@Autowired
	private IAuthRoleUserRepository roleUserDao;
	
	@Autowired
	private IAuthResourceRepository resourceDao;
	
	@Autowired
	private IAuthOperationRepository operationDao;
	
	@Autowired
	private IAuthPermissionRepository permissionDao;
	
	@Autowired
	private IAuthRolePermissionRepository rolePermissionDao;
	
	@Autowired
	private AuthSecurityMetadataSource authSecurityMetadataSource;
	
	@Override
	public void updateCache(){
		authSecurityMetadataSource.getAllConfigAttributes();
	}
	
	@Override
	public AuthUser getUserByUsername(final String username)  throws Exception {
		AuthUser user = userDao.findOne(example -> {
			example.setUsername(username);
			return Example.of(example);
		});
		return user;
	}
	
	@Override
	public AuthResource getResourceByKey(final String key) throws Exception {
		Assert.notNull(key, "资源KEY不能为空。");
		
		return resourceDao.findOne(example -> {
			example.setStatus(StatusType.ACTIVITY.value());
			example.setKey(key);
			return Example.of(example);
		});
	}

	@Override
	public List<AuthResource> findAllSystems() throws Exception {
		return resourceDao.findAll(example -> {
			example.setStatus(StatusType.ACTIVITY.value());
			example.setType(ResourceType.SYSTEM);
			return Example.of(example);
		});
	}
	
	@Override
	public List<AuthResource> findAllResources(final String args) throws Exception {
		Assert.notNull(args, "资源KEY或资源父ID不能为空。");
		
		return resourceDao.findAll(example -> {
			example.setStatus(StatusType.ACTIVITY.value());
			if (StringUtil.isUUID(args)) {
				example.setPid(args);
			} else {
				example.setKey(args);
			}
			return Example.of(example);
		});
	}

	@Override
	public List<AuthOperation> findOperationDict() throws Exception {
		return operationDao.findAll(example -> {
			example.setStatus(StatusType.ACTIVITY.value());
			return Example.of(example);
		});
	}
	
	@Override
	public AuthOperation getOperationByCode(final String code) throws Exception {
		return operationDao.findOne(example -> {
			example.setCode(OperationCode.valueOf(code));
			example.setStatus(StatusType.ACTIVITY.value());
			return Example.of(example);
		});
	}

	@Override
	public AuthRole getRoleByKey(final String key) throws Exception {
		return roleDao.findOne(example -> {
			example.setKey(key);
			example.setStatus(StatusType.ACTIVITY.value());
			return Example.of(example);
		});
	}
	
	@Override
	public List<AuthRole> findAllRoles() throws Exception {
		return roleDao.findAll(example -> {
			example.setStatus(StatusType.ACTIVITY.value());
			return Example.of(example);
		});
	}
	
	@Override
	public List<AuthRole> findAllRoles(final AuthUser user) throws Exception {
		return roleDao.findAllByUser(user.getId());
	}
	
	@Override
	public List<AuthPermission> findAllPermissions() throws Exception {
		return permissionDao.findAll(example -> Example.of(example));
	}
	
	@Override
	public List<AuthPermission> findAllPermissions(final AuthRole role) throws Exception {
		return permissionDao.findByRoleId(role.getId());
	}
	
	@Override
	public List<AuthPermission> findAllPermissions(final String roleId, final OperationCode operationCode) throws Exception {
		return permissionDao.findAllPermissions(roleId, operationCode);
	}
	
	@Override
	public List<AuthRolePermission> findAllRolePermissions(final String roleId, final OperationCode operationCode) throws Exception {
		return rolePermissionDao.findAll(roleId, operationCode);
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public AuthRoleUser assign(final AuthRoleUser entity) throws Exception {
		return (AuthRoleUser)this.saveOrUpdate(entity);
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public List<AuthRoleUser> assign(final List<AuthRoleUser> entities) throws Exception {
		return roleUserDao.save(entities);
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public List<AuthRoleUser> assign(final List<AuthRoleUser> entities, final String userId) throws Exception {
		AuthUser authUser = userDao.findOne(example -> {
			example.setUsername(null);
			example.setBusinessKey(userId);
			return Example.of(example);
		});
		
		entities.parallelStream().forEach((entity) -> entity.setUser(authUser)); 
		
		return roleUserDao.save(entities);
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public AuthRolePermission authorize(final AuthRolePermission entity) throws Exception {
		return (AuthRolePermission)this.saveOrUpdate(entity);
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public List<AuthRolePermission> authorize(final List<AuthRolePermission> entities) throws Exception {
		return rolePermissionDao.save(entities);
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public AuthPermission savePermission(final AuthPermission entity) throws Exception {
		// 先保存资源
		entity.getResource().save();
		
		// 再保存权限
		return permissionDao.save(entity);
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public List<AuthPermission> saveAllPermissions(final List<AuthPermission> entities) throws Exception {
		return permissionDao.save(entities);
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public AuthOperation saveOperation(final AuthOperation entity) throws Exception {
		return operationDao.save(entity);
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public List<AuthOperation> saveAllOperations(final List<AuthOperation> entities) throws Exception {
		return operationDao.save(entities);
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public AuthRole saveRole(final AuthRole entity) throws Exception {
		return roleDao.save(entity);
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public List<AuthRole> saveAllRoles(final List<AuthRole> entities) throws Exception {
		return roleDao.save(entities);
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean deleteAuthUser(final String userid) throws Exception {
		try{
			AuthUser authUser = userDao.findOne((example) -> {
				example.setBusinessKey(userid);
				return Example.of(example);
			});
			
			userDao.delete(authUser);
			return true;
		} catch (Throwable e){
			return false;
		}
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean deleteAuthRoleUser(final String userOrRoleId) throws Exception {
		try{
			AuthUser authUser = new AuthUser();
			authUser.setBusinessKey(userOrRoleId);
			
			List<AuthRoleUser> result = roleUserDao.findAll(example -> {
				example.setRole(null);
				example.setUser(authUser);
				return Example.of(example);
			});
			
			log.info("get result of AuthRoleUser by userId: {}", result);
			
			if (result == null || result.isEmpty()) {
				AuthRole role = new AuthRole(userOrRoleId);
				result = roleUserDao.findAll(example -> {
					example.setRole(role);
					example.setUser(null);
					return Example.of(example);
				});
				log.info("get result of AuthRoleUser by roleId: {}", result);
			}
			
			roleUserDao.delete(result); 
			
			return true;
		} catch (Throwable e){
			return false;
		}
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean deleteAuthPermission(final String resourceid) throws Exception {
		AuthResource resource = new AuthResource(resourceid);
		
		List<AuthPermission> result = permissionDao.findAll(example -> {
			example.setResource(resource);
			return Example.of(example);
		});
		
		permissionDao.delete(result);
		return false;
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean deleteAuthRolePermission(final String roleid) throws Exception {
		AuthRole authRole = new AuthRole(roleid);
		
		List<AuthRolePermission> result = rolePermissionDao.findAll(example -> {
			example.setRole(authRole);
			return Example.of(example);
		});
		
		log.info("get result of AuthRolePermission by roleId: {}", result);
		
		rolePermissionDao.delete(result);
		return false;
	}

	@Override
	public boolean hasPermission(final AuthRole role, final AuthResource resource) throws Exception {
		long count = rolePermissionDao.count(role.getId(), resource.getId());
		return count > 0 ? true : false;
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String password(final String businessKey, final String oldPassword, final String newPassword) throws Exception {
		AuthUser user = userDao.findOne(example -> {
			example.setBusinessKey(businessKey);
			return Example.of(example);
		});
		if (user == null) {
			return "未发现用户的权限信息";
		} else if (StringUtil.isEmpty(oldPassword)) {
			return "原密码不能为空";
		} else if (StringUtil.isEmpty(newPassword)) {
			return "新密码不能为空";
		} else if (!AuthSecurityEndecrypt.match(oldPassword, user.getPassword())) {
			return "原密码错误";
		} else {
			user.setPassword(AuthSecurityEndecrypt.encrypt(newPassword));
			userDao.saveAndFlush(user);
			return "密码已修改";
		}
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public String password(final String businessKey) throws Exception {
		AuthUser user = userDao.findOne(example -> {
			example.setBusinessKey(businessKey);
			example.setUsername(null);
			return Example.of(example);
		});
		if (user == null) {
			return "未发现用户的权限信息";
		} else {
			user.setPassword(AuthSecurityEndecrypt.encrypt(user.getUsername()));
			userDao.saveAndFlush(user);
			return "密码已重置";
		}
	}
}
