package com.arthur.framework.auth.service;

import java.util.List;

import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.orm.service.bo.CommonBoAware;

public interface IAuthResourceService extends CommonBoAware<AuthResource> {

	/**
	 * 获取指定角色的所有资源.
	 * @param role
	 * @author chanlong(陈龙)
	 * @date 2018年1月24日 上午10:53:00
	 */
	List<AuthResource> findAll(final AuthRole role) throws Exception;

	/**
	 * 根据条件获取所有资源.
	 * @param resource
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年2月26日 上午11:21:08
	 */
	List<AuthResource> findAll(final AuthResource resource) throws Exception;
}
