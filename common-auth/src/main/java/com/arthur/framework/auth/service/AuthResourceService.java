package com.arthur.framework.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.auth.dao.IAuthResourceRepository;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.orm.service.bo.CommonBoAdapter;

@Service @Transactional(isolation = Isolation.READ_COMMITTED)
public class AuthResourceService extends CommonBoAdapter<AuthResource> implements IAuthResourceService {

	@Autowired
	private IAuthResourceRepository dao;
	
	@Override
	public List<AuthResource> findAll(final AuthRole role) throws Exception {
		return dao.findAll(role.getId());
	}

	@Override
	public List<AuthResource> findAll(final AuthResource resource) throws Exception {
		return dao.findAll(Example.of(resource), new Sort("level", "serial"));
	}
}
