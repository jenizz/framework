package com.arthur.framework.auth.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.auth.service.IAuthService;

/**
 * 当前登录用户-权限信息
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月24日 下午2:54:36
 * @version 1.0.2017
 */
public class Permission implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 6446616832035963284L;
	
    /**
	 * 添加资源.
	 * @param arp
	 * @author chanlong(陈龙)
	 * @date 2017年11月10日 上午11:44:54
	 */
	public Permission addResource(final String key, final Resource resource) {
		if (!cache.containsKey(key)) cache.put(key, new ArrayList<>());
		cache.get(key).add(resource);
		return this;
	}
	
	public Permission(IAuthService service) {
		this.service = service;
		this.cache = new LinkedHashMap<>();
	}

	@JSONField(serialize = false)
	private IAuthService service;
	
	@JSONField(serialize = false)
	private Map<String, List<Resource>> cache;

	public IAuthService getService() {
		return service;
	}

	public void setService(IAuthService service) {
		this.service = service;
	}

	public Map<String, List<Resource>> getCache() {
		return cache;
	}
	
	public List<Resource> getCache(final String key) {
		try {
			return cache.containsKey(key) ? cache.get(key) : new ArrayList<>();
		} catch (NullPointerException e) {
			return new ArrayList<>();
		}
	}

	public void setCache(Map<String, List<Resource>> cache) {
		this.cache = cache;
	}
}
