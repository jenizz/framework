package com.arthur.framework.auth.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.arthur.framework.auth.dao.IAuthUserRepository;
import com.arthur.framework.auth.entity.AuthUser;

/**
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月4日 上午9:45:23
 * @version 1.0.2017
 */
@Service
public class AuthUserService implements IAuthUserService {

	@Autowired
	private IAuthUserRepository userDao;
	
	/**
	 * Spring Security find user by username
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 * @author chanlong(陈龙)
	 * @date 2017年5月2日 下午3:54:28
	 */
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		Assert.notNull(username, "用户名不能为空。");
		
		try {
			AuthUser user = userDao.findOne(example -> {
				example.setUsername(username);
				example.setBusinessKey(null);
				return Example.of(example) ;
			});
			
			if(user == null){
				throw new UsernameNotFoundException("用户 " + username + " 不存在。");
			}else{
				return user;
			}
		} catch (Exception e) {
			throw new UsernameNotFoundException("用户 " + username + " 不存在。");
		}
	}
}
