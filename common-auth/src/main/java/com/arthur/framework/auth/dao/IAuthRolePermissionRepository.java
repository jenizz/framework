package com.arthur.framework.auth.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.arthur.framework.auth.entity.AuthRolePermission;
import com.arthur.framework.auth.enums.OperationCode;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

public interface IAuthRolePermissionRepository extends CommonRepositoryAware<AuthRolePermission, String> {

	/**
	 * 根据角色ID、资源key、操作码查找权限.
	 * @param roleId 角色ID
	 * @param resourceType 资源Type
	 * @param operationCode 操作码
	 * @author chanlong(陈龙)
	 * @date 2017年5月15日 上午10:21:36
	 */
	@Query("select o from AuthRolePermission o left join o.role role left join o.permission permission left join permission.resource resource left join permission.operation operation where resource.status = 1 and resource.level = 1 and role.id = :role and operation.code = :code order by resource.level, resource.serial")
	List<AuthRolePermission> findAll(@Param("role") final String roleId, @Param("code") final OperationCode operationCode);
	
	/**
	 * 根据角色ID、资源ID，计算权限数量.
	 * @param roleId
	 * @param resourceId
	 * @author chanlong(陈龙)
	 * @date 2017年11月10日 上午11:09:57
	 */
	@Query("select count(o.id) from AuthRolePermission o left join o.role role left join o.permission permission left join permission.resource resource where role.id = :role and resource.id = :resource and resource.status = 1")
	long count(@Param("role") final String roleId, @Param("resource") final String resourceId);
	
}
