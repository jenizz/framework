package com.arthur.framework.auth.enums;

/**
 * <p>描述: 操作类型 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月28日 下午12:44:10
 * @version 1.0.2017
 */
public enum OperationType {
	// URL
	url,
	// rest api
	api,
	// 超链接
	link,
	// 记录
	record,
	// 页面元素
	element,
	// 数据库
	database
}
