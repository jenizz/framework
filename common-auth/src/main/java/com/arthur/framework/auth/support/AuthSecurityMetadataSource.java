package com.arthur.framework.auth.support;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.model.Permission;
import com.arthur.framework.auth.model.Resource;
import com.arthur.framework.auth.service.IAuthResourceService;
import com.arthur.framework.auth.service.IAuthService;

/**
 * <p><b>描述</b>: 安全信息元数据 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月21日 上午11:09:40
 * @version 1.0.2017
 */
@Service
public class AuthSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

	private static final Logger log = LoggerFactory.getLogger(AuthSecurityMetadataSource.class);

	@Autowired
    private IAuthService service;
	
	@Autowired
    private IAuthResourceService rss;
	
	@Autowired
	private RedisTemplate<String, List<Resource>> redis;
	
	private Map<String, Collection<ConfigAttribute>> rolesmap = new LinkedHashMap<>();
	
	@Override
	public Collection<ConfigAttribute> getAttributes(final Object object) throws IllegalArgumentException {
		HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
		for (Iterator<String> iter = rolesmap.keySet().iterator(); iter.hasNext();) {
			String resurl = iter.next();
			if (new AntPathRequestMatcher(resurl).matches(request)) {
				return rolesmap.get(resurl);
			}
		}
		return null;
	}

	/**
	 * 系统启动时初始化加载所有权限配置
	 * @see org.springframework.security.access.SecurityMetadataSource#getAllConfigAttributes()
	 * @author chanlong(陈龙)
	 * @date 2017年3月21日 下午12:37:48
	 */
	@Override
	public Collection<ConfigAttribute> getAllConfigAttributes() {
		Collection<ConfigAttribute> configs = new ArrayList<>();
		try {
			List<AuthRole> roles = service.findAllRoles();
			
			if (roles == null) return configs;
			
			roles.parallelStream().filter(_role -> configs.add(new SecurityConfig(_role.getKey())))
								  .forEach(_role -> processer(new Permission(service), _role));
								  
		} catch (Exception e) {
			log.error("初始化权限配置失败（调用[AuthSecurityMetadataSource.getAllConfigAttributes]接口时出现异常）", e);
		}
		
		return configs;
	}
	
	@Override
	public boolean supports(final Class<?> clazz) {
		return true;
	}
	
	private AuthRole processer(final Permission permission, final AuthRole role) {
		if (role.getId() != null) {
			try {
				List<AuthResource> resources = rss.findAll(role);
				if (resources != null && !resources.isEmpty()) {
					resources.stream().reduce(permission, (cache, item) -> cache.addResource(role.getKey(), Resource.build(item)), (left, right) -> left);
					setCache(role.getKey(), permission.getCache(role.getKey()));
				} else {
					clearCache(role.getKey());
				}
			} catch (Exception e) {
				log.error("初始化权限配置失败（调用[AuthSecurityMetadataSource.processer]接口时出现异常）", e);
			}

		}
		return role;
	}

	private void setCache(final String roleKey, final List<Resource> resource) {
		redis.opsForValue().set(roleKey, resource);
	}
	
	private void clearCache(final String roleKey) {
		redis.delete(roleKey);
	}
}
