package com.arthur.framework.auth.enums;

/**
 * <p><b>描述</b>: 资源类型 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月17日 上午9:44:35
 * @version 1.0.2017
 */
public enum ResourceType {
	SYSTEM, 	// 系统（子系统）
	MOUDLE, 	// 模块
	PAGE, 		// 页面
	MENU, 		// 菜单
	ELEMENT,	// 元素
	CONTROL, 	// 控件
	LINK,		// 链接
	FILE, 		// 文件
	TABLE,		// 数据表
	FIELD,		// 数据字段
	RECORD		// 数据记录
}
