package com.arthur.framework.auth.support;

public final class AuthSecurityConfigConstant {

	public static final String DEF_IGNORED = "/";
	
	public static final String DEF_STATICS = "*.html";
	
	public static final String DEF_LOGIN_URL = "/auth/login";
	
	public static final String DEF_LOGOUT_URL = "/auth/logout";
	
	public static final String DEF_USERNAME_PARAMETER = "username";
	
	public static final String DEF_PASSWORD_PARAMETER = "password";
	
	public static final Integer DEF_USER_LOCKED = 5;
	
}
