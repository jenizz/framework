package com.arthur.framework.auth.support;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.SecurityMetadataSource;
import org.springframework.security.access.intercept.AbstractSecurityInterceptor;
import org.springframework.security.access.intercept.InterceptorStatusToken;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;

public class AuthSecurityInterceptor extends AbstractSecurityInterceptor implements Filter {

	private static final String FILTER_APPLIED = "__Spring_Security_AuthorizationSecurityInterceptor_FilterApplied"; 
	
	private FilterInvocationSecurityMetadataSource metadataSource;
	
	private boolean observeOncePerRequest = true; 
	
	@Override
	public Class<?> getSecureObjectClass() {
		return FilterInvocation.class;
	}

	@Override
	public SecurityMetadataSource obtainSecurityMetadataSource() {
		return this.metadataSource;
	}
	
	public void setMetadataSourceManager(FilterInvocationSecurityMetadataSource metadataSource) {
		this.metadataSource = metadataSource;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		FilterInvocation invocation = new FilterInvocation(request, response, chain);  
        invoke(invocation);
	}

	private void invoke(FilterInvocation invocation) throws IOException, ServletException {
		System.out.println("=== AuthSecurityInterceptor:invoke === ");
		
		FilterChain chain = invocation.getChain();
		HttpServletRequest request = invocation.getRequest();
		HttpServletResponse response = invocation.getHttpResponse();
		
		if (request != null && request.getAttribute(FILTER_APPLIED) != null && observeOncePerRequest) {  
			chain.doFilter(request, response);  
		} else {
			if (request != null) {
				request.setAttribute(FILTER_APPLIED, Boolean.TRUE);
			}

			InterceptorStatusToken token = super.beforeInvocation(invocation);

			try {
				chain.doFilter(request, response);  
			} finally {
				super.finallyInvocation(token);
			}
			
			super.afterInvocation(token, null);
		}
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		System.out.println("=== AuthSecurityInterceptor:init === ");
	}
	
	@Override
	public void destroy() {
		System.out.println("=== AuthSecurityInterceptor:destroy === ");
	}

	public boolean isObserveOncePerRequest() {
		return observeOncePerRequest;
	}

	public void setObserveOncePerRequest(boolean observeOncePerRequest) {
		this.observeOncePerRequest = observeOncePerRequest;
	}
}
