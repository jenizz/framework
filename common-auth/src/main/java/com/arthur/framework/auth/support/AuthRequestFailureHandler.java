package com.arthur.framework.auth.support;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.config.SecurityProperties;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.support.Response;
import com.arthur.framework.util.HttpUtil;
import com.arthur.framework.util.JsonUtil;

/**
 * <p>描述: 登录失败处理器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月12日 上午9:43:44
 * @version 1.0.2017
 */
@Component
public class AuthRequestFailureHandler extends SimpleUrlAuthenticationFailureHandler {
	
	private static final Logger log = LoggerFactory.getLogger(AuthRequestFailureHandler.class);

	private static final Map<String, String> BAD_MESSAGE = new HashMap<>();
	
	@Autowired
	private SecurityProperties properties;
	
	@Autowired
	private IAuthService service;
	
	public Map<String, Integer> counter = new HashMap<>();
	
	static {
		BAD_MESSAGE.put("Bad credentials", "无效的凭证，用户名或密码错误。");
		BAD_MESSAGE.put("User account is locked", "当前用户已锁定。");
	}

	private int i = 0;
	
	@Override
	public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException exception) throws IOException, ServletException {
		Response result = new Response(HttpStatus.UNAUTHORIZED, BAD_MESSAGE.get(exception.getMessage()));
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		try {
			AuthUser user = service.getUserByUsername(username);
			if (!AuthSecurityEndecrypt.match(password, user.getPassword())) {
				counter.put(username, i++);
				if (properties.getLocked() > 0 && counter.get(username) >= properties.getLocked()) {
					user.setStatus(StatusType.DELETE.value());
					user.save();
					
					result.setCode(HttpStatus.OK.value());
					result.setReason(HttpStatus.OK.getReasonPhrase());
					result.setResult("用户密码错误已超过" + properties.getLocked() + "次，当前用户已锁定");
				} else {
					result.setCode(HttpStatus.OK.value());
					result.setReason(HttpStatus.OK.getReasonPhrase());
					result.setResult("用户密码错误。");
				}
			}
		} catch (Exception e) {
			log.error("AuthRequestFailureHandler for getUserByUsername is error : {}", e);
		}

		// 序列化结果
		String json = JsonUtil.serialize(result);
		HttpUtil.write(response, json, HttpStatus.OK.value());
	}

	public void clearCounter(final String key) {
		if (counter.containsKey(key)) counter.remove(key);
	}
}
