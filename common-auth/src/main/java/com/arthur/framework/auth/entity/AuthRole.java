package com.arthur.framework.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.orm.enums.StatusType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p><b>描述</b>: 角色 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月16日 上午11:13:26
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_auth_role")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class AuthRole extends CommonEntityUUID<AuthRole> implements GrantedAuthority {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -1182928483908995959L;
	
	// 角色key
	@Column(name = "key_", nullable = false, unique = true)
	private String key;
	
	// 名称
	@Column(name = "name_")
	private String name;
	
	// 描述
	@Column(name = "desc_")
	private String desc;
	
	// 排序号
	@Column(name = "serial_")
	private Integer serial;
	
	// 状态
	@Column(name = "status_")
	private Integer status;
	
	@Override
	public String getAuthority() {
		return key;
	}
	
	public AuthRole() { }
	
	public AuthRole(String id) {
		this.setId(id);
	}
	
	public AuthRole(StatusType status) {
		this.setStatus(status.value());
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
