package com.arthur.framework.auth.service;

import java.util.List;

import com.arthur.framework.auth.entity.AuthOperation;
import com.arthur.framework.auth.entity.AuthPermission;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.entity.AuthRolePermission;
import com.arthur.framework.auth.entity.AuthRoleUser;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.auth.enums.OperationCode;
import com.arthur.framework.auth.enums.ResourceType;
import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.service.bo.CommonBoAware;

/**
 * <p>描述: 权限相关服务 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月4日 上午9:47:07
 * @version 1.0.2017
 */
public interface IAuthService extends CommonBoAware<CommonEntityAware> {

	/**
	 * 获取所有类型为系统(SYSTEM)的资源.
	 * <br> 注：自动过滤状态非StatusType.ACTIVITY的数据
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:51:49
	 */
	List<AuthResource> findAllSystems() throws Exception;
	
	/**
	 * 根据资源的父ID或KEY，获取所有资源.
	 * <br> 注：自动过滤状态非StatusType.ACTIVITY的数据
	 * @param args 资源的父ID或KEY
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:54:10
	 */
	List<AuthResource> findAllResources(final String args) throws Exception;
	
	/**
	 * 根据资源KEY，获取资源.
	 * <br> 注：自动过滤状态非StatusType.ACTIVITY的数据
	 * @param key 资源KEY
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:50:56
	 */
	AuthResource getResourceByKey(final String key) throws Exception;
	
	/**
	 * 获取操作项的数据字典.
	 * <br> 注：自动过滤状态非StatusType.ACTIVITY的数据
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:55:02
	 */
	List<AuthOperation> findOperationDict() throws Exception;
	
	/**
	 * 根据操作code，获取操作字典数据.
	 * @param code
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 上午11:43:08
	 */
	AuthOperation getOperationByCode(final String code) throws Exception;
	
	/**
	 * 获取所有权限.
	 * <br> 注：自动过滤状态非StatusType.ACTIVITY的数据
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:55:55
	 */
	List<AuthPermission> findAllPermissions() throws Exception;
	
	/**
	 * 获取指定角色的所有权限.
	 * @param role 角色对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午10:33:38
	 */
	List<AuthPermission> findAllPermissions(final AuthRole role) throws Exception;
	
	/**
	 * 根据角色ID、资源key和操作码获取所有权限.
	 * @param roleId 角色ID
	 * @param resourceKey 资源KEY（{@link ResourceType}）
	 * @param operationCode 操作CODE（{@link OperationCode}）
	 * @author chanlong(陈龙)
	 * @date 2017年5月15日 上午10:25:13
	 */
	List<AuthPermission> findAllPermissions(final String roleId, final OperationCode operationCode) throws Exception;
	
	/**
	 * 根据角色ID、资源key和操作码获取所有权限.
	 * @param roleId 角色ID
	 * @param resourceKey 资源KEY（{@link ResourceType}）
	 * @param operationCode 操作CODE（{@link OperationCode}）
	 * @author chanlong(陈龙)
	 * @date 2017年5月15日 上午10:25:13
	 */
	List<AuthRolePermission> findAllRolePermissions(final String roleId, final OperationCode operationCode) throws Exception;
	
	/**
	 * 根据角色KEY，获取角色.
	 * <br> 注：自动过滤状态非StatusType.ACTIVITY的数据
	 * @param key 角色KEY
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:56:29
	 */
	AuthRole getRoleByKey(final String key) throws Exception;
	
	/**
	 * 获取所有角色.
	 * <br> 注：自动过滤状态非StatusType.ACTIVITY的数据
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:57:11
	 */
	List<AuthRole> findAllRoles() throws Exception;
	
	/**
	 * 获取用户所有角色.
	 * @param user 用户对象
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月16日 下午3:31:51
	 */
	List<AuthRole> findAllRoles(final AuthUser user) throws Exception;
	
	/**
	 * 根据用户名，获取用户实体.
	 * @param username
	 * @author chanlong(陈龙)
	 * @date 2017年11月21日 下午12:14:15
	 */
	AuthUser getUserByUsername(final String username) throws Exception;
	
	/**
	 * 添加用户.
	 * @param entity 角色用户实体
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 下午1:43:58
	 */
	AuthRoleUser assign(final AuthRoleUser entity) throws Exception;
	
	/**
	 * 添加角色.
	 * @param entities 角色用户实体集合
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 下午1:50:30
	 */
	List<AuthRoleUser> assign(final List<AuthRoleUser> entities) throws Exception;
	
	/**
	 * 添加角色.
	 * @param entities 角色用户实体集合
	 * @param userId 部门用户ID
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月4日 下午5:25:09
	 */
	List<AuthRoleUser> assign(final List<AuthRoleUser> entities, final String userId) throws Exception;
	
	/**
	 * 授权（将角色与权限进行绑定）.
	 * @param entity 角色权限实体
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:58:50
	 */
	AuthRolePermission authorize(final AuthRolePermission entity) throws Exception;
	
	/**
	 * 授权（将角色与权限进行绑定）.
	 * @param entities 角色权限实体集合
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月15日 下午4:15:26
	 */
	List<AuthRolePermission> authorize(final List<AuthRolePermission> entities) throws Exception;

	/**
	 * 保存操作.
	 * @param entity
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 下午12:56:47
	 */
	AuthOperation saveOperation(final AuthOperation entity) throws Exception;
	
	/**
	 * 批量保存操作.
	 * @param entities
	 * @return
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 下午12:55:23
	 */
	List<AuthOperation> saveAllOperations(final List<AuthOperation> entities) throws Exception;
	
	/**
	 * 保存权限，同时保存资源.
	 * @param entities
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 下午12:53:51
	 */
	AuthPermission savePermission(final AuthPermission entity) throws Exception;
	
	/**
	 * 批量保存权限（关联资源与权限）.
	 * @param entities 实体对象集合
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 上午10:25:03
	 */
	List<AuthPermission> saveAllPermissions(final List<AuthPermission> entities) throws Exception;

	/**
	 * 保存角色.
	 * @param entity
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 下午1:23:10
	 */
	AuthRole saveRole(final AuthRole entity) throws Exception;
	
	/**
	 * 批量保存角色.
	 * @param entities 实体对象集合
	 * @author chanlong(陈龙)
	 * @date 2017年5月5日 下午1:21:42
	 */
	List<AuthRole> saveAllRoles(final List<AuthRole> entities) throws Exception;

	/**
	 * 更新缓存.
	 * @author chanlong(陈龙)
	 * @date 2017年6月23日 下午12:02:54
	 */
	void updateCache();

	/**
	 * 删除权限用户.
	 * @param userid
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月8日 下午12:27:43
	 */
	boolean deleteAuthUser(final String userid) throws Exception;
	
	/**
	 * 删除用户角色关联.
	 * @param userid
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月6日 下午4:50:35
	 */
	boolean deleteAuthRoleUser(final String userid) throws Exception;

	/**
	 * 删除权限.
	 * @param resourceid
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年11月2日 下午4:30:37
	 */
	boolean deleteAuthPermission(final String resourceid) throws Exception;
	
	/**
	 * 删除角色权限关联.
	 * @param roleid
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月7日 下午1:07:22
	 */
	boolean deleteAuthRolePermission(final String roleid) throws Exception;

	/**
	 * 判断角色和资源是否具备权限对应关系.
	 * @param role
	 * @param resource
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年11月10日 上午11:23:14
	 */
	boolean hasPermission(final AuthRole role, final AuthResource resource) throws Exception;

	/**
	 * 根据业务ID，修改用户密码.
	 * @param businessKey
	 * @param oldPassword
	 * @param newPassword
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年11月21日 上午10:13:50
	 */
	String password(final String businessKey, final String oldPassword, final String newPassword) throws Exception;

	/**
	 * 根据业务ID，重置用户密码.
	 * @param businessKey
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年11月21日 下午4:59:25
	 */
	String password(final String businessKey) throws Exception;

}
