package com.arthur.framework.auth.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.arthur.framework.auth.entity.AuthPermission;
import com.arthur.framework.auth.enums.OperationCode;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

public interface IAuthPermissionRepository extends CommonRepositoryAware<AuthPermission, String> {

	/**
	 * 根据角色ID查找权限.
	 * @param roleId 角色ID
	 * @author chanlong(陈龙)
	 * @date 2017年5月15日 上午9:43:15
	 */
	@Query("select a.permission from AuthRolePermission a left join a.role role where role.id = :role")
	List<AuthPermission> findByRoleId(@Param("role") final String roleId);
	
	/**
	 * 根据角色ID、资源key、操作码查找权限.
	 * @param roleId 角色ID
	 * @param resourceType 资源Type
	 * @param operationCode 操作码
	 * @author chanlong(陈龙)
	 * @date 2017年5月15日 上午10:21:36
	 */
	@Query("select a.permission from AuthRolePermission a left join a.role role left join a.permission p left join p.resource r left join p.operation o where r.status = 1 and r.level = 1 and role.id = :role and o.code = :code order by r.level, r.serial")
	List<AuthPermission> findAllPermissions(@Param("role") final String roleId, @Param("code") final OperationCode operationCode);
}
