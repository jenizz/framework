package com.arthur.framework.auth.support;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

/**
 * <p><b>描述</b>: 加密工具类 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月17日 上午10:35:15
 * @version 1.0.2017
 */
public final class AuthSecurityEndecrypt {

	private static final String SITE_WIDE_SECRET = "secret-key";
	private static final PasswordEncoder encoder = new StandardPasswordEncoder(SITE_WIDE_SECRET);

	public static String encrypt(final CharSequence rawPassword) {
		return encoder.encode(rawPassword);
	}

	public static boolean match(final CharSequence rawPassword, String password) {
		return encoder.matches(rawPassword, password);
	}
}
