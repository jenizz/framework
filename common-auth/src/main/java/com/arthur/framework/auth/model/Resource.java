package com.arthur.framework.auth.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.enums.LevelType;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 权限资源
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2018年1月26日 上午10:24:35
 * @version 1.0.2018
 */
public class Resource implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 1071085082204507089L;
	
	@JSONField(ordinal = 1)
	@JsonProperty(index = 1)
	private String id;
	
	@JSONField(ordinal = 2)
	@JsonProperty(index = 2)
	private String pid;
	
	@JSONField(ordinal = 3)
	@JsonProperty(index = 3)
	private String path;
	
	@JSONField(ordinal = 4)
	@JsonProperty(index = 4)
	private String name;
	
	@JSONField(ordinal = 5)
	@JsonProperty(index = 5)
	private String icon;
	
	@JSONField(ordinal = 6)
	@JsonProperty(index = 6)
	private String title;
	
	@JSONField(ordinal = 7)
	@JsonProperty(index = 7)
	private String level;
	
	@JSONField(ordinal = 8)
	@JsonProperty(index = 8)
	private String serial;
	
	@JSONField(ordinal = 9) 
	@JsonProperty(index = 9)
	@PropertyIgnore
	private Set<Resource> children;
	
	/**
	 * 构建实例.
	 * @param resource
	 * @author chanlong(陈龙)
	 * @date 2018年1月24日 下午12:57:52
	 */
	public static Resource build(final AuthResource resource){
		Resource res = new Resource();
		res.id = resource.getId();
		res.pid = resource.getPid();
		res.path = resource.getUrl();
		res.name = resource.getKey();
		res.icon = resource.getIcon();
		res.title = resource.getName();
		res.level = String.valueOf(resource.getLevel());
		res.serial = String.valueOf(resource.getSerial());
		return res;
	}
	
	@Deprecated
	public static Resource build(final AuthResource resource, final boolean async) {
		Resource res = build(resource);
		if (async) {
			res.children = new HashSet<>();
		} else if (resource.hasChildren()) {
			resource.getChildren().forEach(child -> res.addChildren(Resource.build(child)));
		}
		return res;
	}
	
	/**
	 * Map转对象.
	 * @param resource
	 * @author chanlong(陈龙)
	 * @date 2018年1月26日 上午9:11:59
	 */
	public static Resource translate(final Map<String, Object> resource) {
		return ReflectUtil.copyProperties(new Resource(), resource);
	}
	
	/**
	 * 是否有下级子资源.
	 * @author chanlong(陈龙)
	 * @date 2018年1月25日 下午5:28:51
	 */
	public boolean hasChildren() {
		return children != null && !children.isEmpty();
	}
	
	/**
	 * 是否为第一级（除根外）资源.
	 * @author chanlong(陈龙)
	 * @date 2018年1月25日 下午5:28:14
	 */
	@JSONField(serialize = false) @JsonIgnore
	public boolean isParent() {
		return level.equals(String.valueOf(LevelType.FIRST.value()));
	}
	
	@Override
	public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) return false;
        return this == obj || name.equals(((Resource) obj).name);
	}

	@Override
	public int hashCode() {
		return 31 * name.hashCode() + name.hashCode();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public Set<Resource> getChildren() {
		return children;
	}

	public void setChildren(Set<Resource> children) {
		this.children = children;
	}

	public void addChildren(Resource child){
		if (children == null) children = new LinkedHashSet<>();
		children.add(child);
	}
	
	private Resource() { }

}
