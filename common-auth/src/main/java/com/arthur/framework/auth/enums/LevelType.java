package com.arthur.framework.auth.enums;

public enum LevelType {
	ROOT(0), FIRST(1), SECOND(2), THIRD(3), FOURTH(4);
	
	private int value;
	
	private LevelType(int orgin) {
		this.value = orgin;
	}
	
	public int value() {
		return value;
	}
}
