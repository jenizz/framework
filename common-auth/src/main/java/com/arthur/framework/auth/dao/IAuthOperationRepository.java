package com.arthur.framework.auth.dao;

import com.arthur.framework.auth.entity.AuthOperation;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

public interface IAuthOperationRepository extends CommonRepositoryAware<AuthOperation, String> {

}
