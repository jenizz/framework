package com.arthur.framework.auth.enums;

import com.arthur.framework.auth.entity.AuthLogger;
import com.arthur.framework.util.DateUtil;
import com.arthur.framework.util.DateUtil.DatePattern;

public enum LoggerType {
	LOGIN_SUCCESS("用户登录", "登录"), 
	LOGOUT_SUCCESS("用户登出", "登出");

	private String using;
	private String resource;
	private String template = "用户【${currentUser}】, 于【${currentDate}】, 执行【${logname}】操作, 请求ip【${userip}】";
	
	private LoggerType(String using, String resource) {
		this.using = using;
		this.resource = resource;
	}

	public String getUsing() {
		return using;
	}

	public String getResource() {
		return resource;
	}

	public String getDescription(AuthLogger logger) {
		return template.replace("${currentUser}", logger.getUserName())
					   .replace("${currentDate}", DateUtil.format(logger.getTime(), DatePattern.DATETIME.value()))
					   .replace("${logname}", logger.getResource())
					   .replace("${userip}", logger.getIp());
	}
}
