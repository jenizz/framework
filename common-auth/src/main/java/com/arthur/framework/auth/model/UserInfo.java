package com.arthur.framework.auth.model;

import java.io.Serializable;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.orm.support.Processer;

/**
 * 当前登录用户-用户信息
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2018年1月26日 上午10:23:02
 * @version 1.0.2018
 */
public class UserInfo implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -6328266892856637817L;

	@JSONField(ordinal = 0)
	private String userId;
	
	@JSONField(ordinal = 1)
	private String userName;
	
	@JSONField(ordinal = 2)
	private String organId;
	
	@JSONField(ordinal = 3)
	private String organCode;
	
	@JSONField(ordinal = 4)
	private String organName;

	public UserInfo() {}
	
	public UserInfo(AuthUser user) {
		Processer processer = Processer.factory();
		JdbcTemplate jdbc = processer.getJdbc();
		this.setUserInfo(jdbc.queryForObject(getSelect(), new BeanPropertyRowMapper<>(UserInfo.class), user.getBusinessKey()));
	}
	
	public void setUserInfo(UserInfo user) {
		if (user != null) {
			this.userId = user.userId;
			this.userName = user.userName;
			this.organId = user.organId;
			this.organCode = user.organCode;
			this.organName = user.organName;
		}
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrganId() {
		return organId;
	}

	public void setOrganId(String organId) {
		this.organId = organId;
	}

	public String getOrganCode() {
		return organCode;
	}

	public void setOrganCode(String organCode) {
		this.organCode = organCode;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}
	
	private String getSelect(){
		StringBuilder sql = new StringBuilder("select")
									  .append(" u.id userId,")
									  .append(" u.name_ userName,")
									  .append(" u.account_ userAccount,")
									  .append(" o.id organId,")
									  .append(" o.code_ organCode,")
									  .append(" o.name_ organName ")
									  .append("from tb_sys_user u ")
									  .append("left join tb_sys_organ_user t on u.id = t.user_id and t.part_time = '0' ")
									  .append("left join tb_sys_organ o on o.id = t.organ_id ")
									  .append("where u.id = ? ");
		return sql.toString();
	}
}
