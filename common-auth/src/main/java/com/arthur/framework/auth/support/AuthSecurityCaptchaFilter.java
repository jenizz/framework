package com.arthur.framework.auth.support;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.arthur.framework.util.string.StringUtil;

public class AuthSecurityCaptchaFilter extends UsernamePasswordAuthenticationFilter {

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		String vericode = request.getParameter("vericode");
		Object sesscode = request.getSession().getAttribute("DefaultKaptcha");
		if ((StringUtil.isEmpty(vericode) && StringUtil.isEmpty(sesscode)) || vericode.equals(sesscode)) {
			return super.attemptAuthentication(request, response);
		} else {
			throw new RuntimeException("无效的验证码");
		}
	}
}
