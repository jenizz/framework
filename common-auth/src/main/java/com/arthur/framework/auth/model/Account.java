package com.arthur.framework.auth.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.auth.entity.AuthUser;

/**
 * 当前登录用户-账号信息
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2018年1月26日 上午10:23:31
 * @version 1.0.2018
 */
public class Account implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 780876712665550672L;
	
	@JSONField(ordinal = 0)
	private String id;
	
	@JSONField(ordinal = 1)
	private String name;
	
	@JSONField(ordinal = 2)
	private Set<String> roles = new LinkedHashSet<>();
	
	public Account(AuthUser user) {
		this.setId(user.getId());
		this.setName(user.getUsername());
		this.setRoles(user.getAuthorities());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(Set<String> roles) {
		this.roles = roles;
	}
	
	private void setRoles(Collection<? extends GrantedAuthority> authorities) {
		authorities.forEach(role -> roles.add(role.getAuthority()));
	}
}
