package com.arthur.framework.auth.service;

import com.arthur.framework.auth.entity.AuthLogger;
import com.arthur.framework.orm.service.bo.CommonBoAware;

public interface IAuthLoggerService extends CommonBoAware<AuthLogger> {

}
