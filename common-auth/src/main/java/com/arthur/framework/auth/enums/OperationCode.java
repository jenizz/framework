package com.arthur.framework.auth.enums;

/**
 * <p>描述: 操作码 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月15日 上午10:01:18
 * @version 1.0.2017
 */
public enum OperationCode {
	// 可见
	visible,
	// 启用
	enabled,
	// 访问
	invoke,
	// 插入
	insert,
	// 删除
	delete,
	// 更新
	update,
	// 查询
	search
}
