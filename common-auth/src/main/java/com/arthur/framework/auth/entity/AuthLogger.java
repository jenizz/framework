package com.arthur.framework.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.arthur.framework.auth.enums.LoggerType;
import com.arthur.framework.auth.model.Session;
import com.arthur.framework.auth.model.UserInfo;
import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.util.DateUtil;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>描述: 操作日志 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月23日 下午5:29:58
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_sys_logger")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class AuthLogger extends CommonEntityUUID<AuthLogger> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -2831645969551505100L;

	// 操作人IP
	@Column(name = "ip_")
	private String ip;
	
	// 接口地址
	@Column(name = "url_")
	private String url;
	
	// 操作时间
	@Column(name = "time_")
	@OrderBy("time desc")
	private String time;
	
	// 耗时
	@Column(name = "take_")
	private Long take;
	
	// 操作用途
	@Column(name = "using_")
	private String using;
	
	// 操作资源
	@Column(name = "resource_")
	private String resource;
	
	// 日志描述
	@Column(name = "description_")
	private String description;
		
	// 操作人ID
	@Column(name = "user_id")
	private String userId;
	
	// 操作人姓名
	@Column(name = "user_name")
	private String userName;
	
	// 操作部门ID
	@Column(name = "organ_id")
	private String organId;
	
	// 操作部门名称
	@Column(name = "organ_name")
	private String organName;
	
	// 文件路径
	@Column(name = "file_path")
	private String filepath;
	
	// 文件密码
	@Column(name = "file_pwd")
	private String filepswd;
	
	// 操作内容
	@Column(name = "content_")
	private String content;
	
	// 操作内容密码
	@Column(name = "content_pwd")
	private String contentpwd;
	
	public AuthLogger(final LoggerType type, final String ip, final String url, final Session session) {
		UserInfo userinfo = session.getUserinfo();
		this.setIp(ip);
		this.setUrl(url);
		this.setTime(DateUtil.currentime());
		this.setTake(0L);
		this.setUsing(type.getUsing());
		this.setResource(type.getResource());
		this.setUserId(userinfo.getUserId());
		this.setUserName(userinfo.getUserName());
		this.setOrganId(userinfo.getOrganId());
		this.setOrganName(userinfo.getOrganName());
		this.setDescription(type.getDescription(this));
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Long getTake() {
		return take;
	}

	public void setTake(Long take) {
		this.take = take;
	}

	public String getUsing() {
		return using;
	}

	public void setUsing(String using) {
		this.using = using;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrganId() {
		return organId;
	}

	public void setOrganId(String organId) {
		this.organId = organId;
	}

	public String getOrganName() {
		return organName;
	}

	public void setOrganName(String organName) {
		this.organName = organName;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getFilepswd() {
		return filepswd;
	}

	public void setFilepswd(String filepswd) {
		this.filepswd = filepswd;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContentpwd() {
		return contentpwd;
	}

	public void setContentpwd(String contentpwd) {
		this.contentpwd = contentpwd;
	}
}
