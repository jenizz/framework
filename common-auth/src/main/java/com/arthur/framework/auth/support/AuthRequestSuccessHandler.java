
package com.arthur.framework.auth.support;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.csrf.DefaultCsrfToken;
import org.springframework.stereotype.Component;

import com.arthur.framework.auth.entity.AuthLogger;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.auth.enums.LoggerType;
import com.arthur.framework.auth.model.Session;
import com.arthur.framework.auth.service.IAuthLoggerService;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.support.Constant;
import com.arthur.framework.support.Response;
import com.arthur.framework.util.HttpUtil;
import com.arthur.framework.util.JsonUtil;

/**
 * <p>描述: 登录成功处理器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年6月12日 上午9:43:23
 * @version 1.0.2017
 */
@Component
public class AuthRequestSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	private static final Logger log = LoggerFactory.getLogger(AuthRequestSuccessHandler.class);

	@Autowired
	private IAuthService service;
	
	@Autowired
	private IAuthLoggerService logger;
	
	@Autowired
	private RedisTemplate<String, List<Map<String, Object>>> redis;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		try {
			AuthUser user = (AuthUser) authentication.getPrincipal();
			log.info("find user '{}' is success", user.getUsername());
			
			// 获取并设置用户角色
			List<AuthRole> roles = service.findAllRoles(user);
			user.setAuthorities(roles);
			
			// 获取session id
			HttpSession session = request.getSession(false);
			String sessionid = session.getId();
			
			// 获取csrf token
			DefaultCsrfToken csrf = (DefaultCsrfToken)session.getAttribute(Constant.KEY_CSRF_TOKEN);
			String token = csrf.getToken();
			
			// 获取当前用户
			Session current = Session.toCurrentUser(token, sessionid, user, redis.opsForValue());
			
			// 设置session
			session.setAttribute(sessionid, current);
			session.setAttribute(AuthUser.class.getName(), sessionid);
			
			// 序列化结果
			String json = JsonUtil.serialize(new Response(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), current));
			
			// 记录登录日志
			logger.saveOrUpdate(new AuthLogger(LoggerType.LOGIN_SUCCESS, request.getRemoteAddr(), request.getRequestURL().toString(), current));
			
			HttpUtil.write(response, json, HttpStatus.OK.value());
		} catch (Exception e) {
			log.error("Performation AuthRequestSuccessHandler.onAuthenticationSuccess is error : {}", e);
		}
	}

}
