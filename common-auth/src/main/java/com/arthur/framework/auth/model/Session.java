package com.arthur.framework.auth.model;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.data.redis.core.ValueOperations;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.util.string.StringUtil;

/**
 * <p>描述: 当前登陆用户 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月24日 下午1:52:30
 * @version 1.0.2017
 */
public class Session implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -2369720644711327880L;

	@JSONField(ordinal = 0)
	private String token;
	
	@JSONField(ordinal = 1)
	private String session;

	@JSONField(ordinal = 2)
	private Account account;
	
	@JSONField(ordinal = 3)
	private UserInfo userinfo;
	
	@JSONField(ordinal = 4)
	private Set<Resource> permissions = new LinkedHashSet<>();
	
	/**
	 * 将权限用户转为当前登录用户.
	 * @param user
	 * @author chanlong(陈龙)
	 * @date 2018年1月29日 下午1:40:15
	 */
	public static Session toCurrentUser(final AuthUser user) {
		Session current = new Session();
		current.setAccount(new Account(user));
		current.setUserinfo(new UserInfo(user));
		current.setBusinessKey(user.getBusinessKey());
		return current;
	}
	
	/**
	 * 将权限用户转为当前登录用户.
	 * @param user AuthUser
	 * @author chanlong(陈龙)
	 * @param sessionid 
	 * @param token 
	 * @param valueOperations 
	 * @date 2017年6月12日 上午9:34:01
	 */
	public static Session toCurrentUser(final String token, final String session, final AuthUser user, final ValueOperations<String, List<Map<String, Object>>> operations) {
		Session current = new Session();
		current.setToken(token);
		current.setSession(session);
		current.setAccount(new Account(user));
		current.setUserinfo(new UserInfo(user));
		current.setBusinessKey(user.getBusinessKey());
		current.setPermissions(operations);
		return current;
	}
	
	/**
	 * 从session中获取当前登录用户.
	 * @param session HttpSession
	 * @author chanlong(陈龙)
	 * @date 2017年6月12日 上午9:34:43
	 */
	public static Session getCurrentUser(final HttpSession session) {
		Object sessionId = session.getAttribute(AuthUser.class.getName());
		return StringUtil.isEmpty(sessionId) ? null : (Session) session.getAttribute(String.valueOf(sessionId));
	}
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public UserInfo getUserinfo() {
		return userinfo;
	}

	public void setUserinfo(UserInfo userinfo) {
		this.userinfo = userinfo;
	}

	public Set<Resource> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<Resource> permissions) {
		this.permissions = permissions;
	}
	
	/* 设置权限 */
	private void setPermissions(ValueOperations<String, List<Map<String, Object>>> operations) {
		this.getAccount().getRoles().forEach(role -> this.addPermission(operations.get(role)));
	}
	
	/* 添加权限（类型转换）*/
	private Session addPermission(final List<Map<String, Object>> resources) {
		if (resources != null) resources.stream().forEach(item -> {
			Resource resource = Resource.translate(item);
			if (resource.isParent()) permissions.add(resource);
			else addPermission(resource, permissions);
		});
		return this;
	}
	
	/* 添加权限（多级递归）*/
	public void addPermission(Resource resource, Set<Resource> resources) {
		resources.parallelStream().forEach(item -> {
			if (resource.getPid().equals(item.getId())) item.addChildren(resource);
			else if (item.hasChildren()) addPermission(resource, item.getChildren());
		});
	}

	private Session() { }
	
	@JSONField(serialize = false)
	private String businessKey;
	
	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
}
