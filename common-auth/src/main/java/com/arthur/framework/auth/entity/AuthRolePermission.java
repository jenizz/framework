package com.arthur.framework.auth.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity @Table(name = "tb_auth_role_permission")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class AuthRolePermission extends CommonEntityUUID<AuthRolePermission> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 6370564458414428695L;

	@OneToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	private AuthRole role;
	
	@OneToOne(cascade = { CascadeType.REFRESH, CascadeType.REMOVE }, fetch = FetchType.LAZY)
	@JoinColumn(name = "permission_id")
	private AuthPermission permission;

	public AuthRole getRole() {
		return role;
	}

	public void setRole(AuthRole role) {
		this.role = role;
	}

	public AuthPermission getPermission() {
		return permission;
	}

	public void setPermission(AuthPermission permission) {
		this.permission = permission;
	}
	
}
