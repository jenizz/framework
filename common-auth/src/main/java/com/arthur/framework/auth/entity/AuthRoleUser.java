package com.arthur.framework.auth.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity @Table(name = "tb_auth_role_user")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class AuthRoleUser extends CommonEntityUUID<AuthRoleUser> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -6393172563637263618L;

	@OneToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	private AuthRole role;
	
	@OneToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private AuthUser user;

	public AuthRole getRole() {
		return role;
	}

	public void setRole(AuthRole role) {
		this.role = role;
	}

	public AuthUser getUser() {
		return user;
	}

	public void setUser(AuthUser user) {
		this.user = user;
	}
}
