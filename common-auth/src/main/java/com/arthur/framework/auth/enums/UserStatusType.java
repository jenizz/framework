package com.arthur.framework.auth.enums;

/**
 * <p>描述: 用户状态类型 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月28日 下午12:46:31
 * @version 1.0.2017
 */
public enum UserStatusType {

	LOCKED(0), 		  // 锁定
	ENABLED(1), 	  // 开启
	EXPIRED(2), 	  // 过期
	INCERTIFICATE(3); // 证书过期

	private Integer status;

	private UserStatusType(Integer status) {
		this.status = status;
	}

	public boolean compareTo(final Integer status) {
		return this.status == status;
	}
}
