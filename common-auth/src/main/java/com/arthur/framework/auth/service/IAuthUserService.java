package com.arthur.framework.auth.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * 扩展Spring Security UserDetailService接口
 * <p>描述: 仅用于统一命名 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月4日 上午9:45:51
 * @version 1.0.2017
 */
public interface IAuthUserService extends UserDetailsService {

}
