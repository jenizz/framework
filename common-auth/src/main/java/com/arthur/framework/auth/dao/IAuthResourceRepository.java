package com.arthur.framework.auth.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

public interface IAuthResourceRepository extends CommonRepositoryAware<AuthResource, String> {

	/**
	 * 根据角色ID、资源key、操作码查找权限.
	 * @param roleId 角色ID
	 * @param resourceType 资源Type
	 * @param operationCode 操作码
	 * @author chanlong(陈龙)
	 * @date 2017年5月15日 上午10:21:36
	 */
	@Query("select resource from AuthRolePermission o left join o.role role left join o.permission permission left join permission.resource resource left join permission.operation operation where resource.status = 1 and role.id = :role and operation.code = 'visible' order by resource.level, resource.serial")
	List<AuthResource> findAll(@Param("role") final String roleId) throws Exception;
}
