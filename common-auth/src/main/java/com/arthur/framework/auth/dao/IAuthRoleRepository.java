package com.arthur.framework.auth.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

public interface IAuthRoleRepository extends CommonRepositoryAware<AuthRole, String> {

	@Query("select a.role from AuthRoleUser a left join a.user u where u.id = :id")
	List<AuthRole> findAllByUser(@Param("id") final String uid);
}
