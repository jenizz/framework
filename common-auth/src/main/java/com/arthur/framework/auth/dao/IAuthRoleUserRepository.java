package com.arthur.framework.auth.dao;

import com.arthur.framework.auth.entity.AuthRoleUser;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

public interface IAuthRoleUserRepository extends CommonRepositoryAware<AuthRoleUser, String> {

}
