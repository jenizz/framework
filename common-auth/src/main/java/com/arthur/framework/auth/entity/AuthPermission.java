package com.arthur.framework.auth.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>描述: 权限表 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月27日 下午5:16:10
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_auth_permission")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class AuthPermission extends CommonEntityUUID<AuthPermission> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 281853338292793495L;

	// 资源
	@OneToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private AuthResource resource;

	// 操作
	@OneToOne(cascade = { CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private AuthOperation operation;

	public AuthPermission() { }
	
	public AuthPermission(String id) {
		this.setId(id);
	}
	
	public AuthResource getResource() {
		return resource;
	}

	public void setResource(AuthResource resource) {
		this.resource = resource;
	}

	public AuthOperation getOperation() {
		return operation;
	}

	public void setOperation(AuthOperation operation) {
		this.operation = operation;
	}
}
