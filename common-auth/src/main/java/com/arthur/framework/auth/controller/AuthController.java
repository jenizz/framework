package com.arthur.framework.auth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.arthur.framework.auth.entity.AuthUser;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private HttpServletRequest request;
	
	@RequestMapping(path = "/cookie", method = RequestMethod.GET)
	public Boolean cookie(@RequestParam String token) {
		HttpSession session = request.getSession(false);
		try {
			String _token = String.valueOf(session.getAttribute(AuthUser.class.getName()));
			return token.equalsIgnoreCase(_token);
		}catch (NullPointerException e) {
			return false;
		}
	}
}
