package com.arthur.framework.auth.entity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import com.arthur.framework.auth.enums.ResourceType;
import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.ui.vo.able.TreeUIAble;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p><b>描述</b>: 资源 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月16日 下午4:43:24
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_auth_resource")
@JsonIgnoreProperties(value = { "pk", "mapper", "handler", "hibernateLazyInitializer" })
public class AuthResource extends CommonEntityUUID<AuthResource> implements TreeUIAble {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -7948941861135060581L;

	// 父级资源
	@Column(name = "pid_")
    private String pid;
    
	// 资源KEY
	@Column(name = "key_", nullable = false, unique = true)
	private String key;
	
	// 资源URL
	@Column(name = "url_")
	private String url;
	
	// 资源图标
	@Column(name = "icon_")
	private String icon;
	
	// 资源类型（系统、模块、页面、菜单、控件、链接、文件、数据库）
	@Enumerated(EnumType.STRING)
	@Column(name = "type_")
	private ResourceType type;
	
	// 资源名称
	@Column(name = "name_")
	private String name;
	
	// 资源描述
	@Column(name = "desc_")
	private String desc;
	
	// 资源树级别
	@Column(name = "level_")
	private Integer level;
	
	// 排序号
	@Column(name = "serial_")
	private Integer serial;
	
	// 状态
	@Column(name = "status_")
	private Integer status;
	
	// 子级资源
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.REMOVE, CascadeType.REFRESH }, fetch = FetchType.LAZY, mappedBy = "pid")
	@OrderBy("serial")
	@PropertyIgnore
	private List<AuthResource> children;
	
	@Override
	public Map<String, Object> getTreeData(){
		Map<String, Object> map = new LinkedHashMap<>();
		map.put(KEY_TITLE, this.getName());
		map.put(KEY_FOLDER, this.hasChildren());
		map.put(KEY_ATTRIBUTES, this.getAttribute());
		return map;
	}
	
	public AuthResource() { 
		this.setStatus(StatusType.ACTIVITY.value());
	}
	
	public AuthResource(String id) {
		this.setId(id);
	}
	
	public AuthResource(StatusType status) {
		this.setStatus(status.value());
	}
	
	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public ResourceType getType() {
		return type;
	}

	public void setType(ResourceType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<AuthResource> getChildren() {
		return children;
	}

	public void setChildren(List<AuthResource> children) {
		this.children = children;
	}
	
	public AuthResource addChildren(AuthResource resource) {
		if (children == null) children = new ArrayList<>();
		children.add(resource);
		return this;
	}

	public boolean hasChildren() {
		return getChildren() != null && !getChildren().isEmpty();
	}
}
