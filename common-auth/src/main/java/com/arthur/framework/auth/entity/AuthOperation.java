package com.arthur.framework.auth.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.arthur.framework.auth.enums.OperationCode;
import com.arthur.framework.auth.enums.OperationType;
import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.orm.enums.StatusType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p><b>描述</b>: 操作 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月16日 下午4:37:12
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_auth_operation")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class AuthOperation extends CommonEntityUUID<AuthOperation> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 7464142989063333563L;

	// 类型
	@Enumerated(EnumType.STRING)
	@Column(name = "type_")
	private OperationType type;
	
	// 编码
	@Enumerated(EnumType.STRING)
	@Column(name = "code_")
	private OperationCode code;
	
	// 名称
	@Column(name = "name_")
	private String name;
	
	// 描述
	@Column(name = "desc_")
	private String desc;
	
	// 排序号
	@Column(name = "serial_")
	private Integer serial;
	
	// 状态
	@Column(name = "status_")
	private Integer status;
	
	public AuthOperation() { }
	
	public AuthOperation(String id) {
		this.setId(id);
	}
	
	public AuthOperation(StatusType status) {
		this.setStatus(status.value());
	}
	
	public OperationType getType() {
		return type;
	}

	public void setType(OperationType type) {
		this.type = type;
	}

	public OperationCode getCode() {
		return code;
	}

	public void setCode(OperationCode code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
