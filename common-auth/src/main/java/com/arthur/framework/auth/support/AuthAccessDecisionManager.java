package com.arthur.framework.auth.support;

import java.util.Collection;
import java.util.Iterator;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * <p><b>描述</b>: 访问决策 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月21日 上午11:15:26
 * @version 1.0.2017
 */
public class AuthAccessDecisionManager implements AccessDecisionManager {

	/**
	 * 访问决策
	 * @see org.springframework.security.access.AccessDecisionManager#decide(org.springframework.security.core.Authentication, java.lang.Object, java.util.Collection)
	 * @author chanlong(陈龙)
	 * @date 2017年3月24日 上午8:53:47
	 */
	@Override
	public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configs) throws AccessDeniedException, InsufficientAuthenticationException {
		System.err.println("=== AuthAccessDecisionManager ===");
		if (null == configs || configs.size() <= 0) return;
		
		for (Iterator<ConfigAttribute> iter = configs.iterator(); iter.hasNext();) {
			ConfigAttribute config = iter.next();
			String needrole = config.getAttribute();
			for (GrantedAuthority authority : authentication.getAuthorities()) {
				if (needrole.trim().equalsIgnoreCase(authority.getAuthority())) {
					return;
				}
			}
		}
		throw new AccessDeniedException("没有权限, 拒绝访问!");
	}

	@Override
	public boolean supports(ConfigAttribute attribute) {
		return true;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

}
