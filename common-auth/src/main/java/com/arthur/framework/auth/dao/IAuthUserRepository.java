package com.arthur.framework.auth.dao;

import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.orm.service.repository.CommonRepositoryAware;

public interface IAuthUserRepository extends CommonRepositoryAware<AuthUser, String> {

}
