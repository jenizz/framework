package com.arthur.framework.auth.support;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.stereotype.Component;

import com.arthur.framework.auth.entity.AuthLogger;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.auth.enums.LoggerType;
import com.arthur.framework.auth.model.Session;
import com.arthur.framework.auth.service.IAuthLoggerService;

@Component
public class AuthLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
	
	private static final Logger log = LoggerFactory.getLogger(AuthLogoutSuccessHandler.class);
	
	@Autowired
	private IAuthLoggerService logger;
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		try {
			if (authentication != null) {
				AuthUser user = (AuthUser) authentication.getPrincipal();
				log.info("find user '{}' is success", user.getUsername());
				
				// 设置当前用户
				Session current = Session.toCurrentUser(user);

				// 记录登出日志
				logger.saveOrUpdate(new AuthLogger(LoggerType.LOGOUT_SUCCESS, request.getRemoteAddr(), request.getRequestURL().toString(), current));
			}
		} catch (Exception e) {
			throw new RuntimeException("performation AuthLogoutSuccessHandler.onLogoutSuccess is error : {}", e);
		} finally {
			super.onLogoutSuccess(request, response, authentication);
		}
	}

}
