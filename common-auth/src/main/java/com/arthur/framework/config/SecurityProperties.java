package com.arthur.framework.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.arthur.framework.auth.support.AuthSecurityConfigConstant;

@Component
@ConfigurationProperties(prefix = "security.configs")
public class SecurityProperties {
	
	private String[] ignored;
	
	private String[] statics;
	
	private String usernameParameter;
	
	private String passwordParameter;
	
	private String loginUrl;
	
	private String logoutUrl;
	
	private Integer locked;

	public String[] getIgnored() {
		return ignored == null ? new String[] { AuthSecurityConfigConstant.DEF_IGNORED } : ignored;
	}

	public void setIgnored(String[] ignored) {
		this.ignored = ignored;
	}
	
	public String[] getStatics() {
		return statics == null ? new String[] { AuthSecurityConfigConstant.DEF_STATICS } : statics;
	}
	
	public void setStatics(String[] statics) {
		this.statics = statics;
	}

	public String getUsernameParameter() {
		return usernameParameter == null ? AuthSecurityConfigConstant.DEF_USERNAME_PARAMETER : usernameParameter;
	}

	public void setUsernameParameter(String usernameParameter) {
		this.usernameParameter = usernameParameter;
	}

	public String getPasswordParameter() {
		return passwordParameter == null ? AuthSecurityConfigConstant.DEF_PASSWORD_PARAMETER : passwordParameter;
	}

	public void setPasswordParameter(String passwordParameter) {
		this.passwordParameter = passwordParameter;
	}

	public String getLoginUrl() {
		return loginUrl == null ? AuthSecurityConfigConstant.DEF_LOGIN_URL : loginUrl;
	}

	public void setLoginUrl(String loginUrl) {
		this.loginUrl = loginUrl;
	}

	public String getLogoutUrl() {
		return logoutUrl == null ? AuthSecurityConfigConstant.DEF_LOGOUT_URL : logoutUrl;
	}

	public void setLogoutUrl(String logoutUrl) {
		this.logoutUrl = logoutUrl;
	}

	public Integer getLocked() {
		return locked == null ? AuthSecurityConfigConstant.DEF_USER_LOCKED : locked;
	}

	public void setLocked(Integer locked) {
		this.locked = locked;
	}
	
}
