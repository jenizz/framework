package com.arthur.framework.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.arthur.framework.auth.service.AuthUserService;
import com.arthur.framework.auth.support.AuthAccessDecisionManager;
import com.arthur.framework.auth.support.AuthLogoutSuccessHandler;
import com.arthur.framework.auth.support.AuthRequestFailureHandler;
import com.arthur.framework.auth.support.AuthRequestSuccessHandler;
import com.arthur.framework.auth.support.AuthSecurityCaptchaFilter;
import com.arthur.framework.auth.support.AuthSecurityEndecrypt;
import com.arthur.framework.auth.support.AuthSecurityInterceptor;
import com.arthur.framework.auth.support.AuthSecurityMetadataSource;

/**
 * <p>描述: Spring Security 配置 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月23日 下午3:34:22
 * @version 1.0.2017
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableConfigurationProperties(SecurityProperties.class)
@Order(org.springframework.boot.autoconfigure.security.SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	private static final Logger log = LoggerFactory.getLogger(SecurityConfiguration.class);

	@Autowired
	private SecurityProperties properties;

	@Autowired
	private AuthUserService service;
	
	@Autowired
	private AuthRequestFailureHandler authFailureHandler;
	
	@Autowired
	private AuthRequestSuccessHandler authSuccessHandler;
	
	@Autowired
	private AuthLogoutSuccessHandler logoutSuccessHandler;
	
	@Autowired
	private AuthSecurityMetadataSource metadataSource;
	
	@Autowired
	private Environment env;
	
	@Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(properties.getStatics());
    }
	
	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(service).passwordEncoder(new PasswordEncoder() {
			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				return AuthSecurityEndecrypt.match(rawPassword, encodedPassword);
			}

			@Override
			public String encode(CharSequence rawPassword) {
				return AuthSecurityEndecrypt.encrypt(rawPassword);
			}
		});
		auth.eraseCredentials(false);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.addFilterBefore(authSecurityCaptchaFilter(), UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(authSecurityInterceptor(), FilterSecurityInterceptor.class)
			.authorizeRequests()
				.antMatchers(properties.getIgnored()).permitAll()
				.requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
				.anyRequest().authenticated()
				.and()
			.formLogin()
				.loginProcessingUrl(properties.getLoginUrl()).permitAll()
				.usernameParameter(properties.getUsernameParameter())
				.passwordParameter(properties.getPasswordParameter())
				.successHandler(authSuccessHandler)
				.failureHandler(authFailureHandler)
				.and()
			.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher(properties.getLogoutUrl())).permitAll()
				.logoutSuccessHandler(logoutSuccessHandler)
				.deleteCookies("remember-me", "JSESSIONID")
				.invalidateHttpSession(true)
				.and()
			.rememberMe()
				.and()
			.csrf()
				.ignoringAntMatchers(properties.getIgnored())
				.csrfTokenRepository(csrfTokenRepository())
				.and()
			.cors().configurationSource(corsConfigurationSource());
	}
	
	@Bean
	protected AuthSecurityInterceptor authSecurityInterceptor() throws Exception {
		AuthSecurityInterceptor interceptor = new AuthSecurityInterceptor();
		interceptor.setMetadataSourceManager(metadataSource);
		interceptor.setAccessDecisionManager(accessDecisionManagerBean());
		interceptor.setAuthenticationManager(authenticationManagerBean());
		return interceptor;
	}
	
	@Bean
	protected AuthSecurityCaptchaFilter authSecurityCaptchaFilter() throws Exception {
		AuthSecurityCaptchaFilter filter = new AuthSecurityCaptchaFilter();
		filter.setAuthenticationManager(authenticationManagerBean());
		filter.setAuthenticationFailureHandler(authFailureHandler);
		filter.setAuthenticationSuccessHandler(authSuccessHandler);
		filter.setFilterProcessesUrl(properties.getLoginUrl());
		return filter;
	}

	@Bean
	protected AccessDecisionManager accessDecisionManagerBean() {
		return new AuthAccessDecisionManager();
	}
	
	/**
	 * 跨域.
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年3月27日 下午1:55:48
	 */
	@Bean
	protected CorsConfigurationSource corsConfigurationSource() {
		long maxage = 600;
		try {maxage = env.getProperty("server.session.timeout", Long.class);} catch (Throwable e) {log.info("application configuration's property 'server.session.timeout' is invalid settings.");}
		
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		config.setMaxAge(maxage);
		
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}
	
	private CsrfTokenRepository csrfTokenRepository() {
	    HttpSessionCsrfTokenRepository repository = new HttpSessionCsrfTokenRepository();
	    repository.setHeaderName("X-XSRF-TOKEN");
	    return repository;
	}
}
