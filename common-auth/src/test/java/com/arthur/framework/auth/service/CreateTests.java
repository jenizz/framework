package com.arthur.framework.auth.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.arthur.framework.auth.service.create.OperationTest;
import com.arthur.framework.auth.service.create.PermissionTest;
import com.arthur.framework.auth.service.create.ResourceTest;
import com.arthur.framework.auth.service.create.RoleTest;
import com.arthur.framework.auth.service.create.UserTest;

@RunWith(Suite.class)
@SuiteClasses({ 
	ResourceTest.class, 
	OperationTest.class, 
	PermissionTest.class, 
	RoleTest.class, 
	UserTest.class,
	AuthorizeTest.class,
	AssignTest.class,
})
public class CreateTests {

	/* 依次执行：
	 * 1、ResourceTest（初始化资源数据）
	 * 2、OperationTest（初始化操作数据）
	 * 3、PermissionTest（初始化权限数据-关联资源与操作）
	 * 4、RoleTest（初始化角色数据）
	 * 5、UserTest（初始化用户数据）
	 * 6、AuthorizeTest（关联角色及权限）
	 * 7、AssignTest（关联角色及用户）
	 */
}
