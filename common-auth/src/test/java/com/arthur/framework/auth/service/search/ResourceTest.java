package com.arthur.framework.auth.service.search;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.util.JsonUtil;

@SpringBootTest(classes = AuthApplication.class)
public class ResourceTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	/** 查询资源树 */
	@Test
	public void test() throws Exception {
		List<AuthResource> resource = service.findAllResources("RES_SYSTEM_DEMO");
		System.out.println(JsonUtil.serialize(resource));
	}

}
