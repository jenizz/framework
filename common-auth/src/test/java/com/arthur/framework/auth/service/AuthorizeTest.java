package com.arthur.framework.auth.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthPermission;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.entity.AuthRolePermission;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class AuthorizeTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	@Test @Rollback(false)
	public void test() throws Exception {
		List<AuthRolePermission> roles = create("ROLE_SYSTEM");
		List<AuthRolePermission> result = service.authorize(roles);
		assertNotNull(result);
	}
	
	/**
	 * 模拟数据结构：
	 * [{
	 * 	'role':{
	 * 		'id':'<AuthRole.id>'
	 * 	},
	 * 	'permission':{
	 * 		'id':'<AuthPermission.id>'
	 * 	}
	 * },...]
	 * 此结构数据作为service接口参数。
	 */
	private List<AuthRolePermission> create(final String roleKey) throws Exception {
		// 获取角色信息
		AuthRole role = service.getRoleByKey(roleKey);
		
		// 获取权限列表
		List<AuthPermission> permissions = service.findAllPermissions();
		
		// 构造模拟数据
		JSONArray array = new JSONArray();
		permissions.forEach(permission -> {
			JSONObject json = new JSONObject();
			json.put("role", new AuthRole(role.getId()));
			json.put("permission", new AuthPermission(permission.getId()));
			array.fluentAdd(json);
		});
		
		service.updateCache();
		return array.toJavaList(AuthRolePermission.class);
	}
}
