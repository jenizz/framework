package com.arthur.framework.auth.service.search;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthPermission;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.util.JsonUtil;

@SpringBootTest(classes = AuthApplication.class)
public class RelationTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	@Test
	public void test() throws Exception {
		AuthRole role = service.getRoleByKey("ROLE_SYSTEM");
		List<AuthPermission> permissions = service.findAllPermissions(role);
		System.out.println(JsonUtil.serialize(permissions));
	}
}
