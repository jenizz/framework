package com.arthur.framework.auth.service.update;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.auth.service.IAuthUserService;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.util.string.StringUtil;

@SpringBootTest(classes = AuthApplication.class)
public class UserTest extends AbstractEnvironment {

	@Autowired
	private IAuthUserService service;
	
	/** 更新用户表 */
	@Test @Rollback(false)
	public void test() throws Exception {
		AuthUser user = create().save();
		assertNotNull(user);
		assertNotNull(user.getBusinessKey());
	}
	
	/**
	 * 模拟数据结构：
	 *	{
			"id":"<AuthUser.id>",
			"businessKey":"<用户ID>" // 要更新的字段
		}
	 */
	private AuthUser create() throws Exception {
		AuthUser user = (AuthUser) service.loadUserByUsername("superadmin");
		
		// 构造JSON数据
		JSONObject json = new JSONObject();
		json.put("id", user.getId());
		json.put("businessKey", StringUtil.uuid());
		return json.toJavaObject(AuthUser.class);
	}
}
