package com.arthur.framework.auth.service.search;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;

import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.service.IAuthUserService;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class UserTest extends AbstractEnvironment {

	@Autowired
	private IAuthUserService service;
	
	@Test
	public void test() {
		UserDetails detail = service.loadUserByUsername("superadmin");
		System.out.println(detail.getAuthorities());
	}

}
