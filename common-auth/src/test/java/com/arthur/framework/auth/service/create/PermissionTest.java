package com.arthur.framework.auth.service.create;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthOperation;
import com.arthur.framework.auth.entity.AuthPermission;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.enums.ResourceType;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class PermissionTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	/** 创建权限 */
	@Test @Rollback(false)
	public void test() throws Exception {
		// 批量保存权限
		List<AuthPermission> permissions = createAll();
		service.saveAllPermissions(permissions);
		assertNotNull(permissions);
		
		// 新增资源时保存权限
		AuthPermission permission = createOne();
		service.savePermission(permission);
		assertNotNull(permission);
	}
	
	/**
	 * 模拟数据结构：
	 * {
	 * 	 resource: {
	 * 		"key": "RES_MENU_LOGGER",
	 * 		"url": "/authdemo/system/logger",
	 * 		"type": "MENU",
	 * 		"name": "日志管理",
	 * 		"desc": "日志管理",
	 * 		"level": 2,
	 * 		"serial": 0,
	 * 		"status": 1
	 * 	 },
	 * 	 operation: {
	 * 		"id":"<AuthOperation.id>"
	 * 	 }
	 * }
	 * 此结构数据作为service接口参数。
	 * 用于新增资源时，同时新增权限。
	 * 注：操作为预定义的数据字典。
	 */
	private AuthPermission createOne() throws Exception {
		AuthResource module = service.getResourceByKey("RES_MOUDLE_AUDITOR");
		assertNotNull("module为空", module);
		
		AuthResource resource = new AuthResource();
		resource.setPid(module.getId());
		resource.setKey("RES_MENU_LOGGER");
		resource.setUrl("/authdemo/system/logger");
		resource.setType(ResourceType.MENU);
		resource.setName("日志管理");
		resource.setDesc("日志管理");
		resource.setLevel(2);
		resource.setSerial(0);
		resource.setStatus(StatusType.ACTIVITY.value());
		
		// 构造JSON数据
		AuthOperation operation = service.getOperationByCode("visible");
		JSONObject json = new JSONObject();
		json.put("resource", resource);
		json.put("operation", new AuthOperation(operation.getId()));
		
		return json.toJavaObject(AuthPermission.class);
	}
	
	/**
	 * 模拟数据结构：
	  	[{
			"resource":{
				"id":"<AuthResource.id>"
			},
			"operation":{
				"id":"<AuthOperation.id>"
			}
		}, {
			"resource":{
				"id":"<AuthResource.id>"
			},
			"operation":{
				"id":"<AuthOperation.id>"
			}
		},...]
	* 此结构数据作为service接口参数。
	* 用于关联已有资源和操作
	*/
	private List<AuthPermission> createAll() throws Exception{
		AuthResource system = service.getResourceByKey("RES_SYSTEM_DEMO");
		assertNotNull("system为空", system);
		
		List<AuthResource> resources = system.getChildren();
		assertNotNull("resources为空", resources);
		
		List<AuthOperation> operations = service.findOperationDict();
		assertNotNull("operations为空", operations);
		
		List<AuthPermission> permissions = new ArrayList<>();
		AuthOperation operation = operations.get(0);
		
		resources.parallelStream().filter(resource -> !"RES_MOUDLE_SYSTEM".equals(resource.getKey())).forEach(resource -> {
			permissions.add(create(resource.getId(), operation.getId()));
		});
		
		return permissions;
	}
	
	private AuthPermission create(final String rid, final String oid) {
		JSONObject json = new JSONObject();
		json.put("resource", new AuthResource(rid));
		json.put("operation", new AuthOperation(oid));
		return json.toJavaObject(AuthPermission.class);
	}
}
