package com.arthur.framework.auth.service.create;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.enums.ResourceType;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class ResourceTest extends AbstractEnvironment {

	/** 创建资源 */
	@Test @Rollback(false)
	public void test() throws Exception {
		AuthResource _demo = create(ResourceType.SYSTEM.name(), null, "*");
		_demo.save();
		assertNotNull(_demo.getId());
		
		/*
		AuthResource _system = create(ResourceType.MOUDLE.name(), "system", _demo.getId());
		_system.save();
		assertNotNull(_system.getId());
		
		AuthResource _auditor = create(ResourceType.MOUDLE.name(), "auditor", _demo.getId());
		_auditor.save();
		assertNotNull(_auditor.getId());
		
		AuthResource _security = create(ResourceType.MOUDLE.name(), "security", _demo.getId());
		_security.save();
		assertNotNull(_security.getId());*/
	}
	
	/**
	 * 模拟数据结构：
	 * {
	 * 	 key: "RES_SYSTEM_DEMO",
	 * 	 url: "/authdemo",
	 * 	 type: "SYSTEM",	// 枚举类型ResourceType
	 * 	 name: "用户权限测试系统",
	 * 	 desc: "用户权限测试系统",
	 * 	 level: 0
	 * 	 serial: 0,
	 * 	 status: 1,
	 * }
	 */
	public AuthResource create(final String type, final String key, final String pid){
		return CreateResource.valueOf(type).createJson(key, pid).toJavaObject(AuthResource.class);
	}
	
	private enum CreateResource {
		SYSTEM {
			public JSONObject createJson(final String key, final String pid) {
				JSONObject json = new JSONObject();
				json.put("key", "RES_SYSTEM_CREDIT");
				json.put("url", "/credit");
				json.put("type", "SYSTEM");
				json.put("name", "信用平台");
				json.put("desc", "信用平台");
				json.put("level", 0);
				json.put("serial", 0);
				json.put("status", 1);
				return json;
			}
		},
		MOUDLE {
			public JSONObject createJson(final String key, final String pid) {
				JSONObject json = JSON.parseObject(resmap.get(key));
				json.put("pid", pid);
				json.put("type", "MOUDLE");
				json.put("level", 1);
				json.put("status", 1);
				return json;
			}
		};
		
		public abstract JSONObject createJson(final String key, final String pid);
		
		public static Map<String, String> resmap = new HashMap<>();
		
		static {
			resmap.put("system", "{'key':'RES_MOUDLE_SYSTEM', 'url':'/authdemo/system', 'name':'系统管理', 'desc':'系统管理', 'serial':'0'}");
			resmap.put("auditor", "{'key':'RES_MOUDLE_AUDITOR', 'url':'/authdemo/auditor', 'name':'日志审计', 'desc':'日志审计', 'serial':'1'}");
			resmap.put("security", "{'key':'RES_MOUDLE_SECURITY', 'url':'/authdemo/security', 'name':'安全管理', 'desc':'安全管理', 'serial':'2'}");
		}
	}
}
