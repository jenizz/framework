package com.arthur.framework.auth.service.create;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthOperation;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class OperationTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	private List<AuthOperation> entities = new ArrayList<>();
	
	/** 新增操作 */
	@Test @Rollback(false)
	public void test() throws Exception {
		List<AuthOperation> operations = service.saveAllOperations(entities);
		assertNotNull(operations);
	}

	/**
	 * 模拟数据结构：
	 * [{
			"code":"visible",
			"desc":"资源是否可见",
			"name":"可见的",
			"serial":0,
			"status":1,
			"type":"resource"
		},
		{
			"code":"enabled",
			"desc":"资源是否可操作",
			"name":"启用的",
			"serial":1,
			"status":1,
			"type":"resource"
		},...]
	 * 此结构数据作为service接口参数。
	 */
	@Before
	public void init(){
		entities.add(create("visible"));
		entities.add(create("enabled"));
		entities.add(create("insert"));
		entities.add(create("delete"));
		entities.add(create("update"));
		entities.add(create("search"));
	}
	
	private AuthOperation create(final String key) {
		JSONObject json = JSON.parseObject(opmap.get(key));
		json.put("status", 1);
		return json.toJavaObject(AuthOperation.class);
	}
	
	public static Map<String, String> opmap = new HashMap<>();
	
	static {
		opmap.put("visible", "{'code':'visible', 'type':'element', 'name':'可见的', 'desc':'资源是否可见', 'serial':'0'}");
		opmap.put("enabled", "{'code':'enabled', 'type':'element', 'name':'启用的', 'desc':'资源是否可操作', 'serial':'1'}");
		opmap.put("insert", "{'code':'insert', 'type':'database', 'name':'新增', 'desc':'新增数据', 'serial':'2'}");
		opmap.put("delete", "{'code':'delete', 'type':'database', 'name':'删除', 'desc':'删除数据', 'serial':'3'}");
		opmap.put("update", "{'code':'update', 'type':'database', 'name':'更新', 'desc':'更新数据', 'serial':'4'}");
		opmap.put("search", "{'code':'search', 'type':'database', 'name':'查询', 'desc':'查询数据', 'serial':'5'}");
	}
}
