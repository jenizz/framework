package com.arthur.framework.auth.service.create;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class RoleTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	/** 新增角色 */
	@Test @Rollback(false)
	public void test() throws Exception {
		List<AuthRole> roles = service.saveAllRoles(create());
		assertNotNull(roles);
	}

	/**
	 * 模拟数据结构：
	 * [{
			"key":"ROLE_SECURITY",
			"name":"安全管理员",
			"desc":"安全管理员",
			"serial":0,
			"status":1
		},
		{
			"key":"ROLE_AUDITOR",
			"name":"审计管理员",
			"desc":"审计管理员",
			"serial":1,
			"status":1
		},...]
	 * 此结构数据作为service接口参数。
	 */
	private List<AuthRole> create() throws Exception{
		List<AuthRole> roles = new ArrayList<AuthRole>();
		roles.add(create("ROLE_SECURITY", "安全管理员", 0));
		roles.add(create("ROLE_AUDITOR", "审计管理员", 1));
		roles.add(create("ROLE_SYSTEM", "系统管理员", 2));
		return roles;
	}
	
	private AuthRole create(final String key, final String name, final Integer serial) {
		JSONObject json = new JSONObject();
		json.put("key", key);
		json.put("name", name);
		json.put("desc", name);
		json.put("serial", serial);
		json.put("status", StatusType.ACTIVITY.value());
		return json.toJavaObject(AuthRole.class);
	} 
}
