package com.arthur.framework.auth.service.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthOperation;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class OperationTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	/** 更新操作表 */
	@Test @Rollback(false)
	public void test() throws Exception {
		AuthOperation operation = create().save();
		assertNotNull(operation);
		assertEquals(operation.getDesc(), "是否可以查询数据");
	}
	
	/**
	 * 模拟数据结构：
	 *	{
			"id":"<AuthOperation.id>",
			"desc":"是否可以查询数据" 	// 要更新的字段
		}
	 */
	private AuthOperation create() throws Exception {
		AuthOperation operation = service.getOperationByCode("search");
		
		JSONObject json = new JSONObject();
		json.put("id", operation.getId());
		json.put("desc", "是否可以查询数据");
		return json.toJavaObject(AuthOperation.class);
	}
	
}
