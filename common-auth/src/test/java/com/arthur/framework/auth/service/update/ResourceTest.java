package com.arthur.framework.auth.service.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthResource;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class ResourceTest extends AbstractEnvironment {
	
	@Autowired
	private IAuthService service;
	
	/** 更新资源表 */
	@Test @Rollback(false)
	public void test() throws Exception {
		AuthResource resource = create().save();
		assertNotNull(resource);
		assertEquals(resource.getDesc(), "日志管理菜单");
	}
	
	/**
	 * 模拟数据结构：
	 *	{
			"id":"<AuthResource.id>",
			"desc":"日志管理菜单" 	// 要更新的字段
		}
	 */
	private AuthResource create() throws Exception {
		AuthResource resource = service.getResourceByKey("RES_MENU_LOGGER");
		
		JSONObject json = new JSONObject();
		json.put("id", resource.getId());
		json.put("desc", "日志管理菜单");
		return json.toJavaObject(AuthResource.class);
	}
}
