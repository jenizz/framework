package com.arthur.framework.auth.service.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.service.IAuthService;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class RoleTest extends AbstractEnvironment {

	@Autowired
	private IAuthService service;
	
	/** 更新角色表 */
	@Test @Rollback(false)
	public void test() throws Exception {
		AuthRole role = create().save();
		assertNotNull(role);
		assertEquals(role.getDesc(), "管理系统的组织用户信息等");
	}
	
	/**
	 * 模拟数据结构：
	 *	{
			"id":"<AuthRole.id>",
			"desc":"管理系统的组织用户信息等" 	// 要更新的字段
		}
	 */
	private AuthRole create() throws Exception {
		AuthRole role = service.getRoleByKey("ROLE_SYSTEM");
		
		// 构造JSON数据
		JSONObject json = new JSONObject();
		json.put("id", role.getId());
		json.put("desc", "管理系统的组织用户信息等");
		return json.toJavaObject(AuthRole.class);
	}
}
