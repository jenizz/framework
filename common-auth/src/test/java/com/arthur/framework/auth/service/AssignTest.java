package com.arthur.framework.auth.service;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthRole;
import com.arthur.framework.auth.entity.AuthRoleUser;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class AssignTest extends AbstractEnvironment {
	
	@Autowired
	private IAuthService service;
	
	@Autowired
	private IAuthUserService users;
	
	/** 角色用户关联 */
	@Test @Rollback(false)
	public void test() throws Exception {
		List<AuthRoleUser> entities = create();
		List<AuthRoleUser> result = service.assign(entities);
		assertNotNull(result);
	}
	
	/**
	 * 模拟数据结构：
	 * 	[{
			"user":{
				"id":"<AuthUser.id>"
			}
			"role":{
				"id":"<AuthRole.id>"
			}
		},...]
	 * 此结构数据作为service接口参数。
	 */
	private List<AuthRoleUser> create() throws Exception {
		AuthRole role = service.getRoleByKey("ROLE_SYSTEM");
		AuthUser user = (AuthUser) users.loadUserByUsername("superadmin");
		
		JSONObject json = new JSONObject();
		json.put("user", new AuthUser(user.getId()));
		json.put("role", new AuthRole(role.getId()));
		
		JSONArray array = new JSONArray();
		array.fluentAdd(json);
		
		return array.toJavaList(AuthRoleUser.class);
	}

}
