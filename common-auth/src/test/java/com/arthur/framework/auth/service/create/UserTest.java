package com.arthur.framework.auth.service.create;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.AuthApplication;
import com.arthur.framework.auth.entity.AuthUser;
import com.arthur.framework.auth.support.AuthSecurityEndecrypt;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.test.AbstractEnvironment;

@SpringBootTest(classes = AuthApplication.class)
public class UserTest extends AbstractEnvironment {

	/** 新增用户 */
	@Test @Rollback(false)
	public void test() throws Exception {
		AuthUser user = create().save();
		assertNotNull(user);
	}
	
	/**
	 * 模拟数据结构：
	 * {
	 * 	 username:"superadmin",
	 * 	 password:"000000",
	 * 	 serial:0,
	 * 	 status:1
	 * }
	 */
	private AuthUser create() {
		JSONObject json = new JSONObject();
		json.put("username", "superadmin");
		json.put("password", AuthSecurityEndecrypt.encrypt("000000"));
		json.put("serial", 0);
		json.put("status", StatusType.ACTIVITY.value());
		return json.toJavaObject(AuthUser.class);
	}
}
