package com.arthur.framework.workflow.service;

import java.util.HashMap;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.arthur.framework.Application;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.workflow.dao.IWorkflowDao;
import com.arthur.framework.workflow.entity.IWorkflowEntity;

@SpringBootTest(classes = Application.class)
public class WorkflowServiceTest extends AbstractEnvironment {

	@Autowired
	IWorkflowService<IWorkflowEntity> service;
	
	@Autowired
	RepositoryService repository;
	
	@Autowired
    IdentityService identity;
	
	@Autowired
	RuntimeService runtime;
	
	@Autowired
	FormService form;
	
	@Autowired
	TaskService task;
	
	@Autowired
	IWorkflowDao dao;
	
	@Test
	public void test() {
		
		Map<String, Object> variables = new HashMap<>();
		variables.put("userId", "ff8080815af48198015af481b81e0010");
		variables.put("applyUserId", "ff8080815af48198015af481b81e0010");
		startProcess("leave", "ff8080815b5c6613015b5ff60a4a0006", variables);
	}
	
	public void startProcess(final String definitionKey, final String businessKey, final Map<String, Object> variables){
		runtime.startProcessInstanceByKey(definitionKey, businessKey, variables);
	}

}
