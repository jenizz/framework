package com.arthur.framework.workflow.entity;

import java.util.Map;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import com.arthur.framework.orm.entity.CommonEntityAware;

public interface IWorkflowEntity extends CommonEntityAware {

	String getUserId();

	void setUserId(String userId);

	Task getTask();

	void setTask(Task task);

	Map<String, Object> getVariables();

	void setVariables(Map<String, Object> variables);

	ProcessInstance getProcessInstance();

	void setProcessInstance(ProcessInstance processInstance);

	ProcessDefinition getProcessDefinition();

	void setProcessDefinition(ProcessDefinition processDefinition);

	HistoricProcessInstance getHistoricProcessInstance();

	void setHistoricProcessInstance(HistoricProcessInstance historicProcessInstance);
}
