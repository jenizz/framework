package com.arthur.framework.workflow.service;

import java.util.Map;

import org.activiti.engine.runtime.ProcessInstance;

import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.workflow.entity.IWorkflowEntity;

public interface IWorkflowService<E extends IWorkflowEntity> {

	ProcessInstance startProcess(final String processDefinitionKey, final String userId) throws Exception;
	
	ProcessInstance startProcess(final String processDefinitionKey, final E e, final Map<String, Object> variables) throws Exception;
	
	void completeTask(String taskId, Map<String, Object> variables) throws Exception;

	Pagination<E> findTodoTasks(final Class<E> _class, final String userId, int firstResult, int maxResults) throws Exception;

}
