package com.arthur.framework.workflow.entity;

import java.util.Map;

import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import com.arthur.framework.orm.entity.AbstractCommonEntity;

public abstract class AbstractWorkflowEntity<E> extends AbstractCommonEntity<E> implements IWorkflowEntity {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -261508552570255578L;

	private String userId;
	
	// 流程任务
    private Task task;

    // 流程变量
    private Map<String, Object> variables;

    // 当前流程实例
    private ProcessInstance processInstance;

    // 流程定义
    private ProcessDefinition processDefinition;
    
    // 历史流程实例
    private HistoricProcessInstance historicProcessInstance;

    @Override
	public String getUserId() {
		return userId;
	}

    @Override
	public void setUserId(String userId) {
		this.userId = userId;
	}

    @Override
	public Task getTask() {
		return task;
	}

    @Override
	public void setTask(Task task) {
		this.task = task;
	}

    @Override
	public Map<String, Object> getVariables() {
		return variables;
	}

    @Override
	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
	}

    @Override
	public ProcessInstance getProcessInstance() {
		return processInstance;
	}

    @Override
	public void setProcessInstance(ProcessInstance processInstance) {
		this.processInstance = processInstance;
	}

    @Override
	public ProcessDefinition getProcessDefinition() {
		return processDefinition;
	}

    @Override
	public void setProcessDefinition(ProcessDefinition processDefinition) {
		this.processDefinition = processDefinition;
	}

    @Override
	public HistoricProcessInstance getHistoricProcessInstance() {
		return historicProcessInstance;
	}

    @Override
	public void setHistoricProcessInstance(HistoricProcessInstance historicProcessInstance) {
		this.historicProcessInstance = historicProcessInstance;
	}
}
