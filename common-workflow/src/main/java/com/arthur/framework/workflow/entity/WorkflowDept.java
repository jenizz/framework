package com.arthur.framework.workflow.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;

@Entity @Table(name = "tb_act_dept")
public class WorkflowDept extends CommonEntityUUID<WorkflowDept> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -6252267048599006531L;

	private String oid;
	
	private String code;
	
	private String name;
	
	private List<WorkflowUser> users;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WorkflowUser> getUsers() {
		return users;
	}

	public void setUsers(List<WorkflowUser> users) {
		this.users = users;
	}
}
