package com.arthur.framework.workflow.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.engine.FormService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.form.StartFormData;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.workflow.dao.IWorkflowDao;
import com.arthur.framework.workflow.entity.IWorkflowEntity;

@Service @Transactional
public class WorkflowService implements IWorkflowService<IWorkflowEntity> {
	
	private static final Logger log = LoggerFactory.getLogger(WorkflowService.class);

	@Autowired
	private RepositoryService repository;
	
	@Autowired
    private IdentityService identity;
	
	@Autowired
	private RuntimeService runtime;
	
	@Autowired
	private FormService form;
	
	@Autowired
	private TaskService task;
	
	@Autowired
	private IWorkflowDao dao;
	
	@Override
	public ProcessInstance startProcess(final String definitionKey, final String userId) throws Exception {
		ProcessDefinition definition = repository.createProcessDefinitionQuery().processDefinitionKey(definitionKey).latestVersion().singleResult();
		String processDefinitionId = definition.getId();
		
		Map<String, String> result = new HashMap<>();
		try{
			identity.setAuthenticatedUserId(userId);
			StartFormData _form = form.getStartFormData(processDefinitionId);
			List<FormProperty> properties = _form.getFormProperties();
			for (FormProperty property : properties) {
				result.put(property.getId(), property.getValue());
	        }
		}finally {
			identity.setAuthenticatedUserId(null);
		}
		
		return form.submitStartFormData(processDefinitionId, result);
	}
	
	/**
	 * 启动流程.
	 * @param variables
	 * @author chanlong(陈龙)
	 * @date 2017年4月18日 上午9:01:36
	 */
	@Override
	public ProcessInstance startProcess(final String definitionKey, final IWorkflowEntity entity, final Map<String, Object> variables) {
		ProcessInstance process = null;
		try {
			dao.insert(entity);
			log.debug("save entity: {}", entity);
			
			String businessKey = String.valueOf(entity.getPK());
			process = runtime.startProcessInstanceByKey(definitionKey, businessKey, variables);
			
			String pid = process.getId();
			entity.setProcessInstance(process);
			
			log.debug("start process of {key={}, bk={}, pid={}, variables={}}", new Object[]{"leave", businessKey, pid, variables});
		} catch (Exception e) {
			log.error("start process is error: {}", e);
		} finally {
			identity.setAuthenticatedUserId(null);
        }
		return process;
	}
	
	/**
	 * 获取参与者任务列表.
	 * @param assignee
	 * @return
	 * @author chanlong(陈龙)
	 * @throws Exception 
	 * @date 2017年4月18日 上午9:05:13
	 */
	@Override
	public Pagination<IWorkflowEntity> findTodoTasks(final Class<IWorkflowEntity>  _class, final String userId, final int firstResult, final int maxResults) throws Exception {
		List<IWorkflowEntity> results = new ArrayList<IWorkflowEntity>();
		
		TaskQuery taskQuery =task.createTaskQuery().taskCandidateOrAssigned(userId);
		List<Task> tasks = taskQuery.listPage(firstResult, maxResults);
		for (Task task : tasks) {
			String pid = task.getProcessInstanceId();
			ProcessInstance processInstance = runtime.createProcessInstanceQuery().processInstanceId(pid).active().singleResult();
			if (processInstance == null) continue;
			
			String businessKey = processInstance.getBusinessKey();
			if (businessKey == null) continue;
			
			IWorkflowEntity entity = dao.findByBusinessKey(_class, businessKey);
			entity.setTask(task);
			entity.setProcessInstance(processInstance);
			entity.setProcessDefinition(getProcessDefinition(processInstance.getProcessDefinitionId()));
            results.add(entity);
		}
		
		return new Pagination<>(taskQuery.count(), results);
	}
	
	/**
	 * 完成任务.
	 * @param taskId
	 * @param variables
	 * @author chanlong(陈龙)
	 * @date 2017年4月18日 上午9:04:33
	 */
	@Override
	public void completeTask(final String taskId, final Map<String, Object> variables) {
		task.complete(taskId, variables);
	}
	
	protected ProcessDefinition getProcessDefinition(final String processDefinitionId) {
        ProcessDefinition processDefinition = repository.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();
        return processDefinition;
    }
}
