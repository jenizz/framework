package com.arthur.framework.workflow.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;

@Entity @Table(name = "tb_act_user")
public class WorkflowUser extends CommonEntityUUID<WorkflowUser> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -1602148983356710253L;

	private String email;
	
	private String username;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
