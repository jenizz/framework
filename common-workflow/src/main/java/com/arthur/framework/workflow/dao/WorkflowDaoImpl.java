package com.arthur.framework.workflow.dao;

import org.springframework.stereotype.Repository;

import com.arthur.framework.orm.service.dao.CommonDaoAdapter;
import com.arthur.framework.workflow.entity.IWorkflowEntity;

@Repository
public class WorkflowDaoImpl extends CommonDaoAdapter<IWorkflowEntity> implements IWorkflowDao {

	private static final String PROCESS_FIELD_KEY = "processInstanceId";
	
	@Override
	public IWorkflowEntity findByBusinessKey(final Class<IWorkflowEntity> _class, final String businessKey) throws Exception{
		return this.findOne(_class, PROCESS_FIELD_KEY, businessKey);
	}
}
