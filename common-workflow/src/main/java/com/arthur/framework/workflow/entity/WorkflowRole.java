package com.arthur.framework.workflow.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;

/**
 * 
 * <p>描述: 用户角色信息 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年4月19日 下午12:49:17
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_act_role")
public class WorkflowRole extends CommonEntityUUID<WorkflowRole> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -8919171738706142730L;

	private String key;
	
	private String name;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
