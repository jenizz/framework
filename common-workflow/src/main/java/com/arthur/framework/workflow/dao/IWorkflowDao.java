package com.arthur.framework.workflow.dao;

import com.arthur.framework.orm.service.dao.CommonDaoAware;
import com.arthur.framework.workflow.entity.IWorkflowEntity;

public interface IWorkflowDao extends CommonDaoAware<IWorkflowEntity> {

	IWorkflowEntity findByBusinessKey(final Class<IWorkflowEntity> _class, final String businessKey) throws Exception;

}
