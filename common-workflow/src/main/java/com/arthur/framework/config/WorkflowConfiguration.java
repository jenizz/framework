package com.arthur.framework.config;

import java.util.HashMap;
import java.util.Map;

import org.activiti.explorer.servlet.ExplorerApplicationServlet;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.boot.ProcessEngineConfigurationConfigurer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.arthur.framework.workflow.service.IWorkflowUserService;

@Configuration
public class WorkflowConfiguration implements ProcessEngineConfigurationConfigurer {
	
	@Autowired
	private IWorkflowUserService userService;
	
	@Bean
	public ServletRegistrationBean explorerApplicationServlet(){
		Map<String, String> parameters = new HashMap<>();
		parameters.put("widgetset", "org.activiti.explorer.CustomWidgetset");
		
		ServletRegistrationBean registration = new ServletRegistrationBean(new ExplorerApplicationServlet(), "/ui/*", "/VAADIN/*");
		registration.setLoadOnStartup(1);
		registration.setInitParameters(parameters);
		
		return registration;
		
	}

	@Override
	public void configure(SpringProcessEngineConfiguration processEngineConfiguration) {
		Map<Object, Object> beans = new HashMap<>();
		beans.put("userService", userService);
		
		processEngineConfiguration.setBeans(beans);
	}
}
