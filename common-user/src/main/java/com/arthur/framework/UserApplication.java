package com.arthur.framework;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.arthur.framework.orm.service.repository.CommonRepositoryFactoryBean;

@EnableCaching
@EnableJpaRepositories(basePackages = { 
	"com.arthur.framework.**.dao"
}, repositoryFactoryBeanClass = CommonRepositoryFactoryBean.class)
@EnableTransactionManagement
@SpringBootApplication
public class UserApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}
}
