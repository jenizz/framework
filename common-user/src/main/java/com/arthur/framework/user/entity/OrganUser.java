package com.arthur.framework.user.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p>描述: 部门用户关联表 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月22日 下午2:00:08
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_sys_organ_user")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class OrganUser extends CommonEntityUUID<OrganUser> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 8350511454101935546L;

	// 是否兼职
	private Integer partTime;
		
	// 用户
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH }, fetch = FetchType.LAZY)
	private User user;
	
	// 组织ID
	private String organId;
	
	public OrganUser() {
		this.partTime = 0;
	}

	public Integer isPartTime() {
		return partTime;
	}

	public void setPartTime(Integer partTime) {
		this.partTime = partTime;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getOrganId() {
		return organId;
	}

	public void setOrganId(String organId) {
		this.organId = organId;
	}
}
