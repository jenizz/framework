package com.arthur.framework.user.service;

import java.util.List;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.orm.service.bo.CommonBoAware;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.entity.OrganUser;
import com.arthur.framework.user.entity.User;
import com.arthur.framework.user.model.OrganModel;
import com.arthur.framework.user.model.UserModel;

public interface IOrganUserService extends CommonBoAware<CommonEntityAware> {

	/**
	 * 获取用户信息.
	 * @param example 用户实体对象
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月8日 下午12:25:37
	 */
	User getUser(final User example) throws Exception;
	
	/**
	 * 获取用户明细-无级联.
	 * @param example 机构实体对象
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月25日 上午10:25:46
	 */
	UserModel getUserModel(final User example) throws Exception;
	
	/**
	 * 根据机构ID，获取用户列表.
	 * @param example 机构实体对象
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月25日 上午10:25:46
	 */
	List<UserModel> findUsers(final Organ example) throws Exception;
	
	/**
	 * 根据机构ID，获取用户列表.
	 * @param example 机构实体对象
	 * @param limit 分页限制
	 * @param offset 偏移量
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年11月16日 上午8:41:24
	 */
	Pagination<User> findUsers(final OrganUser example) throws Exception;
	
	/**
	 * 获取部门信息.
	 * @param example 机构实体对象
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月25日 上午10:25:46
	 */
	Organ getOrgan(final Organ example) throws Exception;
	
	/**
	 * 获取部门信息-无级联.
	 * @param example 组织实体对象
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月8日 上午11:10:25
	 */
	OrganModel getOrganModel(final Organ example) throws Exception;
	
	/**
	 * 获取上级组织信息.
	 * @param example
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 下午1:35:21
	 */
	OrganModel getParent(final Organ example) throws Exception;
	
	/**
	 * 获取组织列表.
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年11月16日 下午5:35:50
	 */
	List<OrganModel> findOrgans() throws Exception;
	
	/**
	 * 根据用户ID，获取组织列表.
	 * @param example
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 下午1:56:21
	 */
	List<OrganModel> findOrgans(final User user) throws Exception;
	
	/**
	 * 获取组织列表.
	 * @param example
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月8日 下午4:56:45
	 */
	Pagination<Organ> findOrgans(final Organ organ) throws Exception;

	/**
	 * 删除组织用户关联.
	 * @param organuser
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月8日 下午12:16:05
	 */
	void delete(final Iterable<OrganUser> organuser) throws Exception;
}
