package com.arthur.framework.user.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.arthur.framework.orm.service.repository.CommonRepositoryAware;
import com.arthur.framework.user.entity.Organ;

public interface IOrganRepository extends CommonRepositoryAware<Organ, String> {

	Organ getByChildrenId(final String childrenId);

	List<Organ> findByRelationUserId(final String userId);
	
	@Query("from Organ o where o.level = 0 and o.pid is null")
	Organ getRootOrgan();
}
