package com.arthur.framework.user.entity;

import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * <p><b>描述</b>: 用户 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月15日 下午5:03:43
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_sys_user")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class User extends CommonEntityUUID<User> {

	private static final long serialVersionUID = -7076601262807387222L;

	// 用户编码(工号)
	@Column(name = "code_")
	private String code;
	
	// 用户姓名
	@Column(name = "name_")
	private String name;
	
	// 用户邮箱
	@Column(name = "email_")
	private String email;
	
	// 用户手机
	@Column(name = "mobile_")
	private String mobile;
	
	// 用户账号
	@Column(name = "account_", nullable = false, unique = true)
	private String account;
	
	// 固定电话
	@Column(name = "telphone_")
	private String telphone;
	
	// 排序号
	@Column(name = "serial_")
	private Integer serial;
	
	// 状态
	@Column(name = "status_")
	private Integer status;
	
	// 部门用户关联
	@PropertyIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private Set<OrganUser> relation;
	
	@Transient
	private String roles;
	
	public static User newInstance(final Map<String, Object> options) {
		return ReflectUtil.copyProperties(new User(), options);
	}
	
	public User() { }
	
	public User(String id) {
		this.setId(id);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getTelphone() {
		return telphone;
	}

	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Set<OrganUser> getRelation() {
		return relation;
	}

	public void setRelation(Set<OrganUser> relation) {
		this.relation = relation;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}
}
