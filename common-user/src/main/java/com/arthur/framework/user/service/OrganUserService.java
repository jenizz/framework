package com.arthur.framework.user.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.enums.FunctionType;
import com.arthur.framework.orm.helper.SQLHelper;
import com.arthur.framework.orm.helper.builder.SQLBuilderField;
import com.arthur.framework.orm.helper.builder.SQLBuilderTable;
import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.orm.service.bo.CommonBoAdapter;
import com.arthur.framework.user.dao.IOrganRepository;
import com.arthur.framework.user.dao.IOrganUserRepository;
import com.arthur.framework.user.dao.IUserRepository;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.entity.OrganUser;
import com.arthur.framework.user.entity.User;
import com.arthur.framework.user.model.OrganModel;
import com.arthur.framework.user.model.UserModel;

/**
 * <p>描述: 组织及用户的相关服务 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月8日 上午11:09:48
 * @version 1.0.2017
 */
@Service @Transactional(isolation = Isolation.READ_COMMITTED)
public class OrganUserService extends CommonBoAdapter<CommonEntityAware> implements IOrganUserService {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private IUserRepository userDao;
	
	@Autowired
	private IOrganRepository organDao;
	
	@Autowired
	private IOrganUserRepository organUserDao;
	
	@Override
	public User getUser(final User example) throws Exception {
		return userDao.findOne(Example.of(example));
	}
	
	@Override
	public UserModel getUserModel(final User example) throws Exception {
		return new UserModel().toModel(userDao.findOne(Example.of(example)));
	}
	
	@Override
	public List<UserModel> findUsers(final Organ example) throws Exception {
		List<User> users = userDao.findByRelationOrganId(example.getId());
		return users.parallelStream().reduce(new ArrayList<UserModel>(), (models, user) -> {
			models.add(new UserModel().toModel(user)); return models;
		}, (left, right) -> left);
	}
	
	@Override
	public Pagination<User> findUsers(final OrganUser example) throws Exception {
		String oid = example.getOrganId();
		User user = example.getUser();
		
		return userDao.findAll(getSQLHelper(oid, user), user.getOffset(), user.getLimit());
	}
	
	@Override
	public Organ getOrgan(final Organ example) throws Exception  {
		return organDao.findOne(Example.of(example));
	}
	
	@Override
	public OrganModel getOrganModel(final Organ example) throws Exception {
		return new OrganModel().toModel(organDao.findOne(Example.of(example)));
	}
	
	@Override
	public OrganModel getParent(final Organ example) throws Exception {
		return new OrganModel().toModel(organDao.getByChildrenId(example.getId()));
	}

	@Override
	public List<OrganModel> findOrgans() throws Exception {
		Organ example = new Organ();
		example.setLevel(0);
		
		List<OrganModel> result = new ArrayList<>();
		List<Organ> organs = organDao.findAll(Example.of(example), new Sort("serial"));
		organs.parallelStream().sorted(Comparator.comparing(Organ::getSerial)).forEach(root -> result.add(setChildren(root, new OrganModel(root), true)));
		return result;
	}
	
	@Override
	public List<OrganModel> findOrgans(final User user) throws Exception {
		List<Organ> organs = organDao.findByRelationUserId(user.getId());
		return organs.parallelStream().reduce(new ArrayList<OrganModel>(), (models, organ) -> {
			models.add(new OrganModel().toModel(organ)); return models;
		}, (left, right) -> left);
	}
	
	@Override
	public Pagination<Organ> findOrgans(final Organ organ) throws Exception {
		return organDao.findAll(getSQLHelper(organ), organ.getOffset(), organ.getLimit());
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void delete(final Iterable<OrganUser> entities) throws Exception {
		organUserDao.delete(entities);
	}
	
	private SQLHelper getSQLHelper(final Organ organ) {
		String sql = SQLHelper.createBuilder(true).table().name("tb_sys_organ").alias("a")
							  .joins()
							  	.addJoins(new SQLBuilderTable("tb_sys_organ", "b").on("b", "id").eq("a", "pid_"))
							  .field()
							  	.addField("a", "*", null)
							  	.addField("b", "name_", "parent")
							  .where()
							  	.addWhere("a", "code_", OperatorType.LIKE, organ.getCode())
							  	.addWhere("a", "name_", OperatorType.LIKE, organ.getName())
							  	.addWhere("a", "type_", OperatorType.EQ, organ.getType() != null ? organ.getType().value() : null)
							  	.addWhere("a", "status_", OperatorType.EQ, 1)
							  .order()
							  	.addOrder("a", "level_", "asc")
							  	.addOrder("a", "serial_", "asc")
							  .select();
		
		return SQLHelper.newInstance(sql);
	}
	
	// 获取用户列表查询的SQLHelper
	private SQLHelper getSQLHelper(final String oid, final User user) {
		String dbtype = env.getProperty("common.dbtype").toLowerCase();
		
		String subsql = SQLHelper.createBuilder(true).table().name("tb_sys_user").alias("a")
													 .joins()
													 	.addJoins(new SQLBuilderTable("tb_sys_organ_user", "b").on("a", "id").eq("user_id"))
													 	.addJoins(new SQLBuilderTable("tb_auth_user", "c").on("a", "id").eq("business_key"))
													 .field()
													 	.addField("a", "*", null)
													 	.addField("b", "organ_id", null)
													 	.addField("c", "status_", "state")
													 .where()
													 	.addWhere("b", "organ_id", OperatorType.EQ, oid)
													 	.addWhere("a", "status_", OperatorType.EQ, user.getStatus())
													 	.addWhere("a", "name_", OperatorType.LIKE, user.getName())
													 .select();
		
		String groups = SQLHelper.createBuilder(true).table().name("tb_sys_user").alias("a")
													 .joins()
													 	.addJoins(new SQLBuilderTable("tb_auth_user", "c").on("a", "id").eq("business_key"))
													 	.addJoins(new SQLBuilderTable("tb_auth_role_user", "d").on("c", "id").eq("user_id"))
													 	.addJoins(new SQLBuilderTable("tb_auth_role", "e").on("e", "id").eq("d", "role_id"))
													 .field()
													 	.addField("a", "id", null)
													 	.addField(FunctionType.GROUP_CONCAT.db(dbtype), new SQLBuilderField("e", "name_", "roles"))
													 .group()
													 	.addGroup("a", "id")
													 .select();

		String sql = SQLHelper.createBuilder(true).table().name("(" + subsql + ")").alias("o")
							  .joins()
							  	.addJoins(new SQLBuilderTable("(" + groups + ")", "t").on("o", "id").eq("id"))
							  .field()
							 	.addField("o", "*", null)
							 	.addField("t", "roles", null)
							  .where()
							  	.addWhere("t", "roles", OperatorType.LIKE, user.getRoles())
							  .select();

		return SQLHelper.newInstance(sql);
	}
	
	// 设置OrganModel's Children
	private OrganModel setChildren(final Organ parent, final OrganModel model, final boolean root){
		if(parent.hasChildren()){
			List<Organ> children = parent.getChildren();
			children.stream().forEach(child -> {
				OrganModel om = new OrganModel().toModel(child);
				om.setType(child.getType());
				model.addChild(setChildren(child, om, false), root);
			});
		}
		return model;
	}
}
