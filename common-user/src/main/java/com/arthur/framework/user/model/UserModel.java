package com.arthur.framework.user.model;

import com.arthur.framework.orm.model.AbstractCommonModel;
import com.arthur.framework.user.entity.User;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <p><b>描述</b>: 用户 </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月15日 下午5:03:43
 * @version 1.0.2017
 */
public class UserModel extends AbstractCommonModel<UserModel, User> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 758093525851973746L;
	
	private String id;
	private String code;
	@JsonProperty("title")
	private String name;
	private String email;
	private String mobile;
	private String account;
	private String telphone;
	private Integer serial;
	private Integer status;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public Integer getSerial() {
		return serial;
	}
	public void setSerial(Integer serial) {
		this.serial = serial;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
}
