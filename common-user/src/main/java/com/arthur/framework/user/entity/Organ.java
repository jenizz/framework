package com.arthur.framework.user.entity;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.arthur.framework.orm.entity.CommonEntityUUID;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.user.support.OrganType;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <p>描述: 机构表 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月22日 上午10:37:25
 * @version 1.0.2017
 */
@Entity @Table(name = "tb_sys_organ")
@JsonIgnoreProperties(value = { "pk", "handler", "hibernateLazyInitializer" })
public class Organ extends CommonEntityUUID<Organ> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 232090583378527047L;

	@Column(name = "pid_")
	private String pid;
	
	@Enumerated(EnumType.ORDINAL)
	@Column(name = "type_")
	private OrganType type;
	
	@Column(name = "code_", unique = true, nullable = false)
	private String code;
	
	@Column(name = "name_")
	@JsonProperty("title")
	private String name;
	
	@Column(name = "desc_")
	private String desc;
	
	// 机构树级别
	@Column(name = "level_")
	private Integer level;
		
	// 排序号
	@Column(name = "serial_")
	private Integer serial;
	
	// 状态
	@Column(name = "status_")
	private Integer status;
	
	@PropertyIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "pid")
	@OrderBy("serial")
	private List<Organ> children;
	
	// 部门用户关联
	@JsonIgnore
	@PropertyIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "organId")
	private Set<OrganUser> relation;
	
	@Transient
	private String parent;
	
	public static Organ newInstance(final Map<String, Object> options) {
		return ReflectUtil.copyProperties(new Organ(), options);
	}
	
	public boolean hasChildren() {
		return this.getChildren() != null && !this.getChildren().isEmpty();
	}

	public Organ() { 

	}
	
	public Organ(String id) {
		this.setId(id);
	}
	
	public Organ(StatusType status) {
		this.setStatus(status.value());
	}
	
	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public OrganType getType() {
		return type;
	}

	public void setType(OrganType type) {
		this.type = type;
	}
	
	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getSerial() {
		return serial;
	}

	public void setSerial(Integer serial) {
		this.serial = serial;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public List<Organ> getChildren() {
		return children;
	}

	public void setChildren(List<Organ> children) {
		this.children = children;
	}

	public Set<OrganUser> getRelation() {
		return relation;
	}

	public void setRelation(Set<OrganUser> relation) {
		this.relation = relation;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}
}
