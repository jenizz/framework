package com.arthur.framework.user.dao;

import java.util.List;

import com.arthur.framework.orm.service.repository.CommonRepositoryAware;
import com.arthur.framework.user.entity.User;

public interface IUserRepository extends CommonRepositoryAware<User, String> {

	List<User> findByRelationOrganId(final String organId);
}
