package com.arthur.framework.user.support;

/**
 * <p>描述: 组织类型 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月8日 上午10:18:32
 * @version 1.0.2017
 */
public enum OrganType {
	ADMIN(0), NORMAL(1), ORGAN(2), GROUP(3), DEPTS(4);

	private int value;

	private OrganType(int value) {
		this.value = value;
	}

	public int value() {
		return value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
