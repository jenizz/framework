package com.arthur.framework.user.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.orm.model.AbstractCommonModel;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.support.OrganType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "id", "pid", "type", "code", "desc", "title", "level", "serial", "status", "children" })
public class OrganModel extends AbstractCommonModel<OrganModel, Organ> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -489274151039468982L;

	private String id;
	private String pid;
	private String type;
	private String code;
	private String desc;
	@JsonProperty("title") @JSONField(name="title")
	private String name;
	private Integer level;
	private Integer serial;
	private Integer status;
	private List<OrganModel> children;
	
	public OrganModel() {

	}
	
	public OrganModel(Organ organ) {
		this.toModel(organ);
		this.setType(organ.getType());
		this.children = new ArrayList<>();
		this.children.add(new OrganModel(organ, OrganType.ADMIN.name()));
		this.children.add(new OrganModel(organ, OrganType.NORMAL.name()));
	}
	
	public OrganModel(Organ organ, String type) {
		String text = textsmap.get(type);
		this.toModel(organ);
		this.setType(organ.getType());
		this.setDesc(text);
		this.setName(text);
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public String getType() {
		return type;
	}
	public void setType(OrganType type) {
		this.type = type.name();
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Integer getSerial() {
		return serial;
	}
	public void setSerial(Integer serial) {
		this.serial = serial;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public List<OrganModel> getChildren() {
		return children;
	}
	public void setChildren(List<OrganModel> children) {
		this.children = children;
	}
	public void addChild(final OrganModel child, final boolean root) {
		if (root) {
			String type = child.getType();
			int index = indexmap.containsKey(type) ? indexmap.get(type) : OrganType.NORMAL.value();
			this.children.get(index).addChild(child, false);
		} else {
			if (this.children == null) this.children = new ArrayList<>();
			this.children.add(child);
		}
	}
	
	@JsonIgnore @JSONField(serialize = false)
	private Map<String, Integer> indexmap = new HashMap<>();
	@JsonIgnore @JSONField(serialize = false)
	private Map<String, String> textsmap = new HashMap<>();
	{
		indexmap.put(OrganType.ADMIN.name(), 0);
		indexmap.put(OrganType.NORMAL.name(), 1);
		textsmap.put(OrganType.ADMIN.name(), "管理部门");
		textsmap.put(OrganType.NORMAL.name(), "业务部门");
	}
}
