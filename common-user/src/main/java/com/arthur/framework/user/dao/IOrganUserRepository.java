package com.arthur.framework.user.dao;

import com.arthur.framework.orm.service.repository.CommonRepositoryAware;
import com.arthur.framework.user.entity.OrganUser;

public interface IOrganUserRepository extends CommonRepositoryAware<OrganUser, String> {

}
