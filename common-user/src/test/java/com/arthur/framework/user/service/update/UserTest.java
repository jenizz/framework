package com.arthur.framework.user.service.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.UserApplication;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.user.entity.User;
import com.arthur.framework.user.service.IOrganUserService;

@SpringBootTest(classes = UserApplication.class)
public class UserTest extends AbstractEnvironment {

	@Autowired
	private IOrganUserService service;
	
	@Test @Rollback(false)
	public void test() throws Exception {
		// 更新用户信息
		User user = createUser().save();
		assertNotNull(user);
		assertEquals("superadmin", user.getAccount());
	}
	
	/**
	 * 模拟数据结构：
	 * {
			"id":"<User.id>",
			"account":"superadmin",
			...
		}
	 */
	private User createUser() throws Exception {
		// 获取User信息
		User example = new User();
		example.setCode("UE100001");
		example = service.getUser(example);
				
		JSONObject json = new JSONObject();
		json.put("id", example.getId());
		json.put("account", "superadmin");
		return json.toJavaObject(User.class);
	}
	
}
