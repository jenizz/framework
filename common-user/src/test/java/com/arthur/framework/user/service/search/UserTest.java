package com.arthur.framework.user.service.search;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.UserApplication;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.entity.User;
import com.arthur.framework.user.model.OrganModel;
import com.arthur.framework.user.service.IOrganUserService;

@SpringBootTest(classes = UserApplication.class)
public class UserTest extends AbstractEnvironment {

	@Autowired
	private IOrganUserService service;
	
	@Test @Rollback(false)
	public void test() throws Exception {
		Parameters params = new Parameters(Organ.class, 0, 0);
		Pagination<Organ> list = new Organ().query(params);
		System.out.println(list);
		// 根据用户ID查找部门
		//List<OrganModel> organs = service.findOrgans(createUser());
		//System.out.println(JsonUtil.serialize(organs));
		
		// 根据部门ID查找用户
		//List<UserModel> users = service.findUsers(createOrgan(organs.get(0)));
		//System.out.println(JsonUtil.serialize(users));
	}
	
	/**
	 * 模拟数据结构：
	 * {
	 * 	 "id":"<User.id>"
	 * }
	 * @throws Exception 
	 */
	public User createUser() throws Exception {
		// 获取用户信息
		User user = new User();
		user.setCode("UE100001");
		user = service.getUser(user);
		
		// 构造JSON数据
		JSONObject json = new JSONObject();
		json.put("id", user.getId());
		return json.toJavaObject(User.class);
	}
	
	/**
	 * 模拟数据结构：
	 * {
	 * 	 "id":"<Organ.id>"
	 * }
	 * @throws Exception 
	 */
	public Organ createOrgan(final OrganModel model) throws Exception {
		// 构造JSON数据
		JSONObject json = new JSONObject();
		json.put("id", model.getId());
		return json.toJavaObject(Organ.class);
	}
}
