package com.arthur.framework.user.service.create;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.UserApplication;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.support.OrganType;

@SpringBootTest(classes = UserApplication.class)
public class OrganTest extends AbstractEnvironment {

	@Test @Rollback(false)
	public void test() throws Exception {
		// 新增组织信息（根节点）
		Organ root = createRoot().save();
		assertNotNull(root);
		
		// 新增组织信息（公司）
		Organ organ = createOrgan(root).save();
		assertNotNull(organ);
		
		// 新增组织信息（部门）
		Organ depts = createDepts(organ).save();
		assertNotNull(depts);
	}
	
	/**
	 * 模拟数据结构：
	 * {
			"type":"ORGAN",
			"code":"OSL100000",
			"name":"网安集团",
			"desc":"中国网安集团",
			"level":0,
			"serial":0,
			"status":1
		}
	 */
	private Organ createRoot() {
		JSONObject json = new JSONObject();
		json.put("type", OrganType.ORGAN);
		json.put("code", "OSL100000");
		json.put("name", "网安集团");
		json.put("desc", "中国网安集团");
		json.put("level", 0);
		json.put("serial", 0);
		json.put("status", StatusType.ACTIVITY.value());
		return json.toJavaObject(Organ.class);
	}
	
	/**
	 * 模拟数据结构：
	 * {
			"type":"ORGAN",
			"code":"OSL101000",
			"name":"三零卫士",
			"desc":"上海三零卫士",
			"level":1,
			"serial":0,
			"status":1
		}
	 */
	private Organ createOrgan(final Organ organ) {
		JSONObject json = new JSONObject();
		json.put("pid", organ.getId());
		json.put("type", OrganType.ORGAN);
		json.put("code", "OSL101000");
		json.put("name", "三零卫士");
		json.put("desc", "上海三零卫士");
		json.put("level", 1);
		json.put("serial", 0);
		json.put("status", StatusType.ACTIVITY.value());
		return json.toJavaObject(Organ.class);
	}
	
	/**
	 * 模拟数据结构：
	 * {
			"type":"DEPTS",
			"code":"OSL101001",
			"name":"信用事业部",
			"desc":"信用事业部",
			"level":2,
			"serial":0,
			"status":1
		}
	 */
	private Organ createDepts(final Organ organ) {
		JSONObject json = new JSONObject();
		json.put("pid", organ.getId());
		json.put("type", OrganType.DEPTS);
		json.put("code", "OSL101001");
		json.put("name", "信用事业部");
		json.put("desc", "信用事业部");
		json.put("level", 2);
		json.put("serial", 0);
		json.put("status", StatusType.ACTIVITY.value());
		return json.toJavaObject(Organ.class);
	}
}
