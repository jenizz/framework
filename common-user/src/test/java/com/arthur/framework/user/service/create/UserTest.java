package com.arthur.framework.user.service.create;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.UserApplication;
import com.arthur.framework.orm.enums.StatusType;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.entity.OrganUser;
import com.arthur.framework.user.model.OrganModel;
import com.arthur.framework.user.service.IOrganUserService;

@SpringBootTest(classes = UserApplication.class)
public class UserTest extends AbstractEnvironment {

	@Autowired
	private IOrganUserService service;
	
	@Test @Rollback(false)
	public void test() throws Exception {
		// 在指定组织下，新增用户（同时建立组织与用户间的关联）
		OrganUser relation = create().save();
		
		assertNotNull(relation);
	}
	
	/**
	 * 模拟数据结构：
	 * {
			"organId":"<Organ.id>",
			"partTime":false,
			"user":{
				"code": "UE100001",
				"name": "陈小龙",
				"email": "chen@30wish.net",
				"mobile": "18818818888",
				"serial": 0,
				"status": 1
			}
		}
	 */
	private OrganUser create() throws Exception {
		// 获取Organ信息
		Organ example = new Organ();
		example.setCode("OSL101001");
		OrganModel organ = service.getOrganModel(example);
		
		// 构造User数据
		JSONObject user = new JSONObject();
		user.put("code", "UE100002");
		user.put("name", "陈小龙");
		user.put("email", "chen@30wish.net");
		user.put("mobile", "18818818888");
		user.put("serial", "0");
		user.put("status", StatusType.ACTIVITY.value());
		
		// 构造Relatoin数据
		JSONObject relation = new JSONObject();
		relation.put("user", user);
		relation.put("partTime", false);
		relation.put("organId", organ.getId());
		
		return relation.toJavaObject(OrganUser.class);
	}
	
}
