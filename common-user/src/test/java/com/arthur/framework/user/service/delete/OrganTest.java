package com.arthur.framework.user.service.delete;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.arthur.framework.UserApplication;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.user.entity.OrganUser;

@SpringBootTest(classes = UserApplication.class)
public class OrganTest extends AbstractEnvironment {
	
	@Test @Rollback(false)
	public void test() throws Exception {
		OrganUser ou = new OrganUser();
		ou.setId("ff8080815be6c550015be6c566140003");
		ou.delete();
	}
}
