package com.arthur.framework.user.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.arthur.framework.user.service.create.OrganTest;
import com.arthur.framework.user.service.create.UserTest;

@RunWith(Suite.class)
@SuiteClasses({
	OrganTest.class,
	UserTest.class
})
public class CreateTests {

	/* 依次执行：
	 * 1、OrganTest（初始化组织数据）
	 * 2、UserTest（初始化用户数据）
	 */
}
