package com.arthur.framework.user.service.update;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.UserApplication;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.model.OrganModel;
import com.arthur.framework.user.service.IOrganUserService;

@SpringBootTest(classes = UserApplication.class)
public class OrganTest extends AbstractEnvironment {

	@Autowired
	private IOrganUserService service;
	
	@Test @Rollback(false)
	public void test() throws Exception {
		// 更新机构信息
		Organ organ = createOrgan().save();
		assertNotNull(organ);
		assertEquals("信用相关产品研发", organ.getDesc());
	}
	
	/**
	 * 模拟数据结构：
	 * {
			"id":"<Organ.id>",
			"desc":"信用相关产品研发",
			...
		}
	 */
	private Organ createOrgan() throws Exception {
		// 获取Organ信息
		Organ example = new Organ();
		example.setCode("OSL101001");
		OrganModel organ = service.getOrganModel(example);
				
		JSONObject json = new JSONObject();
		json.put("id", organ.getId());
		json.put("desc", "信用相关产品研发");
		return json.toJavaObject(Organ.class);
	}
	
}
