package com.arthur.framework.user.service.search;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.UserApplication;
import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.test.AbstractEnvironment;
import com.arthur.framework.user.entity.Organ;
import com.arthur.framework.user.model.OrganModel;
import com.arthur.framework.user.service.IOrganUserService;
import com.arthur.framework.util.JsonUtil;

@SpringBootTest(classes = UserApplication.class)
public class OrganTest extends AbstractEnvironment {

	@Autowired
	private IOrganUserService service;
	
	@Test
	public void test() throws Exception{
		// 获取根组织
		OrganModel organ = service.getOrganModel(createQuery());
		System.out.println(JsonUtil.serialize(organ));
		
		// 查找子组织
		Pagination<Organ> organs = service.findOrgans(createQuery(organ, "pid"));
		System.out.println(JsonUtil.serialize(organs));
		
		// 查找父组织
		OrganModel parent = service.getParent(createQuery(organs.getRows().get(0), "id"));
		System.out.println(JsonUtil.serialize(parent));
	}
	
	/**
	 * 模拟数据结构：
	 * {
	 * 	 "pid":"<Parent's Organ.id>"
	 * }
	 */
	public Organ createQuery(final OrganModel organ, final String key) {
		JSONObject json = new JSONObject();
		json.put(key, organ.getId());
		return json.toJavaObject(Organ.class);
	}
	
	public Organ createQuery(final Organ organ, final String key) {
		JSONObject json = new JSONObject();
		json.put(key, organ.getId());
		return json.toJavaObject(Organ.class);
	}
	
	/**
	 * 模拟数据结构：
	 * {
	 * 	 "code":"OSL100000"
	 * }
	 */
	public Organ createQuery() {
		JSONObject json = new JSONObject();
		json.put("code", "OSL100000");
		return json.toJavaObject(Organ.class);
	}
}
