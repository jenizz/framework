package com.arthur.framework.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement 	// 开启事务控制
public class OrmConfiguration {

}
