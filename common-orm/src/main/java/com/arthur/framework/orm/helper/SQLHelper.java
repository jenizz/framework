package com.arthur.framework.orm.helper;

import org.apache.ibatis.jdbc.SQL;

import com.arthur.framework.orm.enums.FunctionType;
import com.arthur.framework.orm.helper.builder.SQLBuilderField;
import com.arthur.framework.orm.helper.builder.SQLBuilderTable;
import com.arthur.framework.orm.helper.config.SQLConfigurerBuilder;
import com.arthur.framework.orm.query.enums.OperatorType;

/**
 * <p>描述: SQL构造器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月10日 下午1:36:25
 * @version 1.0.2017
 */
public class SQLHelper extends AbstractSQLHelper {
	
	/**
	 * 构造插入语句.
	 * @param entity 实体对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午1:36:40
	 */
	public static String toInsert(final Object entity) {
		String[] temps = getValues(entity);
		return new SQL(){{
			INSERT_INTO(getTable(entity)).VALUES(temps[0], temps[1]);
		}}.toString();
	}

	/**
	 * 构造更新语句.
	 * @param entity 实体对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午1:37:01
	 */
	public static String toUpdate(final Object entity) {
		String[] temps = getSets(entity);
		return new SQL(){{
			UPDATE(getTable(entity)).SET(temps);
		}}.toString();
	}

	/**
	 * 构造删除语句.
	 * @param entity 实体对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午1:37:19
	 */
	public static String toDelete(final Object entity) {
		return new SQL(){{
			DELETE_FROM(getTable(entity)).WHERE(SQLHelper.toConditions(entity));
		}}.toString();
	}

	/**
	 * 构造查询语句.
	 * @param table SQL表名构造器实例
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午2:02:52
	 */
	public static String toSelect(final Object entity) {
		return new SQL(){{
			SELECT("*").FROM(SQLHelper.toConditions(entity));
		}}.toString();
	}
	
	/**
	 * 创建SQL配置器.
	 * @param name
	 * @param alias
	 * @param isParallel
	 * @author chanlong(陈龙)
	 * @date 2017年5月22日 上午10:40:27
	 */
	public static SQLConfigurerBuilder createBuilder(boolean isParallel) {
		SQLConfigurerBuilder builder = new SQLConfigurerBuilder();
		builder.setParallel(isParallel);
		return builder;
	}
	
	public static SQLHelper newInstance(final String sql) {
		SQLHelper helper = new SQLHelper(sql);
		return helper;
	}
	
	private String sql;
	
	private SQLHelper(String sql){
		this.sql = sql;
	}
	
	public void setSql(String sql) {
		this.sql = sql;
	}
	
	@Override
	public String getSql() {
		return sql;
	}

	@Override
	public String toString() {
		return this.sql;
	}
	
	public static void main(String[] args) {
		/*
		 *select * from (SELECT a.*, GROUP_CONCAT(e.name_) `roles`
			FROM
				tb_sys_user a
			LEFT JOIN tb_sys_organ_user b ON a.id = b.user_id
			left join tb_auth_user c on a.id = c.business_key
			left join tb_auth_role_user d on c.id = d.user_id
			left join tb_auth_role e on e.id = d.role_id
			WHERE
				b.organ_id = 'OuLi20070529174221891'
			and a.status_ = 1
			and a.name_ like '%张%'
			group by a.id) o 
			where o.roles like '%系统管理员%'
			limit 0, 10
		 */
		String sql = SQLHelper.createBuilder(true)
							   .table()
							   	 .name("tb_sys_user")
							   	 .alias("a")
							   .joins()
							   	 .addJoins(new SQLBuilderTable("tb_sys_organ_user", "b").on("a", "id").eq("user_id"))
							   	 .addJoins(new SQLBuilderTable("tb_auth_user", "c").on("a", "id").eq("business_key"))
							   	 .addJoins(new SQLBuilderTable("tb_auth_role_user", "d").on("c", "id").eq("user_id"))
							   	 .addJoins(new SQLBuilderTable("tb_auth_role", "e").on("e", "id").eq("d", "role_id"))
							   .field()
							   	 .addField(new SQLBuilderField("a", "*", null))
							   	 .addField(FunctionType.GROUP_CONCAT, new SQLBuilderField("e", "name_", "roles"))
							   .where()
							   	 .addWhere("b", "organ_id", OperatorType.EQ, "")
							   .group()
							     .addGroup("a", "id")
							   .select();
		System.out.println(sql);
		
	}
}
