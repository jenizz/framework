package com.arthur.framework.orm.query.support;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * <p><b>描述</b>: 分页 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午11:15:15
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class Pagination<ENTITY> implements Serializable {

	private static final long serialVersionUID = 2959768759418849526L;

	private String total;

	@JsonFormat(pattern="yyyy-MM-dd", timezone="GMT+8")
	private List<ENTITY> rows;
	
	public Pagination() {

	}

	public Pagination(long total, List<ENTITY> rows) {
		this.total = String.valueOf(total);
		this.rows = rows;
	}
	
	public List<ENTITY> getRows() {
		return rows;
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> getRows(Class<T> t) {
		return (List<T>)rows;
	}
	
	public void setRows(List<ENTITY> rows) {
		this.rows = rows;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}
}
