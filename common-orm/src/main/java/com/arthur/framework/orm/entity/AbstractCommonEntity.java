package com.arthur.framework.orm.entity;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.orm.query.enums.ValueType;
import com.arthur.framework.orm.query.mapping.FieldMapping;
import com.arthur.framework.orm.query.mapping.GroupMapping;
import com.arthur.framework.orm.query.mapping.OrderMapping;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.orm.support.Processer;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.arthur.framework.util.string.StringUtil;

import io.swagger.annotations.ApiModelProperty;


/**
 * <p><b>Author</b>: long.chan 2017年2月7日 下午3:23:31 </p>
 * <p><b>ClassName</b>: CommonEntityNOID.java </p> 
 * <p><b>Description</b>: </p> 
 *
 * @param <ID>
 */
@MappedSuperclass
public abstract class AbstractCommonEntity<E> implements CommonEntityAware  {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 6868718716266032062L;
	
	private static final Logger log = LoggerFactory.getLogger(AbstractCommonEntity.class);

	// 分页（每页大小）
	@PropertyIgnore @Transient
	@ApiModelProperty(hidden = true)
	private Integer limit = 0;
	
	// 分页（每页起始记录行号）
	@PropertyIgnore @Transient
	@ApiModelProperty(hidden = true)
	private Integer offset = 0;
	
	// 排序（“字段:排序,字段:排序”）
	@PropertyIgnore @Transient
	@ApiModelProperty(hidden = true)
	private String order;
	
	// 分组（“字段,字段”）
	@PropertyIgnore @Transient
	@ApiModelProperty(hidden = true)
	private String group;
	
	/**
	 * 获取对象属性映射表.
	 * @author chanlong(陈龙)
	 * @date 2017年11月16日 下午12:16:22
	 */
	public Map<String, Object> getMapper() {
		return ReflectUtil.getProperties(this);
	}
		
	@Override
	@SuppressWarnings("unchecked")
	public E save(final String... options) throws Exception {
		return (E) Processer.factory().getBo().saveOrUpdate(this);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public E load(final String... options) throws Exception {
		return (E) Processer.factory().getBo().findOne(this);
	}
	
	@Override
	public boolean delete(final String... options) throws Exception {
		return Processer.factory().getBo().delete(this); 
	}
	
	@Override
	public Pagination<E> query(final String... options) throws Exception {
		return query(ReflectUtil.getProperties(this), options);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Pagination<E> query(final Parameters params, final String... options) throws Exception {
		return (Pagination<E>) Processer.factory().getBo().findAll(params);
	}
	
	@Override
	public Pagination<E> query(final Map<String, Object> params, final String... options) throws Exception {
		// 创建查询模型
		Parameters _params = new Parameters(this.getClass(), limit, offset);
				
		// 设置查询条件
		Set<Entry<String, Object>> entry = params.entrySet();
		for (Entry<String, Object> bean : entry) {
			Object value = bean.getValue();
			if (StringUtil.isNotEmpty(value)) {
				processQuery(_params, bean.getKey(), value);
			}
		}

		// 设置排序方式
		Object order = params.get("order");
		if (StringUtil.isNotEmpty(order)) {
			String[] orders = StringUtil.split(order, ",");
			for (String str : orders) {
				String[] sorts = StringUtil.split(str, ":");
				_params.addOrder(new OrderMapping(sorts[0], sorts[1]));
			}
		}
		
		// 设置分组字段
		Object group = params.get("group");
		if (StringUtil.isNotEmpty(group)) {
			String[] groups = StringUtil.split(group, ",");
			for (String str : groups) {
				_params.addGroup(new GroupMapping(str));
			}
		}

		// 执行查询操作
		return query(_params, options);
	}

	@Override
	public void setPK(final Object value) {
		Field field = getFieldByPK(this.getClass());
		try {
			BeanUtils.setProperty(this, field.getName(), value);
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public Object getPK() {
		try {
			Field field = getFieldByPK(this.getClass());
			return BeanUtils.getProperty(this, field.getName());
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public Object getProperty(final String name) {
		try {
			return BeanUtils.getProperty(this, StringUtil.toLowerCaseOnFirst(name));
		} catch (Throwable e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public void setProperty(final String name, final Object value) {
		try {
			if(value instanceof Date){
				ConvertUtils.register(new DateConverter(value), Date.class);
			}
			BeanUtils.setProperty(this, StringUtil.toLowerCaseOnFirst(name), value);
		} catch(Throwable e) {
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 处理查询参数.
	 * @param params
	 * @param key
	 * @param value
	 * @author chanlong(陈龙)
	 * @date 2017年4月19日 下午2:18:54
	 */
	protected void processQuery(final Parameters params, final String key, final Object value){
		switch (key) {
			case "order":
			case "limit":
			case "offset":
			break;
			default:
				params.addAnd(new FieldMapping(ValueType.String, key, value, OperatorType.EQ));
			break;
		}
	}
	
	/** 获取主键字段 **/
	private Field getFieldByPK(final Class<?> clazz) {
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			if (field.getAnnotation(Id.class) != null) {
				return field;
			}
		}
		return getFieldByPK(clazz.getSuperclass());
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
}
