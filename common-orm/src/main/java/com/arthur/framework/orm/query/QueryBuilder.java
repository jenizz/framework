package com.arthur.framework.orm.query;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import com.arthur.framework.orm.query.annotation.Search;
import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.orm.query.enums.RelationType;
import com.arthur.framework.orm.query.mapping.FieldMapping;
import com.arthur.framework.orm.query.mapping.GroupMapping;
import com.arthur.framework.orm.query.mapping.OrderMapping;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>描述</b>: 查询构造器 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月5日 下午3:27:39
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
@Component
public final class QueryBuilder<ENTITY> {
	
	private EntityManager em;
	
	private Parameters params;
	
	private Root<ENTITY> root;
	
	private Class<ENTITY> clazz;
	
	private CriteriaQuery<ENTITY> query;
	
	private CriteriaBuilder builder;
	
	@Autowired
	public QueryBuilder(final EntityManager em) {
		this.em = em;
	}
	
	public QueryBuilder<ENTITY> createQuery(final Parameters params) {
		this.params = params;
		this.clazz = params.getEntityClass();
		this.builder = em.getCriteriaBuilder();
		this.query = builder.createQuery(clazz);
		this.root = query.from(clazz);
		return this;
	}
	
	public ENTITY getSingleResult(final Class<ENTITY> cls, final String field, final Object value) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ENTITY> cq = cb.createQuery(cls);
		Root<ENTITY> cr = cq.from(cls);
		cq.where(cb.equal(cr.get(field), value));
		return em.createQuery(cq).getSingleResult();
	}
	
	public TypedQuery<ENTITY> createParams(final Map<String, Object> params, final TypedQuery<ENTITY> query){
		if (params != null && !params.isEmpty()) {
			Set<String> keys = params.keySet();
			for (String key : keys) {
				Object value = params.get(key);
				query.setParameter(key, value);
			}
		}
		return query;
	}
	
	public TypedQuery<Long> count(){
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<ENTITY> root = query.from(clazz);
		Predicate predicate = toPredicate();
		query.select(builder.count(root)).where(predicate).groupBy(toGroup());
		return em.createQuery(query);
	}
	
	public TypedQuery<Long> count(final Predicate predicate) {
		CriteriaQuery<Long> query = builder.createQuery(Long.class);
		Root<ENTITY> root = query.from(clazz);
		query.select(builder.count(root)).where(predicate).groupBy(toGroup());
		return em.createQuery(query);
	}
	
	public TypedQuery<ENTITY> query(final Predicate predicate){
		query.orderBy(toOrder());
		query.groupBy(toGroup());
		return createTypedQuery(query.where(predicate));
	}
	
	public Predicate toPredicate() {
		String search = params.getSearch();

		Criteria<ENTITY> criteria = new Criteria<ENTITY>();
		
		// and
		if (params.hasAnd()) criteria.add(params.getCriterion(RelationType.AND));
		
		// or
		if (params.hasOr()) criteria.add(params.getCriterion(RelationType.OR));
		
		// search
		if (StringUtil.isNotEmpty(search)) {
			Class<ENTITY> clazz = params.getEntityClass();
			ReflectionUtils.doWithFields(clazz, new FieldCallback<ENTITY>(criteria, search));
		}

		return criteria.toPredicate(root, query, builder);
	}
	
	private TypedQuery<ENTITY> createTypedQuery(final CriteriaQuery<ENTITY> query) {
		int offset = params.getOffset();
		int limit = params.getLimit();
		if (limit <= 0) { // 无分页
			return em.createQuery(query);
		} else {
			return em.createQuery(query).setFirstResult(offset).setMaxResults(limit);
		}
	}
	
	/** 创建排序 **/
	private List<Order> toOrder(){
		List<OrderMapping> orders = params.getOrder();
		List<Order> expression = new ArrayList<Order>();
		if (orders != null && !orders.isEmpty()) {
			for (OrderMapping order : orders) {
				expression.add(order.getExpresion(root, builder));
			}
		}
		return expression;
	}
	
	/** 创建分组 **/
	private List<Expression<?>> toGroup() {
		List<GroupMapping> groups = params.getGroup();
		List<Expression<?>> expression = new ArrayList<Expression<?>>();
		if (groups != null && !groups.isEmpty()) {
			for (GroupMapping group : groups) {
				expression.add(group.getExpresion(root, builder));
			}
		}
		return expression;
	}

	/*
	 * 全字段模糊查询回调
	 * eg:在实体类要模糊查询的字段上增加@Search
	 */
	private static class FieldCallback<ENTITY> implements org.springframework.util.ReflectionUtils.FieldCallback{
		private Criteria<ENTITY> criteria;
		private String search;

		public FieldCallback(Criteria<ENTITY> criteria, final String search) {
			this.criteria = criteria;
			this.search = search;
		}

		@Override
		public void doWith(final Field field) throws IllegalArgumentException, IllegalAccessException {
			Search search = field.getAnnotation(Search.class);
			if (search != null) {
				FieldMapping bean = new FieldMapping();
				bean.setName(field.getName());
				bean.setValue(this.search);
				bean.setOperator(OperatorType.LIKE.name());
				criteria.add(Restrictions.or(Restrictions.exec(bean, true)));
			}
		}
	}

	public void setEntityManager(final EntityManager em){
		this.em = em;
	}
}
