/**
 * 
 * <p><b>Author</b>: long.chan 2017年2月6日 上午9:23:19 </p>
 * <p><b>ClassName</b>: QueryUtil.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.orm.query.support;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.arthur.framework.orm.query.enums.ValueType;
import com.arthur.framework.util.DateUtil;
import com.arthur.framework.util.DateUtil.DatePattern;
import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>描述</b>: </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午9:15:52
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class ValueProcessor {
	
	/**
	 * .
	 * @param value
	 * @param type
	 * @since 2017年3月4日 上午9:15:35
	 * @author chanlong(陈龙)
	 */
	@SuppressWarnings("unchecked")
	public static <T> T parseValue(final Object value, final ValueType type) {
		
		if (StringUtil.isNotEmpty(value)) {
			String[] values;
			
			if(value instanceof Object[]){
				values = StringUtil.split(StringUtil.join(ArrayUtils.toArray((Object[])value), ","), ",");
			}else if(value instanceof List){
				values = ((List<String>)value).toArray(new String[]{});
			}else{
				values = StringUtil.split(value, StringUtil.COMMA);
			}
				
			switch (type) {
				case SqlStr: {
					if(values.length > 1){
						return (T) new StringBuilder("('").append(StringUtil.join(values, "','")).append("')").toString();
					}else{
						return (T) new StringBuilder("'").append(values[0]).append("'").toString();
					}
				}
				case SqlLike: {
					return (T) new StringBuilder("'%").append(values[0]).append("%'").toString();
				}
				case Date: {
					return (T) DateUtil.parseDate(value, DatePattern.DATE, DatePattern.DATETIME);
				}
				case Dates: {
					Date[] returnValue = new Date[values.length];
					for (int i = 0; i < values.length; i++) {
						returnValue[i] = DateUtil.parseDate(values[i], DatePattern.DATE, DatePattern.DATETIME);
					}
					return (T) returnValue;
				}
				case Long: {
					return (T) Long.valueOf(String.valueOf(value));
				}
				case Longs: {
					Long[] returnValue = new Long[values.length];
					for (int i = 0; i < values.length; i++) {
						returnValue[i] = Long.valueOf(values[i]);
					}
					return (T) returnValue;
				}
				case Float: {
					return (T) Float.valueOf(String.valueOf(value));
				}
				case Floats: {
					Float[] returnValue = new Float[values.length];
					for (int i = 0; i < values.length; i++) {
						returnValue[i] = Float.valueOf(values[i]);
					}
					return (T) returnValue;
				}
				case String: {
					return (T) value;
				}
				case Strings: {
					return (T) values;
				}
				case Double: {
					return (T) Double.valueOf(String.valueOf(value));
				}
				case Doubles: {
					Double[] returnValue = new Double[values.length];
					for (int i = 0; i < values.length; i++) {
						returnValue[i] = Double.valueOf(values[i]);
					}
					return (T) returnValue;
				}
				case Integer: {
					return (T) Integer.valueOf(String.valueOf(value));
				}
				case Integers: {
					Integer[] returnValue = new Integer[values.length];
					for (int i = 0; i < values.length; i++) {
						returnValue[i] = Integer.valueOf(values[i]);
					}
					return (T) returnValue;
				}
				default: return (T) value;
			}
		}
		return (T) value;
	}

	@SuppressWarnings("unchecked")
	public static <T> T parseValue(final Object value, final String type) {
		if(StringUtil.isNotEmpty(type)){
			return parseValue(value, ValueType.valueOf(type));
		}else{
			return (T) value;
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T parseValue(final Object value, final String type, final int index) {
		if(values == null) values = (Object[])parseValue(value, type);
		return (T) values[index];
	}
	
	private static Object[] values;
}
