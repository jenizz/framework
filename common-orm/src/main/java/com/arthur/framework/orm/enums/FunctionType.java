package com.arthur.framework.orm.enums;

import java.util.HashMap;
import java.util.Map;

import com.arthur.framework.orm.helper.builder.SQLBuilderField;

public enum FunctionType {

	GROUP_CONCAT {
		public String build(final SQLBuilderField field) {
			return new StringBuilder(dbmap.get(this.getType()).get(this.name())).append("(").append(field.getPrefix()).append(".").append(field.getName()).append(", ',')").toString();
		}
	};
	
	private String type;
	
	public FunctionType db(final String type) {
		this.type = type;
		return this;
	}
	
	public String getType(){
		return type;
	}

	public abstract String build(final SQLBuilderField field);
	
	private static Map<String, Map<String, String>> dbmap = new HashMap<>();
	static {
		dbmap.put("dm", new HashMap<>());
		dbmap.put("pgs", new HashMap<>());
		dbmap.put("mysql", new HashMap<>());
		dbmap.put("oracle", new HashMap<>());
		
		dbmap.get("dm").put(GROUP_CONCAT.name(), "wm_concat");
		dbmap.get("pgs").put(GROUP_CONCAT.name(), "string_agg");
		dbmap.get("mysql").put(GROUP_CONCAT.name(), "group_concat");
		dbmap.get("oracle").put(GROUP_CONCAT.name(), "wm_concat");
	}
	
}
