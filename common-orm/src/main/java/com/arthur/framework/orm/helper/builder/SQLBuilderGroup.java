package com.arthur.framework.orm.helper.builder;

import com.arthur.framework.util.string.StringUtil;

/**
 * <p>描述: SQL分组构造器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月10日 下午3:21:42
 * @version 1.0.2017
 */
public class SQLBuilderGroup extends SQLBuilderAdapter {

	/**
	 * @see com.arthur.framework.orm.helper.builder.SQLBuilder#build()
	 * @author chanlong(陈龙)
	 * @date 2017年7月5日 上午10:16:48
	 */
	@Override @SuppressWarnings("unchecked")
	public <T> T build() {
		String prefix = getPrefix();
		String field = getName();
		return (T) (StringUtil.isNotEmpty(prefix) ? new StringBuilder(prefix).append(".") : new StringBuilder()).append(field).toString();
	}
	
	public SQLBuilderGroup(final String prefix, final String field) {
		setPrefix(prefix);
		setName(field);
	}
}
