/**
 * <p>Copyright:Copyright(c) 2017</p>
 * <p>Company:Professional</p>
 * <p>Package:com.arthur.framework.orm.query.enums</p>
 * <p>File:OrderType.java</p>
 * <p>类更新历史信息</p>
 * @todo chanlong(陈龙) 创建于 2017年4月19日 下午2:32:08
 */
package com.arthur.framework.orm.query.enums;

/**
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年4月19日 下午2:32:08
 * @version 1.0.2017
 */
public enum OrderType {
	asc("asc"), ASC("asc"),
	desc("desc"), DESC("desc");
	
	private String type;
	
	private OrderType(final String type) {
		this.type = type;
	}
	
	public String value() {
		return type;
	}
}
