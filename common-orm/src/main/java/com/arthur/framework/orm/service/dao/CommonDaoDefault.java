package com.arthur.framework.orm.service.dao;

import org.springframework.stereotype.Repository;

import com.arthur.framework.orm.entity.CommonEntityAware;

/**
 * <p><b>描述</b>: 公共DAO的默认实现 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午11:21:50
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
@Repository("defaultDao")
public class CommonDaoDefault extends CommonDaoAdapter<CommonEntityAware> {
	
}
