package com.arthur.framework.orm.helper.config;

/**
 * <p>描述: SQL配置器接口 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月22日 上午10:28:26
 * @version 1.0.2017
 */
public interface SQLConfigurer {

	/**
	 * SQL表配置器.
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月22日 上午10:53:07
	 */
	SQLConfigurerTable table();
	
	/**
	 * SQL字段配置器.
	 * @author chanlong(陈龙)
	 * @throws Exception 
	 * @date 2017年5月12日 下午12:14:28
	 */
	SQLConfigurerField field();

	/**
	 * SQL连接配置器.
	 * @author chanlong(陈龙)
	 * @throws Exception 
	 * @date 2017年5月12日 下午12:14:28
	 */
	SQLConfigurerJoins joins();

	/**
	 * SQL条件配置器.
	 * @author chanlong(陈龙)
	 * @throws Exception 
	 * @date 2017年5月12日 下午12:14:28
	 */
	SQLConfigurerWhere where();

	/**
	 * SQL分组配置器.
	 * @author chanlong(陈龙)
	 * @throws Exception 
	 * @date 2017年5月12日 下午12:14:28
	 */
	SQLConfigurerGroup group();

	/**
	 * SQL排序配置器.
	 * @author chanlong(陈龙)
	 * @throws Exception 
	 * @date 2017年5月12日 下午12:14:28
	 */
	SQLConfigurerOrder order();
	
	/**
	 * 调用SQL配置器，生成SQL语句
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月23日 下午1:11:56
	 */
	<T> T build();
	
	String select();
	
	SQLConfigurer getObjenesis();
	
	void setObjenesis(final SQLConfigurer configurer);
	
	<C extends SQLConfigurer> C setSQLConfigurer(final C configurer);

	<C extends SQLConfigurer> C getSQLConfigurer(final Class<? extends SQLConfigurer> classkey);
}
