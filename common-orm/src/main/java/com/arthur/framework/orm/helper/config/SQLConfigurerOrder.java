package com.arthur.framework.orm.helper.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.arthur.framework.orm.helper.builder.SQLBuilderOrder;

public final class SQLConfigurerOrder extends SQLConfigurerAdapter {

	private List<SQLBuilderOrder> orders = new ArrayList<SQLBuilderOrder>();
	
	@Override @SuppressWarnings("unchecked")
	public String[] build() {
		return (isParallel() ? orders.parallelStream() : orders.stream()).reduce(new ArrayList<String>(), (array, item) -> {
			array.add(item.build()); return array;
		}, (left, right) -> left).toArray(new String[] {});
	}
	
	public SQLConfigurerOrder addOrder(final SQLBuilderOrder order) {
		Assert.notNull(order, "SQLBuilderOrder cannot be null");
		orders.add(order); 
		return this;
	}
	
	public SQLConfigurerOrder addOrder(final String field, final String order) {
		return addOrder(new SQLBuilderOrder(field, order));
	}
	
	public SQLConfigurerOrder addOrder(final String prefix, final String field, final String order) {
		return addOrder(new SQLBuilderOrder(prefix, field, order));
	}
}
