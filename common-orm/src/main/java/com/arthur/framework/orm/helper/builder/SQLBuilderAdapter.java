package com.arthur.framework.orm.helper.builder;

public abstract class SQLBuilderAdapter implements SQLBuilder {

	private String name;
	
	private String alias;
	
	private String prefix;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
