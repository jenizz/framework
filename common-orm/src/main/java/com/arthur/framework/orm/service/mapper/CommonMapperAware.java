package com.arthur.framework.orm.service.mapper;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import com.arthur.framework.orm.helper.SQLProvider;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;

public interface CommonMapperAware<E> {

	@InsertProvider(type = SQLProvider.class, method = "insert")
	public int insert(@Param("entity") final E entity);

	@UpdateProvider(type = SQLProvider.class, method = "update")
	public int update(@Param("entity") final E entity);

	@DeleteProvider(type = SQLProvider.class, method = "delete")
	public int delete(@Param("entity") final E entity);  
  
	@SelectProvider(type = SQLProvider.class, method = "findOne") 
	public E findOne(@Param("entity") final E entity, @Param("params") final Parameters params);
	
	@SelectProvider(type = SQLProvider.class, method = "findAll") 
	public Pagination<E> findAll(@Param("entity") final E entity, @Param("params") final Parameters params);
}
