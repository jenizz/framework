package com.arthur.framework.orm.service.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.helper.SQLHelper;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;

/**
 * <p><b>描述</b>: 数据访问接口 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午12:55:47
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public interface CommonDaoAware<E extends CommonEntityAware>{
	
	public E insert(final E entity) throws Exception;
	
	public E update(final E entity) throws Exception;
	
	public E delete(final E entity) throws Exception;
	
	
	public E findOne(final E entity) throws Exception;
	
	public E findOne(final Class<E> cls, final Serializable id) throws Exception;
	
	public E findOne(final Class<E> cls, final Parameters params) throws Exception;
	
	public E findOne(final Class<E> cls, final String field, final Object value) throws Exception;

	public Pagination<E> findAll(final Parameters params) throws Exception;
	
	public Pagination<E> findAll(final SQLHelper helper, final int offset, final int limit) throws Exception;

	public List<E> findAll(final Class<E> cls, final String jpql) throws Exception;
	
	public List<E> findAll(final Class<E> cls, final String jpql, final Map<String, Object> params) throws Exception;

	public Long count(final Parameters params) throws Exception;

	public Long count(final SQLHelper helper) throws Exception;

}
