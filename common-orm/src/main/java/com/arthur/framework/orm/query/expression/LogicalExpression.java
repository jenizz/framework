package com.arthur.framework.orm.query.expression;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.arthur.framework.orm.query.Criterion;
import com.arthur.framework.orm.query.enums.RelationType;

/**
 * 逻辑条件表达式 用于复杂条件时使用
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2016年1月18日 下午5:17:13
 * @version 1.0.2016
 */
public class LogicalExpression implements Criterion {

	private Criterion[] criterion;  // 逻辑表达式中包含的表达式  
    private RelationType relation;  // 逻辑表达式  
    
    public LogicalExpression(Criterion[] criterions, RelationType relation) {  
        this.criterion = criterions;  
        this.relation = relation;  
    }  
    
	@Override
	public Predicate toPredicate(final Root<?> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {
		List<Predicate> predicates = new ArrayList<Predicate>();
		for (int i = 0; i < this.criterion.length; i++) {
			predicates.add(this.criterion[i].toPredicate(root, query, builder));
		}
		switch (relation) {
			case OR: 
			case or: return builder.or(predicates.toArray(new Predicate[predicates.size()]));
			default: return builder.and(predicates.toArray(new Predicate[predicates.size()]));
		}
	}

}
