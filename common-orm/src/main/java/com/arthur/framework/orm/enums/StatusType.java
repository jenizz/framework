package com.arthur.framework.orm.enums;

public enum StatusType {

	ACTIVITY(1), DELETE(0);

	private int type;

	private StatusType(int type) {
		this.type = type;
	}

	public int value() {
		return type;
	}
}
