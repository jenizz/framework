package com.arthur.framework.orm.service.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.jpa.internal.util.PersistenceUtilHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.orm.query.enums.ValueType;
import com.arthur.framework.orm.query.mapping.FieldMapping;
import com.arthur.framework.orm.query.mapping.OrderMapping;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.orm.service.dao.CommonDaoAware;
import com.arthur.framework.orm.support.Property;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.string.StringUtil;


/**
 * <p>描述: 公共业务接口的适配器类，不同的业务实现类可根据需要覆盖该类中的接口。</p>
 * <p>Company:上海中信信息发展股份有限公司</p>
 * @author chanlong(陈龙)
 * @date 2013年12月10日  下午4:37:27
 * @version 1.0.2013.
 */
public abstract class CommonBoAdapter<E extends CommonEntityAware> implements CommonBoAware<E> {
	
	private static final Logger log = LoggerFactory.getLogger(CommonBoAdapter.class);

	@Resource(name="defaultDao")
	private CommonDaoAware<E> dao;
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public E saveOrUpdate(final E entity) throws Exception {
		if (StringUtil.isEmpty(entity.getPK())) {
			return dao.insert(entity);
		} else {
			switch(PersistenceUtilHelper.isLoaded(entity)){
				case LOADED: return dao.update(entity);
				default: {
					E dest = this.findOne(entity);
					if (dest == null) {
						return dao.insert(entity);
					} else {
						return dao.update(ReflectUtil.copyProperties(entity, dest));
					}
				}
			}
		}
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public List<E> saveOrUpdate(final List<E> entities) throws Exception {
		for (E entity : entities) {
			this.saveOrUpdate(entity);
		}
		return entities;
	}
	
	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public boolean update(final Class<E> cls, final Map<String, Object> properties, final Serializable... ids) throws Exception {
		List<E> entities = this.findAll(cls, ids);
		if (properties != null && !properties.isEmpty()) {
			entities.parallelStream().forEach(entity -> {
				try {
					properties.forEach((name, value) -> ReflectUtil.setProperty(entity, name, value));
					this.saveOrUpdate(entity);
				} catch (Exception e) {
					log.error("invoke update method by id array for property option is error:{}", e.getMessage());
				}
			});
		}
		return true;
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean delete(final E entity) throws Exception {
		try {
			dao.delete(this.findOne(entity));
		} catch (Exception e) {
			throw new Exception(e);
		}
		return true;
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean delete(final Class<E> cls, final Serializable id) throws Exception {
		try {
			 dao.delete(this.findOne(cls, id));
		} catch (Exception e) {
			throw new Exception(e);
		}
		return true;
	}

	@Override @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public boolean delete(final Class<E> cls, final Serializable... ids) throws Exception {
		try {
			for (Serializable id : ids) {
				this.delete(cls, id);
			}
		} catch (Exception e) {
			throw new Exception(e);
		}
		return true;
	}

	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final E entity) throws Exception {
		return dao.findOne(entity);
	}

	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final Class<E> cls, final Serializable id) throws Exception {
		return dao.findOne(cls, id);
	}

	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final Class<E> cls, final Parameters params) throws Exception {
		return dao.findOne(cls, params);
	}

	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final Class<E> cls, final String field, final Object value) throws Exception {
		return dao.findOne(cls, field, value);
	}
	
	@Override
	public List<E> findAll(final Class<E> cls, final String field, final Object value) throws Exception {
		Parameters params = new Parameters(cls, 0, 0);
		if (value instanceof String) {
			params.addAnd(new FieldMapping(ValueType.String, field, value, OperatorType.EQ));
		} else if (value instanceof Date){
			params.addAnd(new FieldMapping(ValueType.Date, field, value, OperatorType.EQ));
		} else if (value instanceof Long){
			params.addAnd(new FieldMapping(ValueType.Long, field, value, OperatorType.EQ));
		} else if (value instanceof Float){
			params.addAnd(new FieldMapping(ValueType.Float, field, value, OperatorType.EQ));
		} else if (value instanceof Double){
			params.addAnd(new FieldMapping(ValueType.Double, field, value, OperatorType.EQ));
		} else if (value instanceof Integer){
			params.addAnd(new FieldMapping(ValueType.Integer, field, value, OperatorType.EQ));
		}
		return dao.findAll(params).getRows();
	}

	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<E> findAll(final Class<E> cls, final Serializable... ids) throws Exception {
		List<E> entities = new ArrayList<E>();
		for (Serializable id : ids) {
			entities.add(this.findOne(cls, id));
		}
		return entities;
	}

	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Pagination<E> findAll(final Parameters params) throws Exception {
		return dao.findAll(params);
	}

	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Long count(final Parameters params) throws Exception {
		return dao.count(params);
	}
	
	@Override @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<Property> findByProperty(final String property, final Map<String, Object> params) throws Exception {
		return null;
	}
	
	@Override
	public List<Property> findByProperty(final Class<E> cls, final String property, final Map<String,Object> params) throws Exception {
		Parameters parameters = new Parameters(cls, 0, 0);
		parameters.addOrder(new OrderMapping(property, "DESC"));
		if (params != null) {
			params.forEach((name, value) -> {
				parameters.addAnd(new FieldMapping(ValueType.String, name, value, OperatorType.EQ));
			});
		}
		Pagination<E> pagination = dao.findAll(parameters);
		List<E> result = pagination.getRows();
		List<Property> data = result.stream().reduce(new ArrayList<>(), (list, item) -> {
			list.add(new Property(property, ReflectUtil.getProperty(item, property)));
			return list;
		}, (left, right) -> left);
		return data;
	}

	/* ===========================================
	 * 构造查询参数，用于拼接jpql语句
	 * =========================================== */
	private StringBuilder jpql;
	
	/**
	 * 要查询的实体.
	 * @param entityName
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2016年4月9日 下午7:24:48
	 */
	protected CommonBoAware<E> from(final String entityName){
		jpql = new StringBuilder("select o from ").append(entityName).append(" o where 1=1 ");
		return this;
	}
	
	/**
	 * 构造jpql的where条件.
	 * @param map
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2016年4月9日 下午7:24:13
	 */
	protected String where(final ModelMap map){
		if(map != null && !map.isEmpty()){
			Set<String> keys = map.keySet();
			for (String key : keys) {
				String[] fields = StringUtil.split(key, "_");
				String field = fields[0];
				String operator = fields.length > 1 ? fields[1] : OperatorType.EQ.name();
				
				switch (OperatorType.valueOf(operator)) {
					case EQ:
						jpql.append(" and o.").append(field).append(" = :").append(field);
					break;
					case GT:
						jpql.append(" and o.").append(field).append(" > :").append(field);
					break;
					case LT:
						jpql.append(" and o.").append(field).append(" < :").append(field);
					break;
					case GE:
						jpql.append(" and o.").append(field).append(" >= :").append(field);
					break;
					case LE:
						jpql.append(" and o.").append(field).append(" <= :").append(field);
					break;
					case LIKE:
						jpql.append(" and o.").append(field).append(" like :").append(field);
					break;
					default:
						jpql.append(" and o.").append(field).append(" = :").append(field);
					break;
				}
			}
		}
		return jpql.toString();
	}
}
