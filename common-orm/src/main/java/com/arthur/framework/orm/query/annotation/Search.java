package com.arthur.framework.orm.query.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 列表全字段查询注解
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2016年1月30日 下午1:19:52
 * @version 1.0.2016
 */
@Target({FIELD})
@Retention(RUNTIME)
public @interface Search {

	String value() default "";
	
}
