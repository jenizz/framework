package com.arthur.framework.orm.helper.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import com.arthur.framework.orm.helper.builder.SQLBuilderWhere;
import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.util.string.StringUtil;

public final class SQLConfigurerWhere extends SQLConfigurerAdapter {
	
	/**
	 * 返回条件集.
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午3:18:40
	 */
	@Override @SuppressWarnings("unchecked")
	public String[] build() {
		try{
			return (isParallel() ? wheremap.get(target).parallelStream() : wheremap.get(target).stream())
					.reduce(new ArrayList<String>(), (array, item) -> {
						String _where = item.build();
						if (StringUtil.isNotEmpty(_where)) {
							array.add(_where);
						}
						return array;
					}, (left, right) -> left).toArray(new String[] {});
		} catch (NullPointerException e) {
			return new String[] { "1=1" };
		}
	}

	/**
	 * 添加SQL条件构造器.
	 * @param where 条件构造器
	 * @author chanlong(陈龙)
	 * @date 2017年5月23日 下午1:31:47
	 */
	public SQLConfigurerWhere addWhere(final SQLBuilderWhere where) {
		Assert.notNull(where, "SQLBuilderWhere cannot be null");
		if (wheremap.get(target) == null) wheremap.put(target, new ArrayList<>());
		wheremap.get(target).add(where);
		return this;
	}
	
	/**
	 * 添加SQL条件构造器.
	 * @param sql 条件的sql语句
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 下午12:31:47
	 */
	public SQLConfigurerWhere addWhere(final String sql) {
		return addWhere(new SQLBuilderWhere(sql));
	}
	
	/**
	 * 添加SQL条件构造器.
	 * @param field 	字段名
	 * @param operator	操作符
	 * @param value		查询值
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 下午12:32:26
	 */
	public SQLConfigurerWhere addWhere(final String field, final OperatorType operator, final Object... value) {
		return addWhere(new SQLBuilderWhere(null, field, operator, value));
	}
	
	/**
	 * 添加SQL条件构造器.
	 * @param table 	表别名
	 * @param field 	字段名
	 * @param operator	操作符
	 * @param value		查询值
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 下午12:32:26
	 */
	public SQLConfigurerWhere addWhere(final String table, final String field, final OperatorType operator, final Object... value) {
		return addWhere(new SQLBuilderWhere(table, field, operator, value));
	}
	
	public SQLConfigurerWhere and() {
		target = "and"; return this;
	}
	
	public SQLConfigurerWhere or() {
		target = "or"; return this;
	}
	
	public boolean isNotEmpty(){
		return wheremap.get(target) != null && !wheremap.get(target).isEmpty();
	}
	
	private String target = "and";
	private Map<String, List<SQLBuilderWhere>> wheremap = new HashMap<>();
}
