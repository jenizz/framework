package com.arthur.framework.orm.service.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import com.arthur.framework.orm.helper.JPQLHelper;
import com.arthur.framework.orm.helper.SQLHelper;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;

/**
 * <p><b>描述</b>: Spring Data JPA 自定义全局Dao </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午1:03:45
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
@NoRepositoryBean
public interface CommonRepositoryAware<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
	
	/**
	 * 根据实体的Example查询.
	 * @param function lambda表达式
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:17:56
	 */
	public T findOne(final Function<T, Example<T>> function) throws Exception;
	
	/**
	 * 根据实体的Example查询.
	 * @param function lambda表达式
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:18:27
	 */
	public List<T> findAll(final Function<T, Example<T>> function) throws Exception;
	
	/**
	 * 分页查询接口.
	 * @param params 查询参数构造器
	 * @throws Exception
	 * @since 2017年3月4日 下午1:04:14
	 * @author chanlong(陈龙)
	 */
	public Pagination<T> findAll(final Parameters params) throws Exception;

	/**
	 * 分页查询接口.
	 * @param helper SQL构造器
	 * @param offset 分页起始行数
	 * @param limit 每页显示条数
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年4月12日 下午4:53:48
	 */
	public Pagination<T> findAll(final SQLHelper helper, final int offset, final int limit);
	
	/**
	 * 查询接口.
	 * @param helper SQL构造器
	 * @author chanlong(陈龙)
	 * @date 2017年12月15日 上午11:27:01
	 */
	public List<Map<String, Object>> findAll(final SQLHelper helper);
	
	/**
	 * 查询接口.
	 * @param helper SQL构造器
	 * @author chanlong(陈龙)
	 * @date 2017年12月15日 上午11:27:25
	 */
	public Map<String, Object> findOne(final SQLHelper helper);
	
	/**
	 * 分页查询接口.
	 * @param sql
	 * @param offset
	 * @param limit
	 * @author chanlong(陈龙)
	 * @date 2017年12月15日 下午5:46:46
	 */
	public Pagination<T> queryForPagination(final String sql, final int offset, final int limit);
	
	/**
	 * 查询接口.
	 * @param sql
	 * @author chanlong(陈龙)
	 * @date 2017年12月15日 下午5:47:05
	 */
	public List<Map<String, Object>> queryForList(final String sql);
	
	/**
	 * 查询接口.
	 * @param sql
	 * @author chanlong(陈龙)
	 * @date 2017年12月15日 下午5:47:18
	 */
	public Map<String, Object> queryForMap(final String sql);
	
	/**
	 * 分页查询接口.
	 * @param helper JPQL构造器
	 * @param offset 分页起始行数
	 * @param limit 每页显示条数
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年4月12日 下午4:53:07
	 */
	public Pagination<T> findAll(final JPQLHelper helper, final int offset, final int limit) throws Exception;

	/**
	 * 查询结果总数.
	 * @param helper SQL构造器
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:22:28
	 */
	public Long count(final SQLHelper helper) throws Exception;
	
	/**
	 * 查询结果总数.
	 * @param helper JPQL构造器
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:23:45
	 */
	public Long count(final JPQLHelper helper) throws Exception;
	
	/**
	 * 查询结果总数.
	 * @param params 查询参数构造器
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年5月4日 上午9:24:10
	 */
	public Long count(final Parameters params) throws Exception;

	
}
