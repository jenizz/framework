package com.arthur.framework.orm.helper.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.arthur.framework.orm.enums.FunctionType;
import com.arthur.framework.orm.helper.builder.SQLBuilderField;

/**
 * <p>描述: SQL字段配置器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月12日 上午10:49:56
 * @version 1.0.2017
 */
public final class SQLConfigurerField extends SQLConfigurerAdapter {

	private List<SQLBuilderField> fields = new ArrayList<>();
	
	/**
	 * 添加SQL字段.
	 * @param field SQLField
	 * @author chanlong(陈龙)
	 * @date 2017年5月12日 上午10:03:11
	 */
	public SQLConfigurerField addField(final SQLBuilderField field){
		Assert.notNull(field, "SQLBuilderField cannot be null");
		fields.add(field);
		return this;
	};
	
	/**
	 * 添加SQL字段构造器.
	 * @param field 字段名称
	 * @param alias 字段别名
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午1:23:05
	 */
	public SQLConfigurerField addField(final String field, final String alias){
		return addField(new SQLBuilderField(field, alias));
	}
	
	/**
	 * 添加SQL字段构造器.
	 * @param table 表别名
	 * @param field 字段名
	 * @param alias 字段别名
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 上午9:03:13
	 */
	public SQLConfigurerField addField(final String prefix, final String field, final String alias){
		return addField(new SQLBuilderField(prefix, field, alias));
	}
	
	public SQLConfigurerField addField(final FunctionType type, final SQLBuilderField field) {
		return addField(new SQLBuilderField(type.build(field), field.getAlias()));
	}

	/**
	 * 返回字段集.
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午1:31:26
	 */
	@Override @SuppressWarnings("unchecked")
	public String[] build() {
		if(fields.isEmpty()){
			return new String[] {"*"};
		}else{
			return (isParallel() ? fields.parallelStream() : fields.stream()).reduce(new ArrayList<String>(), (array, item) -> {
				array.add(item.build()); return array;
			}, (left, right) -> left).toArray(new String[] {});
		}
	}
}
