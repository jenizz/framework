package com.arthur.framework.orm.helper.config;

import com.arthur.framework.orm.helper.builder.SQLBuilderTable;

/**
 * <p>描述: SQL表配置器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月12日 下午12:13:56
 * @version 1.0.2017
 */
public final class SQLConfigurerTable extends SQLConfigurerAdapter {

	/**
	 * @see com.arthur.framework.orm.helper.config.SQLConfigurerAdapter#build()
	 * @author chanlong(陈龙)
	 * @date 2017年7月5日 上午10:33:54
	 */
	@Override @SuppressWarnings("unchecked")
	public String build() {
		return table == null ? "" : table.build();
	}
	
	private SQLBuilderTable table;
	
	public SQLConfigurerTable name(final String name) {
		if (table == null) table = new SQLBuilderTable();
		table.setName(name);
		return this;
	}
	
	public SQLConfigurerTable alias(final String alias) {
		if (table == null) table = new SQLBuilderTable();
		table.setAlias(alias);
		return this;
	}
}
