package com.arthur.framework.orm.query;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

/**
 * <p><b>描述</b>: 条件表达式容器 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午12:45:34
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class Criteria<T> implements Specification<T> {

	private List<Criterion> criterions = new ArrayList<Criterion>();
	
	/**
	 * 添加条件.
	 * @param criterion
	 * @author chanlong(陈龙)
	 * @date 2016年1月18日 下午4:48:48
	 */
	public void add(Criterion criterion) {
		if (criterion != null) {
			criterions.add(criterion);
		}
	}
	
	/**
	 * 清空条件.
	 * @author chanlong(陈龙)
	 * @date 2016年1月18日 下午4:50:11
	 */
	public void clear(){
		criterions.clear();
	}
	
	/**
	 * 构造条件表达式
	 * @see org.springframework.data.jpa.domain.Specification#toPredicate(javax.persistence.criteria.Root, javax.persistence.criteria.CriteriaQuery, javax.persistence.criteria.CriteriaBuilder)
	 * @since 2017年3月4日 下午12:43:14
	 * @author chanlong(陈龙)
	 */
	@Override
	public Predicate toPredicate(final Root<T> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {
		return createPredicates(root, query, builder);
	}
	
	/** 构造条件 **/
	private Predicate createPredicates(final Root<T> root, final CriteriaQuery<?> query, final CriteriaBuilder builder){
		Predicate predicate = builder.conjunction();
		
		if (!criterions.isEmpty()) {  
            List<Predicate> predicates = new ArrayList<Predicate>(); 
            predicates.add(predicate);
            
            for(Criterion criterion : criterions){  
                predicates.add(criterion.toPredicate(root, query, builder));  
            }  
            
            // 将所有条件用 and 联合起来  
            if (predicates.size() > 0) {  
            	return builder.and(predicates.toArray(new Predicate[predicates.size()]));  
            }  
        }
		
		return predicate;
	}
}
