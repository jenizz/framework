package com.arthur.framework.orm.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * <p><b>Author</b>: long.chan 2017年2月7日 下午3:34:18 </p>
 * <p><b>ClassName</b>: CommonEntityUUID.java </p> 
 * <p><b>Description</b>: </p> 
 */
@MappedSuperclass
public abstract class CommonEntityIdentity<E> extends AbstractCommonEntity<E> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 6868718716266032062L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
