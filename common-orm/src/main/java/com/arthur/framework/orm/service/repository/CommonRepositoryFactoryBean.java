package com.arthur.framework.orm.service.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

/**
 * <p><b>描述</b>: Spring Data JPA 自定义工厂 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午1:06:29
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class CommonRepositoryFactoryBean<T extends JpaRepository<S, ID>, S, ID extends Serializable> extends JpaRepositoryFactoryBean<T, S, ID> {

	/**
	 * 构造函数.
	 * @param repositoryInterface
	 * @since 2017年3月4日 下午1:06:09
	 * @author chanlong(陈龙)
	 */
	public CommonRepositoryFactoryBean(final Class<? extends T> repositoryInterface) {
		super(repositoryInterface);
	}

	/**
	 * 创建 SpringDataJpa 容器工厂
	 * @see org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean#createRepositoryFactory(javax.persistence.EntityManager)
	 * @since 2017年3月4日 下午1:06:17
	 * @author chanlong(陈龙)
	 */
	@Override
	protected RepositoryFactorySupport createRepositoryFactory(final EntityManager em) {
		return new CommonRepositoryFactory(em);
	}
}
