package com.arthur.framework.orm.query.mapping;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.arthur.framework.orm.query.Criterion;
import com.arthur.framework.orm.query.Restrictions;
import com.arthur.framework.orm.query.enums.RelationType;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Parameters implements Serializable {
	
	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -8580808826914296664L;
	
	/* 实体类
	 * eg: ajax data : {"entity":"com.app.miwawa.api.esm.entity.Adviser"}
	 */
	private String entity;
	
	private Class<?> entityClass;
	
	/* 列表模糊搜索
	 * eg:
	 * 	@Search @Column(...)
	 * 	private String name;
	 */
	private String search;
	
	/* 分页大小
	 * 每页显示的条目数
	 */
	private Integer limit = 10;
	
	/* 当前分页开始条目序号
	 * offset = current page * limit - limit
	 */
	private Integer offset = 0;
	
	/* 排序集合
	 * eg:  ajax data : { "order":[{"field":"invalid","type":"desc"}] }
	 */
	private List<OrderMapping> order = new ArrayList<>();
	
	/* 分组集合
	 * eg:  ajax data : { "group":[{"field":"articleId"}] }
	 */
	private List<GroupMapping> group = new ArrayList<>();
	
	/* 关系为and的条件集合
	 * eg:  ajax data : { "and":[{"type":"String", "field":"invalid", "value":"F", "operator":"EQ"}] }
	 * eg:  ajax data : { "and":[{"type":"Date", "field":"createDate", "value":["yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm:ss"], "operator":"BETWEEN"}] }
	 */
	private List<FieldMapping> and = new ArrayList<>();
	
	/* 关系为or的条件集合
	 * eg:  ajax data : { "or":[{"type":"String", "field":"invalid", "value":"F", "operator":"EQ"}]}
	 */
	private List<FieldMapping> or = new ArrayList<>();
	
	public Criterion getCriterion(final RelationType type) {
		List<Criterion> criterions = new ArrayList<>();
		switch (type) {
			case OR:
			case or:
				for (FieldMapping field : or) {
					criterions.add(field.getExpresion(null, null));
				}
				return Restrictions.or(criterions.toArray(new Criterion[]{}));
			default:
				for (FieldMapping field : and) {
					criterions.add(field.getExpresion(null, null));
				}
				return Restrictions.and(criterions.toArray(new Criterion[]{}));
		}
	}
	
	public Parameters addOrder(final OrderMapping mapping) {
		if (!order.contains(mapping)) {
			order.add(mapping);
		}
		return this;
	}
	
	public Parameters addGroup(final GroupMapping mapping){
		if (!group.contains(mapping)) {
			group.add(mapping);
		}
		return this;
	}
	
	public Parameters addAnd(final FieldMapping mapping) {
		if(!and.contains(mapping)){
			and.add(mapping);
		}
		return this;
	}
	
	public Parameters addOr(final FieldMapping mapping) {
		if(!or.contains(mapping)){
			or.add(mapping);
		}
		return this;
	}
	
	public boolean hasOrder() {
		return order != null && !order.isEmpty();
	}

	public boolean hasGroup() {
		return group != null && !group.isEmpty();
	}
	
	public boolean hasAnd() {
		return and != null && !and.isEmpty();
	}
	
	public boolean hasOr() {
		return or != null && !or.isEmpty();
	}
	
	public Parameters() { }
	
	public Parameters(String entity) {
		this.entity = entity;
	}
	
	public Parameters(Class<?> entity) {
		this.entity = entity.getName();
	}
	
	public Parameters(String entity, Integer limit, Integer offset) {
		this.limit = limit;
		this.offset = offset;
		this.entity = entity;
	}
	
	public Parameters(Class<?> entity, Integer limit, Integer offset) {
		this.limit = limit;
		this.offset = offset;
		this.entity = entity.getName();
		this.entityClass = entity;
	}
	
	public Parameters(Class<?> entity, String limit, String offset) {
		this.limit = Integer.valueOf(limit);
		this.offset = Integer.valueOf(offset);
		this.entity = entity.getName();
		this.entityClass = entity;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}
	
	@SuppressWarnings("unchecked")
	public <T> Class<T> getEntityClass() {
		try {
			return (Class<T>) (entityClass != null ? entityClass : Class.forName(entity));
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("获取类对象的实例失败：{}", e);
		}
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public List<OrderMapping> getOrder() {
		return order;
	}

	public void setOrder(List<OrderMapping> order) {
		this.order = order;
	}

	public List<GroupMapping> getGroup() {
		return group;
	}

	public void setGroup(List<GroupMapping> group) {
		this.group = group;
	}

	public List<FieldMapping> getAnd() {
		return and;
	}

	public void setAnd(List<FieldMapping> and) {
		this.and = and;
	}

	public List<FieldMapping> getOr() {
		return or;
	}

	public void setOr(List<FieldMapping> or) {
		this.or = or;
	}
}
