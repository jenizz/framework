package com.arthur.framework.orm.helper.config;

import com.arthur.framework.orm.helper.builder.SQLBuilderParent;

/**
 * <p>描述: SQL配置器构造类 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年7月5日 上午9:29:58
 * @version 1.0.2017
 */
public final class SQLConfigurerBuilder extends SQLConfigurerAdapter {
	
	private SQLBuilderParent parent;
	
	public SQLConfigurerBuilder() {
		setObjenesis(this);
	}
	
	public SQLConfigurerBuilder prefix(final String prefix) {
		if (parent == null) parent = new SQLBuilderParent();
		parent.setPrefix(prefix);
		return this;
	}
}
