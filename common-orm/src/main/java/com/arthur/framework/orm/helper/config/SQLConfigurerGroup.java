package com.arthur.framework.orm.helper.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

import com.arthur.framework.orm.helper.builder.SQLBuilderGroup;

/**
 * <p>描述: SQL分组配置器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月12日 上午10:50:21
 * @version 1.0.2017
 */
public final class SQLConfigurerGroup extends SQLConfigurerAdapter {

	private List<SQLBuilderGroup> groups = new ArrayList<SQLBuilderGroup>();
	
	/**
	 * 返回分组集.
	 * @author chanlong(陈龙)
	 * @date 2017年5月10日 下午3:18:53
	 */
	@Override @SuppressWarnings("unchecked")
	public String[] build() {
		return (isParallel() ? groups.parallelStream() : groups.stream()).reduce(new ArrayList<String>(), (array, item) -> {
			array.add(item.build()); return array;
		}, (left, right) -> left).toArray(new String[] {});
	}
	
	/**
	 * 添加SQL分组构造器.
	 * @param group SQLGroup
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 下午12:34:28
	 */
	public SQLConfigurerGroup addGroup(final SQLBuilderGroup group) {
		Assert.notNull(group, "SQLBuilderGroup cannot be null");
		groups.add(group); 
		return this;
	}
	
	/**
	 * 添加SQL分组构造器.
	 * @param field 字段名
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 下午12:35:06
	 */
	public SQLConfigurerGroup addGroup(final String field) {
		return addGroup(new SQLBuilderGroup(null, field));
	}
	
	/**
	 * 添加SQL分组构造器.
	 * @param table 表别名
	 * @param field 字段名
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 下午12:35:06
	 */
	public SQLConfigurerGroup addGroup(final String table, final String field) {
		return addGroup(new SQLBuilderGroup(table, field));
	}
}
