package com.arthur.framework.orm.support;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.service.bo.CommonBoAware;

/**
 * <p><b>描述</b>: 业务处理器 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午6:41:53
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class Processer{
	
	// business object
	private CommonBoAware<?> bo;
	
	// jdbc
	private JdbcTemplate jdbc;
	
	// jdbc sql properties
	private SQLProperties properties;
	
	// jdbc by named parameter
	private NamedParameterJdbcTemplate ndbc;
	
	private static Processer intance;
	
	public static Processer factory(){
		if(intance == null){
			intance = new Processer();
		}
		return intance;
	}
	
	private Processer() {}
	
	/**
	 * 
	 * <p><b>Title</b>: registerProcess </p> 
	 * <p><b>Author</b>: long.chan 2017年2月6日 上午10:04:56 </p>
	 * <p><b>Description</b>:  </p> 
	 * @param env 
	 *
	 * @param bo
	 * @param jdbc
	 * @param executor
	 *
	 */
	public void registerProcess(final CommonBoAware<?> bo, final JdbcTemplate jdbc){
		if(this.bo == null) this.bo = bo;
		if(this.jdbc == null) this.jdbc = jdbc;
		if(this.properties == null) this.properties = SQLProperties.newInstance();
		
		if(this.ndbc == null) this.ndbc = jdbc != null ? new NamedParameterJdbcTemplate(jdbc.getDataSource()) :  null;
	}
	
	@SuppressWarnings("unchecked")
	public <E extends CommonEntityAware> CommonBoAware<E> getBo() {
		return (CommonBoAware<E>) bo;
	}

	public void setBo(CommonBoAware<CommonEntityAware> bo) {
		this.bo = bo;
	}

	public JdbcTemplate getJdbc() {
		return jdbc;
	}

	public void setJdbc(JdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	public SimpleJdbcCall getCall() {
		return new SimpleJdbcCall(jdbc);
	}

	public SQLProperties getProperties() {
		return properties;
	}

	public void setProperties(SQLProperties properties) {
		this.properties = properties;
	}

	public NamedParameterJdbcTemplate getNdbc() {
		return ndbc;
	}

	public void setNdbc(NamedParameterJdbcTemplate ndbc) {
		this.ndbc = ndbc;
	}
}
