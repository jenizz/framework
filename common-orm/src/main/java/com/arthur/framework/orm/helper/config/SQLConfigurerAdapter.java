package com.arthur.framework.orm.helper.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.ObjectUtils;

/**
 * <p>描述: SQL配置器适配器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月22日 上午10:28:56
 * @version 1.0.2017
 */
public abstract class SQLConfigurerAdapter implements SQLConfigurer {
		
	@Override
	public SQLConfigurerTable table() { return getObjenesis().setSQLConfigurer(new SQLConfigurerTable()); }
	
	@Override
	public SQLConfigurerField field() { return getObjenesis().setSQLConfigurer(new SQLConfigurerField()); }
	
	@Override
	public SQLConfigurerJoins joins() { return getObjenesis().setSQLConfigurer(new SQLConfigurerJoins()); }
	
	@Override
	public SQLConfigurerWhere where() { return getObjenesis().setSQLConfigurer(new SQLConfigurerWhere()); }
	
	@Override
	public SQLConfigurerGroup group() { return getObjenesis().setSQLConfigurer(new SQLConfigurerGroup()); }
	
	@Override
	public SQLConfigurerOrder order() { return getObjenesis().setSQLConfigurer(new SQLConfigurerOrder()); }
	
	@Override
	public String select() {
		SQLConfigurer configurer = getObjenesis();
		SQLConfigurerTable table = configurer.getSQLConfigurer(SQLConfigurerTable.class);
		SQLConfigurerField field = configurer.getSQLConfigurer(SQLConfigurerField.class);
		SQLConfigurerJoins joins = configurer.getSQLConfigurer(SQLConfigurerJoins.class);
		SQLConfigurerWhere where = configurer.getSQLConfigurer(SQLConfigurerWhere.class);
		SQLConfigurerGroup group = configurer.getSQLConfigurer(SQLConfigurerGroup.class);
		SQLConfigurerOrder order = configurer.getSQLConfigurer(SQLConfigurerOrder.class);
		
		return new SQL(){{
			SQL _SQL = SELECT((field == null ? new String[]{"*"} : field.build())).FROM(table.build());
			if (hasLeftJoins(joins)) _SQL.LEFT_OUTER_JOIN(joins.left().build());
			if (hasRightJoins(joins)) _SQL.RIGHT_OUTER_JOIN(joins.right().build());
			
			if (hasAndWhere(where)) _SQL.AND().WHERE(where.and().build());
			if (hasOrWhere(where)) _SQL.OR().WHERE(where.or().build());
			if (!ObjectUtils.isEmpty(group)) _SQL.GROUP_BY(group.build());
			if (!ObjectUtils.isEmpty(order)) _SQL.ORDER_BY(order.build());
		}}.toString();
	}
	
	@Override
	public <T> T build() {
		return null;
	}
	
	// 是否并发处理
	private boolean isParallel;
	
	public void setParallel(boolean isParallel) {
		this.isParallel = isParallel;
	}
	
	public boolean isParallel(){
		return isParallel;
	}

	// 对象实例池
	private Map<String, SQLConfigurer> objenesis = new LinkedHashMap<>();
	
	@Override
	public void setObjenesis(final SQLConfigurer configurer) {
		objenesis.put(SQLConfigurer.class.getName(), configurer);
	}
	
	@Override
	public synchronized SQLConfigurer getObjenesis() {
		return objenesis.get(SQLConfigurer.class.getName());
	}

	// 配置实例寄存器
	private Map<Class<? extends SQLConfigurer>, SQLConfigurer> configmap = new LinkedHashMap<>();
	
	@Override
	public <C extends SQLConfigurer> C setSQLConfigurer(final C configurer) {
		configurer.setObjenesis(this);
		configmap.put(configurer.getClass(), configurer);
		return configurer;
	}
	
	@Override @SuppressWarnings("unchecked")
	public <C extends SQLConfigurer> C getSQLConfigurer(final Class<? extends SQLConfigurer> classkey) {
		return (C) configmap.get(classkey);
	}
	
	private boolean hasAndWhere(final SQLConfigurerWhere where){
		return where != null && where.and().isNotEmpty();
	}
	
	private boolean hasOrWhere(final SQLConfigurerWhere where){
		return where != null && where.or().isNotEmpty();
	}
	
	private boolean hasLeftJoins(final SQLConfigurerJoins joins){
		return joins != null && joins.left().isNotEmpty();
	}
	
	private boolean hasRightJoins(final SQLConfigurerJoins joins){
		return joins != null && joins.right().isNotEmpty();
	}
}