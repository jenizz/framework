package com.arthur.framework.orm.helper;

import java.util.Map;

import com.arthur.framework.orm.helper.SQLHelper;

/**
 * <p><b>描述</b>: </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月12日 上午2:07:21
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class SQLProvider {

	/**
	 * .
	 * @param params
	 * @return
	 * @since 2017年3月12日 上午2:07:29
	 * @author chanlong(陈龙)
	 */
	public String insert(final Map<String, Object> params) {
		return SQLHelper.toInsert(params.get("entity"));
	}

	/**
	 * .
	 * @param params
	 * @return
	 * @since 2017年3月12日 上午2:07:34
	 * @author chanlong(陈龙)
	 */
	public String update(final Map<String, Object> params) {
		return SQLHelper.toUpdate(params.get("entity"));
	}

	/**
	 * .
	 * @param params
	 * @return
	 * @since 2017年3月12日 上午2:07:39
	 * @author chanlong(陈龙)
	 */
	public String delete(final Map<String, Object> params) {
		return SQLHelper.toDelete(params.get("entity"));
	}

	/**
	 * .
	 * @param params
	 * @return
	 * @since 2017年3月12日 上午2:07:43
	 * @author chanlong(陈龙)
	 */
	public String findOne(final Map<String, Object> params) {
		return null;
	}

	/**
	 * .
	 * @param params
	 * @return
	 * @since 2017年3月12日 上午2:07:47
	 * @author chanlong(陈龙)
	 */
	public String findAll(final Map<String, Object> params) {
		return null;
	}
}
