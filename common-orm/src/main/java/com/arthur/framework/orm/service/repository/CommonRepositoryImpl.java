package com.arthur.framework.orm.service.repository;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Predicate;

import org.hibernate.SQLQuery;
import org.hibernate.transform.BasicTransformerAdapter;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.arthur.framework.orm.helper.JPQLHelper;
import com.arthur.framework.orm.helper.SQLHelper;
import com.arthur.framework.orm.query.QueryBuilder;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;

/**
 * <p><b>描述</b>: Spring Data JPA 自定义全局Dao </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午1:07:14
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
@NoRepositoryBean
public class CommonRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements CommonRepositoryAware<T, ID> {

    private EntityManager entityManager;
    
    private QueryBuilder<T> builder;
    
    private T entity;
    
    private class ResultTransformer extends BasicTransformerAdapter {
		/** serialVersionUID(long):. */
		private static final long serialVersionUID = -1007803464732334201L;

		@Override
		public Object transformTuple(Object[] tuple, String[] aliases) {
			Map<String, Object> result = new LinkedHashMap<>();
			for(int i = 0, size = aliases.length; i < size; i++) {
				if (tuple[i] instanceof BigInteger) {
					tuple[i] = new BigDecimal((BigInteger)tuple[i]);
				} else if (tuple[i] instanceof Integer) {
					tuple[i] = new BigDecimal(Integer.valueOf(tuple[i].toString()));
				}
				result.put(aliases[i].toLowerCase(), tuple[i]);
			}
			return result;
		}
    }
    
    @Override
	public T findOne(final Function<T, Example<T>> function) throws Exception {
		return this.findOne(function.apply(this.entity));
	}

	@Override
	public List<T> findAll(final Function<T, Example<T>> function) throws Exception {
		return this.findAll(function.apply(this.entity));
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Pagination<T> findAll(final SQLHelper helper, final int offset, final int limit) {
		Query query = entityManager.createNativeQuery(helper.getSql());
		query.unwrap(SQLQuery.class).setResultTransformer(new ResultTransformer());
		List<T> result = query.setFirstResult(offset).setMaxResults(limit).getResultList();
		return new Pagination<T>(count(helper), result);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> findAll(final SQLHelper helper) {
		Query query = entityManager.createNativeQuery(helper.getSql());
		query.unwrap(SQLQuery.class).setResultTransformer(new ResultTransformer());
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<String, Object> findOne(final SQLHelper helper) {
		Query query = entityManager.createNativeQuery(helper.getSql());
		query.unwrap(SQLQuery.class).setResultTransformer(new ResultTransformer());
		return (Map<String, Object>) query.getSingleResult();
	}
	
	@Override
	public Pagination<T> queryForPagination(final String sql, final int offset, final int limit) {
		return findAll(SQLHelper.newInstance(sql), offset, limit);
	}
	
	@Override
	public List<Map<String, Object>> queryForList(final String sql) {
		return findAll(SQLHelper.newInstance(sql));
	}

	@Override
	public Map<String, Object> queryForMap(final String sql) {
		return findOne(SQLHelper.newInstance(sql));
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Pagination<T> findAll(final JPQLHelper helper, final int offset, final int limit) throws Exception {
		Query query = entityManager.createQuery(helper.getSql());
		List<T> result = query.setFirstResult(offset).setMaxResults(limit).getResultList();
		return new Pagination<T>(count(helper), result);
	}
	
	@Override
	public Pagination<T> findAll(final Parameters params) throws Exception {
		Predicate predicate = builder.createQuery(params).toPredicate();
		List<T> result = builder.query(predicate).getResultList();
		long count = builder.count(predicate).getSingleResult().longValue();
		return new Pagination<T>(count, result);
	}
	
	@Override
	public Long count(final SQLHelper helper) {
		Query query = entityManager.createNativeQuery(helper.getCountSql());
		return Long.valueOf(String.valueOf(query.getSingleResult()));
	}
	
	@Override
	public Long count(final JPQLHelper helper) throws Exception {
		TypedQuery<Long> query = entityManager.createQuery(helper.getCountSql(), Long.class);
		return query.getSingleResult();
	}
	
	@Override
	public Long count(final Parameters params) throws Exception {
		return 0L;
	}

	public CommonRepositoryImpl(final Class<T> domainClass, final EntityManager em) {
		this(JpaEntityInformationSupport.getEntityInformation(domainClass, em), em);
		this.setEntity(domainClass);
	}

	public CommonRepositoryImpl(final JpaEntityInformation<T, ?> information, final EntityManager em) {
		super(information, em);
		this.setEntityManager(em);
		this.setBuilder(new QueryBuilder<>(em));
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public QueryBuilder<T> getBuilder() {
		return builder;
	}

	public void setBuilder(QueryBuilder<T> builder) {
		this.builder = builder;
	}

	public T getEntity() {
		return entity;
	}

	public void setEntity(T entity) {
		this.entity = entity;
	}
	
	public void setEntity(Class<T> _class) {
		try {
			this.entity = _class.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException("new entity instance is error", e);
		}
	}
}
