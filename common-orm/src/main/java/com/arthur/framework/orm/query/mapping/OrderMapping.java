package com.arthur.framework.orm.query.mapping;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

/**
 * <p><b>描述</b>: 排序字段映射 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午8:52:38
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class OrderMapping implements IMapping<Order> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 7279837458933506687L;

	private String type;
	
	private String field;
	
	public OrderMapping() { }
	
	public OrderMapping(final String field, final String type) {
		this.field = field;
		this.type = type;
	}
	
	/**
	 * 返回排序表达式.
	 * @param builder
	 * @param root
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2016年1月20日 下午3:09:21
	 */
	@Override
	public Order getExpresion(final Root<?> root, final CriteriaBuilder builder){
		switch(OrderType.valueOf(type)){
			case DESC:
			case desc:
				return builder.desc(root.get(field));
			default:
				return builder.asc(root.get(field));
		}
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getField() {
		return field;
	}


	public void setField(String field) {
		this.field = field;
	}

	private enum OrderType{
		asc,desc,ASC,DESC
	}
}
