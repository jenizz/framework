package com.arthur.framework.orm.query.expression;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.arthur.framework.orm.query.Criterion;
import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.orm.query.mapping.FieldMapping;
import com.arthur.framework.util.string.StringUtil;

/**
 * 简单条件表达式
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * 
 * @author chanlong(陈龙)
 * @date 2016年1月18日 下午4:52:24
 * @version 1.0.2016
 */
public class SimpleExpression implements Criterion {
	
	private FieldMapping field;
	
	public SimpleExpression() { }
	
	public SimpleExpression(final FieldMapping field) {
		this.field = field;
	}
	
	@Override
	public Predicate toPredicate(final Root<?> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {
		String name = field.getName();
		OperatorType operator = field.getOperatorType();
		
		Path<?> expression = null;  
        if(name.contains(".")){  
            String[] names = StringUtil.split(name, ".");  
            expression = root.get(names[0]);  
            for (int i = 1; i < names.length; i++) {  
                expression = expression.get(names[i]);  
            }  
        }else{  
            expression = root.get(name);  
        }  
        
        return operator.toPredicate(field, expression, builder);
	}
}
