package com.arthur.framework.orm.helper.builder;

import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.util.string.StringUtil;

public class SQLBuilderWhere extends SQLBuilderAdapter {
	
	private String sql;
	
	private Object value;
	
	private OperatorType operator;
	
	/**
	 * @see com.arthur.framework.orm.helper.builder.SQLBuilder#build()
	 * @author chanlong(陈龙)
	 * @date 2017年7月5日 上午10:21:02
	 */
	@Override @SuppressWarnings("unchecked")
	public <T> T build() {
		String table = getPrefix();
		String field = getName();
		return (T) (StringUtil.isNotEmpty(sql) ? sql : StringUtil.isNotEmpty(value) ? operator.toPredicate(table, field, value) : null);
	}
	
	public SQLBuilderWhere() {

	}

	public SQLBuilderWhere(final String sql) {
		this.sql = sql;
	}
	
	public SQLBuilderWhere(final String field, final String operator, final Object value) {
		setName(field);
		this.value = value;
		this.operator = OperatorType.valueOf(operator);
	}
	
	public SQLBuilderWhere(final String field, final OperatorType operator, final Object value) {
		setName(field);
		this.value = value;
		this.operator = operator;
	}
	
	public SQLBuilderWhere(final String prefix, final String field, final OperatorType operator, final Object value) {
		setPrefix(prefix);
		setName(field);
		this.value = value;
		this.operator = operator;
	}
	
	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public OperatorType getOperator() {
		return operator;
	}

	public void setOperator(OperatorType operator) {
		this.operator = operator;
	}
}
