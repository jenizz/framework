package com.arthur.framework.orm.entity;

import java.io.Serializable;
import java.util.Map;

import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;

/**
 * <p><b>Author</b>: long.chan 2017年2月6日 上午9:12:54 </p>
 * <p><b>ClassName</b>: CommonEntityAware.java </p> 
 * <p><b>Description</b>: </p> 
 *
 * @param <ID>
 */
public interface CommonEntityAware extends Serializable {

	public <E> E save(final String... options) throws Exception;
	
	public <E> E load(final String... options) throws Exception;
	
	public boolean delete(final String... options) throws Exception;
	
	public Pagination<?> query(final String... options) throws Exception;
	
	public <E> Pagination<E> query(final Parameters params, final String... options) throws Exception;
	
	public Pagination<?> query(final Map<String, Object> params, final String... options) throws Exception;
	
	public Object getPK();
	
	public void setPK(final Object value);
	
	public Object getProperty(final String name);
	
	public void setProperty(final String name, final Object value);
	
}
