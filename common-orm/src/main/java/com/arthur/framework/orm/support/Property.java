/**
 * <p>Copyright:Copyright(c) 2018</p>
 * <p>Company:Professional</p>
 * <p>Package:com.arthur.framework.orm.support</p>
 * <p>File:Property.java</p>
 * <p>类更新历史信息</p>
 * @todo chanlong(陈龙) 创建于 2018年3月16日 上午11:13:29
 */
package com.arthur.framework.orm.support;

import java.io.Serializable;

/**
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2018年3月16日 上午11:13:29
 * @version 1.0.2018
 */
public class Property implements Serializable {
	
	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 8332766570621599557L;

	private String name;
	
	private Object value;
	
	public Property() {

	}
	
	public Property(String name, Object value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
