package com.arthur.framework.orm.query.enums;

public enum ValueType {
	Sql, SqlStr, SqlLike,
	Date, Dates,
	Long, Longs,
	Float, Floats,
	String, Strings,
	Double, Doubles,
	Integer, Integers
}
