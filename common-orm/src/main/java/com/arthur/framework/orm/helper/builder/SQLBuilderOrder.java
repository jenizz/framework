package com.arthur.framework.orm.helper.builder;

import com.arthur.framework.util.string.StringUtil;

public class SQLBuilderOrder extends SQLBuilderAdapter {

	@Override @SuppressWarnings("unchecked")
	public <T> T build() {
		String table = getPrefix();
		String field = getName();
		return (T) (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(" ").append(order).toString();
	}
	
	public SQLBuilderOrder(String field, String order) {
		setName(field);
		this.order = order;
	}
	
	public SQLBuilderOrder(String prefix, String field, String order) {
		setPrefix(prefix);
		setName(field);
		this.order = order;
	}

	private String order;
	
	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}
}
