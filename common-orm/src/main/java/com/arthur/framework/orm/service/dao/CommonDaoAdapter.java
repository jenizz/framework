package com.arthur.framework.orm.service.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.Predicate;

import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.helper.SQLHelper;
import com.arthur.framework.orm.query.QueryBuilder;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;

/**
 * <p><b>描述</b>: 数据访问接口 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午12:54:59
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public abstract class CommonDaoAdapter<E extends CommonEntityAware> implements CommonDaoAware<E> {
	
	@Autowired
	private QueryBuilder<E> queryBuilder;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public E insert(final E entity) throws Exception {
		Assert.notNull(entity, "实体对象不能为空!");
		entityManager.persist(entity);
		return entity;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public E update(final E entity) throws Exception {
		Assert.notNull(entity, "实体对象不能为空!");
		E _entity = entityManager.merge(entity);
		entityManager.flush();
		return _entity;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public E delete(final E entity) throws Exception {
		Assert.notNull(entity, "实体对象不能为空!");
		entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
		return entity;
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final E entity) throws Exception {
		Assert.notNull(entity, "实体对象不能为空!");
		Object pk = entity.getPK();
		Assert.notNull(pk, "主键不能为空!");
		return (E) entityManager.getReference(entity.getClass(), pk);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final Class<E> cls, final Serializable id) throws Exception {
		Assert.notNull(id, "主键不能为空!");
		return entityManager.getReference(cls, id);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final Class<E> cls, final Parameters params) throws Exception {
		Assert.notNull(params, "查询参数不能为空!");
		Predicate predicate = getQueryBuilder().createQuery(params).toPredicate();
		List<E> result = getQueryBuilder().query(predicate).getResultList();
		if(result != null && !result.isEmpty()) {
			return result.get(0);
		}else{
			return null;
		}
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public E findOne(final Class<E> cls, final String field, final Object value) throws Exception {
		Assert.notNull(field, "字段不能为空!");
		Assert.notNull(value, "值不能为空!");
		return getQueryBuilder().getSingleResult(cls, field, value);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Pagination<E> findAll(final Parameters params) throws Exception {
		Assert.notNull(params, "查询参数不能为空!");
		Predicate predicate = getQueryBuilder().createQuery(params).toPredicate();
		List<E> result = getQueryBuilder().query(predicate).getResultList();
		long count = getQueryBuilder().count(predicate).getSingleResult().longValue();
		return new Pagination<E>(count, result);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Pagination<E> findAll(final SQLHelper helper, final int offset, final int limit) throws Exception {
		Query query = entityManager.createNativeQuery(helper.getSql());
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<E> result = query.setFirstResult(offset).setMaxResults(limit).getResultList();
		return new Pagination<E>(count(helper), result);
	}

	@Override
	public List<E> findAll(final Class<E> cls, final String jpql) throws Exception {
		return findAll(cls, jpql, null);
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<E> findAll(final Class<E> cls, final String jpql, final Map<String, Object> params) throws Exception {
		TypedQuery<E> query = entityManager.createQuery(jpql, cls);
		return getQueryBuilder().createParams(params, query).getResultList();
	}

	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Long count(final Parameters params) throws Exception {
		return getQueryBuilder().createQuery(params).count().getSingleResult().longValue();
	}
	
	@Override
	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Long count(final SQLHelper helper) {
		Query query = entityManager.createNativeQuery(helper.getCountSql());
		return Long.valueOf(String.valueOf(query.getSingleResult()));
	}

	public QueryBuilder<E> getQueryBuilder() {
		return queryBuilder;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
}

