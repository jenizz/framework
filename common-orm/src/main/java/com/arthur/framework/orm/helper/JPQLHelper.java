package com.arthur.framework.orm.helper;

public class JPQLHelper extends AbstractSQLHelper {

	private String sql;
	
	public JPQLHelper(final String jpql){
		this.sql = jpql;
	}
	
	@Override
	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}
	
}
