package com.arthur.framework.orm.helper.builder;

public interface SQLBuilder {

	/**
	 * 构造SQL语句.
	 * @author chanlong(陈龙)
	 * @date 2017年5月23日 下午2:25:07
	 */
	<T> T build();
	
}
