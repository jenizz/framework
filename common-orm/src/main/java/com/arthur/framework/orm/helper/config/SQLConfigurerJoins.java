package com.arthur.framework.orm.helper.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;

import com.arthur.framework.orm.helper.builder.SQLBuilderJoins;
import com.arthur.framework.orm.helper.builder.SQLBuilderTable;

public final class SQLConfigurerJoins extends SQLConfigurerAdapter {

	@Override @SuppressWarnings("unchecked")
	public String[] build() {
		try{
			return (isParallel() ? joinmap.get(target).parallelStream() : joinmap.get(target).stream()).reduce(new ArrayList<String>(), (array, item) -> {
				array.add(item.build()); return array;
			}, (left, right) -> left).toArray(new String[] {});
		} catch (NullPointerException e) {
			return null;
		}
	}
	
	/**
	 * 添加连接构造器.
	 * @param join
	 * @author chanlong(陈龙)
	 * @date 2017年5月23日 下午1:32:50
	 */
	public SQLConfigurerJoins addJoins(final SQLBuilderJoins join) {
		Assert.notNull(join, "SQLJoin cannot be null");
		if (joinmap.get(target) == null) joinmap.put(target, new ArrayList<>());
		joinmap.get(target).add(join);
		return this;
	}

	/**
	 * 添加连接构造器.
	 * @param table
	 * @author chanlong(陈龙)
	 * @date 2017年5月23日 下午1:33:15
	 */
	public SQLConfigurerJoins addJoins(final SQLBuilderTable table) {
		return addJoins(new SQLBuilderJoins(table));
	}
	
	public SQLConfigurerJoins left() {
		target = "left"; return this;
	}
	
	public SQLConfigurerJoins right() {
		target = "right"; return this;
	}
	
	public boolean isNotEmpty(){
		return joinmap.get(target) != null && !joinmap.get(target).isEmpty();
	}
	
	private String target = "left";
	private Map<String, List<SQLBuilderJoins>> joinmap = new HashMap<>();
}
