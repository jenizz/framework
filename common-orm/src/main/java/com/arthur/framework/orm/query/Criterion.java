package com.arthur.framework.orm.query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * <p><b>描述</b>: 条件表达式接口 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午9:10:44
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public interface Criterion {

	/**
	 * 构造条件表达式.
	 * @param root
	 * @param query
	 * @param builder
	 * @return
	 * @since 2017年3月4日 上午9:11:01
	 * @author chanlong(陈龙)
	 */
    public Predicate toPredicate(Root<?> root, CriteriaQuery<?> query,  CriteriaBuilder builder);  
}
