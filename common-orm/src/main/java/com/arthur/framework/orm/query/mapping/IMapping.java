/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月3日 下午5:19:27 </p>
 * <p><b>ClassName</b>: IParameters.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.orm.query.mapping;

import java.io.Serializable;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

/**
 * <p><b>Author</b>: long.chan 2017年3月3日 下午5:19:27 </p>
 * <p><b>ClassName</b>: IParameters.java </p> 
 * <p><b>Description</b>: </p> 
 */
public interface IMapping<T> extends Serializable {

	public T getExpresion(final Root<?> root, final CriteriaBuilder builder);
}
