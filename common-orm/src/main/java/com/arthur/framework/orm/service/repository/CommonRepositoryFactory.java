package com.arthur.framework.orm.service.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.util.Assert;

/**
 * <p><b>描述</b>: Spring Data JPA 自定义工厂 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 下午1:05:00
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class CommonRepositoryFactory extends JpaRepositoryFactory {

	private final EntityManager entityManager;
	
	public CommonRepositoryFactory(EntityManager em) {
		super(em);
		Assert.notNull(em, "实体管理器不能为空");
		this.entityManager = em;
	}

	@Override
	protected <T, ID extends Serializable> SimpleJpaRepository<?, ?> getTargetRepository(RepositoryInformation information, EntityManager em) {
		return new CommonRepositoryImpl<>(information.getDomainType(), em);
	}

	@Override
	protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
		return CommonRepositoryImpl.class;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
}
