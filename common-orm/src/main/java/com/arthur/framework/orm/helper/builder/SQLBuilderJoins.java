package com.arthur.framework.orm.helper.builder;

public class SQLBuilderJoins extends SQLBuilderAdapter {

	private SQLBuilderTable table;

	public SQLBuilderJoins(SQLBuilderTable table) {
		this.table = table;
	}
	
	public <T> T build(){
		return table.build();
	}
}
