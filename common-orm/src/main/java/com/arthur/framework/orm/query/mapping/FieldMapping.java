package com.arthur.framework.orm.query.mapping;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Root;

import com.arthur.framework.orm.query.Criterion;
import com.arthur.framework.orm.query.Restrictions;
import com.arthur.framework.orm.query.enums.OperatorType;
import com.arthur.framework.orm.query.enums.ValueType;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * <p><b>描述</b>: </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午8:36:36
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
@JsonAutoDetect
public class FieldMapping implements IMapping<Criterion> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 974856913461014603L;

	private String type;
	
	private String name;
	
	private Object value;
	
	private String operator;
	
	public FieldMapping() { }
	
	public FieldMapping(final String type, final String name, final Object value, final String operator) { 
		this.type = type;
		this.name = name;
		this.value = value;
		this.operator = operator;
	}
	
	public FieldMapping(final ValueType type, final String name, final Object value, final OperatorType operator) { 
		this.type = type.name();
		this.name = name;
		this.value = value;
		this.operator = operator.name();
	}
	
	@Override
	public Criterion getExpresion(final Root<?> root, final CriteriaBuilder builder) {
		return Restrictions.exec(this, true);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public String getOperator() {
		return operator;
	}
	
	public OperatorType getOperatorType() {
		return OperatorType.valueOf(operator);
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}
	
}
