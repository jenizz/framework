package com.arthur.framework.orm.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

/**
 * <p><b>Author</b>: long.chan 2017年2月7日 下午3:34:18 </p>
 * <p><b>ClassName</b>: CommonEntityUUID.java </p> 
 * <p><b>Description</b>: </p> 
 */
@MappedSuperclass
public abstract class CommonEntityUUID<E> extends AbstractCommonEntity<E> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 6868718716266032062L;

	@Id
	@GeneratedValue(generator = "UUIDHexGenerator")
	@GenericGenerator(name = "UUIDHexGenerator", strategy = "uuid.hex")
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
