package com.arthur.framework.orm.query.mapping;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;

/**
 * <p><b>描述</b>: 分组字段映射 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午8:53:25
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class GroupMapping implements IMapping<Expression<?>> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 7279837458933506687L;

	private String field;
	
	public GroupMapping() { }

	public GroupMapping(final String field) {
		this.field = field;
	}
	
	/**
	 * 返回分组条件.
	 * @see com.arthur.framework.orm.query.mapping.IMapping#getExpresion(javax.persistence.criteria.CriteriaBuilder, javax.persistence.criteria.Root)
	 * @since 2017年3月4日 上午8:55:53
	 * @author chanlong(陈龙)
	 */
	@Override
	public Expression<?> getExpresion(final Root<?> root, final CriteriaBuilder builder) {
		return root.get(field);
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
}
