package com.arthur.framework.orm.query;

import com.arthur.framework.orm.query.enums.RelationType;
import com.arthur.framework.orm.query.expression.LogicalExpression;
import com.arthur.framework.orm.query.expression.SimpleExpression;
import com.arthur.framework.orm.query.mapping.FieldMapping;
import com.arthur.framework.orm.query.support.ValueProcessor;

/**
 * <p><b>描述</b>: 动态条件构造器 </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月4日 上午10:48:43
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public class Restrictions {
	
	/**
	 * 条件.
	 * @param bean
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2016年1月20日 下午4:48:47
	 */
	public static SimpleExpression exec(final FieldMapping field, boolean ignoreNull) {  
		Object value = field.getValue();
		if (ignoreNull && value == null) {
			return null;
		} else {
			return new SimpleExpression(field);
		}
    }
	
	public static LogicalExpression or(final Criterion... criterions){  
        return new LogicalExpression(criterions, RelationType.OR);  
    } 
	
	public static LogicalExpression and(final Criterion... criterions){  
        return new LogicalExpression(criterions, RelationType.AND);  
    }
	
	public static LogicalExpression in(final FieldMapping field, boolean ignoreNull) {
		Object value = field.getValue();
		if (ignoreNull && value == null) return null;

		Object[] values = ValueProcessor.<Object[]> parseValue(value, field.getType());
		SimpleExpression[] expressions = new SimpleExpression[values.length];
		for (int i = 0; i < values.length; i++) {
			expressions[i] = new SimpleExpression(field);
		}
		return new LogicalExpression(expressions, RelationType.OR);
	}
}
