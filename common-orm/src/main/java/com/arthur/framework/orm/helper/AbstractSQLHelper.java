package com.arthur.framework.orm.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Table;

import com.arthur.framework.util.reflect.ReflectUtil;
import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>描述</b>: </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月12日 上午2:10:23
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public abstract class AbstractSQLHelper {
	
	public abstract String getSql();
	
	public String getCountSql(){
		String sql = getSql().toUpperCase();
		String orderby = "ORDER BY";
		String groupby = "GROUP BY";
		String str = sql.substring(0, sql.indexOf("FROM"));
		if (!isSubselect(sql, orderby) && sql.contains(orderby)) {
			sql = sql.substring(0, sql.lastIndexOf(orderby));
		}
		if (!isSubselect(sql, groupby) && sql.contains(groupby)) {
			sql = sql.substring(0, sql.lastIndexOf(groupby));
		}
		return sql.replace(str, "select count(0) ");
	}
	
	/**
	 * 获取表名.
	 * @param entity
	 * @return
	 * @since 2017年3月12日 上午2:12:41
	 * @author chanlong(陈龙)
	 */
	protected static String getTable(final Object entity) {
		return getTable(entity.getClass());
	}
	
	/**
	 * 获取表名.
	 * @param clazz
	 * @return
	 * @since 2017年3月12日 上午2:10:28
	 * @author chanlong(陈龙)
	 */
	protected static String getTable(final Class<?> clazz) {
		Table table = clazz.getAnnotation(Table.class);
		if (table != null) {
			return table.name();
		} else {
			return clazz.getSimpleName();
		}
	}
	
	protected static String[] getValues(final Object entity) { 
		List<String> names = new ArrayList<>();
		List<String> values = new ArrayList<>();
		
		Map<String, Object> map = ReflectUtil.getProperties(entity);
		Set<String> entry = map.keySet();
		entry.forEach(key -> {
			names.add(key);
			values.add(getKeyValue(key));
		});
		
		return new String[] { 
			names.stream().collect(Collectors.joining(",")),
			values.stream().collect(Collectors.joining(",")) 
		};
    }  
	
	protected static String[] getSets(Object entity) {
		List<String> names = new ArrayList<>();
		
		Map<String, Object> map = ReflectUtil.getProperties(entity);
		Set<String> entry = map.keySet();
		entry.forEach(key -> {
			if(StringUtil.isNotEmpty(map.get(key))){
				names.add(getSetValue(key));
			}
		});
		
		return new String[] { 
			names.stream().collect(Collectors.joining(",")),
		};
	}
	
	protected static String getSetValue(final String key) {
		return new StringBuilder(key).append("=").append("#{entity.").append(key).append("}").toString();
	}
	
	protected static String getKeyValue(final String key) {
		return new StringBuilder("#{entity.").append(key).append("}").toString();
	}
	
	/**
	 * 获取查询条件.
	 * @param entity
	 * @return
	 * @since 2017年3月13日 下午1:42:57
	 * @author chanlong(陈龙)
	 */
	protected static String[] toConditions(final Object entity){
		List<String> conditions = new ArrayList<>();
		Map<String, Object> map = ReflectUtil.getProperties(entity);
		Set<String> entry = map.keySet();
		entry.forEach(key -> {
			if(StringUtil.isNotEmpty(map.get(key))){
				//conditions.add(toCondition(key, OperatorType.eq.value(), "entity"));
			}
		});
		return conditions.toArray(new String[]{});
	}
	
	// 判断`regex`是否为子查询
	private boolean isSubselect(final String sql, final String regex) {
		int indexof = sql.lastIndexOf(")");
		return indexof > -1 && sql.lastIndexOf(regex) < indexof;
	}
}
