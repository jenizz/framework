package com.arthur.framework.orm.helper.builder;

import com.arthur.framework.util.string.StringUtil;

/**
 * <p>描述: SQL字段构造器</p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月10日 下午1:11:48
 * @version 1.0.2017
 */
public class SQLBuilderField extends SQLBuilderAdapter {

	/**
	 * @see com.arthur.framework.orm.helper.builder.SQLBuilder#build()
	 * @author chanlong(陈龙)
	 * @date 2017年7月5日 上午10:15:01
	 */
	@Override @SuppressWarnings("unchecked")
	public <T> T build() {
		String prefix = getPrefix();
		String field = getName();
		String alias = getAlias();
		
		if(StringUtil.isNotEmpty(prefix)){
			return (T) toString(prefix);
		}else{
			return (T) (StringUtil.isNotEmpty(alias) 
					 ? new StringBuilder().append(field).append(" as ").append(alias).toString()
					 : new StringBuilder().append(field).toString());
		}
	}
	
	public SQLBuilderField(String field, String alias) {
		setName(field);
		setAlias(alias);
	}
	
	public SQLBuilderField(String prefix, String field, String alias) {
		setPrefix(prefix);
		setName(field);
		setAlias(alias);
	}
	
	private String toString(final String prefix) {
		String field = getName();
		String alias = getAlias();
		
		return StringUtil.isNotEmpty(alias) 
				 ? new StringBuilder(prefix).append(".").append(field).append(" as ").append(alias).toString()
				 : new StringBuilder(prefix).append(".").append(field).toString();
	}
}
