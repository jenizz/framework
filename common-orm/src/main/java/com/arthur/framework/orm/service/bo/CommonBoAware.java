package com.arthur.framework.orm.service.bo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.query.mapping.Parameters;
import com.arthur.framework.orm.query.support.Pagination;
import com.arthur.framework.orm.support.Property;

/**
 * <p><b>Author</b>: long.chan 2017年2月6日 上午9:46:48 </p>
 * <p><b>ClassName</b>: CommonBoAware.java </p> 
 * <p><b>Description</b>: </p> 
 *
 * @param <ENTITY>
 * @param <ID>
 */
public interface CommonBoAware<E extends CommonEntityAware> {
	
	/**
	 * 保存或持久化实体.
	 * @param entity
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:30:37
	 */
	E saveOrUpdate(final E entity) throws Exception;
	
	/**
	 * 批量保存或持久化实体.
	 * @param entities
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:31:08
	 */
	List<E> saveOrUpdate(final List<E> entities) throws Exception;
	
	/**
	 * 根据主键数组，批量更新指定类型的实体，的指定属性.
	 * @param cls
	 * @param ids
	 * @param properties
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 下午1:32:55
	 */
	boolean update(final Class<E> cls, final Map<String, Object> properties, final Serializable... ids) throws Exception;
	
	/**
	 * 删除指定实体.
	 * @param entity
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:31:25
	 */
	boolean delete(final E entity) throws Exception;
	
	/**
	 * 根据主键，删除指定类型的实体.
	 * @param cls
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:33:43
	 */
	boolean delete(final Class<E> cls, final Serializable id) throws Exception;

	/**
	 * 根据主键数组，批量删除指定类型的实体.
	 * @param cls
	 * @param ids
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:34:33
	 */
	boolean delete(final Class<E> cls, final Serializable... ids) throws Exception;
	
	/**
	 * 查询实体的唯一结果.
	 * @param entity
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:35:00
	 */
	E findOne(final E entity) throws Exception;
	
	/**
	 * 根据主键，查询指定类型的实体，的唯一结果.
	 * @param cls
	 * @param id
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:36:32
	 */
	E findOne(final Class<E> cls, final Serializable id) throws Exception;
	
	/**
	 * 根据查询参数构造器，查询指定类型的实体，的唯一结果.
	 * @param cls
	 * @param params
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:37:53
	 */
	E findOne(final Class<E> cls, final Parameters params) throws Exception;
	
	/**
	 * 根据指定字段名与字段值，查询指定类型的实体，的唯一结果.
	 * @param cls
	 * @param field
	 * @param value
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:38:28
	 */
	E findOne(final Class<E> cls, final String field, final Object value) throws Exception;
	
	/**
	 * 根据主键数组，查询指定类型的实体，的结果集.
	 * @param cls
	 * @param ids
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:39:11
	 */
	List<E> findAll(final Class<E> cls, final Serializable... ids) throws Exception;
	
	/**
	 * 根据指定字段名与字段值，查询指定类型的实体，的结果集.
	 * @param cls
	 * @param field
	 * @param value
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 下午4:18:13
	 */
	List<E> findAll(final Class<E> cls, final String field, final Object value) throws Exception;
	
	/**
	 * 根据查询参数构造器，查询实体的分页结果集.
	 * @param params
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:40:54
	 */
	Pagination<E> findAll(final Parameters params) throws Exception;

	/**
	 * 根据查询参数构造器，查询结果集总数.
	 * @param params
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:46:12
	 */
	Long count(final Parameters params) throws Exception;
	
	/**
	 * 根据查询参数对，查询指定属性的结果集.</br>
	 * PS:一般用于查询下拉项，此接口需具体业务BO，自行实现
	 * @param property
	 * @param params
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:41:52
	 */
	List<Property> findByProperty(final String property, final Map<String,Object> params) throws Exception;
	
	/**
	 * 根据查询参数对，查询指定类型的，指定属性的结果集.</br>
	 * PS:一般用于查询下拉项
	 * @param cls
	 * @param property
	 * @param params
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月16日 上午11:43:04
	 */
	List<Property> findByProperty(final Class<E> cls, final String property, final Map<String, Object> params) throws Exception;

}
