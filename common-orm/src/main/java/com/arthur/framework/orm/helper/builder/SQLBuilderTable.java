package com.arthur.framework.orm.helper.builder;

import org.springframework.util.ObjectUtils;

import com.arthur.framework.util.string.StringUtil;

/**
 * <p>描述: SQL表名构造器</p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月10日 下午12:25:23
 * @version 1.0.2017
 */
public class SQLBuilderTable extends SQLBuilderAdapter {
	
	private SQLBuilderField equals;
	
	private SQLBuilderField field;
	
	/**
	 * @see com.arthur.framework.orm.helper.builder.SQLBuilder#build()
	 * @author chanlong(陈龙)
	 * @date 2017年7月5日 上午10:16:24
	 */
	@Override @SuppressWarnings("unchecked")
	public <T> T build() {
		String prefix = getPrefix();
		String alias = getAlias();
		String name = getName();
		
		StringBuilder strb = new StringBuilder();
		
		if(StringUtil.isNotEmpty(prefix)) strb.append(prefix).append(".");
		strb.append(name);
		if(StringUtil.isNotEmpty(alias)) strb.append(" ").append(alias);
		
		if(!ObjectUtils.isEmpty(field)){
			strb.append(" on ").append(field.build().toString()).append(" = ").append(equals.<String>build());
		}
		return (T) strb.toString();
	}
	
	public SQLBuilderTable on(final SQLBuilderField field) {
		this.field = field; return this;
	}
	
	public SQLBuilderTable on(final String table, final String field) {
		return on(new SQLBuilderField(table, field, null));
	}
	
	public SQLBuilderTable eq(final String field) {
		this.equals = new SQLBuilderField(getAlias(), field, null); return this;
	}
	
	public SQLBuilderTable eq(final String prefix, String field) {
		this.equals = new SQLBuilderField(prefix, field, null); return this;
	}
	
	public SQLBuilderTable() {

	}
	
	public SQLBuilderTable(String name) {
		setName(name);
	}
	
	public SQLBuilderTable(String name, String alias) {
		setName(name);
		setAlias(alias);
	}

}
