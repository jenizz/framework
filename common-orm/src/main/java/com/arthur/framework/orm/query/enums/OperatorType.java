package com.arthur.framework.orm.query.enums;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import com.arthur.framework.orm.query.mapping.FieldMapping;
import com.arthur.framework.orm.query.support.ValueProcessor;
import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>描述</b>: </p>
 * <p><b>Company</b>: Professional</p>
 * @since 2017年3月14日 上午9:24:09
 * @author chanlong(陈龙)
 * @version 1.0.2017
 */
public enum OperatorType {
	/** 包含 **/
	in(" in ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return IN.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return IN.toPredicate(table, field, value);
		}
	},
	IN(" in ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return expression.in(ValueProcessor.<Object[]>parseValue(field.getValue(), field.getType()));
		}
		
		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 不包含
	notIn(" not in ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return NOTIN.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return NOTIN.toPredicate(table, field, value);
		}
	},
	NOTIN(" not in ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.not(expression.in(ValueProcessor.<Object[]>parseValue(field.getValue(), field.getType())));
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 等于
	eq(" = ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return EQ.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return EQ.toPredicate(table, field, value);
		}
	},
	EQ(" = ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.equal(expression, ValueProcessor.<Object>parseValue(field.getValue(), field.getType()));  
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 不等于
	ne(" <> ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return NE.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return NE.toPredicate(table, field, value);
		}
	},
	NE(" <> ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.notEqual(expression, ValueProcessor.<Object>parseValue(field.getValue(), field.getType()));
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 大于等于
	ge(" >= ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return GE.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return GE.toPredicate(table, field, value);
		}
	},
	GE(" >= ") {
		@Override
		@SuppressWarnings("unchecked")
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.greaterThanOrEqualTo((Path<Comparable<Object>>)expression, ValueProcessor.<Comparable<Object>>parseValue(field.getValue(), field.getType()));
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 小于等于
	le(" <= ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return LE.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return LE.toPredicate(table, field, value);
		}
	},
	LE(" <= ") {
		@Override
		@SuppressWarnings("unchecked")
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.lessThanOrEqualTo((Path<Comparable<Object>>)expression, ValueProcessor.<Comparable<Object>>parseValue(field.getValue(), field.getType()));
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 大于
	gt(" > ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return GT.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return GT.toPredicate(table, field, value);
		}
	},
	GT(" > ") {
		@Override
		@SuppressWarnings("unchecked")
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.greaterThan((Path<Comparable<Object>>)expression, ValueProcessor.<Comparable<Object>>parseValue(field.getValue(), field.getType()));
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 小于
	lt(" < ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return LT.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return LT.toPredicate(table, field, value);
		}
	},
	LT(" < ") {
		@Override
		@SuppressWarnings("unchecked")
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.lessThan((Path<Comparable<Object>>)expression, ValueProcessor.<Comparable<Object>>parseValue(field.getValue(), field.getType()));
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlStr)).toString();
		}
	},
	// 模糊
	like(" like ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return LIKE.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return LIKE.toPredicate(table, field, value);
		}
	},
	LIKE(" like ") {
		@Override
		@SuppressWarnings("unchecked")
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return builder.like((Expression<String>) expression, "%" + field.getValue() + "%");  
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(ValueProcessor.<String>parseValue(value, ValueType.SqlLike)).toString();
		}
	},
	// 为空
	isNull(" is null ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return ISNULL.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return ISNULL.toPredicate(table, field, value);
		}
	},
	ISNULL(" is null ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return expression.isNull();
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).toString();
		}
	},
	// 非空
	isNotNull(" is not null ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return ISNOTNULL.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return ISNOTNULL.toPredicate(table, field, value);
		}
	},
	ISNOTNULL(" is not null ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return expression.isNotNull();
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).toString();
		}
	},
	// 两者之间
	between(" between ") {
		@Override
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			return BETWEEN.toPredicate(field, expression, builder);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			return BETWEEN.toPredicate(table, field, value);
		}
	},
	BETWEEN(" between ") {
		@Override
		@SuppressWarnings("unchecked")
		public Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder) {
			Object value = field.getValue();
			String type = field.getType();
			Comparable<Object> _s = ValueProcessor.<Comparable<Object>>parseValue(value, type, 0);
			Comparable<Object> _o = ValueProcessor.<Comparable<Object>>parseValue(value, type, 1);
			return builder.between((Path<Comparable<Object>>)expression, _s, _o);
		}

		@Override
		public String toPredicate(final String table, final String field, final Object value) {
			Comparable<Object> _s = ValueProcessor.<Comparable<Object>>parseValue(value, "SqlStr", 0);
			Comparable<Object> _o = ValueProcessor.<Comparable<Object>>parseValue(value, "SqlStr", 1);
			return (StringUtil.isNotEmpty(table) ? new StringBuilder(table).append(".") : new StringBuilder()).append(field).append(value()).append(_s).append(" and ").append(_o).toString();
		}
	};	
	
	private String operate;
	
	private OperatorType(final String operate) {
		this.operate = operate;
	}
	
	public String value() {
		return operate;
	}

	/**
	 * 构造SQL查询条件.
	 * @param table
	 * @param field
	 * @param value
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 上午9:39:02
	 */
	public abstract String toPredicate(final String table, final String field, final Object value);
	
	/**
	 * 构造JPA查询条件.
	 * @param field
	 * @param expression
	 * @param builder
	 * @author chanlong(陈龙)
	 * @date 2017年5月11日 上午9:39:08
	 */
	public abstract Predicate toPredicate(final FieldMapping field, final Path<?> expression, final CriteriaBuilder builder);
}
