package com.arthur.framework.orm.support;

import java.util.Map;
import java.util.Map.Entry;

import com.arthur.framework.util.PropertiesUtil;
import com.arthur.framework.util.string.StringUtil;

import java.util.Properties;
import java.util.Set;

public class SQLProperties {
	
	private static final String SQL_PATH = "/sql.xml";
	private static Properties properties = new Properties();
	private static SQLProperties instance = null;
	
	/**
	 * 创建SQLProperties实例.
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年3月31日 上午11:40:32
	 */
	public static SQLProperties newInstance(){
		if (instance == null) {
			instance = new SQLProperties();
		}
		
		PropertiesUtil.loader(SQL_PATH, properties);
		
		return instance;
	}

	/**
	 * 获取配置内容（可用于SQL配置文件）.
	 * @param key 键
	 * @param fields 要替换的内容
	 * @return 值
	 * @since 2017年3月4日 下午4:06:19
	 * @author chanlong(陈龙)
	 */
	public String getSQL(final String key, final String... fields) {
		String str = properties.getProperty(key);
		for (int i = 0; i < fields.length; i++) {
			str = str.replace("{" + i + "}", fields[i]);
		}
		return str;
	}
	
	/**
	 * 获取配置内容（可用于SQL配置文件）.
	 * @param key 键
	 * @param params 要替换的内容
	 * @return 值
	 * @since 2017年3月4日 下午4:08:03
	 * @author chanlong(陈龙)
	 */
	public String getSQL(final String key, final Map<String, Object> params) {
		String str = properties.getProperty(key);
		Set<Entry<String, Object>> entryset = params.entrySet();
		for (Entry<String, Object> entry : entryset) {
			str.replace("{" + entry.getKey() + "}", String.valueOf(entry.getValue()));
		}
		return str;
	}
	
	/**
	 * 获取配置内容（可用于SQL配置文件）.
	 * @param key 键
	 * @param regex 替换依据
	 * @param fields 要替换的内容
	 * @return String
	 * @since 2017年3月4日 下午4:20:15
	 * @author chanlong(陈龙)
	 */
	public String getSQL(final String key, final String regex, final String[] fields) {
		String str = properties.getProperty(key);
		return str.replace(regex, StringUtil.joinSQL(fields, "'"));
	}
}
