package com.arthur.framework.orm.service.bo;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.orm.entity.CommonEntityAware;

@Service("defaultBo")
@Transactional
public class CommonBoDefault extends CommonBoAdapter<CommonEntityAware> {

}
