package com.arthur.framework.orm.model;

import java.io.Serializable;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.util.reflect.ReflectUtil;

/**
 * <p>描述: 抽象数据模型 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月9日 下午12:50:44
 * @version 1.0.2017
 */
public abstract class AbstractCommonModel<M, E extends CommonEntityAware> implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -2320758697150214931L;

	/**
	 * 获取实体对象的数据模型.
	 * @param entity 实体对象
	 * @author chanlong(陈龙)
	 * @date 2017年5月9日 下午12:51:17
	 */
	@SuppressWarnings("unchecked")
	public M toModel(final E entity){
		return (M) ReflectUtil.copyProperties(entity, this);
	}
	
	/**
	 * 获取实体对象.
	 * @author chanlong(陈龙)
	 * @date 2017年9月18日 上午11:56:35
	 */
	public E toEntity(final E entity) {
		return (E) ReflectUtil.copyProperties(this, entity);
	}
}
