package com.arthur.framework.orm.helper;

import org.junit.Test;

import com.arthur.framework.orm.query.enums.OperatorType;

public class SQLHelperTest {

	@Test
	public void test() throws Exception {
		String sql = SQLHelper.createBuilder(true)
							  	.prefix("arthur")
				 			  .table()
				 			  	.name("tb_demo")
				 			  	.alias("o")
				 			  .field()
				 			  	.addField("name", "name")
				 			  .where()
				 			  	.addWhere("o", "name", OperatorType.EQ, "long")
				 			  .group()
				 			  	.addGroup("id")
				 			  .order()
				 			  	.addOrder("date", "desc")
				 			  .select();
		
		System.out.println(sql);
	}
}
