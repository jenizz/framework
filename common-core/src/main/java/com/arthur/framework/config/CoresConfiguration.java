/**
 * 
 * <p><b>Author</b>: long.chan 2017年2月17日 下午3:26:26 </p>
 * <p><b>ClassName</b>: TaskExecutorConfig.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.config;

import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.jetty.JettyEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * <p><b>Author</b>: long.chan 2017年2月17日 下午3:26:26 </p>
 * <p><b>ClassName</b>: Configurations.java </p> 
 * <p><b>Description</b>: </p> 
 */
@Configuration
@EnableSwagger2					// 开启Swagger
@PropertySource("classpath:configurations.properties")
public class CoresConfiguration {

	@Autowired
	private Environment env;
	
	/**
	 * 启用jetty服务.
	 * @author chanlong(陈龙)
	 * @date 2017年4月10日 下午2:37:10
	 */
	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		return new JettyEmbeddedServletContainerFactory();
	}
	
	/**
	 * 配置Kaptcha图片验证码生成规则.
	 * @author chanlong(陈龙)
	 * @date 2017年8月17日 上午11:36:41
	 */
	@Bean  
    public DefaultKaptcha getKaptchaBean(){  
		RelaxedPropertyResolver resolver = new RelaxedPropertyResolver(env, "config.verify.");
		Map<String, Object> map = resolver.getSubProperties("code.");
		
        Properties properties=new Properties();  
        properties.putAll(map);       
        
        DefaultKaptcha defaultKaptcha=new DefaultKaptcha();  
        defaultKaptcha.setConfig(new Config(properties));  
        return defaultKaptcha;  
    }  
	
	/**
	 * Swagger配置.
	 * http://[ip]:[port]/[context]/swagger-ui.html
	 * @author chanlong(陈龙)
	 * @date 2017年4月10日 下午2:34:01
	 */
	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("full-credit-api")
													  .apiInfo(apiInfo())
													  .select()
													   	.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
													   	.paths(PathSelectors.any())
													  .build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Arthurs")
								   .description("SpringBoot Arthurs")
								   .termsOfServiceUrl("https://git.oschina.net/arthurs")
								   .contact(new Contact("chanlong", "https://git.oschina.net/arthurs", "chenl_sh@30wish.net"))
								   .version("1.0.0")
								   .build();
	}
}
