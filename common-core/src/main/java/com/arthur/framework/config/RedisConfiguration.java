package com.arthur.framework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableCaching												// 开启缓存
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 600) // 开启Session同步
@ConditionalOnClass({ RedisTemplate.class })
public class RedisConfiguration extends CachingConfigurerSupport {
	
	@Bean
    public <T> CacheManager cacheManager(final RedisTemplate<String, T> redisTemplate) {  
        return new RedisCacheManager(redisTemplate);
    }
	
	@Autowired
	private ObjectMapper mapper;
	
	@Bean @SuppressWarnings("unchecked")
	public <T> RedisTemplate<String, T> redisTemplate(final RedisConnectionFactory factory) {
		Jackson2JsonRedisSerializer<T> serializer = (Jackson2JsonRedisSerializer<T>) new Jackson2JsonRedisSerializer<>(Object.class);
		serializer.setObjectMapper(mapper);
		
		RedisTemplate<String, T> template = new RedisTemplate<>();
		template.setConnectionFactory(factory);
		template.setKeySerializer(new StringRedisSerializer());
		template.setValueSerializer(serializer);
		template.setHashValueSerializer(serializer);
		template.setEnableTransactionSupport(false);
		template.afterPropertiesSet();
		return template;
	}
	
	@Bean @Override  
    public CacheErrorHandler errorHandler() {  
		CacheErrorHandler cacheErrorHandler = new CacheErrorHandler() {
			@Override
			public void handleCachePutError(final RuntimeException e, final Cache cache, Object key, final Object value) {
				System.out.println(value);
			}
			
			@Override
			public void handleCacheEvictError(final RuntimeException e, final Cache cache, final Object key) {
				System.out.println(key);
			}
			
			@Override
			public void handleCacheGetError(final RuntimeException e, final Cache cache, final Object key) {
				System.out.println(key);
			}

			@Override
			public void handleCacheClearError(final RuntimeException e, final Cache cache) {
				System.out.println(cache.getName());
			}
		};
		return cacheErrorHandler;
    }  
}
