package com.arthur.framework.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@ConditionalOnMissingBean(name = { "securityProperties" })
public class CrossConfiguration extends WebMvcConfigurerAdapter {
	
	private static final Logger log = LoggerFactory.getLogger(CrossConfiguration.class);
	
	@Autowired
	private Environment env;

	@Override
	public void addCorsMappings(final CorsRegistry registry) {
		long maxage = 600;
		try {maxage = env.getProperty("server.session.timeout", Long.class);} catch (Throwable e) {log.info("application configuration's property 'server.session.timeout' is invalid settings.");}
		registry.addMapping("/**").maxAge(maxage).allowCredentials(true).allowedOrigins("*").allowedHeaders("*").allowedMethods("*");
	}
}
