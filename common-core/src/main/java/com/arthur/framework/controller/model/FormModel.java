package com.arthur.framework.controller.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.arthur.framework.util.JsonUtil;
import com.arthur.framework.util.reflect.ReflectUtil;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class FormModel implements Serializable{

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 7342979099633143792L;
	
	/* 实体类
	 * eg: ajax data : {
	 * 	"entity":"com.arthur.framework.auth.entity.AuthUser"
	 * 	"bean":{
	 * 		"id":"UID0001",
	 * 		"username":"test",
	 * 		"password":"000000"
	 * 	}
	 * }
	 */
	private String entity;
	
	private Map<String, Object> bean;
	
	private List<Map<String, Object>> beans;
	
	public <T> List<T> getEntities() {
		List<T> entities = new ArrayList<T>();
		if(beans != null && !beans.isEmpty()){
			for (Map<String, Object> map : beans) {
				String json = JsonUtil.serialize(map);
				T _entity = JsonUtil.getBean(json, ReflectUtil.getClass(entity));
				entities.add(_entity);
			}
		}
		return entities;
	}
	
	public String getEntityName(){
		return ReflectUtil.getClass(entity).getSimpleName();
	}
	
	public <T> T getEntity(Class<T> _class) {
		String json = JsonUtil.serialize(bean);
		return JsonUtil.getBean(json, ReflectUtil.getClass(entity));
	}
	
	public <T> Class<T> getEntity() {
		return ReflectUtil.getClass(entity);
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public Map<String, Object> getBean() {
		return bean;
	}

	public void setBean(Map<String, Object> bean) {
		this.bean = bean;
	}

	public List<Map<String, Object>> getBeans() {
		return beans;
	}

	public void setBeans(List<Map<String, Object>> beans) {
		this.beans = beans;
	}
	
}
