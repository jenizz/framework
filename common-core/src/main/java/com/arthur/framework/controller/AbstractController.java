package com.arthur.framework.controller;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.service.bo.CommonBoAware;
import com.arthur.framework.orm.support.Processer;

/**
 * <p><b>描述</b>: AbstractController </p>
 * <p>&emsp;&emsp;All controllers by POJO's operator can be extends This, The generic type 'E' is extends to CommonEntityAware. </p>
 * <p><b>Company</b>: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年3月18日 上午10:07:13
 * @version 1.0.2017
 */
public abstract class AbstractController<E extends CommonEntityAware> extends AbstractContextController {

	@Resource(name = "defaultBo")
	private CommonBoAware<E> bo;
	
	@PostConstruct
	public void init() {
		Processer.factory().registerProcess(bo, getJdbc());
	}
	
	/**
	 * 获取公共BO对象.
	 * @author chanlong(陈龙)
	 * @date 2017年8月17日 上午11:16:44
	 */
	public CommonBoAware<E> getBo() {
		return bo;
	}

}
