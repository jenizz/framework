package com.arthur.framework.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;

import com.arthur.framework.util.string.StringUtil;

/**
 * <p><b>描述</b>: CommonController </p>
 * <p>&emsp;&emsp;All controllers can be extends This. You can get ApplicatonContext、HttpServletResponse、HttpServletRequest、HttpSession、Cookie、JdbcTemplate and ThreadPoolTaskExecutor's Bean Object</p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年8月17日 上午11:08:33
 * @version 1.0.2017
 */
public abstract class AbstractContextController implements ApplicationContextAware {
	
	public static final String RETURN_KEY_MESSAGE = "message";

	private ApplicationContext context;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;

	@Autowired
	private JdbcTemplate jdbc;
	
	/**
	 * 获取JdbcTemplate.
	 * @author chanlong(陈龙)
	 * @date 2017年8月17日 上午11:06:44
	 */
	public JdbcTemplate getJdbc() {
		return jdbc;
	}
	
	/**
	 * 获取ApplicationContext.
	 * @author chanlong(陈龙)
	 * @date 2017年8月17日 上午11:01:20
	 */
	public ApplicationContext getContext() {
		return context;
	}

	/**
	 * 获取HttpServletResponse.
	 * @author chanlong(陈龙)
	 * @date 2017年8月17日 上午11:02:43
	 */
	public HttpServletResponse getResponse() {
		return response;
	}
	
	public HttpServletResponse getResponse(final ResponseType type) {
		return type.processResponse(response);
	}
	
	/**
	 * 获取HttpServletRequest.
	 * @author chanlong(陈龙)
	 * @date 2017年8月17日 上午11:03:26
	 */
	public HttpServletRequest getRequest() {
		return request;
	}
	
	/**
	 * 获取HttpSession.
	 * @param isNew
	 * @author chanlong(陈龙)
	 * @date 2017年8月17日 上午11:00:36
	 */
	public HttpSession getSession(boolean... isNew){
		return isNew != null && isNew.length > 0 ? request.getSession(isNew[0]) : request.getSession(false);
	}

	/**
	 * 获取cookies.
	 * @author chanlong(陈龙)
	 * @date 2017年3月28日 上午10:12:31
	 */
	public Cookie[] getCookies(){
		return request.getCookies();
	}
	
	/**
	 * 获取cookie.
	 * @param name
	 * @author chanlong(陈龙)
	 * @date 2017年3月28日 上午10:12:43
	 */
	public Cookie getCookie(final String name) {
		Cookie[] cookies = getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equalsIgnoreCase(name)) {
				return cookie;
			}
		}
		return null;
	}
	
	/**
	 * 设置cookie.
	 * @param name
	 * @param value
	 * @author chanlong(陈龙)
	 * @date 2017年3月28日 上午10:12:56
	 */
	public void setCookie(final String name, final String value){
		Cookie cookie = new Cookie(name, value);
		response.addCookie(cookie);
	}

	/**
	 * 检查参数.
	 * @param params
	 * @author chanlong(陈龙)
	 * @date 2017年3月18日 上午10:00:25
	 */
	public boolean check(final Object... params){
		boolean _return = false;
		try{
			for (Object param : params) {
				_return = StringUtil.isEmpty(param);
			}
		}catch(NullPointerException e){
			return true;
		}
		return _return;
	}
	
	public enum ResponseType {
		image{
			@Override
			public HttpServletResponse processResponse(final HttpServletResponse response) {
				response.setDateHeader("Expires", 0);  
		        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");  
		        response.addHeader("Cache-Control", "post-check=0, pre-check=0");  
		        response.setHeader("Pragma", "no-cache");  
		        response.setContentType("image/jpeg");  
		        return response;
			}
		};
		public abstract HttpServletResponse processResponse(final HttpServletResponse response);
	}
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}
}
