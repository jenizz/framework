package com.arthur.framework.controller;

import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.arthur.framework.datasource.support.DataSourceCrypt;
import com.arthur.framework.orm.support.Procedure;
import com.arthur.framework.support.Constant;
import com.arthur.framework.support.Response;
import com.google.code.kaptcha.impl.DefaultKaptcha;

import io.swagger.annotations.ApiOperation;

/**
 * <p><b>Author</b>: long.chan 2017年2月6日 上午11:42:01 </p>
 * <p><b>ClassName</b>: CommonContoller.java </p> 
 * <p><b>Description</b>: </p> 
 */
@RestController @RequestMapping({ "/common" })
public class CoreContoller extends AbstractContextController {
	
	private static final Logger log = LoggerFactory.getLogger(CoreContoller.class);
	
	@Autowired
	private DefaultKaptcha kaptcha;
	
	@Autowired
	private RedisTemplate<String, Object> redis;
	
	/**
	 * 生成图片验证码.
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年9月6日 上午8:58:05
	 */
	@ApiOperation("生成图片验证码")
	@RequestMapping(path = "/vcode", method = RequestMethod.GET)
	public Response verifycode() throws Exception {
		HttpServletResponse response = getResponse(ResponseType.image);
		
		String capText = kaptcha.createText();  
		getSession().setAttribute("DefaultKaptcha", capText);
		
        BufferedImage image = kaptcha.createImage(capText);  
        ServletOutputStream out = response.getOutputStream();  
        ImageIO.write(image, "jpg", out); 
        
        return new Response(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
	}
	
	/**
	 * 加密.
	 * @author chanlong(陈龙)
	 * @date 2017年9月6日 上午8:58:21
	 */
	@ApiOperation("加密")
	@RequestMapping(path = "/crypt/{text}", method = RequestMethod.GET)
	public Object crypt(@PathVariable final String text) {
		return DataSourceCrypt.encrypt(text);
	}
	
	/**
	 * 调用存储过程.
	 * @param model
	 * @return
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2016年9月7日 下午6:25:02
	 */
	@ApiOperation("存储过程")
	@RequestMapping(path = "/pcall", method = RequestMethod.POST)
	public Response call(@RequestBody final Procedure procedure) throws Exception {
		// 参数检查
		if (check(procedure)) return new Response(HttpStatus.BAD_REQUEST, "错误的请求格式");
		
		try{
			// 调用存储过程
			return new Response(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), procedure.call(getJdbc()));
		} catch (Exception e) {
			log.error("调用公用存储过程接口失败 {}", e);
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), e);
		}
	}
	
	/**
	 * 设置请求上下文.
	 * @author chanlong(陈龙)
	 * @date 2017年9月5日 上午11:32:20
	 */
	@ApiOperation("设置请求上下文")
	@RequestMapping(path = "/context", method = RequestMethod.PUT)
	public Response setRequestContext(@RequestBody final ModelMap params) {
		try{
			// json 格式转换
			if (params.get("userinfo") instanceof String) {
				params.put("userinfo", JSONObject.parseObject(String.valueOf(params.get("userinfo"))));
			}
			
			String ip = getRequest().getRemoteAddr();
			redis.opsForValue().set("context_" + ip, params);
			return new Response(HttpStatus.OK, HttpStatus.OK.getReasonPhrase());
		} catch (Throwable e) {
			log.error("设置请求上下文失败 {}", e);
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "设置请求上下文失败", e.getMessage());
		}
	}
	
	/**
	 * 获取请求上下文.
	 * @author chanlong(陈龙)
	 * @date 2017年9月5日 上午11:32:20
	 */
	@ApiOperation("获取请求上下文")
	@RequestMapping(path = "/context", method = RequestMethod.GET)
	public Response getRequestContext() {
		try{
			String ip = getRequest().getRemoteAddr();
			return new Response(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), redis.opsForValue().get("context_" + ip));
		} catch (Throwable e) {
			log.error("获取请求上下文失败 {}", e);
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, "获取请求上下文失败", e.getMessage());
		}
	}
	
	/**
	 * 获取请求上下文 当前登录用户.
	 * @param sessionid
	 * @return
	 * @author chanlong(陈龙)
	 * @date 2017年9月5日 上午11:48:59
	 */
	@ApiOperation("获取请求上下文 当前登录用户")
	@RequestMapping(path = "/context/{sessionid}", method = RequestMethod.GET)
	public Object getRequestContext(@PathVariable final String sessionid) {
		return getSession().getAttribute(sessionid);
	}
	
	/**
	 * 获取session信息.
	 * @param key
	 * @author chanlong(陈龙)
	 * @date 2017年9月5日 上午11:52:16
	 */
	@ApiOperation("获取session信息")
	@RequestMapping(value = "/session/{key}", method = RequestMethod.GET)
	public Response session(@PathVariable final String key, HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		Object result = session.getAttribute(key);
		return new Response(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), result);
	}
	
	/**
	 * 获取session信息.
	 * @param request
	 * @author chanlong(陈龙)
	 * @date 2017年9月5日 上午11:49:43
	 */
	@ApiOperation("获取session信息")
	@RequestMapping(value = "/sessions", method = RequestMethod.GET)
	public Response sessions(HttpServletRequest request) {
		try{
			HttpSession session = request.getSession(false);
			Object token = session.getAttribute(Constant.KEY_CSRF_TOKEN);
			Map<String, Object> map = new HashMap<>();
			map.put("tokenId", PropertyUtils.getProperty(token, "token"));
			map.put("sessionId", session.getId());
			return new Response(HttpStatus.OK, HttpStatus.OK.getReasonPhrase(), map);
		}  catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
			return new Response(HttpStatus.INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), e);
		}
		
	}
}
