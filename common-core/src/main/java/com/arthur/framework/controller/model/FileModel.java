package com.arthur.framework.controller.model;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class FileModel implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 3730284371060360363L;
	
	private String mode;
	
	private String type;

	private String group;
	
	private String recordId;
	
	private boolean notsaved;
	
	private MultipartFile file;
	
	private MultipartFile[] files;

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public boolean isNotsaved() {
		return notsaved;
	}

	public void setNotsaved(boolean notsaved) {
		this.notsaved = notsaved;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public MultipartFile[] getFiles() {
		return files;
	}

	public void setFiles(MultipartFile[] files) {
		this.files = files;
	}
}
