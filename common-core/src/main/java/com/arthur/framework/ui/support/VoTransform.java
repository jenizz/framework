package com.arthur.framework.ui.support;

import java.util.function.Consumer;

/**
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2018年2月28日 上午10:09:39
 * @version 1.0.2018
 */
@FunctionalInterface
public interface VoTransform<V> extends Consumer<V> {

}
