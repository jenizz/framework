package com.arthur.framework.ui;

import java.io.Serializable;

/**
 * <p>描述: VO接口</p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2018年2月28日 上午10:09:52
 * @version 1.0.2018
 */
public interface CommonVoAware extends Serializable {
	
}
