package com.arthur.framework.ui.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.ui.CommonVoAware;
import com.arthur.framework.ui.support.VoTransform;
import com.arthur.framework.util.reflect.ReflectUtil.PropertyIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TreeVo implements CommonVoAware {
	
	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -8129121481576305106L;
	
	@JSONField(ordinal = 1)
	@JsonProperty(index = 1)
	private String title; 			// 节点标题
	
	@JSONField(ordinal = 2)
	@JsonProperty(index = 2)
	private boolean folder;			// 节点是否为父节点
	
	@JSONField(ordinal = 3)
	@JsonProperty(index = 3)
	private boolean expand;			// 节点展开状态（用于父节点）
	
	@JSONField(ordinal = 4)
	@JsonProperty(index = 4)
	private boolean checked;		// 节点勾选状态（用于复选框节点）
	
	@JSONField(ordinal = 5)
	@JsonProperty(index = 5)
	private boolean selected;		// 节点选中状态（用于子节点）
	
	@JSONField(ordinal = 6)
	@JsonProperty(index = 6)
	@PropertyIgnore
	private Map<String, Object> attributes;
	
	@JSONField(ordinal = 9)
	@JsonProperty(index = 9)
	@PropertyIgnore
	private List<TreeVo> children;	// 子节点
	
	public TreeVo(final VoTransform<TreeVo> transform) {
		this.children = new ArrayList<>();
		transform.accept(this);
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isFolder() {
		return folder;
	}

	public void setFolder(boolean folder) {
		this.folder = folder;
	}

	public boolean isExpand() {
		return expand;
	}

	public void setExpand(boolean expand) {
		this.expand = expand;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	@SuppressWarnings("unchecked")
	public void setAttributes(Object attributes) {
		setAttributes((Map<String, Object>)attributes);
	}
	
	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public List<TreeVo> getChildren() {
		return children;
	}

	public void setChildren(List<TreeVo> children) {
		this.children = children;
	}
	
	public void addChild(final TreeVo child) {
		if (this.children == null) this.children = new ArrayList<>();
		this.children.add(child);
	}

}
