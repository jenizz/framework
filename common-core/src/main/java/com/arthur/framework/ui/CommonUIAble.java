package com.arthur.framework.ui;

import java.util.Map;

import com.arthur.framework.support.PropertyAware;
import com.arthur.framework.util.reflect.ReflectUtil;

public interface CommonUIAble<T> extends PropertyAware {

	/**
	 * 根据视图组件数据生成VO对象.<br>
	 * PS：应由具体的VO接口实现
	 * @author chanlong(陈龙)
	 * @date 2018年2月28日 上午11:48:48
	 */
	T build();
	
	/**
	 * 获取业务数据作为VO对象的扩展属性.<br>
	 * PS：树型视图组件的业务数据存放在attribute属性中
	 * @author chanlong(陈龙)
	 * @date 2018年2月28日 下午4:32:59
	 */
	default Map<String, Object> getAttribute() {
		return ReflectUtil.getProperties(this);
	}
}
