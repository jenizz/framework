package com.arthur.framework.ui.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.ui.CommonVoAware;
import com.fasterxml.jackson.annotation.JsonProperty;

public class OptionVo implements CommonVoAware {
	
	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -8129121481576305106L;
	
	@JSONField(ordinal = 1)
	@JsonProperty(index = 1)
	private String label; 		// 下拉项的text
	
	@JSONField(ordinal = 2)
	@JsonProperty(index = 2)
	private Object value;		// 下拉项的value

	public OptionVo() {

	}
	
	public OptionVo(String label, Object value) {
		this.label = label;
		this.value = value;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

}
