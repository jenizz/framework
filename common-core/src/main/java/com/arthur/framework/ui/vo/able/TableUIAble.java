package com.arthur.framework.ui.vo.able;

import com.arthur.framework.ui.CommonUIAble;
import com.arthur.framework.ui.vo.TableVo;

public interface TableUIAble extends CommonUIAble<TableVo> {

	@Override
	default TableVo build() {
		return null;
	}
}
