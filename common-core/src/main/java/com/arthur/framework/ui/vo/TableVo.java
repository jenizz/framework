package com.arthur.framework.ui.vo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;
import com.arthur.framework.ui.CommonVoAware;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TableVo implements CommonVoAware {
	
	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -8129121481576305106L;
	
	@JSONField(ordinal = 1)
	@JsonProperty(index = 1)
	private long total; 					// 总数
	
	@JSONField(ordinal = 2)
	@JsonProperty(index = 2)
	private List<Map<String, Object>> rows;	// 数据集

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<Map<String, Object>> getRows() {
		return rows;
	}

	public void setRows(List<Map<String, Object>> rows) {
		this.rows = rows;
	}

	public void addRow(Map<String, Object> row) {
		if (this.rows == null) this.rows = new ArrayList<>();
		this.rows.add(row);
	}
}
