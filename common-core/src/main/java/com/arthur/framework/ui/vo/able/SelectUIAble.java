package com.arthur.framework.ui.vo.able;

import com.arthur.framework.ui.CommonUIAble;
import com.arthur.framework.ui.vo.OptionVo;

public interface SelectUIAble extends CommonUIAble<OptionVo>  {
	
	@Override
	default OptionVo build() {
		return null;
	}
}
