package com.arthur.framework.ui.vo.able;

import java.util.Map;

import com.arthur.framework.ui.CommonUIAble;
import com.arthur.framework.ui.vo.TreeVo;

public interface TreeUIAble extends CommonUIAble<TreeVo> {
	
	final String KEY_TITLE = "title";
	final String KEY_FOLDER = "folder";
	final String KEY_ATTRIBUTES = "attributes";

	@Override
	default TreeVo build() {
		Map<String, Object> data = getTreeData();
		return new TreeVo((vo) -> {
			vo.setTitle(String.valueOf(data.get(KEY_TITLE)));
			vo.setFolder(Boolean.valueOf(String.valueOf(data.get(KEY_FOLDER))));
			vo.setAttributes(data.get(KEY_ATTRIBUTES));
		});
	}

	/**
	 * 根据业务数据生成树型视图组件所需的数据.<br>
	 * PS：必须由业务对象重写
	 * @author chanlong(陈龙)
	 * @date 2018年2月28日 上午11:46:34
	 */
	Map<String, Object> getTreeData();
}
