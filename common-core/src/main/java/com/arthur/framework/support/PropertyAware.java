package com.arthur.framework.support;

import com.arthur.framework.util.reflect.ReflectUtil;

/**
 * 操作对象属性
 * <p>描述: </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2018年2月28日 上午9:07:05
 * @version 1.0.2018
 */
public interface PropertyAware {
	
	/**
	 * 设置对象属性.
	 * @param name 属性名
	 * @param value 值
	 * @author chanlong(陈龙)
	 * @date 2018年2月28日 上午9:09:35
	 */
	default <T> void set(final String name, final T value) {
		ReflectUtil.setProperty(this, name, value);
	}
	
	/**
	 * 获取对象属性.
	 * @param name 属性名
	 * @author chanlong(陈龙)
	 * @date 2018年2月28日 上午9:09:45
	 */
	default <T> T get(final String name) {
		return ReflectUtil.getProperty(this, name);
	}
}
