package com.arthur.framework.support;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect
public class Response implements Serializable {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 1090934453680693192L;

	private Integer code;
	
	private String reason;
	
	private Object result;
	
	public Response() {

	}
	
	public Response(HttpStatus code, String reason, Object... result) {
		this.code = code.value();
		this.result = result != null && result.length > 0 ? result[0] : getResult("message", "no result!");
		this.reason = reason;
	}
	
	public Response(HttpStatus code, String reason, String key, Object value) {
		this.code = code.value();
		this.result = getResult(key, value);
		this.reason = reason;
	}

	private Object getResult(String key, Object value) {
		Map<String, Object> map = new HashMap<>();
		map.put(key, value);
		return map;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
}
