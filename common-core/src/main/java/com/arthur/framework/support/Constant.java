package com.arthur.framework.support;

public interface Constant {

	public static final String KEY_CSRF_TOKEN = "org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository.CSRF_TOKEN";
}
