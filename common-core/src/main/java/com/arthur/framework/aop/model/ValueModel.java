package com.arthur.framework.aop.model;

import java.lang.annotation.Annotation;
import java.util.LinkedHashMap;

/**
 * <p>描述: 参数值模型 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年7月26日 上午9:02:20
 * @version 1.0.2017
 */
public class ValueModel extends LinkedHashMap<Class<? extends Annotation>, Object> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = 7695983114771196160L;
	
}
