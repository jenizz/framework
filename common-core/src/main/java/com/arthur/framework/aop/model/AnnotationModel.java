package com.arthur.framework.aop.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * <p>描述: 参数注解模型 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年7月26日 上午9:02:36
 * @version 1.0.2017
 */
public class AnnotationModel<A> extends ArrayList<Serializable> {

	/** serialVersionUID(long):. */
	private static final long serialVersionUID = -3027911961371882177L;

	public void add(final A annotation, final Object value) {
		this.add(new InnerModel(annotation, value));
	}
	
	@SuppressWarnings("unchecked")
	public A getAnnotation(final int index){
		return ((InnerModel)this.get(index)).getAnnotation();
	}
	
	@SuppressWarnings("unchecked")
	public Object getValue(final int index){
		return this.isEmpty() ? "" : ((InnerModel)this.get(index)).getValue();
	}
	
	private class InnerModel implements Serializable {
		/** serialVersionUID(long):. */
		private static final long serialVersionUID = -1754816194291066137L;
		
		private A annotation;
		private Object value;
		
		public InnerModel(A annotation, Object value) {
			this.annotation = annotation;
			this.value = value;
		}

		public A getAnnotation() {
			return annotation;
		}

		public Object getValue() {
			return value;
		}
	}
}
