/**
 * 
 * <p><b>Author</b>: long.chan 2017年2月7日 上午11:33:16 </p>
 * <p><b>ClassName</b>: ControllerInterceptor.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.aop;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.arthur.framework.datasource.MultiDataSourceHolder;
import com.arthur.framework.datasource.support.DataSource;
import com.arthur.framework.util.DebugUtil;

/**
 * <p><b>Author</b>: long.chan 2017年2月7日 上午11:33:16 </p>
 * <p><b>ClassName</b>: ControllerInterceptor.java </p> 
 * <p><b>Description</b>: </p> 
 */
@Aspect
@Component
public class MultiDataSourceInterceptor extends AbstractInterceptor<DataSource> {
	
	private static final Logger log = LoggerFactory.getLogger(MultiDataSourceInterceptor.class);

	private static final String DEFAULT_DATA_SOURCE = "default";

	public void changeDataSource(final JoinPoint point){
		try {
			DataSource db = getAnnotation(point, DataSource.class);
			MultiDataSourceHolder.setDataSource(db.value());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Around("@annotation(com.arthur.framework.datasource.support.DataSource)")
	public Object performed(final ProceedingJoinPoint point) {
		DebugUtil.ready();
		
		try {
			// 被拦截方法执行前，AOP预处理
			changeDataSource(point);
			
			// 执行被拦截的方法
			Object returnmap = point.proceed();
			
			// 被拦截的方法执行后，继续处理
			chain(point, returnmap);
			
			return returnmap;
		} catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void chain(JoinPoint point, Object returnmap) throws Exception {
		Method method = getMethodBySignature(point);
		process(method, returnmap);
	}

	@Override
	protected void process(Method method, Object returnmap) throws Exception {
		MultiDataSourceHolder.setDataSource(DEFAULT_DATA_SOURCE);
	}

	@Override
	protected void exception(JoinPoint point, Throwable e) {
		log.error("处理AOP拦截事务失败：{}", e);
	}
	
}
