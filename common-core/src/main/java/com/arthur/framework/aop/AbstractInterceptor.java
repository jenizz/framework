/**
 * <p>Copyright:Copyright(c) 2017</p>
 * <p>Company:Professional</p>
 * <p>Package:com.arthur.framework.support</p>
 * <p>File:AbstractInterceptor.java</p>
 * <p>类更新历史信息</p>
 * @todo chanlong(陈龙) 创建于 2017年5月25日 下午4:39:26
 */
package com.arthur.framework.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import com.arthur.framework.aop.model.AnnotationModel;
import com.arthur.framework.aop.model.ParameterModel;
import com.arthur.framework.aop.model.ValueModel;

/**
 * <p>描述: AOP抽象拦截器 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年5月25日 下午4:39:26
 * @version 1.0.2017
 */
public abstract class AbstractInterceptor<A extends Annotation> implements CommonInterceptor<A> {
	
	/**
	 * 执行后续处理.
	 * @param point
	 * @author chanlong(陈龙)
	 * @throws Exception 
	 * @date 2017年7月24日 下午2:24:48
	 */
	protected abstract void chain(final JoinPoint point, final Object returnmap) throws Exception;
	
	/**
	 * 处理日志逻辑.
	 * @param _this
	 * @param method
	 * @param returnmap
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 下午6:00:13
	 */
	protected abstract void process(final Method method, final Object returnmap) throws Exception;
	
	/**
	 * 执行异常.
	 * @param point
	 * @param e
	 * @author chanlong(陈龙)
	 * @date 2017年7月24日 下午2:25:12
	 */
	protected abstract void exception(final JoinPoint point, final Throwable e);
	
	/**
	 * 预处理AOP拦截器.
	 * @param point
	 * @param _class
	 * @author chanlong(陈龙)
	 * @date 2017年7月24日 下午2:13:30
	 */
	protected void preinit(final JoinPoint point, final Class<A> _class, final String... includes) {
		Method method = getMethodBySignature(point);
		Parameter[] params = method.getParameters();
		Object[] args = point.getArgs();
		
		setContext(_class, includes);
		setParameterModel(point, params, args);
	}
	
	/**
	 * 根据方法签名获取方法对象.
	 * @param point
	 * @author chanlong(陈龙)
	 * @date 2017年7月18日 下午2:07:07
	 */
	protected Method getMethodBySignature(final JoinPoint point){
		MethodSignature signature = (MethodSignature)point.getSignature();
		return signature.getMethod();
	}
	
	/**
	 * 获取方法指定的注解对象.
	 * @param joinPoint
	 * @param _class
	 * @throws Exception
	 * @author chanlong(陈龙)
	 * @date 2018年3月22日 下午4:43:33
	 */
	protected A getAnnotation(final JoinPoint joinPoint, final Class<A> _class) throws Exception {
		Method method = getMethodBySignature(joinPoint);
		return  method.getAnnotation(_class);
	}
	
	private ParameterModel<A> parameter;
	
	private JoinPoint point;
	
	private Class<A> _class;
	
	private String[] includes;
	
	/**
	 * 设置参数模型.
	 * @param point
	 * @param params
	 * @param args
	 * @author chanlong(陈龙)
	 * @date 2017年7月28日 上午9:55:32
	 */
	public void setParameterModel(final JoinPoint point, final Parameter[] params, final Object[] args) {
		this.parameter = new ParameterModel<>();
		this.point = point;
		
		for(int i = 0, size = params.length; i <  size; i++){
			A annotation = params[i].getAnnotation(_class);
			if (annotation != null && args[i] != null) {
				parameter.addAnnotationModel(annotation, args[i]);
			} else if (args[i] != null) {
				parameter.addValueModel(params[i], args[i], includes);
			}
		}
	}
	
	@Override
	public AnnotationModel<A> getAnnotationModel(){
		return parameter.getAnnotationModel();
	}
	
	@Override
	public ValueModel getValueModel(){
		return parameter.getValueModel();
	}

	@Override
	public ParameterModel<A> getParameterModel() {
		return parameter;
	}

	@Override
	public JoinPoint getJoinPoint() {
		return point;
	}
	
	/* 设置上下文属性 */
	private void setContext(final Class<A> _class, String[] includes) {
		this._class = _class;
		this.includes = includes;
	}
}
