package com.arthur.framework.aop;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 拦截器处理接口
 * <p>描述: 由具体的处理类实现 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年7月26日 上午11:15:23
 * @version 1.0.2017
 */
public interface ProcessorIntercepter<A extends Annotation> {

	/**
	 * .
	 * @param interceptor
	 * @param _method
	 * @param _return
	 * @author chanlong(陈龙)
	 * @date 2017年7月28日 上午9:15:54
	 */
	void perform(final CommonInterceptor<A> interceptor, final ThreadPoolTaskExecutor executor, final Method _method, final Object _return);
}
