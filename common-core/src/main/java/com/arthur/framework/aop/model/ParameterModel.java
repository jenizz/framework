package com.arthur.framework.aop.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

import org.apache.commons.lang3.ArrayUtils;

/**
 * <p>描述: 方法参数模型 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年7月26日 上午9:01:55
 * @version 1.0.2017
 */
public class ParameterModel<A> {
	
	/**
	 * 添加方法参数值映射.<br>
	 * 映射关系为：{ 注解: 值(非空) }<br>
	 * 映射关系中的注解由参数 'includes' 指定<br>
	 * @param parameter 参数对象
	 * @param value     参数值
	 * @param includes  要包含的注解(注解类名)
	 * @author chanlong(陈龙)
	 * @date 2017年7月26日 上午9:26:50
	 */
	public void addValueModel(final Parameter parameter, final Object value, final String... includes) {
		Annotation[] annotations = parameter.getAnnotations();
		for (Annotation _annotation : annotations) {
			Class<? extends Annotation> _class = _annotation.annotationType();
			String _name = _class.getName();
			if (ArrayUtils.contains(includes, _name)) {
				valueModel.put(_class, value);
			}
		}
	}
	
	/**
	 * .
	 * @param annotation
	 * @param value
	 * @author chanlong(陈龙)
	 * @date 2017年7月26日 上午9:35:07
	 */
	public void addAnnotationModel(final A annotation, final Object value) {
		annotationModel.add(annotation, value);
	}
	
	private ValueModel valueModel;
	
	private AnnotationModel<A> annotationModel;
	
	public ParameterModel() {
		valueModel = new ValueModel();
		annotationModel = new AnnotationModel<>();
	}

	public ValueModel getValueModel() {
		return valueModel;
	}

	public void setValueModel(ValueModel valueModel) {
		this.valueModel = valueModel;
	}

	public AnnotationModel<A> getAnnotationModel() {
		return annotationModel;
	}

	public void setAnnotationModel(AnnotationModel<A> annotationModel) {
		this.annotationModel = annotationModel;
	}
}
