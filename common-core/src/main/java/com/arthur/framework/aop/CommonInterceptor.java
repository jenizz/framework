package com.arthur.framework.aop;

import java.lang.annotation.Annotation;

import org.aspectj.lang.JoinPoint;

import com.arthur.framework.aop.model.AnnotationModel;
import com.arthur.framework.aop.model.ParameterModel;
import com.arthur.framework.aop.model.ValueModel;

/**
 * <p>描述: AOP拦截器接口 </p>
 * <p>Company: Professional</p>
 * @author chanlong(陈龙)
 * @date 2017年7月26日 上午8:44:42
 * @version 1.0.2017
 */
public interface CommonInterceptor<A extends Annotation> {

	public static final String PERFORM = "perform"; 
	
	/**
	 * 返回切入点.
	 * @author chanlong(陈龙)
	 * @date 2017年7月26日 上午11:40:43
	 */
	JoinPoint getJoinPoint();
	
	/**
	 * 返回方法参数模型.
	 * @author chanlong(陈龙)
	 * @date 2017年7月26日 上午8:48:09
	 */
	ParameterModel<A> getParameterModel();

	/**
	 * 返回注解模型.
	 * @author chanlong(陈龙)
	 * @date 2017年7月26日 上午11:58:29
	 */
	AnnotationModel<A> getAnnotationModel();

	/**
	 * 返回参数值模型.
	 * @author chanlong(陈龙)
	 * @date 2017年7月26日 上午11:58:36
	 */
	ValueModel getValueModel();

}
