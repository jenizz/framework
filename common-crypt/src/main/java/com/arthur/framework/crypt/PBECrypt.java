/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:49:39 </p>
 * <p><b>ClassName</b>: PBECrypt.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import java.security.Key;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:49:39 </p>
 * <p><b>ClassName</b>: PBECrypt.java </p> 
 * <p><b>Description</b>: PBE安全编码组件  </p> 
 */
public abstract class PBECrypt extends BasicCrypt {

	public static final String ALGORITHM = "PBEWITHMD5andDES";
	
	/**
	 * 
	 * <p><b>Title</b>: initSalt </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:50:22 </p>
	 * <p><b>Description</b>: 初始化盐 </p> 
	 *
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] initSalt() throws Exception {
		byte[] salt = new byte[8];
		Random random = new Random();
		random.nextBytes(salt);
		return salt;
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encrypt </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:51:34 </p>
	 * <p><b>Description</b>: 加密  </p> 
	 *
	 * @param data
	 * @param password
	 * @param salt
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encrypt(byte[] data, String password, byte[] salt) throws Exception {
		Key key = toKey(password);

		PBEParameterSpec paramSpec = new PBEParameterSpec(salt, 100);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: decrypt </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:52:00 </p>
	 * <p><b>Description</b>: 解密 </p> 
	 *
	 * @param data
	 * @param password
	 * @param salt
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] decrypt(byte[] data, String password, byte[] salt) throws Exception {
		Key key = toKey(password);

		PBEParameterSpec paramSpec = new PBEParameterSpec(salt, 100);
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

		return cipher.doFinal(data);

	}
	
	/** 转换密钥 **/
	private static Key toKey(String password) throws Exception {
		PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray());
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
		SecretKey secretKey = keyFactory.generateSecret(keySpec);
		return secretKey;
	}
}
