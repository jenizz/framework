/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午4:28:20 </p>
 * <p><b>ClassName</b>: ECCCrypt.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.crypto.Cipher;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午4:53:26 </p>
 * <p><b>ClassName</b>: Certificate.java </p> 
 * <p><b>Description</b>: 证书组件 </p> 
 */
public abstract class Certificator extends BasicCrypt {

	public static final String KEY_STORE = "JKS";
	public static final String X509 = "X.509";

	/**
	 * 
	 * <p><b>Title</b>: encryptByPrivateKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:03:49 </p>
	 * <p><b>Description</b>: 私钥加密 </p> 
	 *
	 * @param data
	 * @param keyStorePath
	 * @param alias
	 * @param password
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encryptByPrivateKey(byte[] data, String keyStorePath, String alias, String password) throws Exception {
		PrivateKey privateKey = getPrivateKey(keyStorePath, alias, password);
		Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);
		return cipher.doFinal(data);
	}

	/**
	 * 
	 * <p><b>Title</b>: decryptByPrivateKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:04:13 </p>
	 * <p><b>Description</b>: 私钥解密 </p> 
	 *
	 * @param data
	 * @param keyStorePath
	 * @param alias
	 * @param password
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] decryptByPrivateKey(byte[] data, String keyStorePath, String alias, String password) throws Exception {
		PrivateKey privateKey = getPrivateKey(keyStorePath, alias, password);
		Cipher cipher = Cipher.getInstance(privateKey.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return cipher.doFinal(data);
	}

	/**
	 * 
	 * <p><b>Title</b>: encryptByPublicKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:04:32 </p>
	 * <p><b>Description</b>: 公钥加密 </p> 
	 *
	 * @param data
	 * @param certificatePath
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encryptByPublicKey(byte[] data, String certificatePath) throws Exception {
		PublicKey publicKey = getPublicKey(certificatePath);
		Cipher cipher = Cipher.getInstance(publicKey.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return cipher.doFinal(data);
	}

	/**
	 * 
	 * <p><b>Title</b>: decryptByPublicKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:04:43 </p>
	 * <p><b>Description</b>: 公钥解密 </p> 
	 *
	 * @param data
	 * @param certificatePath
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] decryptByPublicKey(byte[] data, String certificatePath) throws Exception {
		PublicKey publicKey = getPublicKey(certificatePath);
		Cipher cipher = Cipher.getInstance(publicKey.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, publicKey);
		return cipher.doFinal(data);
	}

	/**
	 * 
	 * <p><b>Title</b>: sign </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:06:45 </p>
	 * <p><b>Description</b>: 签名 </p> 
	 *
	 * @param sign
	 * @param keyStorePath
	 * @param alias
	 * @param password
	 * @return
	 * @throws Exception
	 *
	 */
	public static String sign(byte[] sign, String keyStorePath, String alias, String password) throws Exception {
		X509Certificate x509Certificate = (X509Certificate) getCertificate(keyStorePath, alias, password);
		KeyStore ks = getKeyStore(keyStorePath, password);
		PrivateKey privateKey = (PrivateKey) ks.getKey(alias, password.toCharArray());
		
		Signature signature = Signature.getInstance(x509Certificate.getSigAlgName());
		signature.initSign(privateKey);
		signature.update(sign);
		return encryptBASE64(signature.sign());
	}

	/**
	 * 
	 * <p><b>Title</b>: verify </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:07:08 </p>
	 * <p><b>Description</b>: 验证签名 </p> 
	 *
	 * @param data
	 * @param sign
	 * @param certificatePath
	 * @return
	 * @throws Exception
	 *
	 */
	public static boolean verify(byte[] data, String sign, String certificatePath) throws Exception {
		X509Certificate x509Certificate = (X509Certificate) getCertificate(certificatePath);
		PublicKey publicKey = x509Certificate.getPublicKey();

		Signature signature = Signature.getInstance(x509Certificate.getSigAlgName());
		signature.initVerify(publicKey);
		signature.update(data);
		return signature.verify(decryptBASE64(sign));
	}

	/**
	 * 
	 * <p><b>Title</b>: verifyCertificate </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:06:24 </p>
	 * <p><b>Description</b>:  验证Certificate </p> 
	 *
	 * @param certificatePath
	 * @return
	 *
	 */
	public static boolean verifyCertificate(String certificatePath) {
		return verifyCertificate(new Date(), certificatePath);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: verifyCertificate </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:07:45 </p>
	 * <p><b>Description</b>: 验证Certificate </p> 
	 *
	 * @param keyStorePath
	 * @param alias
	 * @param password
	 * @return
	 *
	 */
	public static boolean verifyCertificate(String keyStorePath, String alias, String password) {
		return verifyCertificate(new Date(), keyStorePath, alias, password);
	}

	/**
	 * 
	 * <p><b>Title</b>: verifyCertificate </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:06:35 </p>
	 * <p><b>Description</b>:  验证Certificate </p> 
	 *
	 * @param date
	 * @param certificatePath
	 * @return
	 *
	 */
	public static boolean verifyCertificate(Date date, String certificatePath) {
		boolean status = true;
		try {
			Certificate certificate = getCertificate(certificatePath);
			status = verifyCertificate(date, certificate);
		} catch (Exception e) {
			status = false;
		}
		return status;
	}
	
	/**
	 * 
	 * <p><b>Title</b>: verifyCertificate </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午5:07:32 </p>
	 * <p><b>Description</b>: 验证Certificate </p> 
	 *
	 * @param date
	 * @param keyStorePath
	 * @param alias
	 * @param password
	 * @return
	 *
	 */
	public static boolean verifyCertificate(Date date, String keyStorePath, String alias, String password) {
		boolean status = true;
		try {
			Certificate certificate = getCertificate(keyStorePath, alias, password);
			status = verifyCertificate(date, certificate);
		} catch (Exception e) {
			status = false;
		}
		return status;
	}
	
	/** 获得KeyStore **/
	private static KeyStore getKeyStore(String keyStorePath, String password) throws Exception {
		FileInputStream is = new FileInputStream(keyStorePath);
		KeyStore ks = KeyStore.getInstance(KEY_STORE);
		ks.load(is, password.toCharArray());
		is.close();
		return ks;
	}
	
	/** 由KeyStore获得私钥 **/
	private static PrivateKey getPrivateKey(String keyStorePath, String alias, String password) throws Exception {
		KeyStore ks = getKeyStore(keyStorePath, password);
		PrivateKey key = (PrivateKey) ks.getKey(alias, password.toCharArray());
		return key;
	}

	/** 由Certificate获得公钥 **/
	private static PublicKey getPublicKey(String certificatePath) throws Exception {
		Certificate certificate = getCertificate(certificatePath);
		PublicKey key = certificate.getPublicKey();
		return key;
	}

	/** 获得Certificate **/
	private static Certificate getCertificate(String certificatePath) throws Exception {
		CertificateFactory certificateFactory = CertificateFactory.getInstance(X509);
		FileInputStream in = new FileInputStream(certificatePath);

		Certificate certificate = certificateFactory.generateCertificate(in);
		in.close();

		return certificate;
	}

	/** 获得Certificate **/
	private static Certificate getCertificate(String keyStorePath, String alias, String password) throws Exception {
		KeyStore ks = getKeyStore(keyStorePath, password);
		Certificate certificate = ks.getCertificate(alias);

		return certificate;
	}
	
	/** 验证证书是否过期或无效 **/
	private static boolean verifyCertificate(Date date, Certificate certificate) {
		boolean status = true;
		try {
			X509Certificate x509Certificate = (X509Certificate) certificate;
			x509Certificate.checkValidity(date);
		} catch (Exception e) {
			status = false;
		}
		return status;
	}
}
