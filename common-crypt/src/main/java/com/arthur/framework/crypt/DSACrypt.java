/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:36:57 </p>
 * <p><b>ClassName</b>: DSACrypt.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.interfaces.DSAPrivateKey;
import java.security.interfaces.DSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:36:57 </p>
 * <p><b>ClassName</b>: DSACrypt.java </p> 
 * <p><b>Description</b>: DSA安全编码组件 </p> 
 *
 */
public class DSACrypt extends BasicCrypt {

	public static final String ALGORITHM = "DSA";
	
	private static final int KEY_SIZE = 1024;
	private static final String DEFAULT_SALT = "0f22507a10bbddd07d8a3082122966e3";  
    private static final String PUBLIC_KEY = "DSAPublicKey";  
    private static final String PRIVATE_KEY = "DSAPrivateKey";
    
    /**
     * 
     * <p><b>Title</b>: sign </p> 
     * <p><b>Author</b>: long.chan 2017年3月2日 下午3:39:01 </p>
     * <p><b>Description</b>: 用私钥对信息生成数字签名 </p> 
     *
     * @param data
     * @param privateKey
     * @return
     * @throws Exception
     *
     */
	public static String sign(byte[] data, String privateKey) throws Exception {
		byte[] keyBytes = decryptBASE64(privateKey);

		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);

		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);

		PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);

		Signature signature = Signature.getInstance(keyFactory.getAlgorithm());
		signature.initSign(priKey);
		signature.update(data);

		return encryptBASE64(signature.sign());
	}
	
	/**
	 * 
	 * <p><b>Title</b>: verify </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:44:49 </p>
	 * <p><b>Description</b>: 校验数字签名 </p> 
	 *
	 * @param data
	 * @param publicKey
	 * @param sign
	 * @return
	 * @throws Exception
	 *
	 */
	public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {
		byte[] keyBytes = decryptBASE64(publicKey);

		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);

		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);

		PublicKey pubKey = keyFactory.generatePublic(keySpec);

		Signature signature = Signature.getInstance(keyFactory.getAlgorithm());
		signature.initVerify(pubKey);
		signature.update(data);

		return signature.verify(decryptBASE64(sign));
	}
	
	/**
	 * 
	 * <p><b>Title</b>: initKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:46:45 </p>
	 * <p><b>Description</b>: 生成密钥 </p> 
	 *
	 * @return
	 * @throws Exception
	 *
	 */
	public static Map<String, Object> initKey() throws Exception {  
        return initKey(DEFAULT_SALT);  
    }  
	
	/**
	 * 
	 * <p><b>Title</b>: initKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:45:27 </p>
	 * <p><b>Description</b>: 生成密钥 </p> 
	 *
	 * @param seed
	 * @return
	 * @throws Exception
	 *
	 */
	public static Map<String, Object> initKey(String seed) throws Exception {
		KeyPairGenerator keygen = KeyPairGenerator.getInstance(ALGORITHM);

		SecureRandom secureRandom = new SecureRandom();
		secureRandom.setSeed(seed.getBytes());
		keygen.initialize(KEY_SIZE, secureRandom);

		KeyPair keys = keygen.genKeyPair();

		DSAPublicKey publicKey = (DSAPublicKey) keys.getPublic();
		DSAPrivateKey privateKey = (DSAPrivateKey) keys.getPrivate();

		Map<String, Object> map = new HashMap<String, Object>(2);
		map.put(PUBLIC_KEY, publicKey);
		map.put(PRIVATE_KEY, privateKey);

		return map;
	}
	
	/**
	 * 
	 * <p><b>Title</b>: getPrivateKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:46:51 </p>
	 * <p><b>Description</b>: 取得私钥 </p> 
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 *
	 */
	public static String getPrivateKey(Map<String, Object> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PRIVATE_KEY);
		return encryptBASE64(key.getEncoded());
	}
	
	/**
	 * 
	 * <p><b>Title</b>: getPublicKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:46:56 </p>
	 * <p><b>Description</b>: 取得公钥 </p> 
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 *
	 */
	public static String getPublicKey(Map<String, Object> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PUBLIC_KEY);
		return encryptBASE64(key.getEncoded());
	}
}
