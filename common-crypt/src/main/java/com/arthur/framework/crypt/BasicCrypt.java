/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:23:17 </p>
 * <p><b>ClassName</b>: Coder.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import java.security.MessageDigest;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:23:17 </p>
 * <p><b>ClassName</b>: Coder.java </p> 
 * <p><b>Description</b>: 基础加密组件 </p> 
 */
public abstract class BasicCrypt {

	public static final String KEY_SHA = "SHA";
	public static final String KEY_MD5 = "MD5";
	public static final String KEY_MAC = "HmacMD5";
    
    /**
     * 
     * <p><b>Title</b>: decryptBASE64 </p> 
     * <p><b>Author</b>: long.chan 2017年3月2日 下午2:27:48 </p>
     * <p><b>Description</b>: BASE64解密 </p> 
     *
     * @param key
     * @return
     * @throws Exception
     *
     */
	public static byte[] decryptBASE64(String crypt) throws Exception {
		return Base64.decodeBase64(crypt);
	}
    
    /**
     * 
     * <p><b>Title</b>: encryptBASE64 </p> 
     * <p><b>Author</b>: long.chan 2017年3月2日 下午2:28:25 </p>
     * <p><b>Description</b>: BASE64加密 </p> 
     *
     * @param key
     * @return
     * @throws Exception
     *
     */
	public static String encryptBASE64(byte[] data) throws Exception {
		return Base64.encodeBase64String(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encryptMD5 </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:29:22 </p>
	 * <p><b>Description</b>: MD5加密 </p> 
	 *
	 * @param data
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encryptMD5(byte[] data) throws Exception {
		MessageDigest md5 = MessageDigest.getInstance(KEY_MD5);
		md5.update(data);
		return md5.digest();
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encryptSHA </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:30:25 </p>
	 * <p><b>Description</b>: SHA加密 </p> 
	 *
	 * @param data
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encryptSHA(byte[] data) throws Exception {
		MessageDigest sha = MessageDigest.getInstance(KEY_SHA);
		sha.update(data);
		return sha.digest();
	}
	
	/**
	 * 
	 * <p><b>Title</b>: initMacKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:31:15 </p>
	 * <p><b>Description</b>: 初始化HMAC密钥  </p> 
	 *
	 * @return
	 * @throws Exception
	 *
	 */
	public static String initMacKey() throws Exception {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(KEY_MAC);
		SecretKey secretKey = keyGenerator.generateKey();
		return encryptBASE64(secretKey.getEncoded());
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encryptHMAC </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:32:03 </p>
	 * <p><b>Description</b>: HMAC加密 </p> 
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encryptHMAC(byte[] data, String key) throws Exception {
		SecretKey secretKey = new SecretKeySpec(decryptBASE64(key), KEY_MAC);
		Mac mac = Mac.getInstance(secretKey.getAlgorithm());
		mac.init(secretKey);
		return mac.doFinal(data);
	}
}
