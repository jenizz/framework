package com.arthur.framework.crypt;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public abstract class MD5Crypt extends BasicCrypt {

	/**
	 * MD5加密.
	 * @param data
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 下午4:38:52
	 */
	public static String encrypt(final String data) {
		try {
			return encrypt(data.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return encrypt(data.getBytes());
		}
	}
	
	/**
	 * MD5加密.
	 * @param data
	 * @author chanlong(陈龙)
	 * @date 2017年7月25日 下午4:40:48
	 */
	public static String encrypt(final byte[] data) {
		try {
			BigInteger md5 = new BigInteger(encryptMD5(data));
			return md5.toString(16);
		} catch (Exception e) {
			throw new RuntimeException("MD5加密失败:{}", e);
		}
	}
	
}
