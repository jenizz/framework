/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:38:28 </p>
 * <p><b>ClassName</b>: DESCrypt.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:38:28 </p>
 * <p><b>ClassName</b>: DESCrypt.java </p> 
 * <p><b>Description</b>: DES安全编码组件 </p> 
 */
public abstract class DESCrypt extends BasicCrypt {

	public static final String ALGORITHM = "DES";
	
	/**
	 * 
	 * <p><b>Title</b>: decrypt </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:43:05 </p>
	 * <p><b>Description</b>: 解密 </p> 
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] decrypt(byte[] data, String key) throws Exception {
		Key _key = toKey(decryptBASE64(key));

		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, _key);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encrypt </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:43:44 </p>
	 * <p><b>Description</b>: 加密 </p> 
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encrypt(byte[] data, String key) throws Exception {
		Key k = toKey(decryptBASE64(key));
		Cipher cipher = Cipher.getInstance(ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, k);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: initKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:44:26 </p>
	 * <p><b>Description</b>: 生成密钥 </p> 
	 *
	 * @return
	 * @throws Exception
	 *
	 */
	public static String initKey() throws Exception {
		return initKey(null);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: initKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:44:44 </p>
	 * <p><b>Description</b>: 生成密钥 </p> 
	 *
	 * @param salt
	 * @return
	 * @throws Exception
	 *
	 */
	public static String initKey(final String salt) throws Exception {
		SecureRandom secureRandom = null;

		if (salt != null) {
			secureRandom = new SecureRandom(decryptBASE64(salt));
		} else {
			secureRandom = new SecureRandom();
		}

		KeyGenerator kg = KeyGenerator.getInstance(ALGORITHM);
		kg.init(secureRandom);

		SecretKey secretKey = kg.generateKey();

		return encryptBASE64(secretKey.getEncoded());
	}
	
	/** 转换密钥 **/
	private static Key toKey(byte[] key) throws Exception {
		DESKeySpec dks = new DESKeySpec(key);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
		SecretKey secretKey = keyFactory.generateSecret(dks);
		return secretKey;
	}
}
