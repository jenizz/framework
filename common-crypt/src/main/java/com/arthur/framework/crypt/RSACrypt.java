/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:56:47 </p>
 * <p><b>ClassName</b>: RSACrypt.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:56:47 </p>
 * <p><b>ClassName</b>: RSACrypt.java </p> 
 * <p><b>Description</b>: RSA安全编码组件 </p> 
 */
public abstract class RSACrypt extends BasicCrypt {

	public static final String KEY_ALGORITHM = "RSA";
	public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

	private static final String PUBLIC_KEY = "RSAPublicKey";
	private static final String PRIVATE_KEY = "RSAPrivateKey";
	
	/**
	 * 
	 * <p><b>Title</b>: sign </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:57:54 </p>
	 * <p><b>Description</b>: 用私钥对信息生成数字签名 </p> 
	 *
	 * @param data
	 * @param privateKey
	 * @return
	 * @throws Exception
	 *
	 */
	public static String sign(byte[] data, String privateKey) throws Exception {
		byte[] keyBytes = decryptBASE64(privateKey);

		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);

		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);

		PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);

		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initSign(priKey);
		signature.update(data);

		return encryptBASE64(signature.sign());
	}
	
	/**
	 * 
	 * <p><b>Title</b>: verify </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:59:27 </p>
	 * <p><b>Description</b>: 校验数字签名  </p> 
	 *
	 * @param data
	 * @param publicKey
	 * @param sign
	 * @return
	 * @throws Exception
	 *
	 */
	public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {
		byte[] keyBytes = decryptBASE64(publicKey);

		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);

		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);

		PublicKey pubKey = keyFactory.generatePublic(keySpec);

		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initVerify(pubKey);
		signature.update(data);

		return signature.verify(decryptBASE64(sign));
	}
	
	/**
	 * 
	 * <p><b>Title</b>: decryptByPrivateKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:00:09 </p>
	 * <p><b>Description</b>: 用私钥解密 </p> 
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] decryptByPrivateKey(byte[] data, String key) throws Exception {
		byte[] keyBytes = decryptBASE64(key);

		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);

		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: decryptByPublicKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:00:58 </p>
	 * <p><b>Description</b>: 用公钥解密 </p> 
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] decryptByPublicKey(byte[] data, String key) throws Exception {
		byte[] keyBytes = decryptBASE64(key);

		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);

		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, publicKey);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encryptByPublicKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:01:38 </p>
	 * <p><b>Description</b>: 用公钥加密  </p> 
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encryptByPublicKey(byte[] data, String key) throws Exception {
		byte[] keyBytes = decryptBASE64(key);

		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);

		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encryptByPrivateKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:02:15 </p>
	 * <p><b>Description</b>: 用私钥加密 </p> 
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encryptByPrivateKey(byte[] data, String key) throws Exception {
		byte[] keyBytes = decryptBASE64(key);

		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);

		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: getPrivateKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:03:45 </p>
	 * <p><b>Description</b>: 取得私钥  </p> 
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 *
	 */
	public static String getPrivateKey(Map<String, Object> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PRIVATE_KEY);
		return encryptBASE64(key.getEncoded());
	}
	
	/**
	 * 
	 * <p><b>Title</b>: getPublicKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:03:48 </p>
	 * <p><b>Description</b>: 取得公钥  </p> 
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 *
	 */
	public static String getPublicKey(Map<String, Object> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PUBLIC_KEY);
		return encryptBASE64(key.getEncoded());
	}
	
	/**
	 * 
	 * <p><b>Title</b>: initKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:04:34 </p>
	 * <p><b>Description</b>: 初始化密钥  </p> 
	 *
	 * @return
	 * @throws Exception
	 *
	 */
	public static Map<String, Object> initKey() throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		keyPairGen.initialize(1024);

		KeyPair keyPair = keyPairGen.generateKeyPair();

		RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();

		RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

		Map<String, Object> keyMap = new HashMap<String, Object>(2);

		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);
		return keyMap;
	}
}
