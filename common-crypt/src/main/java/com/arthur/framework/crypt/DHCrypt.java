/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:17:19 </p>
 * <p><b>ClassName</b>: DHCrypt.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.interfaces.DHPrivateKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHParameterSpec;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:17:19 </p>
 * <p><b>ClassName</b>: DHCrypt.java </p> 
 * <p><b>Description</b>: DH安全编码组件 </p> 
 */
public abstract class DHCrypt extends BasicCrypt {

	public static final String ALGORITHM = "DH";
	public static final String SECRET_ALGORITHM = "DES";  
	
	private static final int KEY_SIZE = 1024;
    private static final String PUBLIC_KEY = "DHPublicKey";  
    private static final String PRIVATE_KEY = "DHPrivateKey";  
    
    /**
     * 
     * <p><b>Title</b>: initKey </p> 
     * <p><b>Author</b>: long.chan 2017年3月2日 下午3:19:01 </p>
     * <p><b>Description</b>: 初始化甲方密钥 </p> 
     *
     * @return
     * @throws Exception
     *
     */
	public static Map<String, Object> initKey() throws Exception {
		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM);
		keyPairGenerator.initialize(KEY_SIZE);

		KeyPair keyPair = keyPairGenerator.generateKeyPair();

		DHPublicKey publicKey = (DHPublicKey) keyPair.getPublic();

		DHPrivateKey privateKey = (DHPrivateKey) keyPair.getPrivate();

		Map<String, Object> keyMap = new HashMap<String, Object>(2);

		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);
		return keyMap;
	}
	
	/**
	 * 
	 * <p><b>Title</b>: initKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:19:42 </p>
	 * <p><b>Description</b>: 初始化乙方密钥  </p> 
	 *
	 * @param key
	 * @return
	 * @throws Exception
	 *
	 */
	public static Map<String, Object> initKey(String key) throws Exception {
		byte[] keyBytes = decryptBASE64(key);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
		PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);

		DHParameterSpec dhParamSpec = ((DHPublicKey) pubKey).getParams();

		KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(keyFactory.getAlgorithm());
		keyPairGenerator.initialize(dhParamSpec);

		KeyPair keyPair = keyPairGenerator.generateKeyPair();

		DHPublicKey publicKey = (DHPublicKey) keyPair.getPublic();

		DHPrivateKey privateKey = (DHPrivateKey) keyPair.getPrivate();

		Map<String, Object> keyMap = new HashMap<String, Object>(2);

		keyMap.put(PUBLIC_KEY, publicKey);
		keyMap.put(PRIVATE_KEY, privateKey);

		return keyMap;
	}
	
	/**
	 * 
	 * <p><b>Title</b>: encrypt </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:20:48 </p>
	 * <p><b>Description</b>: 加密 </p> 
	 *
	 * @param data
	 * @param publicKey
	 * @param privateKey
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] encrypt(byte[] data, String publicKey, String privateKey) throws Exception {
		SecretKey secretKey = getSecretKey(publicKey, privateKey);

		Cipher cipher = Cipher.getInstance(secretKey.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: decrypt </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:21:14 </p>
	 * <p><b>Description</b>: 解密 </p> 
	 *
	 * @param data
	 * @param publicKey
	 * @param privateKey
	 * @return
	 * @throws Exception
	 *
	 */
	public static byte[] decrypt(byte[] data, String publicKey, String privateKey) throws Exception {
		SecretKey secretKey = getSecretKey(publicKey, privateKey);
		
		Cipher cipher = Cipher.getInstance(secretKey.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, secretKey);

		return cipher.doFinal(data);
	}
	
	/**
	 * 
	 * <p><b>Title</b>: getSecretKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:21:47 </p>
	 * <p><b>Description</b>: 构建密钥 </p> 
	 *
	 * @param publicKey
	 * @param privateKey
	 * @return
	 * @throws Exception
	 *
	 */
	private static SecretKey getSecretKey(String publicKey, String privateKey) throws Exception {
		byte[] pubKeyBytes = decryptBASE64(publicKey);

		KeyFactory keyFactory = KeyFactory.getInstance(ALGORITHM);
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(pubKeyBytes);
		PublicKey pubKey = keyFactory.generatePublic(x509KeySpec);

		byte[] priKeyBytes = decryptBASE64(privateKey);

		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(priKeyBytes);
		Key priKey = keyFactory.generatePrivate(pkcs8KeySpec);

		KeyAgreement keyAgree = KeyAgreement.getInstance(keyFactory.getAlgorithm());
		keyAgree.init(priKey);
		keyAgree.doPhase(pubKey, true);

		SecretKey secretKey = keyAgree.generateSecret(SECRET_ALGORITHM);

		return secretKey;
	}
	
	/**
	 * 
	 * <p><b>Title</b>: getPrivateKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:22:24 </p>
	 * <p><b>Description</b>: 取得私钥  </p> 
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 *
	 */
	public static String getPrivateKey(Map<String, Object> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PRIVATE_KEY);
		return encryptBASE64(key.getEncoded());
	}
	
	/**
	 * 
	 * <p><b>Title</b>: getPublicKey </p> 
	 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:22:50 </p>
	 * <p><b>Description</b>: 取得公钥 </p> 
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 *
	 */
	public static String getPublicKey(Map<String, Object> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PUBLIC_KEY);
		return encryptBASE64(key.getEncoded());
	} 
}
