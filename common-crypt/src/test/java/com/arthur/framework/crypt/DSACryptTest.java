/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:48:26 </p>
 * <p><b>ClassName</b>: DSACryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:48:26 </p>
 * <p><b>ClassName</b>: DSACryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class DSACryptTest {

	@Test
	public void test() throws Exception {
		String inputStr = "abc";
		byte[] data = inputStr.getBytes();

		// 构建密钥
		Map<String, Object> keyMap = DSACrypt.initKey();

		// 获得密钥
		String publicKey = DSACrypt.getPublicKey(keyMap);
		String privateKey = DSACrypt.getPrivateKey(keyMap);

		System.err.println("公钥:\r" + publicKey);
		System.err.println("私钥:\r" + privateKey);

		// 产生签名
		String sign = DSACrypt.sign(data, privateKey);
		System.err.println("签名:\r" + sign);

		// 验证签名
		boolean status = DSACrypt.verify(data, publicKey, sign);
		System.err.println("状态:\r" + status);
		assertTrue(status);
	}
}
