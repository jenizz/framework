/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:28:00 </p>
 * <p><b>ClassName</b>: DHCryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:28:00 </p>
 * <p><b>ClassName</b>: DHCryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class DHCryptTest {

	@Test
	public void test() throws Exception {
		// 生成甲方密钥对儿
		Map<String, Object> aKeyMap = DHCrypt.initKey();
		String aPublicKey = DHCrypt.getPublicKey(aKeyMap);
		String aPrivateKey = DHCrypt.getPrivateKey(aKeyMap);

		System.err.println("甲方公钥:\r" + aPublicKey);
		System.err.println("甲方私钥:\r" + aPrivateKey);

		// 由甲方公钥产生本地密钥对儿
		Map<String, Object> bKeyMap = DHCrypt.initKey(aPublicKey);
		String bPublicKey = DHCrypt.getPublicKey(bKeyMap);
		String bPrivateKey = DHCrypt.getPrivateKey(bKeyMap);

		System.err.println("乙方公钥:\r" + bPublicKey);
		System.err.println("乙方私钥:\r" + bPrivateKey);

		String aInput = "abc ";
		System.err.println("原文: " + aInput);

		// 由甲方公钥，乙方私钥构建密文
		byte[] aCode = DHCrypt.encrypt(aInput.getBytes(), aPublicKey, bPrivateKey);
		System.err.println("密文: " + DHCrypt.encryptBASE64(aCode));

		// 由乙方公钥，甲方私钥解密
		byte[] aDecode = DHCrypt.decrypt(aCode, bPublicKey, aPrivateKey);
		String aOutput = (new String(aDecode));

		System.err.println("解密: " + aOutput);

		assertEquals(aInput, aOutput);

		System.err.println(" ===============反过来加密解密================== ");
		String bInput = "def ";
		System.err.println("原文: " + bInput);

		// 由乙方公钥，甲方私钥构建密文
		byte[] bCode = DHCrypt.encrypt(bInput.getBytes(), bPublicKey, aPrivateKey);

		// 由甲方公钥，乙方私钥解密
		byte[] bDecode = DHCrypt.decrypt(bCode, aPublicKey, bPrivateKey);
		String bOutput = (new String(bDecode));

		System.err.println("解密: " + bOutput);

		assertEquals(bInput, bOutput);
	}
}
