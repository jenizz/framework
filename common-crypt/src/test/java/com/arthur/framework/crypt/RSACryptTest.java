/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:07:30 </p>
 * <p><b>ClassName</b>: RSACryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午3:07:30 </p>
 * <p><b>ClassName</b>: RSACryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class RSACryptTest {

	private String publicKey;
	private String privateKey;

	@Before
	public void setUp() throws Exception {
		Map<String, Object> keyMap = RSACrypt.initKey();

		publicKey = RSACrypt.getPublicKey(keyMap);
		privateKey = RSACrypt.getPrivateKey(keyMap);
		System.err.println("公钥: \n\r" + publicKey);
		System.err.println("私钥： \n\r" + privateKey);
	}

	@Test
	public void test() throws Exception {
		System.err.println("公钥加密——私钥解密");
		String inputStr = "abc";
		byte[] data = inputStr.getBytes();

		byte[] encodedData = RSACrypt.encryptByPublicKey(data, publicKey);

		byte[] decodedData = RSACrypt.decryptByPrivateKey(encodedData, privateKey);

		String outputStr = new String(decodedData);
		System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr);
		assertEquals(inputStr, outputStr);
	}

	@Test
	public void testSign() throws Exception {
		System.err.println("私钥加密——公钥解密");
		String inputStr = "sign";
		byte[] data = inputStr.getBytes();

		byte[] encodedData = RSACrypt.encryptByPrivateKey(data, privateKey);

		byte[] decodedData = RSACrypt.decryptByPublicKey(encodedData, publicKey);

		String outputStr = new String(decodedData);
		System.err.println("加密前: " + inputStr + "\n\r" + "解密后: " + outputStr);
		assertEquals(inputStr, outputStr);

		System.err.println("私钥签名——公钥验证签名");
		// 产生签名
		String sign = RSACrypt.sign(encodedData, privateKey);
		System.err.println("签名:\r" + sign);

		// 验证签名
		boolean status = RSACrypt.verify(encodedData, publicKey, sign);
		System.err.println("状态:\r" + status);
		assertTrue(status);
	}
}
