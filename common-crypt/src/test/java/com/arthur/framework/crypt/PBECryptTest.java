/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:52:45 </p>
 * <p><b>ClassName</b>: PBECryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:52:45 </p>
 * <p><b>ClassName</b>: PBECryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class PBECryptTest {

	@Test
	public void test() throws Exception {
		String inputStr = "miwawa";
		System.err.println("原文: " + inputStr);
		byte[] input = inputStr.getBytes();

		String pwd = "TJZ8GNX8Xs52Ejzc";
		System.err.println("密码: " + pwd);

		byte[] salt = PBECrypt.initSalt();

		byte[] data = PBECrypt.encrypt(input, pwd, salt);

		System.err.println("加密后: " + PBECrypt.encryptBASE64(data));

		byte[] output = PBECrypt.decrypt(data, pwd, salt);
		String outputStr = new String(output);

		System.err.println("解密后: " + outputStr);
		assertEquals(inputStr, outputStr);
	}
}
