/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:33:09 </p>
 * <p><b>ClassName</b>: CryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Test;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:33:09 </p>
 * <p><b>ClassName</b>: CryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class BasicCryptTest {
	
	@Test
	public void test() throws Exception {
		//String inputStr = "简单加密";
		String inputStr = "miwawa";
		System.err.println("原文:\n" + inputStr);

		byte[] inputData = inputStr.getBytes();
		String code = BasicCrypt.encryptBASE64(inputData);

		System.err.println("BASE64加密后:\n" + code);

		byte[] output = BasicCrypt.decryptBASE64(code);

		String outputStr = new String(output);

		System.err.println("BASE64解密后:\n" + outputStr);

		// 验证BASE64加密解密一致性
		assertEquals(inputStr, outputStr);

		// 验证MD5对于同一内容加密是否一致
		assertArrayEquals(BasicCrypt.encryptMD5(inputData), BasicCrypt.encryptMD5(inputData));

		// 验证SHA对于同一内容加密是否一致
		assertArrayEquals(BasicCrypt.encryptSHA(inputData), BasicCrypt.encryptSHA(inputData));

		String key = BasicCrypt.initMacKey();
		System.err.println("Mac密钥:\n" + key);

		// 验证HMAC对于同一内容，同一密钥加密是否一致
		assertArrayEquals(BasicCrypt.encryptHMAC(inputData, key), BasicCrypt.encryptHMAC(inputData, key));
//280a20dc4fe86bf40b6e22e06333b79f
		BigInteger md5 = new BigInteger(BasicCrypt.encryptMD5(inputData));
		System.err.println("MD5:\n" + md5.toString(16));

		BigInteger sha = new BigInteger(BasicCrypt.encryptSHA(inputData));
		System.err.println("SHA:\n" + sha.toString(32));

		BigInteger mac = new BigInteger(BasicCrypt.encryptHMAC(inputData, key));
		System.err.println("HMAC:\n" + mac.toString(16));
	}
}
