/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:46:12 </p>
 * <p><b>ClassName</b>: DESCryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.crypt;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * <p><b>Author</b>: long.chan 2017年3月2日 下午2:46:12 </p>
 * <p><b>ClassName</b>: DESCryptTest.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
public class DESCryptTest {
	
	@Test
	public void test() throws Exception {
		String inputStr = "TJZ8GNX8Xs52Ejzc";
		String key = "TJZ8GNX8Xs52Ejzc";
		System.err.println("原文:\t" + inputStr);

		System.err.println("密钥:\t" + key);

		byte[] inputData = inputStr.getBytes();
		inputData = DESCrypt.encrypt(inputData, key);

		System.err.println("加密后:\t" + DESCrypt.encryptBASE64(inputData));

		byte[] outputData = DESCrypt.decrypt(inputData, key);
		String outputStr = new String(outputData);

		System.err.println("解密后:\t" + outputStr);

		assertEquals(inputStr, outputStr);
	}
}
