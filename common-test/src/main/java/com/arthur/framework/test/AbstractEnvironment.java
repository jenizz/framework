/**
 * 
 * <p><b>Author</b>: long.chan 2017年3月6日 上午10:23:11 </p>
 * <p><b>ClassName</b>: AbstractEnvironment.java </p> 
 * <p><b>Description</b>: </p> 
 *
 */
package com.arthur.framework.test;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.arthur.framework.orm.entity.CommonEntityAware;
import com.arthur.framework.orm.service.bo.CommonBoAware;
import com.arthur.framework.orm.support.Processer;

/**
 * <p><b>Author</b>: long.chan 2017年3月6日 上午10:23:11 </p>
 * <p><b>ClassName</b>: AbstractEnvironment.java </p> 
 * <p><b>Description</b>: </p> 
 */
@RunWith(SpringRunner.class)
@Transactional
@WebAppConfiguration
public abstract class AbstractEnvironment {

	@Autowired
	protected JdbcTemplate jdbc;
	
	@Resource(name = "defaultBo")
	protected CommonBoAware<CommonEntityAware> bo;
	
	@Before
	public void init() throws Exception{
		Processer.factory().registerProcess(bo, jdbc);
	}
}
